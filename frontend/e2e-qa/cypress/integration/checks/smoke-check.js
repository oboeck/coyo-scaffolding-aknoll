describe('Smoke Check', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.visit('/');
  });

  it('should login', function () {
    cy.server();
    cy.route('GET', '/web/like-targets/comment?**').as('getCommentsLikes');
    cy.login('rl', 'demo');
    cy.url()
        .should('eq', Cypress.config().baseUrl + '/home/timeline');
    cy.wait('@getCommentsLikes');
    cy.percySnapshot('Timeline landing page after login');
  });
});
