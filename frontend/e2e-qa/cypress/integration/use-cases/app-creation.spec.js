describe('App creation', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour()
        .then(() => {
          cy.visit('/');
        });
  });

  afterEach(function () {
    cy.logout();
  });

  it('should create a page with all possible apps', function () {

    cy.server();
    cy.route('PUT', '/api/apps/configurations/**').as('putAppConfig');
    cy.route('GET', '/web/api-clients').as('getClientSecrets');

    // Enable all apps
    const appsToEnable = ['blog', 'content', 'events', 'file-library', 'form', 'forum', 'list', 'task', 'timeline',
      'wiki', 'championship'];
    appsToEnable.forEach(function (app) {
      cy.apiAppConfiguration(app, 'page', 'true')
          .its('status').should('eq', 200);
      cy.wait(3000); // Wait for the request to be done, otherwise it will throw an authorization error
    });

    // Create page and create all apps
    cy.apiCreatePage().then((pageObject) => {
      cy.visit('/pages/' + pageObject.body.displayName.replace(/ /gi, '-'));
      cy.createApp('Blog');
      cy.get('[data-test=panel-blog-app]')
          .should('be.visible');
      cy.createApp('Content');
      cy.get('[data-test=panel-content-app]')
          .should('be.visible');
      cy.createApp('Events');
      cy.get('[data-test=panel-events-app]')
          .should('be.visible');
      cy.createApp('Documents');
      cy.get('[data-test=panel-documents-app]')
          .should('be.visible');
      cy.createApp('Form');
      cy.get('[data-test=panel-form-app]')
          .should('be.visible');
      cy.createApp('Forum');
      cy.get('[data-test=panel-forum-app]')
          .should('be.visible');
      cy.createApp('List');
      cy.get('[data-test=panel-list-app]')
          .should('be.visible');
      cy.createApp('Tasks');
      cy.get('[data-test=panel-tasks-app]')
          .should('be.visible');
      cy.createApp('Timeline');
      cy.get('[data-test=panel-timeline-app]')
          .should('be.visible');
      cy.createApp('Wiki');
      cy.get('[data-test=panel-wiki-app]')
          .should('be.visible');
      cy.createApp('Championship');
      cy.get('[data-test=panel-championship-app]')
          .should('be.visible');
    });
  });
});
