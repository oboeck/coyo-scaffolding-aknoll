describe('Event creation', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour()
        .then(() => {
          cy.visit('/');
        });
  });

  afterEach(function () {
    cy.logout();
  });

  it('should create an event and invite a host during the creation', function () {
    cy.createEvent(true);
  });

  it('should create an event without inviting a host during the creation', function () {
    cy.createEvent(false);
  });

});
