describe('Login', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.visit('/');
  });

  it('should reset the password', function () {
    cy.resetPassword('rl');
  });

  it('should try to login with a wrong password', function () {
    cy.login('rl', 'password');
    // Check for failure
    cy.get('[data-test=text-login-error]')
        .should('contain', 'Authentication Failed');
  });

  it('should login', function () {
    cy.login('rl', 'demo');
    // Check for successful login
    cy.url()
        .should('eq', Cypress.config().baseUrl + '/home/timeline');
  });
});
