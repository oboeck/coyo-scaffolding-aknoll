describe('Page creation', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour()
        .then(() => {
          cy.visit('/');
        });
  });

  afterEach(function () {
    cy.logout();
  });

  it('should create a private page without auto-subscription', function () {
    cy.createPage('private', 'off');
  });

  it('should create a public page with auto-subscribe for everyone', function () {
    cy.createPage('public', 'everyone');
  });

  it('should create a private page with auto-subscribe for selected users', function () {
    cy.createPage('private', 'selected');
  });
});
