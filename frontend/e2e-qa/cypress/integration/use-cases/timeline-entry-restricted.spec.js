/// <reference types="Cypress" />
/// <reference types="../support" />

describe('Timeline entry creation', function () {

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin('rl', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.logout();
    });

    it('should create a restricted timeline entry', () => {
        const timeLineEntry = 'qa timeline-post for create restricted post test' + Date.now();

        cy.server();
        cy.route('GET', '/web/timeline-items/new?**').as('getNewTimelineItems');
        cy.route('GET', '/web/users/*/subscriptions**').as('getSubscriptions');
        cy.route('POST', '/web/timeline-items**').as('postTimelineItems');

        cy.get('[data-test=textarea-timeline-post]')
            .type(timeLineEntry);
        cy.get('[data-test=button-timeline-submit]')
            .click();
        cy.wait('@postTimelineItems');
        cy.wait('@getNewTimelineItems');
        cy.wait('@getSubscriptions');
        cy.get('[data-test=timeline-item]').eq(0)
            .should('contain', timeLineEntry);
    });


    it('should not be possible to comment on a timeline entry of restricted post', () => {
        const newPost = 'qa timeline-post for comment to restricted post test ' + Date.now();
        cy.createTimelinePostAPI(newPost, true).showNewTimelinePosts();

        cy.get('[data-test=timeline-item]').contains(newPost).parents('[data-test=timeline-item]').as('post').within(() => {
            cy.get('[data-test=timeline-comment-message]')
                .should('not.exist');
        })
    });


    it('should not be possible to like a restricted timeline entry', () => {
        const newPost = 'qa timeline-post for like to restricted post test ' + Date.now();
        cy.createTimelinePostAPI(newPost, true).showNewTimelinePosts();
        cy.server();
        cy.route('GET', '/web/like-targets/timeline-item**').as('getLikeTimelineItem');

        cy.get('[data-test=timeline-item]').contains(newPost).parents('[data-test=timeline-item]').as('post').within(() => {
            cy.wait('@getLikeTimelineItem').its('status').should('eq', 200);
            cy.get('[data-test=like-button]')
                .should('not.exist');
        });
    });

    it('should be possible to delete a restricted timeline entry', () => {
        const newPost = 'qa timeline-post for delete restricted post test ' + Date.now();
        cy.createTimelinePostAPI(newPost, true).showNewTimelinePosts();

        cy.server();
        cy.route('DELETE', '/web/timeline-items/**').as('deleteTimelineItem');

        cy.get('[data-test=timeline-item]').contains(newPost).parents('[data-test=timeline-item]').within(() => {
            cy.get('[data-test=button-timeline-entry-edit-dropdown]')
                .click()
                .get('[data-test=delete-post-button]')
                .click();
        });
        cy.wait('@deleteTimelineItem');
        cy.get('[data-test=timeline-item]').should('not.contain', newPost);
    });
});
