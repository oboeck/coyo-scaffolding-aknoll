// ***********************************************************
// Custom commands related to user management
// ***********************************************************

/**
 * Command for creating a user
 * @param {string} group can be 'admins' or 'users'
 * @param {string} role can be 'admin', 'user', or 'external'
 *
 */

Cypress.Commands.add('createUser', function (group, role) {
  cy.server();
  cy.route('GET', '/web/groups?_page=0&_pageSize=50&displayName=' + group.replace(/ /g, "+") + '&status=ACTIVE')
      .as('getGroup');
  cy.route('GET', '/web/roles?_page=0&_pageSize=50&displayName=' + role.replace(/ /g, "+")).as('getRole');
  cy.route('GET', '/web/users**').as('getUserList');

  // Assert that you are in administration area
  cy.url()
      .should('contain', Cypress.config().baseUrl + '/admin/user-management/users');

  // Click create User button
  cy.get('[data-test=user-create-button]')
      .eq(0)
      .click();

  // Choose first and lastname of user and set email accordingly
  const firstName = 'User' + '-' + Date.now();
  cy.get('[data-test=user-firstname]')
      .click()
      .type(firstName);
  const lastName = 'Name' + '-' + Date.now();
  cy.get('[data-test=user-lastname]')
      .type(lastName);
  const email = firstName + '@' + lastName + '.com';
  cy.get('[data-test=user-email]')
      .type(email);

  // Choose user group
  cy.get('[data-test=user-group] > :nth-child(1)')
      .click();
  cy.get('input[placeholder="Select groups"]')
      .type(group)
      .wait('@getGroup');
  cy.get('input[placeholder="Select groups"]')
      .type('{enter}');

  // Choose user role
  cy.get('[data-test=user-role] > :nth-child(1)')
      .click();
  cy.get('input[placeholder="Select roles"]')
      .type(role)
      .wait('@getRole');
  cy.get('input[placeholder="Select roles"]')
      .type('{enter}');

  // Make user active
  cy.get('[data-test=checkbox-user-active] > [data-test=checkbox]')
      .click();

  // Make user superadmin
  if (role === 'Admin') {
    cy.get('[data-test=checkbox-user-superadmin] > [data-test=checkbox]')
        .click();
  }

  // Set user password
  const password = 'Test1234';
  cy.get('[data-test=user-password]')
      .type(password);

  // Submit
  cy.get('[data-test=submit]')
      .click();

  // Check if user was created
  cy.get('[data-test=user-search-filter')
      .clear()
      .type(firstName + ' ' + lastName);
  cy.wait('@getUserList');
  cy.get('[data-test=panel-list-user-management]')
      .should('contain', firstName + ' ' + lastName)
      .should('contain', group)
      .should('contain', role);
});
