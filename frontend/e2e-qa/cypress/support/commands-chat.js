// ***********************************************************
// Custom commands related to the chat
// ***********************************************************

Cypress.Commands.add('expandChatSidebar', function () {
  cy.get('[data-test=button-chat-sidebar-expand]')
      .click();
  cy.get('[data-test=text-messaging-channel-title]').then((messagingTitle) => {
    expect(messagingTitle).to.have.text('Channels');
    expect(messagingTitle).to.be.visible;
  });
});

Cypress.Commands.add('collapseChatSidebar', function () {
  cy.get('[data-test=button-chat-sidebar-collapse]')
      .click();
  cy.get('[data-test=text-messaging-channel-title]').then((messagingTitle) => {
    expect(messagingTitle).to.not.be.visible;
  });
});

/**
 * Command for editing the status of the user
 * @param {string} status can be 'online', 'away', 'busy', 'gone' or 'offline'
 * @param {string} message is the status message
 *
 */

Cypress.Commands.add('changePresenceStatus', function (status, message) {
  cy.get('[data-test=button-presence-status]')
      .click();
  cy.get(`[data-test=button-presence-status-${status.toUpperCase()}`)
      .click();
  cy.get('[data-test=input-status-message]')
      .clear()
      .type(message);
  cy.get('[data-test=button-status-message-save]')
      .click();
  cy.get('[data-test=button-presence-status-close]')
      .click();
  cy.get('[data-test=icon-presence-status]')
      .should('have.class', 'zmdi zmdi-circle ' + status.toUpperCase());
});

/**
 * Command for create a new single message channel and send a message
 * @param {string} user is the name of the user for the new message channel
 * @param {string} message is the chat message
 *
 */

Cypress.Commands.add('newSingleMessageChannel', function (user, message) {
  // Route for logout check
  cy.server();
  cy.route('POST', '/web/auth/logout').as('logout');

  // Get mail of receiver for later login
  cy.request(`/web/users?term=${user}`)
      .then((receiverResponse) => {

        // Get current user for later assertion
        cy.request('GET', '/web/users/me').then((currentUserResponse) => {
          cy.get('[data-test=button-create-chat-channel]')
              .click();
          cy.get(`[data-test="button-user-list-messaging-channel-${user}"]`)
              .click();
          cy.get('[data-test=textarea-message-form]')
              .type(message);
          cy.get('[data-test=button-chat-message-send]')
              .click();
          cy.get('[data-test=text-chat-message]').last()
              .should('contain', message);

          //Logout and login with receiver to check if message was sent
          cy.apiLogout().then(() => {
            cy.visit('/');
            cy.wait('@logout');
            cy.clearCookies().then(() => {
              cy.apiLogin(receiverResponse.body.content[0].email, 'demo')
                  .apiDismissTour()
                  .then(() => {
                    cy.visit('/');
                  });
            });
          });
          cy.get(`[data-test="button-message-channel-${currentUserResponse.body.displayName}"]`)
              .click();
          cy.get('[data-test=text-chat-message]').last()
              .should('contain', message);
        });
      });
});

/**
 * Command for create a new group message channel with two users and send a message
 * @param {string} user1 is the name of the first user for the new message channel
 * @param {string} user2 is the name of the second user for the new message channel
 * @param {string} channelName is the name for the new group message channel
 * @param {string} message is the chat message
 *
 */

Cypress.Commands.add('newGroupMessageChannel', function (user1, user2, channelName, message) {
  // Route for logout check
  cy.server();
  cy.route({
    method: "POST",
    url: "/web/auth/logout"
  })
      .as('logout');

  // Get mail of receiver for later login
  cy.request(`/web/users?term=${user1}`)
      .then((receiverResponse) => {
        cy.request('GET', '/web/users/me').then((currentUserResponse) => {
          cy.get('[data-test=button-create-chat-channel]')
              .click();
          cy.get('[data-test=button-create-group-channel]')
              .click();
          cy.get('[data-test=input-channel-name]')
              .clear()
              .type(channelName);
          cy.get(`[data-test="button-user-list-messaging-channel-${user1}"]`)
              .click();
          cy.get('[data-test=button-add-user]')
              .click();
          cy.get(`[data-test="button-user-list-messaging-channel-${user2}"]`)
              .click();
          cy.get('[data-test=button-save-group-channel]')
              .click();
          cy.get('[data-test=textarea-message-form]')
              .type(message);
          cy.get('[data-test=button-chat-message-send]')
              .click();
          cy.get('[data-test=text-chat-message]').last()
              .should('contain', message);
          cy.get('[data-test=text-messaging-channel-name]')
              .should('have.text', channelName);
          cy.get('[data-test=container-message-channel]')
              .should('contain', user1)
              .should('contain', user2)
              .should('contain', currentUserResponse.body.displayName);

          //Logout and login with one receiver to check if message was sent
          cy.apiLogout().then(() => {
            cy.visit('/');
            cy.wait('@logout');
            cy.clearCookies().then(() => {
              cy.apiLogin(receiverResponse.body.content[0].email, 'demo')
                  .apiDismissTour()
                  .then(() => {
                    cy.visit('/');
                  });
            });
          });
          cy.get(`[data-test="button-message-channel-${channelName}"]`)
              .click();
          cy.get('[data-test=text-chat-message]').last()
              .should('contain', message);
          cy.get('[data-test=text-messaging-channel-name]')
              .should('have.text', channelName);
          cy.get('[data-test=container-message-channel]')
              .should('contain', user1)
              .should('contain', user2)
              .should('contain', currentUserResponse.body.displayName);
        });
      });
});

/**
 * Command to check the behavior of emojies in the chat
 * @param {string} user is the name of the user for the new message channel
 * * @param {string} message is the chat message
 *
 */

Cypress.Commands.add('newSingleMessageChannelWithEmojies', function (user, message) {
  cy.server();
  cy.route('POST', '/web/message-channels/**').as('messageSent');
  cy.get('[data-test=button-create-chat-channel]')
      .click();
  cy.get(`[data-test="button-user-list-messaging-channel-${user}"]`)
      .click();
  cy.get('[data-test=button-emoji-picker]')
      .click();
  cy.get('span[title="Smileys & People"]').last()
      .click();
  cy.get('button[aria-label="😀"]')
      .click();
  cy.get('[data-test=button-emoji-picker]')
      .click();
  cy.get('[data-test=textarea-message-form]')
      .type(message);
  cy.get('[data-test=button-emoji-picker]')
      .click();
  cy.get('button[aria-label="😁"]')
      .click();
  cy.get('[data-test=button-emoji-picker]')
      .click();
  cy.get('[data-test=button-chat-message-send]')
      .click();
  cy.wait('@messageSent');
  cy.get('[data-test=text-chat-message]').last()
      .should('contain', '😀' + message + '😁');
});