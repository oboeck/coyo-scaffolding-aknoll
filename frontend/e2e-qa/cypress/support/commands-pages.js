// ***********************************************************
// Custom commands related to pages
// ***********************************************************

/**
 * Command for creating a page
 * @param {string} visibility can be 'public' or 'private'
 * @param {string} subscription can be 'off', 'everyone', or 'selected'
 *
 */

Cypress.Commands.add('createPage', function (visibility, subscription) {
  cy.server();
  cy.route('GET', '/web/pages/check-name**').as('getNameCheck');

  // Visit page overview and start creation
  cy.get('[data-test=navigation-pages]')
      .click();
  cy.url()
      .should('contain', Cypress.config().baseUrl + '/pages');
  cy.get('[data-test=button-create-page]').eq(0)
      .click();
  cy.url()
      .should('contain', Cypress.config().baseUrl + '/pages/create');

  // Setup the new page
  const pageName = 'qa-page-' + visibility + '-' + Date.now();
  cy.get('[data-test=input-name]')
      .type(pageName);
  cy.wait('@getNameCheck');
  cy.get('[data-test="button-page-creation-general submit"]')
      .click();

  // Set visibility
  cy.get('[data-test=radio-page-' + visibility + ']')
      .check();
  cy.get('[data-test="button-page-creation-access submit"]')
      .click();

  // Set Auto-subscription
  cy.get('[data-test=radio-subscription-' + subscription + ']')
      .check();
  if (subscription === 'selected') {
    cy.get('[data-test=button-select-user] > [data-test=button-user-chooser]')
        .click();
    cy.get('[data-test=list-user-chooser-user]').eq(0)
        .click();
    cy.get('[data-test=button-user-chooser-submit]')
        .click();
  }
  cy.get('[data-test="button-page-creation-subscription submit"]')
      .click();
  cy.get('[data-test=text-page-title]')
      .contains(pageName);
});

/**
 * Command for creating a page via API
 * @param {string} name describes the name of the page
 * @param {string} visibility can be 'public' or 'private'
 *
 */

Cypress.Commands.add('apiCreatePage', function (name, visibility) {
  return cy.apiBearerToken('robert.lang@coyo4.com', 'demo').then((bearerToken) => cy.request({
    url: Cypress.env('backendUrl') + '/api/pages',
    method: 'POST',
    auth: {
      bearer: bearerToken
    },
    body: {
      name: name || Cypress.faker.lorem.words(),
      visibility: visibility || 'PUBLIC',
      adminIds: [this.user.id]
    }
  }));
});