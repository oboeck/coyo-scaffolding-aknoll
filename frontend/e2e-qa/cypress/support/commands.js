// ***********************************************************
// Custom commands related to different tasks
// ***********************************************************

Cypress.Commands.add('adminAreaNavigation', function () {
  cy.get('[data-test=navigation-dropdown]')
      .click();
  cy.get('[data-test=navigation-administration]')
      .click();
});

Cypress.Commands.add('logout', function () {
  cy.server();
  cy.route('POST', '/web/auth/logout').as('logout');
  cy.apiLogout().then(() => {
    cy.visit('/');
    cy.wait('@logout');
    cy.clearCookies();
  });
});

/**
 * Command for searching for up to two words using the global search
 * @param {string} the first word should be a single word
 * @param {string} the second word should be a single word
 *
 */

Cypress.Commands.add('search', function (firstWord, secondWord) {
  cy.server();
  cy.route('GET', '/web/search**').as('searchResults');
  cy.route('GET', 'web/quick-entity-search**').as('quickEntitySearch');
  cy.get('[data-test=navigation-search-icon]')
      .click();
  if (secondWord === undefined) {
    cy.get('[data-test=navigation-global-search-input]')
        .type(`${firstWord}{enter}`);
  } else {
    cy.get('[data-test=navigation-global-search-input]')
        .type(`${firstWord} ${secondWord}{enter}`);
  }
  cy.wait('@quickEntitySearch');
  cy.get('[data-test=navigation-global-search-input]')
      .type('{esc}');
});

/**
 * Command for creating an app via API
 * @param {string} app can be 'Blog', 'Content', 'Events', 'Documents', 'Form', 'Forum', 'List', 'Tasks', 'Timeline', 'Wiki', or 'Championship'
 *
 */

Cypress.Commands.add('createApp', function (app) {
  cy.server();
  cy.route('GET', '/web/apps/configurations**').as('getAppsModal');
  cy.get('[data-test=button-add-app]')
      .click();
  cy.wait('@getAppsModal');
  cy.get('[data-test=button-app-chooser]').contains(app)
      .click();
  switch (app) {
    case 'List':
      cy.get('[data-test=input-list-settings-element-name]')
          .type(app);
      break;
    case 'Form':
      cy.get('[data-test=input-form-title]')
          .type(app);
      break;
  }
  cy.get('[data-test="button-app-chooser-save submit"]')
      .click();
  if (app === 'Content') {
    cy.get('[data-test=button-content-edit-save]')
        .click();
  }
});
