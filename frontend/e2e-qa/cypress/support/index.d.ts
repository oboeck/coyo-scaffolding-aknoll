/// <reference types="Cypress" />

declare namespace Cypress {
    interface Chainable {
        createTimelineEntry(timeLineEntry: string): Cypress.Chainable<JQuery>;
        editTimelineEntry(editPost: string): Cypress.Chainable<JQuery>;
        commentTimelineEntry(editPost: string): Cypress.Chainable<JQuery>;
        likeTimelineEntry(editPost: string): Cypress.Chainable<JQuery>;
        unlikeTimelineEntry(editPost: string): Cypress.Chainable<JQuery>;
        deleteTimelineEntry(editPost: string): Cypress.Chainable<JQuery>;
        requestAuth(method: string, path: string, body?: string): Cypress.Chainable<JQuery>;
        createTimelinePostAPI(message: string, isRestricted: boolean):  Cypress.Chainable<JQuery>;
        showNewTimelinePosts():  Cypress.Chainable<JQuery>;
    }
}
