/// <reference types="Cypress" />

describe('Timeline comments', () => {
  let timelinePostMessage = '';
  let targetId = '';
  let targetType = '';

  beforeEach(() => {
    timelinePostMessage = Cypress.faker.random.word();
    cy.clearLocalStorage().login('ib', 'demo').dismissTour();
    cy.createTimelinePost(timelinePostMessage, false).then((response) => {
      targetId = response.id;
      targetType = response.typeName;
      cy.log('Set targetId', targetId);
      cy.log('Set targetType', targetType);
    }).showNewTimelinePosts();
  });

  context('desktop', () => {

    it('should create a comment on a timeline post', () => {
      const commentMessage = Cypress.faker.random.word();
      cy.visit('/home/timeline')
        .get('[data-test="timeline-stream"]')
        .contains('[data-test="timeline-item"]', timelinePostMessage).within(() => {
          cy.get('[data-test="comment-form"]').within(() => {
            cy.get('[data-test="timeline-comment-message"]').click().type(commentMessage)
              .get('[translate="COMMENTS.ACTIONS.SEND"]').should('be.visible')
              .get('[translate="COMMENTS.ACTIONS.CANCEL"]').should('be.visible')
                .get('[translate="COMMENTS.ACTIONS.SEND"]').click();
          });
          cy.get('[data-test="comment-message"]').should('contain',commentMessage);
        });
    });

    it('should edit the comment on a timeline post', () => {
      const comment = Cypress.faker.random.word();
      cy.createTimelineComment(comment, targetId, targetType)
        .visit('/home/timeline')
        .get('[data-test="timeline-stream"]')
        .contains('[data-test="timeline-item"]', comment).within(() => {
          cy.get('.comment .context-menu').invoke('show').click()
            .get('[data-test="edit-comment-button"]').click()
            .get('[data-test="timeline-comment-message"]').click().type(' - edited')
            .get('[translate="COMMENTS.ACTIONS.SEND"]').should('be.visible')
            .get('[translate="COMMENTS.ACTIONS.CANCEL"]').should('be.visible')
            .get('[data-test="mobile-comment-submit-button"]').should('not.be.visible')
            .get('[translate="COMMENTS.ACTIONS.SEND"]').click()
            .get('[data-test="comment-message"]').should('contain', comment + ' - edited');
      });
    });
  });

  context('mobile', () => {

    beforeEach(() => {
      cy.viewport('iphone-4');
    });

    it('should create a comment on a timeline post', () => {
      const commentMessage = Cypress.faker.random.word();
      cy.visit('/home/timeline')
          .get('[data-test="timeline-stream"]')
          .contains('[data-test="timeline-item"]', timelinePostMessage).within(() => {
        cy.get('[data-test="comment-form"]').within(() => {
          cy.get('[data-test="timeline-comment-message"]').click().type(commentMessage)
              .get('[translate="COMMENTS.ACTIONS.SEND"]').should('not.be.visible')
              .get('[translate="COMMENTS.ACTIONS.CANCEL"]').should('not.be.visible')
              .get('[data-test="mobile-comment-submit-button"]').should('be.visible')
              .get('[data-test="mobile-comment-submit-button"]').click();
        });
        cy.get('[data-test="comment-message"]').should('contain',commentMessage);
      });
    });

    it('should edit the comment on a timeline post', () => {
      const comment = Cypress.faker.random.word();
      cy.createTimelineComment(comment, targetId, targetType)
          .visit('/home/timeline')
          .get('[data-test="timeline-stream"]')
          .contains('[data-test="timeline-item"]', comment).within(() => {
        cy.get('.comment .context-menu').invoke('show').click()
            .get('[data-test="edit-comment-button"]').click()
            .get('[data-test="timeline-comment-message"]').click().type(' - edited')
            .get('[translate="COMMENTS.ACTIONS.SEND"]').should('not.be.visible')
            .get('[translate="COMMENTS.ACTIONS.CANCEL"]').should('not.be.visible')
            .get('[data-test="mobile-edit-chancel-button"]').should('be.visible')
            .get('[data-test="mobile-edit-submit-button"]').should('be.visible')
            .get('[data-test="mobile-edit-submit-button"]').click()
            .get('[data-test="comment-message"]').should('contain', comment + ' - edited');
      });
    });
  });
});
