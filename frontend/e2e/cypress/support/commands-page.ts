// ***********************************************************
// Custom commands related to coyo pages
//
// Commands defined in this file may utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// Ideally, these commands utilize only variables defined in
// authentication commands at `./commands-auth.ts`,
// ***********************************************************

Cypress.Commands.add('createPage', function (name: string) {
  return cy.requestAuth('POST', '/web/pages', {
    name: name,
    visibility: 'PUBLIC',
    active: true,
    adminIds: [this.user.id],
    adminGroupIds: [],
    categoryIds: [],
    categories: null,
    autoSubscribeType: 'NONE',
    defaultLanguage: null,
    memberGroupIds: [],
    memberIds: [],
    translations: {}
  }).its('body');
});
