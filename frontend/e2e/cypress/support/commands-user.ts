// ***********************************************************
// Custom commands related to users
//
// Commands defined in this file may utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// Ideally, these commands utilize only variables defined in
// authentication commands at `./commands-auth.ts`,
// ***********************************************************

Cypress.Commands.add('dismissTour', function (topics) {
  return cy.requestAuth('PUT', '/web/users/' + this.user.id + '/tour', {
    visited: topics || [
      'navigation',
      'messaging',
      'timeline',
      'pages',
      'workspaces',
      'events',
      'profile',
      'colleagues'
    ]
  });
});
