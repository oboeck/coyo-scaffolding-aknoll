// ***********************************************************
// General custom commands
//
// Commands defined in this file must not utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// ***********************************************************

Cypress.Commands.add('path', function () {
  return cy.location('pathname');
});

Cypress.Commands.add('requestAuth', function (method, path, body) {
  return cy.request({
    method: method,
    url: Cypress.env('backendUrl') + path,
    headers: method === 'GET' ? {
      'Accept': 'application/json',
      'X-Coyo-Current-User': this.user.id
    } : {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRF-TOKEN': this.csrf,
      'X-Coyo-Current-User': this.user.id
    },
    body: body
  });
});
