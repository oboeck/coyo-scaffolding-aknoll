// ***********************************************************
// General custom commands
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Returns the pathname of the the global `window.location` object of the
     * page that is currently active.
     *
     * @example
     *    cy.path();
     */
    path(): Chainable<Location>;

    /**
     * Makes an authenticated HTTP request with a specific method.
     *
     * @see https://on.cypress.io/request
     * @example
     *    cy.requestAuth('POST', '/users', {name: 'Jane'})
     */
    requestAuth(method: HttpMethod, path: string, body?: RequestBody): Chainable<Response>
  }
}
