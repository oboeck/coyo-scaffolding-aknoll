// ***********************************************************
// Custom commands related to apps
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Creates a new form app with the given name for given sender and returns it.
     *
     * @param name The name to use.
     * @param senderId The senderId to use.
     * @param enableNotifications true if notifications should be enabled.
     * @example
     *    cy.createFormApp('My Page', 'senderId', true);
     */
    createFormApp(name: string, senderId: string, enableNotifications: boolean): Chainable<any>;

    /**
     * Creates a new form app entry with the given value for a given field and sender and returns it.
     * @param value The field value to use.
     * @param fieldId The field id to use.
     * @param senderId  The senderId to use.
     * @param appId The app id to use.
     * @example
     *  cy.createFormAppEntry('Text field value', 'fieldId' 'senderId', 'appId');
     */
    createFormAppEntry(value: string, fieldId: string, senderId: string, appId: string): Chainable<any>;

    /**
     * Adds a new field to a given form app with the given name and given type and returns it.
     *
     * @param name The name to use.
     * @param type The type to use.
     * @param senderId The senderId to use.
     * @param appId The app id to use.
     * @example
     *    cy.addFieldToFormApp('Textfield', 'senderId', 'appId', 'text');
     */
    addFieldToFormApp(name: string, type: string, senderId: string, appId: string): Chainable<any>;
  }
}