// ***********************************************************
// Custom commands related to authentication
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Retrieves a new CSRF token and returns it. The token is also provided as
     * a context variable named `csrf`.
     *
     * @example
     *    cy.csrf();
     */
    csrf(): Chainable<string>;

    /**
     * Logs in using the given credentials and returns the current user. The
     * user is also provided as a context variable named `user`.
     *
     * @param {string} [username] The username to use. Defaults to `Cypress.env('username')`.
     * @param {string} [password] The password to use. Defaults to `Cypress.env('password')`.
     * @example
     *    cy.login();
     *    cy.login('username@example.com', 'secret');
     */
    login(username?: string, password?: string): Chainable<any>;

    /**
     * Logs out the current user.
     *
     * @example
     *    cy.logout();
     */
    logout(): Chainable<any>;
  }
}
