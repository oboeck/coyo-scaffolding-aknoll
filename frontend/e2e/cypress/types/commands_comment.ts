// ***********************************************************
// Custom commands related to coyo timeline item comments
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Creates a new timeline comment and returns it.
     *
     * @param message The timeline comment message.
     * @param targetId The target id.
     * @param targetType The target type. Default is 'timeline-item'.
     * @example
     *    cy.createTimelineComment('comment message', 'timeline-id', 'timeline-item');
     */
    createTimelineComment(message: string, targetId: string, targetType: string): Chainable<any>;
  }
}
