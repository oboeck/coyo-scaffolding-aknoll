// ***********************************************************
// Custom commands related to coyo pages
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Creates a new page with the given name and returns it.
     *
     * @param {string} name The name to use.
     * @example
     *    cy.createPage('My Page');
     */
    createPage(name: string): Chainable<any>;
  }
}