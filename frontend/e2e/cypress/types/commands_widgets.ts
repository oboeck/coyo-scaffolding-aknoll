// ***********************************************************
// Custom commands related to widgets
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Creates a new landing page with the given name and returns it.
     *
     * @param {string} name The name to use.
     * @param {boolean} [force=true] If set to `true`, enforces the creation by deleting any entities with the same name beforehand.
     * @example
     *    cy.createLandingPage('My Page');
     *    cy.createLandingPage('My Page', true);
     */
    createLandingPage(name: string, force?: boolean): Chainable<any>;
  }
}
