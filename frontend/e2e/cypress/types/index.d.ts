// ***********************************************************
// Additional type definitions
// ***********************************************************

declare namespace Cypress {

  interface Cypress {

    /**
     * Faker library
     *
     * @see https://github.com/marak/Faker.js
     * @example
     *    Cypress.faker.name.firstName();
     *    Cypress.faker.name.lastName();
     */
    faker: Faker.FakerStatic
  }
}
