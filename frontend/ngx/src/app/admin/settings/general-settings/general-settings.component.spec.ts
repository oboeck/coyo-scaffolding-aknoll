import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {JitTranslationSettingsService} from '@app/admin/settings/jit-translation-settings/jit-translation-settings.service';
import {SettingsService} from '@domain/settings/settings.service';
import {TranslateService} from '@ngx-translate/core';
import {Ng1CoyoNotification} from '@root/typings';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {of} from 'rxjs';
import {GeneralSettingsComponent} from './general-settings.component';

describe('GeneralSettingsComponent', () => {
  let component: GeneralSettingsComponent;
  let fixture: ComponentFixture<GeneralSettingsComponent>;
  let settingsSerivce: jasmine.SpyObj<SettingsService>;
  let jitTranslationSettingsService: jasmine.SpyObj<JitTranslationSettingsService>;
  let translateService: jasmine.SpyObj<TranslateService>;
  let notificationService: jasmine.SpyObj<Ng1CoyoNotification>;

  const NONE = 'NONE';
  const DEEPL = 'DEEPL';
  const TEST_API_KEY = 'test-key-123';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GeneralSettingsComponent],
      imports: [ReactiveFormsModule],
      providers: [
        {
          provide: SettingsService,
          useValue: jasmine.createSpyObj('settingsService', ['retrieveAndForceRefresh', 'update'])
        },
        {
          provide: JitTranslationSettingsService,
          useValue: jasmine.createSpyObj('jitTranslationSettingsService', ['getSettings', 'putSettings',
            'validateApiKey'])
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('translateService', ['instant'])
        },
        {
          provide: NG1_NOTIFICATION_SERVICE,
          useValue: jasmine.createSpyObj('notificationService', ['success', 'error'])
        },
      ]
    }).overrideTemplate(GeneralSettingsComponent, '')
      .compileComponents();

    settingsSerivce = TestBed.get(SettingsService);
    jitTranslationSettingsService = TestBed.get(JitTranslationSettingsService);
    translateService = TestBed.get(TranslateService);
    notificationService = TestBed.get(NG1_NOTIFICATION_SERVICE);
    jitTranslationSettingsService.validateApiKey.and.returnValue(of(true));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralSettingsComponent);
    component = fixture.componentInstance;
  });

  it('Init form for general backend settings', () => {
    // given
    const COYO = 'COYO';
    const settingsMap = new Map();
    settingsMap.set('networkName', COYO);
    setFormGroupSettings(settingsMap, {activeProvider: NONE, apiKey: null});

    // when
    fixture.detectChanges();

    // then
    expect(component.settingsForm.get('backendSettings').get('networkName').value).toBe(COYO);
    expect(component.settingsForm.valid).toBeTruthy();
  });

  it('Init form for jit translation service settings', () => {
    // given
    setFormGroupSettings(new Map(), {activeProvider: DEEPL, apiKey: TEST_API_KEY});

    // when
    fixture.detectChanges();

    // then
    expect(component.settingsForm.get('jitTranslationSettings').get('activeProvider').value).toBe('DEEPL');
    expect(component.settingsForm.valid).toBeTruthy();
  });

  it('Custom form validators jit-settings valid', () => {
    // given
    setFormGroupSettings(new Map(), {activeProvider: DEEPL, apiKey: TEST_API_KEY});

    // when
    fixture.detectChanges();

    // then
    expect(component.settingsForm.valid).toBeTruthy();
  });

  it('Custom form validators jit-settings invalid', () => {
    // given
    setFormGroupSettings(new Map(), {activeProvider: DEEPL, apiKey: ''});

    // when
    fixture.detectChanges();

    // then
    expect(component.settingsForm.valid).toBeFalsy();

  });

  it('Custom form validators user anonmyization valid', () => {
    // given
    const settingsMap = new Map();
    settingsMap.set('deletedUserAnonymizationActive', 'true');
    settingsMap.set('deletedUserDisplayName', 'DummyUser');

    setFormGroupSettings(settingsMap, {activeProvider: NONE, apiKey: null});

    // when
    fixture.detectChanges();

    // then
    expect(component.settingsForm.valid).toBeTruthy();
  });

  it('Custom form validators user anonmyization invalid', () => {
    // given
    const settingsMap = new Map();
    settingsMap.set('deletedUserAnonymizationActive', 'true');
    settingsMap.set('deletedUserDisplayName', '');

    setFormGroupSettings(settingsMap, {activeProvider: NONE, apiKey: null});

    // when
    fixture.detectChanges();

    // then
    expect(component.settingsForm.valid).toBeFalsy();
    expect(component.userAnonymizationActive$.subscribe(active => expect(active).toBeTruthy()));
  });

  it('User anonymization collapse is activated', () => {
    // given
    const settingsMap = new Map();
    settingsMap.set('deletedUserAnonymizationActive', 'true');
    setFormGroupSettings(settingsMap, {activeProvider: NONE, apiKey: null});

    // when
    fixture.detectChanges();

    // then
    component.userAnonymizationActive$.subscribe(active => expect(active).toBeTruthy());
  });

  it('User anonymization collapse is not activated', () => {
    // given
    const settingsMap = new Map();
    settingsMap.set('deletedUserAnonymizationActive', 'false');
    setFormGroupSettings(settingsMap, {activeProvider: NONE, apiKey: null});

    // when
    fixture.detectChanges();

    // then
    component.userAnonymizationActive$.subscribe(active => expect(active).toBeFalsy());
  });

  it('Jit-settings collapse is activated', () => {
    // given
    setFormGroupSettings(new Map(), {activeProvider: DEEPL, apiKey: TEST_API_KEY});

    // when
    fixture.detectChanges();

    // then
    component.isTranslationProviderActive$.subscribe(active => expect(active).toBeTruthy());
  });

  it('Jit-settings collapse is not activated', () => {
    // given
    setFormGroupSettings(new Map(), {activeProvider: NONE, apiKey: TEST_API_KEY});
    // when
    fixture.detectChanges();

    // then
    component.isTranslationProviderActive$.subscribe(active => expect(active).toBeFalsy());
  });

  it('should submit the values from the settingsForm', () => {
    // given
    const COYO = 'COYO';
    const settingsMap = new Map();
    const jitSettingsSentValue = {activeProvider: DEEPL, apiKey: TEST_API_KEY};
    const jitSettingsReturnValue = {activateTranslation: true, activeProvider: DEEPL, apiKey: TEST_API_KEY};
    settingsMap.set('networkName', COYO);
    setFormGroupSettings(settingsMap, jitSettingsSentValue);

    settingsSerivce.update.and.returnValue(of(settingsMap));
    jitTranslationSettingsService.putSettings.and.returnValue(of(jitSettingsSentValue));
    fixture.detectChanges();

    const backendSettings: FormGroup = component.settingsForm.get('backendSettings') as FormGroup;
    const jitTranslationSettings: FormGroup = component.settingsForm.get('jitTranslationSettings') as FormGroup;
    jitTranslationSettings.markAsDirty();

    // when
    component.submit();

    // then
    expect(settingsSerivce.update).toHaveBeenCalledWith(backendSettings.getRawValue());
    expect(jitTranslationSettingsService.putSettings).toHaveBeenCalledWith(jitSettingsReturnValue);
    expect(notificationService.success).toHaveBeenCalledWith('ADMIN.SETTINGS.SAVE.SUCCESS');
    expect(jitTranslationSettings.getRawValue()).toEqual(jitSettingsReturnValue);
  });

  it('should set default deletedUserDisplayName when it is empty', () => {
    // given
    const deletedUserName = 'Deleted User';
    const settingsMap = new Map();
    settingsMap.set('networkName', 'COYO');
    settingsMap.set('deletedUserAnonymizationActive', 'true');
    setFormGroupSettings(settingsMap, {activeProvider: NONE, apiKey: null});

    translateService.instant.and.returnValue(deletedUserName);
    settingsSerivce.update.and.returnValue(of(settingsMap));
    jitTranslationSettingsService.putSettings.and.returnValue(of({activateTranslation: false, activeProvider: NONE, apiKey: ''}));
    fixture.detectChanges();

    // when
    component.submit();

    // then
    expect(translateService.instant).toHaveBeenCalledWith('ADMIN.SETTINGS.ANONYMIZE_DELETEDUSERS.DELETED_NAME.DEFAULT');
    expect(component.settingsForm.get('backendSettings').get('deletedUserDisplayName').value).toBe(deletedUserName);
  });

  it('should reset jitsettings from form when activateTranslation is switched to false', () => {
    // given
    setFormGroupSettings(new Map(), {activeProvider: DEEPL, apiKey: TEST_API_KEY});
    fixture.detectChanges();

    // when
    component.settingsForm.get('jitTranslationSettings').get('activateTranslation').setValue(false);

    // then
    expect(component.settingsForm.get('jitTranslationSettings').get('activeProvider').value).toBe(null);
    expect(component.settingsForm.get('jitTranslationSettings').get('apiKey').value).toBe(null);
  });

  it('should enable API input field', () => {
    // given
    setFormGroupSettings(new Map(), {activeProvider: NONE, apiKey: null});
    fixture.detectChanges();

    // when
    component.enableApiInput();

    // then
    expect(component.settingsForm.get('jitTranslationSettings').get('apiKey').disabled).toBe(false);

  });

  function setFormGroupSettings(settingsMap: any, jitSettings: any): void {
    settingsSerivce.retrieveAndForceRefresh.and.returnValue(of(settingsMap));
    jitTranslationSettingsService.getSettings.and.returnValue(of(jitSettings));
  }
});
