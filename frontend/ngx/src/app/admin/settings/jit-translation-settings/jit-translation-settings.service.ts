import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {JitTranslationActiveSettings} from '@app/admin/settings/jit-translation-settings/jit-translation-active-settings';
import {JitTranslationSettings} from '@app/admin/settings/jit-translation-settings/jit-translation-settings';
import {Observable, throwError} from 'rxjs';
import {catchError, map, shareReplay} from 'rxjs/operators';

/**
 * Service to handle jit-translation-settings
 */
@Injectable({
  providedIn: 'root'
})
export class JitTranslationSettingsService {

  private isActive$: Observable<JitTranslationActiveSettings>;

  constructor(private http: HttpClient) {
  }

  /**
   * Get the jit-translation settings.
   * @return an observable which holds the jit-translation settings
   */
  getSettings(): Observable<JitTranslationSettings> {
    return this.http.get<JitTranslationSettings>('/web/translation/settings', {
      headers: new HttpHeaders({
        handleErrors: 'false'
      })
    });
  }

  /**
   * Stores the given settings in the jit-translation service.
   * @param settings which are to be stored
   * @return an observable which holds the new saved jit-translation settings
   */
  putSettings(settings: JitTranslationSettings): Observable<JitTranslationSettings> {
    this.isActive$ = null;
    return this.http.put<JitTranslationSettings>('/web/translation/settings', settings);
  }

  /**
   * Checks if there is an active translation provider configured
   *
   * @return The active translation provider settings
   */
  getActiveSettings(): Observable<JitTranslationActiveSettings> {
    if (!this.isActive$) {
      this.isActive$ = this.http.get<JitTranslationActiveSettings>('/web/translation/settings/activeLanguages', {
        headers: {
          handleErrors: 'false'
        }
      }).pipe(catchError((err: HttpErrorResponse) => {
          this.isActive$ = null;
          return throwError(err);
        })
      ).pipe(shareReplay(1));
    }
    return this.isActive$;
  }

  /**
   * Validates if the given api key can be used to access the translation provider
   * @param settings The currently configured settings
   *
   * @return true if the api key is valid
   */
  validateApiKey(settings: JitTranslationSettings): Observable<boolean> {
    return this.http.post<{apiKeyValid: boolean}>('/web/translation/settings/validate/api-key', settings)
      .pipe(map(result => result.apiKeyValid));
  }
}
