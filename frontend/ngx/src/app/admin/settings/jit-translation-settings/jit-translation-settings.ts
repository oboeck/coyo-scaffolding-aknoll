/**
 * Interface for the jit-translation-settings
 */
export interface JitTranslationSettings {

  activeProvider: string;

  apiKey: string;
}
