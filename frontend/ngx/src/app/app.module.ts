import {ModuleWithProviders, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AccountModule} from '@app/account/account.module';
import {AdminModule} from '@app/admin/admin.module';
import {EngageModule} from '@app/engage/engage.module';
import {O365Module} from '@app/integration/o365/o365.module';
import {MessagingModule} from '@app/messaging/messaging.module';
import {TeamsRedirectModule} from '@app/msteams/teams-redirect.module';
import {NotificationsModule} from '@app/notifications/notifications.module';
import {ReportsModule} from '@app/reports/reports.module';
import {SearchModule} from '@app/search/search.module';
import {TimelineModule} from '@app/timeline/timeline.module';
import {AppsModule} from '@apps/apps.module';
import {CoreModule} from '@core/core.module';
import {TranslationCompilerService} from '@core/i18n/translation-compiler/translation-compiler.service';
import {TranslationLoaderService} from '@core/i18n/translation-loader/translation-loader.service';
import {TranslateCompiler, TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {AnchorModule} from '@shared/anchor/anchor.module';
import {ContextMenuModule} from '@shared/context-menu/context-menu.module';
import {DividerModule} from '@shared/divider/divider.module';
import {DownloadModule} from '@shared/download/download.module';
import {FileModule} from '@shared/files/file.module';
import {HashtagModule} from '@shared/hashtags/hashtag.module';
import {IconModule} from '@shared/icon/icon.module';
import {MarkdownModule} from '@shared/markdown/markdown.module';
import {RteModule} from '@shared/rte/rte.module';
import {SenderUIModule} from '@shared/sender-ui/sender-ui.module';
import {SocialModule} from '@shared/social/social.module';
import {SpinnerModule} from '@shared/spinner/spinner.module';
import {TimeModule} from '@shared/time/time.module';
import {TrustHtmlModule} from '@shared/trust-html/trust-html.module';
import {TrustUrlModule} from '@shared/trust-url/trust-url.module';
import {UIRouterUpgradeModule} from '@uirouter/angular-hybrid';
import {WidgetsModule} from '@widgets/widgets.module';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import 'froala-editor/js/plugins.pkgd.min.js';
import {CollapseModule} from 'ngx-bootstrap';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ModalModule} from 'ngx-bootstrap/modal';
import {PopoverModule} from 'ngx-bootstrap/popover';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {TimepickerModule} from 'ngx-bootstrap/timepicker';
import {TooltipConfig, TooltipModule} from 'ngx-bootstrap/tooltip';
import {ClipboardModule} from 'ngx-clipboard';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {MomentModule} from 'ngx-moment';
import {NgxPermissionsModule} from 'ngx-permissions';
import {environment} from '../environments/environment';
import {BootstrapComponent} from './app.bootstrap.component';
import './app.bootstrap.component.downgrade';
import {GsuiteModule} from './integration/gsuite/gsuite.module';
import {IntegrationModule} from './integration/integration.module';
import {LaunchpadModule} from './launchpad/launchpad.module';

export const uiRouterModule: ModuleWithProviders = UIRouterUpgradeModule.forRoot();
export const buttonsModule: ModuleWithProviders = ButtonsModule.forRoot();
export const modalModule: ModuleWithProviders = ModalModule.forRoot();
export const tabsModule: ModuleWithProviders = TabsModule.forRoot();
export const tooltipModule: ModuleWithProviders = TooltipModule.forRoot();
export const froalaEditor: ModuleWithProviders = FroalaEditorModule.forRoot();
export const froalaView: ModuleWithProviders = FroalaViewModule.forRoot();
export const loggerModule: ModuleWithProviders = LoggerModule.forRoot({level: environment.production ? NgxLoggerLevel.WARN : NgxLoggerLevel.DEBUG});
export const permissionsModule: ModuleWithProviders = NgxPermissionsModule.forRoot();
export const translateModule: ModuleWithProviders = TranslateModule.forRoot({
  loader: {
    provide: TranslateLoader,
    useClass: TranslationLoaderService
  },
  compiler: {
    provide: TranslateCompiler,
    useClass: TranslationCompilerService
  }
});
export const bsDropdownModule: ModuleWithProviders = BsDropdownModule.forRoot();
export const bsDatePickerModule: ModuleWithProviders = BsDatepickerModule.forRoot();
export const bsTimePickerModule: ModuleWithProviders = TimepickerModule.forRoot();
export const popoverModule: ModuleWithProviders = PopoverModule.forRoot();
export const collapseModule: ModuleWithProviders = CollapseModule.forRoot();

/**
 * The ngx-bootstrap default tooltip configuration.
 *
 * @return the tooltip configuration
 */
export function getTooltipConfig(): TooltipConfig {
  return Object.assign(new TooltipConfig(), {
    container: 'body',
    triggers: 'hover',
    delay: 750
  });
}

/**
 * The main application module.
 */
@NgModule({
  imports: [
    AccountModule,
    AdminModule,
    AnchorModule,
    AppsModule,
    BrowserModule,
    bsDatePickerModule,
    bsDropdownModule,
    bsTimePickerModule,
    buttonsModule,
    ClipboardModule,
    ContextMenuModule,
    CoreModule,
    DividerModule,
    DownloadModule,
    EngageModule,
    FileModule,
    froalaEditor,
    froalaView,
    IntegrationModule,
    GsuiteModule,
    O365Module,
    HashtagModule,
    IconModule,
    LaunchpadModule,
    loggerModule,
    MarkdownModule,
    MessagingModule,
    modalModule,
    MomentModule,
    NotificationsModule,
    permissionsModule,
    popoverModule,
    ReportsModule,
    RteModule,
    SearchModule,
    SenderUIModule,
    SocialModule,
    SpinnerModule,
    tabsModule,
    TimelineModule,
    TimeModule,
    tooltipModule,
    translateModule,
    TrustHtmlModule,
    TrustUrlModule,
    uiRouterModule,
    WidgetsModule,
    collapseModule,
    BrowserAnimationsModule,
    TeamsRedirectModule
  ],
  declarations: [BootstrapComponent],
  entryComponents: [BootstrapComponent],
  providers: [{provide: TooltipConfig, useFactory: getTooltipConfig}]
})
export class AppModule {

  /**
   * Function is called when bootstrapping the angular 2 part of the application.
   */
  ngDoBootstrap(): void {
  }
}
