import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {messagesDe} from './de.apps.messages';
import {messagesEn} from './en.apps.messages';

/**
 * Module for Apps
 */
@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ]
})
export class AppsModule {}
