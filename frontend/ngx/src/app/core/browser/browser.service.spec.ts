import {inject, TestBed} from '@angular/core/testing';
import {WINDOW} from '@root/injection-tokens';
import {BrowserService} from './browser.service';

describe('BrowserService', () => {
  let windowMock: any; // any so userAgent can be set later

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BrowserService,
        {provide: WINDOW, useValue: {navigator: {userAgent: ''}, Modernizr: {video: {h264: false}}}}
      ]
    });

    windowMock = TestBed.get(WINDOW);
    windowMock.CSS = jasmine.createSpyObj('CSS', ['supports']);

  });

  it('should support css variables', inject([BrowserService], (service: BrowserService) => {
    windowMock.CSS.supports.and.returnValue(true);
    expect(service.supportsCSSVariables()).toBeTruthy();
  }));

  it('should not support css variables', inject([BrowserService], (service: BrowserService) => {
    windowMock.CSS.supports.and.returnValue(false);
    expect(service.supportsCSSVariables()).toBeFalsy();
  }));

  it('should be internet explorer', inject([BrowserService], (service: BrowserService) => {
    windowMock.navigator.userAgent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/5.0)';
    expect(service.isInternetExplorer()).toBeTruthy();
  }));

  it('should not be internet explorer', inject([BrowserService], (service: BrowserService) => {
    windowMock.navigator.userAgent = 'Chrome 70.0.3538.77';
    expect(service.isInternetExplorer()).toBeFalsy();
  }));

  it('should support quicktime video', inject([BrowserService], (service: BrowserService) => {
    windowMock.navigator.userAgent = 'Chrome 70.0.3538.77';
    windowMock.Modernizr.video.h264 = true;
    expect(service.supportsQuicktimeVideo()).toBe(true);
  }));

  it('should not support quicktime video', inject([BrowserService], (service: BrowserService) => {
    windowMock.navigator.userAgent = 'Chrome 70.0.3538.77';
    windowMock.Modernizr.video.h264 = false;
    expect(service.supportsQuicktimeVideo()).toBe(false);
  }));
});
