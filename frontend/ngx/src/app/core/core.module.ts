import {CommonModule} from '@angular/common';
import {ErrorHandler, NgModule} from '@angular/core';
import {ErrorHandlerService} from '@core/error/error-handler.service';
import {UserStorageService} from '@core/storage/user-storage/user-storage.service';
import cssVars from '@coyo/css-vars-ponyfill';
import '@domain/integration-settings/user-integration-settings.service.downgrade.ts';
import '@domain/like/like.service.downgrade';
import '@domain/settings/settings.service.downgrade';
import '@domain/subscription/subscription.service.downgrade';
import {CSS_VARS, JQUERY, WINDOW} from '@root/injection-tokens';
import * as $ from 'jquery';
import {HttpModule} from './http/http.module';
import {I18nModule} from './i18n/i18n.module';
import './theme/theme.service.downgrade';

window['$'] = $;
window['jQuery'] = $;

/**
 * Module for core components and services that may only be imported once by the app module.
 */
@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    I18nModule
  ],
  providers: [
    {provide: ErrorHandler, useClass: ErrorHandlerService},
    {provide: CSS_VARS, useValue: cssVars},
    {provide: WINDOW, useValue: window},
    {provide: JQUERY, useFactory: jQueryFactory}
  ]
})
export class CoreModule {

  constructor(userStorageService: UserStorageService) {
    userStorageService.init();
  }

}

/**
 * Factory for the jQuery service.
 *
 * @return the jQuery service object
 */
export function jQueryFactory(): JQueryStatic {
  return $;
}
