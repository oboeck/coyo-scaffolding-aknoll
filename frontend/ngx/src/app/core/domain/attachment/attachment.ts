import {GoogleFileMetaData} from '@app/integration/gsuite/google-api/google-file-metadata';
import {storageType} from '@domain/attachment/storage';

/**
 * An attachment symbolizes an uploaded file
 */
export interface Attachment {
  servialVersionUID: number;
  id: string;
  name: string;
  contentType: string;
  length: number;
  created: Date;
  modified: Date;
  storage: storageType;
  downloadUrl: string;
  fileId: string;
  modelId: string;
  deleted: boolean;
  canEdit?: boolean;
  exportLinks?: GoogleFileMetaData['exportLinks'];
}
