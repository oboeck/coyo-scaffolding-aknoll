export type storageType = 'LOCAL_BLOB_STORAGE' | 'LOCAL_FILE_LIBRARY' | 'G_SUITE' | 'OFFICE_365';

export const LOCAL_BLOB: storageType = 'LOCAL_BLOB_STORAGE';
export const LOCAL_FILE_LIBRARY: storageType = 'LOCAL_FILE_LIBRARY';
export const GSUITE: storageType = 'G_SUITE';
export const OFFICE365: storageType = 'OFFICE_365';
