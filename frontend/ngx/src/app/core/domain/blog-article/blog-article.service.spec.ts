import {HttpClientTestingModule} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {BlogArticleService} from './blog-article.service';

describe('BlogArticleService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [BlogArticleService, {
      provide: UrlService, useValue: jasmine.createSpyObj('urlService', [''])
    }]
  }));

  it('should be created', inject([BlogArticleService], (service: BlogArticleService) => {
    expect(service).toBeTruthy();
  }));
});
