import {App} from '@domain/apps/app';
import {BaseModel} from '@domain/base-model/base-model';
import {Sender} from '@domain/sender/sender';
import {Target} from '@domain/sender/target';

/**
 * Interface for a blog article
 */
export interface BlogArticle extends BaseModel {
  author: Sender;
  senderId: string;
  appId: string;
  title: string;
  teaserText: string;
  teaserImage: {
    fileId: string;
    senderId: string;
  };
  usedLanguage: string;
  publishDate: Date;
  app?: App;
  articleTarget: Target;
  published?: boolean;
  _permissions?: {
    like?: boolean;
    comment?: boolean;
  };
  showTeaserWithText: boolean;

  buildLayoutName(appId: string, language: string): string;
}
