import {TimelineFormAttachment} from '@shared/files/attachment-btn/types/timeline-form-attachment';
import {TimelineFormCoyoLibraryAttachment} from '@shared/files/attachment-btn/types/timeline-form-coyo-library-attachment';

/**
 * The comment request dto.
 */
export interface CommentRequest {
  authorId: string;
  targetId: string;
  targetType: string;
  message: string;
  attachments: TimelineFormAttachment[];
  fileLibraryAttachments: TimelineFormCoyoLibraryAttachment[];
}
