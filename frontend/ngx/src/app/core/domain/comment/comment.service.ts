import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {CommentRequest} from '@domain/comment/comment-request';
import {DomainService} from '@domain/domain/domain.service';
import {Page} from '@domain/pagination/page';
import {Sender} from '@domain/sender/sender';
import * as _ from 'lodash';
import {Observable, Subject} from 'rxjs';
import {bufferTime, filter, map, mergeMap, share, take} from 'rxjs/operators';
import {Comment} from './comment';

/**
 * A comment page request.
 */
interface CommentPageRequest {
  targetType: string;
  requestSubject: Subject<string>;
  resultSubject: Subject<{ [key: string]: Page<Comment> }>;
}

/**
 * Service to manage comments.
 */
@Injectable({
  providedIn: 'root'
})
export class CommentService extends DomainService<CommentRequest, Comment> {

  /**
   * The number of milliseconds to throttle bulk requests of this service.
   */
  static readonly THROTTLE: number = 50;

  private subjects: CommentPageRequest[] = [];

  constructor(http: HttpClient, urlService: UrlService) {
    super(http, urlService);
  }

  getBaseUrl(): string {
    return '/web/comments';
  }

  /**
   * Gets the initial page for comments and bulks simulaneous requests.
   * @param targetId The target id
   * @param targetType The target type
   * @returns An observable that emits the initial comment page of the requested target
   */
  getInitialPage(targetId: string, targetType: string): Observable<Page<Comment>> {
    const request = this.subjects.find(r => r.targetType === targetType)
      || this.buildRequest(targetType);

    request.requestSubject.next(targetId);
    return request.resultSubject.asObservable()
      .pipe(filter(pages => _.has(pages, targetId)))
      .pipe(map(pages => pages[targetId]))
      .pipe(take(1));
  }

  /**
   * Get the original author of a comment.
   *
   * @param id The comment id
   *
   * @returns An observable resolving the sender representation of the original author and then completes.
   */
  getOriginalAuthor(id: string): Observable<Sender> {
    return this.http.get<Sender>(this.getBaseUrl() + '/' + id + '/original-author');
  }

  private buildRequest(targetType: string): CommentPageRequest {
    const subject: CommentPageRequest = {
      targetType,
      requestSubject: new Subject(),
      resultSubject: new Subject()
    };
    this.subjects.push(subject);
    this.setup(subject);
    return subject;
  }

  private setup(request: CommentPageRequest): void {
    request.requestSubject
      .pipe(bufferTime(CommentService.THROTTLE))
      .pipe(filter(ids => ids && ids.length > 0))
      .pipe(mergeMap(ids => this.request(request.targetType, ids)), share())
      .subscribe(result => request.resultSubject.next(result));
  }

  private request(targetType: string, targetIds: string[]): Observable<{ [key: string]: Page<Comment> }> {
    return this.http.get<{ [key: string]: Page<Comment> }>(`/web/comments/${targetType}`, {
      headers: {
        etagBulkId: 'ids'
      },
      params: {
        ids: targetIds,
        _permissions: '*'
      }
    });
  }
}
