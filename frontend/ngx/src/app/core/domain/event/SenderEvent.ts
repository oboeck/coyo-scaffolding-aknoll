import {SenderEventSettings} from '@domain/event/SenderEventSettings';
import {Sender} from '@domain/sender/sender';
import {ShareableSender} from '@domain/sender/shareable-sender';

/**
 * Domain modal for Events
 */
export interface SenderEvent extends ShareableSender {
  attendingCount: number;
  creator: Sender;
  displayName: string;
  endDate: Date;
  fullDay: boolean;
  limitedParticipants: SenderEventSettings;
  place: string;
  requestDefiniteAnswer: boolean;
  showParticipants: boolean;
  startDate: Date;
}
