import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {DomainService} from '@domain/domain/domain.service';
import {SenderEvent} from '@domain/event/SenderEvent';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService extends DomainService<Event, Event> {

  constructor(http: HttpClient,
              urlService: UrlService) {
    super(http, urlService);
  }

  protected getBaseUrl(): string {
    return '/web/events';
  }

  /**
   * Gets event data.
   *
   * @param eventId the event id.
   * @return the requested event.
   */
  getEvent(eventId: string): Observable<SenderEvent> {
    const url = this.getUrl({eventId}, '/{eventId}');
    return this.http.get<SenderEvent>(url, {params: {_permissions: '*'}});
  }

  /**
   * Update a given event data.
   *
   * @param event the event with the updated data.
   * @return the updated event.
   */
  updateEvent(event: SenderEvent): Observable<SenderEvent> {
    const url = this.getUrl({eventId: event.id}, '/{eventId}');
    return this.http.put<SenderEvent>(url, event, {params: {_permissions: '*'}});
  }

  /**
   * Delete the event.
   *
   * @param eventId the event id.
   * @return the removed event.
   */
  deleteEvent(eventId: string): Observable<SenderEvent> {
    const url = this.getUrl({eventId}, '/{eventId}');
    return this.http.delete<SenderEvent>(url);
  }
}
