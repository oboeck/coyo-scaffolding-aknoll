import {HttpClientTestingModule} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import * as _ from 'lodash';
import {Document} from '../document';
import {DocumentService} from './document.service';

describe('DocumentService', () => {
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DocumentService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['getBackendUrl', 'join'])
      }]
    });

    urlService = TestBed.get(UrlService);
    urlService.getBackendUrl.and.returnValue('backendUrl');
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  it('should be created', inject([DocumentService], (service: DocumentService) => {
    expect(service).toBeTruthy();
  }));

  it('should return the correct URL', inject([DocumentService], (service: DocumentService) => {
    // when
    const url = service.getUrl({senderId: 'senderID'});

    // then
    expect(url).toEqual('/web/senders/senderID/documents');
  }));

  it('should extract the sender ID for a given URL', inject([DocumentService], (service: DocumentService) => {
    // given
    const url1 = '/web/senders/89725890-1720-4571-8b8e-bf232f6215ae/documents';
    const url2 = '/web/invalid';

    // when
    const id1 = service.extractSenderId(url1);
    const id2 = service.extractSenderId(url2);

    // then
    expect(id1).toEqual('89725890-1720-4571-8b8e-bf232f6215ae');
    expect(id2).toBeNull();
  }));

  it('should return the download URL for a given document', inject([DocumentService], (service: DocumentService) => {
    // given
    const document1 = {id: 'ID', senderId: 'senderID'} as Document;
    const document2 = {id: 'ID', senderId: 'senderID', modified: 42} as Document;

    // when
    const url1 = service.getDownloadUrl(document1);
    const url2 = service.getDownloadUrl(document2);

    // then
    expect(url1).toEqual('backendUrl/web/senders/senderID/documents/ID');
    expect(url2).toEqual('backendUrl/web/senders/senderID/documents/ID?modified=42');
  }));
});
