import {HttpHeaders, HttpResponse} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {UserIntegrationSettingsService} from '@domain/integration-settings/user-integration-settings.service';

describe('UserIntegrationSettingsService', () => {
  let userIntegrationSettingsService: UserIntegrationSettingsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserIntegrationSettingsService,
        {
          provide: UrlService,
          useValue: {}
        }]
    });

    userIntegrationSettingsService = TestBed.get(UserIntegrationSettingsService);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should call api to get calendar sync status', () => {
    // when
    const observable = userIntegrationSettingsService.isCalendarSyncEnabled();

    // then
    observable.subscribe(result => {
      expect(result).toBe(true);
    });
    const req = httpMock.expectOne('/web/users/integration-settings/calendar-sync');
    expect(req.request.method).toBe('GET');
    req.event(new HttpResponse<boolean>({body: true}));
    // work a round, because 'req.flush(true);' is not possible yet
    // -> https://github.com/angular/angular/issues/20690
  });

  it('should call API to disable the calendar sync', () => {
    // given
    const httpOptions = {
      headers: new HttpHeaders({
        handleErrors: 'false'
      })
    };

    // when
    const observable = userIntegrationSettingsService.disableCalendarSync(httpOptions);

    // then
    observable.subscribe(result => {
      expect(result).toBeNull();
    });
    const req = httpMock.expectOne('/web/users/integration-settings/calendar-sync/disable');
    expect(req.request.method).toBe('PUT');
    req.flush(null);

  });

  it('should call API to enable the calendar sync', () => {
    // given
    const httpOptions = {
      headers: new HttpHeaders({
        handleErrors: 'false'
      })
    };

    // when
    const observable = userIntegrationSettingsService.enableCalendarSync(httpOptions);

    // then
    observable.subscribe(result => {
      expect(result).toBeNull();
    });
    const req = httpMock.expectOne('/web/users/integration-settings/calendar-sync/enable');
    expect(req.request.method).toBe('PUT');
  });
});
