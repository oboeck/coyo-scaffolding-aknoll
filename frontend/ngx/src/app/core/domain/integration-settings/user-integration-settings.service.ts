import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {DomainService} from '@domain/domain/domain.service';
import {UserIntegrationSettings} from '@domain/integration-settings/user-integration-settings';
import {Observable} from 'rxjs';

/**
 * Service for file requests
 */
@Injectable({
  providedIn: 'root'
})
export class UserIntegrationSettingsService extends DomainService<UserIntegrationSettings, UserIntegrationSettings> {

  protected getBaseUrl(): string {
    return '/web/users/integration-settings';
  }

  constructor(http: HttpClient, urlService: UrlService) {
    super(http, urlService);
  }

  /**
   * Get the current status of the users calendar sync settings.
   * @return Observable of boolean
   */
  isCalendarSyncEnabled(): Observable<boolean> {
    return this.http.get<boolean>(this.getBaseUrl() + '/calendar-sync');
  }

  /**
   * Update settings to no longer sync COYO events with external calendar.
   *
   * @param options request options forwarded to http request
   * @return Observable put request
   */
  disableCalendarSync(options: { headers: HttpHeaders } | null): Observable<any> {
    return this.http.put(this.getBaseUrl() + '/calendar-sync/disable', null, options);
  }

  /**
   * Update settings to enable sync of COYO events with external calendar.
   *
   * @param options options request options forwarded to http request
   * @return Observable put request
   */
  enableCalendarSync(options: { headers: HttpHeaders } | null): Observable<any> {
    return this.http.put(this.getBaseUrl() + '/calendar-sync/enable', null, options);
  }
}
