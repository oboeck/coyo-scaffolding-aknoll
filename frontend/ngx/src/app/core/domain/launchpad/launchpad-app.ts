/**
 * Represents a launchpad app.
 */
export interface LaunchpadApp {
  name: string;
  icon: string;
  url: string;
}
