import {Sender} from './../sender/sender';

/**
 * Accumulated information of likes for a given likeable entity.
 */
export interface LikeInfo {
  count: number;
  latest: Sender[];
  likedBySender: boolean;
}
