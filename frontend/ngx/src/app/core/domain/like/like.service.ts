import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {DomainService} from '@domain/domain/domain.service';
import {CoyoConfig} from '@root/typings';
import {NG1_COYO_CONFIG} from '@upgrade/upgrade.module';
import * as _ from 'lodash';
import {BehaviorSubject, Observable, Subject, timer} from 'rxjs';
import {debounceTime, map, tap} from 'rxjs/operators';
import {Like} from './like';
import {LikeInfo} from './like-info';
import {LikeState} from './like-state';

interface LikeInfoRequest {
  senderId: string;
  targetType: string;
  requestSubject: Subject<string>;
  subscriptions: { [key: string]: BehaviorSubject<LikeState> };
}

/**
 * Service to retrieve and manage likes.
 */
@Injectable({
  providedIn: 'root'
})
export class LikeService extends DomainService<Like, Like> {

  static readonly INITIAL_STATE: LikeState = {
    isLoading: false,
    isLiked: false,
    total: [],
    totalCount: 0,
    others: [],
    othersCount: 0
  };

  /**
   * The number of milliseconds to throttle GET requests of this service.
   */
  static readonly THROTTLE: number = 50;

  /*
   * This array of possible info requests will not be cleared when the requests
   * are completed. They are reused when another request with the same key comes
   * in. We expect a rather small number of different throttled requests and thus
   * take the risk of a minimal memory leak here.
   */
  private subjects: { [key: string]: LikeInfoRequest } = {};

  constructor(http: HttpClient, urlService: UrlService, @Inject(NG1_COYO_CONFIG) coyoConfig: CoyoConfig) {
    super(http, urlService);
    const reloadTime = 1000 * 60 * coyoConfig.likeReloadIntervalMinutes;
    timer(reloadTime, reloadTime)
      .pipe(tap(() => this.cleanUp()))
      .subscribe(() => {
        this.updateLikes();
      });
  }

  /**
   * Likes the given target as the given sender.
   *
   * @param senderId the ID of the acting sender
   * @param targetId the ID of the target
   * @param targetType the type of the target
   * @return an `Observable` holding the the latest like information
   */
  like(senderId: string, targetId: string, targetType: string): Observable<LikeInfo> {
    const key = this.getKey(senderId, targetType);
    const subject = this.subjects[key];

    if (subject && subject.subscriptions && subject.subscriptions[targetId]) {
      const value = subject.subscriptions[targetId].getValue();
      subject.subscriptions[targetId].next({...value, isLoading: true});
    }

    return this.http.post<LikeInfo>(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`, null)
      .pipe(tap(likeInfo => {
        if (subject && subject.subscriptions && subject.subscriptions[targetId]) {
          subject.subscriptions[targetId].next(this.toLikeState(likeInfo, senderId));
        }
      }));
  }

  /**
   * Unlikes the given target as the given sender.
   *
   * @param senderId the ID of the acting sender
   * @param targetId the ID of the target
   * @param targetType the type of the target
   * @return an `Observable` holding the the latest like information
   */
  unlike(senderId: string, targetId: string, targetType: string): Observable<LikeInfo> {
    const key = this.getKey(senderId, targetType);
    const subject = this.subjects[key];

    if (subject && subject.subscriptions && subject.subscriptions[targetId]) {
      const value = subject.subscriptions[targetId].getValue();
      subject.subscriptions[targetId].next({...value, isLoading: true});
    }

    return this.http.delete<LikeInfo>(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`)
      .pipe(tap(likeInfo => {
        if (subject && subject.subscriptions && subject.subscriptions[targetId]) {
          subject.subscriptions[targetId].next(this.toLikeState(likeInfo, senderId));
        }
      }));
  }

  /**
   * Retrieves the like state for the given target as the given sender. Like information is
   * retrieved initially and periodically in a configurable interval. If the target was liked or unliked by the sender
   * the state change will be emitted immediately.
   *
   * You need to unsubscribe to prevent memory leaks.
   *
   * @param senderId the ID of the acting sender
   * @param targetId the ID of the target
   * @param targetType the type of the target
   * @param skipInitRequest flag for skipping the initial request which is done immediately
   * @return an `Observable` holding the the like state
   */
  getLikeTargetState$(senderId: string,
                      targetId: string,
                      targetType: string,
                      skipInitRequest: boolean = false): Observable<LikeState> {
    const key = this.getKey(senderId, targetType);
    const subject = this.subjects[key] || this.buildRequest(senderId, targetType);

    if (!subject.subscriptions[targetId]) {
      subject.subscriptions[targetId] = new BehaviorSubject({...LikeService.INITIAL_STATE, ...{isLoading: true}});
    }

    if (!skipInitRequest) {
      subject.requestSubject.next(targetId);
    } else {
      subject.subscriptions[targetId].next({
        ...subject.subscriptions[targetId].getValue(),
        ...{isLoading: false}
      });
    }

    return subject.subscriptions[targetId];
  }

  protected getBaseUrl(): string {
    return '/web/like-targets/{targetType}/{targetId}/likes';
  }

  private setup(request: LikeInfoRequest): void {
    const buffer: Set<string> = new Set();
    request.requestSubject
      .pipe(map(value => this.collect(buffer, value)))
      .pipe(debounceTime(LikeService.THROTTLE))
      .pipe(tap(() => buffer.clear()))
      .subscribe(ids => this.request(request.senderId, request.targetType, ids)
        .subscribe(
          likeInfos => this.sendResponses(request.senderId, request.targetType, likeInfos),
          () => this.revertLoading(request.senderId, request.targetType, ids)));
  }

  private revertLoading(senderId: string, targetType: string, ids: string[]): void {
    ids.forEach(targetId => {
      const key = this.getKey(senderId, targetType);
      const subject = this.subjects[key];
      if (subject && subject.subscriptions[targetId]) {
        const subscription = subject.subscriptions[targetId];
        subscription.next({
          ...subscription.getValue(),
          ...{isLoading: false}
        });
      }
    });
  }

  private sendResponses(senderId: string, targetType: string, likeInfos: { [key: string]: LikeInfo }): void {
    _.keys(likeInfos).forEach(targetId => {
      const key = this.getKey(senderId, targetType);
      const subject = this.subjects[key];
      if (subject && subject.subscriptions[targetId]) {
        subject.subscriptions[targetId].next(this.toLikeState(likeInfos[targetId], senderId));
      }
    });
  }

  private toLikeState(likeInfo: LikeInfo, senderId: string): LikeState {
    return {
      isLoading: false,
      isLiked: likeInfo.likedBySender,
      total: likeInfo.latest,
      totalCount: likeInfo.count,
      others: _.reject(likeInfo.latest, {id: senderId}),
      othersCount: likeInfo.count - (likeInfo.likedBySender ? 1 : 0)
    };
  }

  private buildRequest(senderId: string, targetType: string): LikeInfoRequest {
    const subject: LikeInfoRequest = {
      senderId,
      targetType,
      requestSubject: new Subject(),
      subscriptions: {}
    };
    const key = this.getKey(senderId, targetType);
    this.subjects[key] = subject;
    this.setup(subject);
    return subject;
  }

  private collect(set: Set<string>, value: string): string[] {
    set.add(value);
    return Array.from(set);
  }

  private request(senderId: string, targetType: string, targetIds: string[]): Observable<{ [key: string]: LikeInfo }> {
    return this.http.get<{ [key: string]: LikeInfo }>(`/web/like-targets/${targetType}`, {
      headers: {
        etagBulkId: 'ids'
      },
      params: {
        senderId,
        ids: targetIds
      }
    });
  }

  private getKey(senderId: string, targetType: string): string {
    return _.join([targetType, senderId], '-');
  }

  private cleanUp(): void {
    _.forEach(this.subjects, subject => {
      subject.subscriptions = _.pickBy(subject.subscriptions, subscription => subscription.observers.length);
    });
  }

  private updateLikes(): void {
    _.forEach(_.values(this.subjects), subject => {
      _.forEach(_.keys(subject.subscriptions), targetId => {
        subject.requestSubject.next(targetId);
      });
    });
  }
}
