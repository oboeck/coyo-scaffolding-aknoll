/**
 * An entity that can be likes by a sender.
 */
export interface Likeable {
  id: string;
  typeName: 'timeline-item' | 'blog-article' | 'wiki-article';
}
