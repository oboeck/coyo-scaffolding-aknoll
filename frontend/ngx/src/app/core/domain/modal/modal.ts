import {OnDestroy} from '@angular/core';
import {UIRouter} from '@uirouter/angular';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Subject, Subscription} from 'rxjs';
import {skip} from 'rxjs/operators';

/**
 * Base class for all Modal types.
 *
 * Implements a close method which emits a result value of type `T`.
 */
export class Modal<T> implements OnDestroy {

  onClose: Subject<T> = new Subject<T>();

  private stateChangeSubscription: Subscription;

  constructor(public modal: BsModalRef, uiRouter: UIRouter) {
    this.onClose = new Subject();
    // Need to skip the first because it's a shareReplay Observable which emits
    // the last transition change as first value
    this.stateChangeSubscription = uiRouter.globals.start$
      .pipe(skip(1))
      .subscribe(() => this.close());
  }

  /**
   * Closes the modal and emits the close value to the onClose subject.
   *
   * @param closeValue the value returned from the modal or `undefined`
   * if the modal does not return a value
   */
  close(closeValue?: T): void {
    this.onClose.next(closeValue);
    this.modal.hide();
    this.unsubscribe();
  }

  ngOnDestroy(): void {
    this.onClose.complete();
    this.unsubscribe();
  }

  private unsubscribe(): void {
    if (this.stateChangeSubscription && !this.stateChangeSubscription.closed) {
      this.stateChangeSubscription.unsubscribe();
    }
  }
}
