import {Sender} from '@domain/sender/sender';
import {Attachment} from 'app/core/domain/attachment/attachment';

/**
 * Contains all relevant information of a preview of a file inside a timeline post.
 * It extends the Attachment interface
 */
export interface FilePreview extends Attachment {
  attachment: boolean;
  groupId: string;
  senderId?: string;
  previewAvailable?: boolean;
  author?: Sender;
  previewUrl?: string;
  showSize?: boolean;
  externalFileUrl?: string;
}
