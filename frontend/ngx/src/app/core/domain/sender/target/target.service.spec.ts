import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {User} from '@domain/user/user';
import {Ng1TargetService} from '@root/typings';
import {NG1_TARGET_SERVICE} from '@upgrade/upgrade.module';
import {NgxPermissionsService} from 'ngx-permissions';
import {of} from 'rxjs';
import {Target} from '../target';
import {TargetService} from './target.service';

describe('TargetService', () => {
  let authService: jasmine.SpyObj<AuthService>;
  let permissionsService: jasmine.SpyObj<NgxPermissionsService>;
  let ng1TargetService: jasmine.SpyObj<Ng1TargetService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TargetService, {
        provide: NG1_TARGET_SERVICE,
        useValue: jasmine.createSpyObj('targetService', ['getLink'])
      }, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['getUser'])
      }, {
        provide: NgxPermissionsService,
        useValue: jasmine.createSpyObj('NgxPermissionsService', ['hasPermission'])
      }]
    });

    authService = TestBed.get(AuthService);
    permissionsService = TestBed.get(NgxPermissionsService);
    ng1TargetService = TestBed.get(NG1_TARGET_SERVICE);
  });

  it('should get link from ng1 service', inject([TargetService], (service: TargetService) => {
    // given
    const target = {} as Target;
    const link = 'http://some.link/test';
    ng1TargetService.getLink.and.returnValue(link);

    // when
    const result = service.getLinkTo(target);

    // then
    expect(result).toBe(link);
  }));

  it('should determine if user can access link to another user',
    inject([TargetService], (service: TargetService) => {
      // given
      const user = {id: ''} as User;
      const target: Target = {
        name: 'user',
        params: {id: ''}
      } as Target;

      authService.getUser.and.returnValue(of(user));
      permissionsService.hasPermission.and.returnValue(of(true));

      // when
      const result = service.canLinkTo(target);

      // then
      result.subscribe(value => {
        expect(permissionsService.hasPermission).toHaveBeenCalledWith('ACCESS_OWN_USER_PROFILE');
        expect(value).toBe(true);
      });
    })
  );

  it('should determine if user can access link to an event',
    inject([TargetService], (service: TargetService) => {
      // given
      const user = {id: ''} as User;
      const target: Target = {
        name: 'event',
        params: {id: ''}
      } as Target;

      authService.getUser.and.returnValue(of(user));
      permissionsService.hasPermission.and.returnValue(of(true));

      // when
      const result = service.canLinkTo(target);

      // then
      result.subscribe(value => {
        expect(permissionsService.hasPermission).toHaveBeenCalledWith('ACCESS_EVENTS');
        expect(value).toBe(true);
      });
    })
  );

  it('should get a target link for the current user',
    fakeAsync(inject([TargetService], (service: TargetService) => {
      // given
      const target: Target = {} as Target;
      const user = {id: ''} as User;
      const targetLink = 'test-link';
      authService.getUser.and.returnValue(of(user));
      permissionsService.hasPermission.and.returnValue(of(true));
      ng1TargetService.getLink.and.returnValue(targetLink);

      // when
      const result = service.getLinkForCurrentUser(target);

      // then
      result.subscribe(link => {
        expect(link).toBe(targetLink);
      });
      tick();
    }))
  );

  it('should get no target link for the current user that do not have permission to the target',
    fakeAsync(inject([TargetService], (service: TargetService) => {
      // given
      const target: Target = {} as Target;
      const user = {id: ''} as User;
      authService.getUser.and.returnValue(of(user));
      permissionsService.hasPermission.and.returnValue(of(false));

      // when
      const result = service.getLinkForCurrentUser(target);

      // then
      result.subscribe(link => {
        expect(link).toBeUndefined();
        expect(ng1TargetService.getLink).not.toHaveBeenCalled();
      });
      tick();
    }))
  );
});
