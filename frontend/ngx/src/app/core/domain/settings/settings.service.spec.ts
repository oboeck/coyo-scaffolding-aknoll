import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {getTestBed, inject, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {Ng1AuthService} from '@root/typings';
import {of} from 'rxjs';
import {SettingsService} from './settings.service';

describe('SettingsService', () => {
  let injector: TestBed;
  let authService: jasmine.SpyObj<Ng1AuthService>;
  let httpMock: HttpTestingController;

  const response = {
    key: 'value'
  };
  const mapResponse = new Map<string, string>();
  mapResponse.set('key', 'value');

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SettingsService, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('authService', ['isAuthenticated'])
      }]
    });

    injector = getTestBed();
    authService = injector.get(AuthService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    // verify that no outstanding requests are present after finishing a test
    httpMock.verify();
  });

  it('should be retrieved from public API', inject([SettingsService], (service: SettingsService) => {
    authService.isAuthenticated.and.returnValue(false);
    service.retrieve().subscribe(result => {
      expect(result).toEqual(mapResponse);
      expect(service['cache$']).toBeDefined();
    });
    const req = httpMock.expectOne(request => request.url === '/web/settings/public');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(response);
  }));

  it('should be retrieved from secured API', inject([SettingsService], (service: SettingsService) => {
    authService.isAuthenticated.and.returnValue(true);
    service.retrieve().subscribe(result => {
      expect(result).toEqual(mapResponse);
    });
    const req = httpMock.expectOne(request => request.url === '/web/settings');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(response);
  }));

  it('should be retrieved from cache', inject([SettingsService], (service: SettingsService) => {
    authService.isAuthenticated.and.returnValue(false);
    service.retrieve();
    service.retrieve().subscribe(result => {
      expect(result).toEqual(mapResponse);
      expect(service['cache$']).toBeDefined();
    });
    const req = httpMock.expectOne(request => request.url === '/web/settings/public');
    req.flush(response);
  }));

  it('should invalidate cache and retrieve from server', inject([SettingsService], (service: SettingsService) => {
    authService.isAuthenticated.and.returnValue(false);
    service['cache$'] = of(mapResponse);
    service.retrieveAndForceRefresh().subscribe(result => {
      expect(result).toEqual(mapResponse);
      expect(service['cache$']).toBeDefined();
    });
    const req = httpMock.expectOne(request => request.url === '/web/settings/public');
    expect(req.request.method).toEqual('GET');

    req.flush(response);
  }));

  it('should retrieve single key', inject([SettingsService], (service: SettingsService) => {
    authService.isAuthenticated.and.returnValue(false);
    service.retrieveByKey('key').subscribe(result => {
      expect(result).toEqual(mapResponse.get('key'));
    });
    const req = httpMock.expectOne(request => request.url === '/web/settings/public');
    req.flush(response);
  }));

  it('should retrieve single key from cache', inject([SettingsService], (service: SettingsService) => {
    authService.isAuthenticated.and.returnValue(false);
    service.retrieveByKey('key');
    service.retrieveByKey('key').subscribe(result => {
      expect(result).toEqual(mapResponse.get('key'));
    });
    const req = httpMock.expectOne(request => request.url === '/web/settings/public');
    req.flush(response);
  }));

  it('should retrieve single key and invalidate cache', inject([SettingsService], (service: SettingsService) => {
    authService.isAuthenticated.and.returnValue(false);
    service['cache$'] = of(mapResponse);
    service.retrieveByKeyAndForceRefresh('key').subscribe(result => {
      expect(result).toEqual(mapResponse.get('key'));
    });
    const req = httpMock.expectOne(request => request.url === '/web/settings/public');
    req.flush(response);
  }));
});
