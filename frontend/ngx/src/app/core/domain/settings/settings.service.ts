import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AuthService} from '@core/auth/auth.service';
import * as _ from 'lodash';
import {Observable} from 'rxjs';
import {first, map, shareReplay} from 'rxjs/operators';

const API_ENDPOINT = '/web/settings';
const API_ENDPOINT_PUBLIC = '/web/settings/public';
const CACHE_SIZE = 1;

/**
 * Provides COYO's general settings.
 */
@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  private cache$: Observable<Map<string, string>>;

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  /**
   * Retrieves and caches the general settings from the backend. If the current user is not authenticated,
   * only public settings will be retrieved. Upon login, the cached settings are cleared.
   *
   * @return a promise of the settings map
   */
  retrieve(): Observable<Map<string, any>> {
    if (!this.cache$) {
      this.cache$ = this.requestSettings().pipe(shareReplay(CACHE_SIZE));
    }
    return this.cache$.pipe(first());
  }

  /**
   * Clears the cache and retrieves and caches the general settings from the backend.
   * If the current user is not authenticated, only public settings will be retrieved.
   * Upon login, the cached settings are cleared.
   *
   * @return a promise of the settings map
   */
  retrieveAndForceRefresh(): Observable<Map<string, string>> {
    this.invalidateCache();
    return this.retrieve();
  }

  /**
   * Retrieves a single setting.
   *
   * @param key the setting's key
   * @returns a promise that resolves to the setting's value
   */
  retrieveByKey(key: string): Observable<string> {
    return this.retrieve().pipe(map(value => value.get(key)));
  }

  /**
   * Clears the cache and retrieves a single setting by a key.
   *
   * @param key the setting's key
   * @returns a promise that resolves to the setting's value
   */
  retrieveByKeyAndForceRefresh(key: string): Observable<string> {
    this.invalidateCache();
    return this.retrieveByKey(key);
  }

  /**
   * Allows manually clearing the settings cache.
   */
  invalidateCache(): void {
    this.cache$ = null;
  }

  /**
   * Updates the general settings in the database.
   * @param partialSettings with general settings to store.
   * @return the new state of the general settings.
   */
  update(partialSettings: Map<string, string>): Observable<Map<string, string>> {
    return this.http.put<{ [key: string]: string; }>(API_ENDPOINT, partialSettings)
      .pipe(map(result => new Map(_.toPairs(result))));
  }

  private requestSettings(): Observable<Map<string, string>> {
    const endpoint = this.authService.isAuthenticated() ? API_ENDPOINT : API_ENDPOINT_PUBLIC;
    return this.http.get<{ [key: string]: string; }>(endpoint)
      .pipe(map(result => new Map(_.toPairs(result))));
  }
}
