import {BaseModel} from '@domain/base-model/base-model';
import {Share} from '@domain/share/share';

/**
 * Entity that can be shared
 */
export interface Shareable extends BaseModel {
  itemId: string;
  typeName: 'timeline-item' | 'blog-article' | 'wiki-article';
  shares?: Share[];
  parentPublic?: boolean;
  itemType: string;
  _permissions?: {
    share?: boolean;
    sticky?: boolean;
  };
}
