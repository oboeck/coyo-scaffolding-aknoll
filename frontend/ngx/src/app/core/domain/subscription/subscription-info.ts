/**
 * Additional data on user subscriptions.
 */
export interface SubscriptionInfo {
  autoSubscribe: [string];
  favorite: [string];
}
