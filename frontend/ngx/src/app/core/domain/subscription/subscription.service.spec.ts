import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {discardPeriodicTasks, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {SocketService} from '@core/socket/socket.service';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {RichPage} from '@domain/pagination/richPage';
import {Search} from '@domain/pagination/search';
import {Sender} from '@domain/sender/sender';
import {SubscriptionInfo} from '@domain/subscription/subscription-info';
import {User} from '@domain/user/user';
import {of} from 'rxjs';
import {Subscription} from './subscription';
import {SubscriptionService} from './subscription.service';

describe('SubscriptionService', () => {
  let httpTestingController: HttpTestingController;
  let authService: jasmine.SpyObj<AuthService>;
  let socketService: jasmine.SpyObj<SocketService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SubscriptionService,
        {provide: AuthService, useValue: jasmine.createSpyObj('AuthService', ['getUser'])},
        {provide: SocketService, useValue: jasmine.createSpyObj('SocketService', ['listenTo$'])}
      ]
    });

    authService = TestBed.get(AuthService);
    socketService = TestBed.get(SocketService);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(() => {
    authService.getUser.and.returnValue(of({id: 'userId'}));
    socketService.listenTo$.and.returnValue(of());
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should subscribe', inject([SubscriptionService], (service: SubscriptionService) => {
    // given
    const senderId = 'senderId';
    const targetId = 'targetId';
    const targetType = 'targetType';

    // when
    const response = {} as Subscription;
    const result$ = service.subscribe(senderId, targetId, targetType);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/subscriptions');
    expect(req.request.method).toEqual('POST');
    expect(req.request.body.senderId).toEqual(senderId);
    expect(req.request.body.targetId).toEqual(targetId);
    expect(req.request.body.targetType).toEqual(targetType);

    // finally
    req.flush(response);
  }));

  it('should unsubscribe', inject([SubscriptionService], (service: SubscriptionService) => {
    // given
    const targetId = 'targetId';

    // when
    const response = {} as Subscription;
    const result$ = service.unsubscribe(targetId);

    // then
    result$.subscribe(data => expect(data).toBeUndefined());
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/subscriptions');
    expect(req.request.method).toEqual('DELETE');
    expect(req.request.params.get('targetId')).toEqual(targetId);

    // finally
    req.flush(response);
  }));

  it(`should immediately return an empty Observable when called without IDs`, fakeAsync(inject([SubscriptionService], (service: SubscriptionService) => {
    // when
    const result$ = service.getSubscriptionsById();

    // then
    result$.subscribe(data => expect(data).toBeUndefined());

    discardPeriodicTasks();
  })));

  it(`should get a single subscription by ID`, fakeAsync(inject([SubscriptionService], (service: SubscriptionService) => {
    // given
    const targetId = 'targetId';

    // when
    const response = [{targetId: targetId} as Subscription];
    const result$ = service.getSubscriptionsById(targetId);

    // then
    result$.subscribe(data => expect(data).toEqual(response[0]));
    tick(100);
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/subscriptions');
    expect(req.request.method).toEqual('GET');
    expect(req.request.params.get('targetIds')).toEqual(targetId);

    // finally
    req.flush(response);
    discardPeriodicTasks();
  })));

  it(`should get multiple subscriptions by ID`, fakeAsync(inject([SubscriptionService], (service: SubscriptionService) => {
    // given
    const targetId1 = 'targetId1';
    const targetId2 = 'targetId2';

    // when
    const response = [{targetId: targetId1} as Subscription, {targetId: targetId2} as Subscription];
    const result$ = service.getSubscriptionsById(targetId1, targetId2);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    tick(100);
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/subscriptions');
    expect(req.request.method).toEqual('GET');
    expect(req.request.params.getAll('targetIds')).toEqual([targetId1, targetId2]);

    // finally
    req.flush(response);
    discardPeriodicTasks();
  })));

  it(`should bundle multiple subscriptions by ID`, fakeAsync(inject([SubscriptionService], (service: SubscriptionService) => {
    // given
    const targetId1 = 'targetId1';
    const targetId2 = 'targetId2';

    // when
    const response = [{targetId: targetId1} as Subscription, {targetId: targetId2} as Subscription];
    const result1$ = service.getSubscriptionsById(targetId1);
    const result2$ = service.getSubscriptionsById(targetId2);

    // then
    result1$.subscribe(data => expect(data).toEqual(response[0]));
    result2$.subscribe(data => expect(data).toEqual(response[1]));
    tick(100);
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/subscriptions');
    expect(req.request.method).toEqual('GET');
    expect(req.request.params.getAll('targetIds')).toEqual([targetId1, targetId2]);

    // finally
    req.flush(response);
    discardPeriodicTasks();
  })));

  it(`should immediately return an empty Observable when called without types`, fakeAsync(inject([SubscriptionService], (service: SubscriptionService) => {
    // when
    const result$ = service.getSubscriptionsByType();

    // then
    const subscription = result$.subscribe(data => expect(data).toBeUndefined());
    tick();
    expect(subscription.closed).toBeTruthy();
    discardPeriodicTasks();
  })));

  it(`should get multiple subscriptions by target type`, fakeAsync(inject([SubscriptionService], (service: SubscriptionService) => {
    // given
    const targetType1 = 'targetType1';
    const targetType2 = 'targetType2';

    // when
    const response = [{targetType: targetType1} as Subscription, {targetType: targetType2} as Subscription];
    const result$ = service.getSubscriptionsByType(targetType1, targetType2);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    tick(100);
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/subscriptions');
    expect(req.request.method).toEqual('GET');
    expect(req.request.params.getAll('targetType')).toEqual([targetType1, targetType2]);

    // finally
    req.flush(response);
    discardPeriodicTasks();
  })));

  it(`should bundle multiple subscriptions by type`, fakeAsync(inject([SubscriptionService], (service: SubscriptionService) => {
    // given
    const targetType1 = 'targetType1';
    const targetType2 = 'targetType2';

    // when
    const response = [{targetType: targetType1} as Subscription, {targetType: targetType2} as Subscription];
    const result1$ = service.getSubscriptionsByType(targetType1);
    const result2$ = service.getSubscriptionsByType(targetType2);
    tick(100);

    // then
    result1$.subscribe(data => expect(data).toEqual([response[0]]));
    result2$.subscribe(data => expect(data).toEqual([response[1]]));
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/subscriptions');
    expect(req.request.method).toEqual('GET');
    expect(req.request.params.getAll('targetType')).toEqual([targetType1, targetType2]);

    // finally
    req.flush(response);
    discardPeriodicTasks();
  })));

  it('should get subscribed senders', inject([SubscriptionService], (service: SubscriptionService) => {
    // given
    const search = new Search('query');
    const pageable = new Pageable(0, 8);

    // when
    const response = {} as RichPage<Sender, SubscriptionInfo>;
    const result$ = service.getSubscribedSenders(search, pageable);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/subscriptions/senders');
    expect(req.request.method).toEqual('GET');
    expect(req.request.params.get('term')).toEqual('query');
    expect(req.request.params.get('_page')).toEqual('0');
    expect(req.request.params.get('_pageSize')).toEqual('8');

    // finally
    req.flush(response);
  }));

  it('should get subscribed users', inject([SubscriptionService], (service: SubscriptionService) => {
    // given
    const pageable = new Pageable(0, 8);

    // when
    const response = {} as Page<User>;
    const result$ = service.getSubscribedUsers(pageable);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/subscriptions/users');
    expect(req.request.method).toEqual('GET');
    expect(req.request.params.get('_page')).toEqual('0');
    expect(req.request.params.get('_pageSize')).toEqual('8');

    // finally
    req.flush(response);
  }));
});
