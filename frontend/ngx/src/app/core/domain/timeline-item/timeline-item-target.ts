import {Likeable} from '@domain/like/likeable';
import {Shareable} from '@domain/share/shareable';
import {Subscribable} from '@domain/subscription/subscribable';

/**
 * Target information for timeline item
 */
export interface TimelineItemTarget extends Shareable, Likeable, Subscribable {
  _permissions: {
    like?: boolean;
    share?: boolean;
    sticky?: boolean;
  };
  socialPermissions: {
    likesShown: boolean;
    commentsShown: boolean;
    commentsAndLikesNotAllowed: boolean;
  };
}
