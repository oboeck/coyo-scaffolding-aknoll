/**
 * The timeline item type.
 */

export type TimelineItemType =  'activity' | 'blog' | 'event' | 'page' | 'post' | 'wiki' | 'workspace';
