import {Attachment} from '@domain/attachment/attachment';
import {BaseModel} from '@domain/base-model/base-model';
import {Likeable} from '@domain/like/likeable';
import {LinkPreview} from '@domain/preview/link-preview';
import {VideoPreview} from '@domain/preview/video-preview';
import {Sender} from '@domain/sender/sender';
import {Share} from '@domain/share/share';
import {TimelineItemData} from '@domain/timeline-item/timeline-item-data';
import {TimelineItemType} from '@domain/timeline-item/timeline-item-type';

/**
 * Model for timeline items
 */
export interface TimelineItem extends BaseModel, Likeable {
  author: Sender;
  originalAuthorId: string;
  data: TimelineItemData;
  stickyExpiry: Date;
  typeName: 'timeline-item';
  itemType: TimelineItemType;
  shares: Share[];
  recipients: Sender[];
  parentPublic: boolean;
  restricted: boolean;
  unread: boolean;
  isNew: boolean;
  linkPreviews: LinkPreview[];
  videoPreviews: VideoPreview[];
  attachments: Attachment[];
  relevantShare: Share;
  _permissions?: {
    accessoriginalauthor?: boolean;
    actAsSender?: boolean;
    comment?: boolean;
    delete?: boolean;
    edit?: boolean;
    like?: boolean;
    share?: boolean;
    sticky?: boolean;
  };
  socialPermissions?: {
    commentsShown: boolean;
    likesShown: boolean;
    commentsAndLikesNotAllowed: boolean;
  };
}
