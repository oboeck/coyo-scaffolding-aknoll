import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {SocketService} from '@core/socket/socket.service';
import {DomainService} from '@domain/domain/domain.service';
import * as _ from 'lodash';
import {concat, Observable, Subject} from 'rxjs';
import {bufferTime, filter, first, map, mergeMap, pluck, share} from 'rxjs/operators';
import {PresenceStatus} from './presence-status';
import {User} from './user';

/**
 * Service to retrieve and manage users.
 */
@Injectable({
  providedIn: 'root'
})
export class UserService extends DomainService<User, User> {

  readonly requestPresenceStatusSubject: Subject<User> = new Subject<User>();
  readonly bulkedPresenceStatusRequest: Observable<{[id: string]: PresenceStatus}> = this.requestPresenceStatusSubject
    .pipe(bufferTime(150), filter(users => users && users.length > 0))
    .pipe(map(users => _.uniqBy(users, 'id')))
    .pipe(mergeMap(users => this.http.get<{[id: string]: PresenceStatus}>('/web/users/presence-status', {
        params: {
          userIds: users.map(usr => usr.id).join(',')
        }
      })), share());

  constructor(
    @Inject(HttpClient) protected http: HttpClient,
    @Inject(UrlService) protected urlService: UrlService,
    private socketService: SocketService) {
    super(http, urlService);
  }

  protected getBaseUrl(): string {
    return '/web/users';
  }

  /**
   * Retrieves the current user online count.
   *
   * @returns the current user online count
   */
  getUserOnlineCount(): Observable<number> {
    return this.http.get<{ count: number }>(this.getBaseUrl() + '/online/count')
      .pipe(map(result => Math.max(1, result.count)));
  }

  /**
   * Retrieves the presence status of a user. Optionally, you may provide a $scope, which will result in an ongoing
   * subscription to changes of the presence status and additional calls to the provided callback method. We need
   * a $scope so that we can terminate the subscription when the $scope is destroyed.
   * Internally, the requests are collected for a digest cycle and then a bulk request is sent to the backend.
   *
   * @param user The user to get the presence status for
   * @returns an observable
   */
  getPresenceStatus$(user: User): Observable<PresenceStatus> {
    const destination = '/topic/user.' + user.id + '.presenceStatusChanged';
    setTimeout(() => {
      // emit user after returning the observable so we can subscribe before the value is emitted
      this.requestPresenceStatusSubject.next(user);
    });
    const initialStatus$ = this.bulkedPresenceStatusRequest.pipe(filter(result => result[user.id] !== undefined), pluck(user.id), first());
    const changedStatus$ = this.socketService.listenTo$(destination).pipe(map(result => result.content));
    return concat(initialStatus$, changedStatus$);
  }
}
