import {HttpErrorResponse} from '@angular/common/http';
import {inject, TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {NG1_COYO_CONFIG, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import * as _ from 'lodash';
import {Observable, of} from 'rxjs';
import {ErrorService} from './error.service';

describe('ErrorService', () => {
  const translateServiceMock: jasmine.SpyObj<TranslateService> = jasmine.createSpyObj('translateService', ['get']);
  const stateMock: jasmine.SpyObj<IStateService> = jasmine.createSpyObj('$state', ['go']);
  const contentTypes: object = {
    'default': {icon: 'circle-o', color: '#999', label: '-', plural: '-'},
    'workspace': {icon: 'apps', color: '#FFA800', label: 'ENTITY_TYPE.WORKSPACE', plural: 'ENTITY_TYPE.WORKSPACES'},
    'page': {icon: 'layers', color: '#C16AA5', label: 'ENTITY_TYPE.PAGE', plural: 'ENTITY_TYPE.PAGES'},
    'user': {icon: 'account', color: '#444', label: 'ENTITY_TYPE.USER', plural: 'ENTITY_TYPE.USERS'},
    'file': {icon: 'file', color: '#43b19e', label: 'ENTITY_TYPE.FILE', plural: 'ENTITY_TYPE.FILES'},
    'timeline-item': {icon: 'comment-text', color: '#e02a6f', label: 'ENTITY_TYPE.TIMELINE_ITEM', plural: 'ENTITY_TYPE.TIMELINE_ITEMS'},
    'timeline-share': {label: 'ENTITY_TYPE.TIMELINE_SHARE'},
    'app': {icon: 'puzzle-piece', label: 'ENTITY_TYPE.APP', color: '#317DC1'},
    'event': {icon: 'calendar', label: 'ENTITY_TYPE.EVENT', plural: 'ENTITY_TYPE.EVENTS', color: '#990066'},
    'message': {icon: 'comments', label: 'ENTITY_TYPE.MESSAGE', color: '#1ec3cd'},
    'message-channel': {icon: 'comments', label: 'ENTITY_TYPE.MESSAGE_CHANNEL', color: '#1ec3cd'},
    'comment': {label: 'ENTITY_TYPE.COMMENT'},
    'email-activation': {label: 'ENTITY_TYPE.EMAIL_ACTIVATION'},
    'file-library': {label: 'ENTITY_TYPE.FILE_LIBRARY'},
    'landing-page': {label: 'ENTITY_TYPE.LANDING_PAGE', plural: 'ENTITY_TYPE.LANDING_PAGES', icon: 'globe', color: '#317DC1'},
    'language': {label: 'ENTITY_TYPE.LANGUAGE'},
    'like': {label: 'ENTITY_TYPE.LIKE'},
    'role': {label: 'ENTITY_TYPE.ROLE'},
    'subscription': {label: 'ENTITY_TYPE.SUBSCRIPTION'},
    'user-push-device': {label: 'ENTITY_TYPE.USER_PUSH_DEVICE'},
    'widget': {label: 'ENTITY_TYPE.WIDGET'}
  };
  const coyoConfig: object = {entityTypes: contentTypes};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ErrorService,
        {provide: TranslateService, useValue: translateServiceMock},
        {provide: NG1_STATE_SERVICE, useValue: stateMock},
        {provide: NG1_COYO_CONFIG, useValue: coyoConfig}
      ]
    });

    translateServiceMock.get.and.callFake( (key: string, interpolateParams?: object) => {
      if (interpolateParams) {
        const myString: string = JSON.stringify(interpolateParams);
        const result: string = key + ':' + myString;
        return of(result);
      } else {
        return of(key);
      }
    });

  });

  it('should be created', inject([ErrorService], (errorService: ErrorService) => {
    expect(errorService).toBeTruthy();
  }));

  describe('Service: errorService', () => {

    it('should navigate to error state', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const message = 'error message';
      const status = 500;

      // when
      errorService.showErrorPage(message, status, undefined);

      // then
      expect(stateMock.go).toHaveBeenCalledWith('error',
        {message: message, status: status, buttons: undefined},
        {location: false, inherit: false});
    }));
  });

  describe('get message from response', () => {

    it('should handle unauthorized', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({status: 401});

      // when
      const result: Observable<string> = errorService.getMessage(errorResponse);
      result.subscribe((key: string) => {
        // then
        expect(key).toBe('ERRORS.UNAUTHORIZED');
      });
    }));

    it('should handle forbidden', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({status: 403});

      // when
      errorService.getMessage(errorResponse).subscribe(
        (key: string) => {
          // then
          expect(key).toBe('ERRORS.FORBIDDEN');
        });
    }));

    it('should handle bad geateway', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({status: 503});

      // when
      errorService.getMessage(errorResponse).subscribe(
        (key: string) => {
          // then
          expect(key).toBe('ERRORS.SERVER_UNAVAILABLE');
        });
    }));

    it('should handle server unavailable', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({status: -1});

      // when
      errorService.getMessage(errorResponse).subscribe(
        (key: string) => {
          // then
          expect(key).toBe('ERRORS.SERVER_UNAVAILABLE');
        });
    }));

    it('should handle unknown', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({});

      // when
      errorService.getMessage(errorResponse).subscribe(
        (key: string) => {
          // then
          expect(key).toBe('ERRORS.STATUSCODE.UNKNOWN');
        });
    }));

    it('should handle entity not found for type', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({});
      _.set(errorResponse, 'error.errorStatus', 'NOT_FOUND');
      _.set(errorResponse, 'error.context.entityType', 'page');

      // when
      errorService.getMessage(errorResponse).subscribe(
        (key: string) => {
          // then
          expect(key).toBe('ERRORS.STATUSCODE.NOT_FOUND:{"entityType":"ENTITY_TYPE.PAGE"}');
        });
    }));

    it('should handle entity not found without type', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({});
      _.set(errorResponse, 'error.errorStatus', 'NOT_FOUND');

      // when
      errorService.getMessage(errorResponse).subscribe(
        (key: string) => {
          // then
          expect(key).toBe('ERRORS.STATUSCODE.NOT_FOUND.DEFAULT');
        });
    }));

    it('should handle entity deleted for type', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({});
      _.set(errorResponse, 'error.errorStatus', 'DELETED');
      _.set(errorResponse, 'error.context.entityType', 'page');

      // when
      errorService.getMessage(errorResponse).subscribe(
        (key: string) => {
          // then
          expect(key).toBe('ERRORS.STATUSCODE.DELETED:{"entityType":"ENTITY_TYPE.PAGE"}');
        });
    }));

    it('should handle entity deleted without type', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({});
      _.set(errorResponse, 'error.errorStatus', 'DELETED');

      // when
      errorService.getMessage(errorResponse).subscribe(
        (key: string) => {

          // then
          expect(key).toBe('ERRORS.STATUSCODE.DELETED.DEFAULT');
        });
    }));

    it('should handle other error code', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({});
      _.set(errorResponse, 'error.errorStatus', 'ERROR');
      _.set(errorResponse, 'error.context', {test: 'test'});

      // when
      errorService.getMessage(errorResponse).subscribe(
        (key: string) => {
          // then
          expect(key).toBe('ERRORS.STATUSCODE.ERROR:{"test":"test"}');
        });
    }));
  });

  describe('get validation errors from error response', () => {
    it('should return empty object if response contains no field errors', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const errorResponse: HttpErrorResponse = new HttpErrorResponse({});

      // when
      const result: object = errorService.getValidationErrors(errorResponse);

      // then
      expect(result).toEqual({});
    }));

    it('should transform field error list to object tree', inject([ErrorService], (errorService: ErrorService) => {
      // given
      const fieldErrors: {}[] = [
        {key: 'key1', code: 'code1'},
        {key: 'key2', code: 'code2'}
      ];

      const errorResponse: HttpErrorResponse = new HttpErrorResponse({});
      _.set(errorResponse, 'error.fieldErrors', fieldErrors);

      // when
      const result: object = errorService.getValidationErrors(errorResponse);

      // then
      expect(result).toEqual({key1: {code1: true}, key2: {code2: true}});
    }));
  });
});
