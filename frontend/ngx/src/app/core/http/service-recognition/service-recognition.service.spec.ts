import {TestBed} from '@angular/core/testing';

import {ServiceRecognitionService} from './service-recognition.service';

describe('ServiceRecognitionService', () => {
  let service: ServiceRecognitionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(ServiceRecognitionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should recognize translation service urls', () => {
    // given
    const path = '/web/translation/test';

    // when
    const result = service.getTargetService(path);

    // then
    expect(result).toBeTruthy();
  });

  it('should not recognize urls to backend service', () => {
    // given
    const path = '/web/translations/test';

    // when
    const result = service.getTargetService(path);

    // then
    expect(result).toBeNull();
  });

  it('should not recognize urls containing the path without slash before web', () => {
    // given
    const path = '/world-wide-web/translations/test';

    // when
    const result = service.getTargetService(path);

    // then
    expect(result).toBeNull();
  });

  it('should recognize translation service urls preceded by backend url', () => {
    // given
    const path = 'http://test.com/web/translation/test';

    // when
    const result = service.getTargetService(path);

    // then
    expect(result).toBeTruthy();
  });

  it('should detect a disabled service', () => {
    // when
    const result = service.isServiceDisabled('translation');

    // then
    expect(result).toBeTruthy();
  });

  it('should detect an enabled service', () => {
    // when
    const result = service.isServiceDisabled('not-translation');

    // then
    expect(result).toBeFalse();
  });
});
