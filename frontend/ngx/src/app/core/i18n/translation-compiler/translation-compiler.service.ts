import {DOCUMENT} from '@angular/common';
import {Inject, Injectable, Optional} from '@angular/core';
import * as _ from 'lodash';
import {NGXLogger} from 'ngx-logger';
import {
  MESSAGE_FORMAT_CONFIG,
  MessageFormatConfig,
  TranslateMessageFormatCompiler
} from 'ngx-translate-messageformat-compiler';

/**
 * This translation compiler uses jQuery to sanitize all of its output. Unfortunately, efforts to use Angular's
 * `DomSanitizer` did not work out, since the sanitizer will convert umlauts to HTML escaped entities.
 */
@Injectable({
  providedIn: 'root'
})
export class TranslationCompilerService extends TranslateMessageFormatCompiler {

  constructor(private log: NGXLogger,
              @Inject(DOCUMENT) private document: Document,
              @Inject(MESSAGE_FORMAT_CONFIG) @Optional() config?: MessageFormatConfig) {
    super(config);
    // hack for languages which do not have 'one' as pluralization parameter (for example chinese) but still uses
    // english fallback message keys
    if (this['messageFormat'] && typeof this['messageFormat'].disablePluralKeyChecks === 'function') {
      this['messageFormat'].disablePluralKeyChecks();
    }
  }

  /**
   * Compiles a single translation in a single language.
   *
   * @param value the translated value
   * @param lang the language
   * @return the compiled value
   */
  compile(value: string, lang: string): (params: any) => string {
    try {
      return params => this.resolve(super.compile(value, lang)(params));
    } catch (e) {
      this.log.error(`Could not compile translations for language '${lang}'`, {...e, ...{value}});
      return () => value;
    }
  }

  /**
   * Compiles a set of translations.
   *
   * @param translations the translations object
   * @param lang the language
   * @return the compiled values
   */
  compileTranslations(translations: any, lang: string): any {
    try {
      const compiledTranslations = super.compileTranslations(translations, lang);
      return _.mapValues(compiledTranslations, fn => (params: any) => this.resolve(fn(params)));
    } catch (e) {
      const {success, error} = this.locateError(translations, lang);
      this.log.error(`Could not compile translations for language '${lang}'`, error);
      return success;
    }
  }

  private resolve(result: any): string {
    if (result) {
      const elem = this.document.createElement('div');
      elem.innerHTML = result;
      return elem.textContent;
    }
    return result;
  }

  private locateError(translations: any, lang: string): {success: any, error: any} {
    let success = {};
    const error = {};
    _.forEach(translations, (value, key) => {
      try {
        const translation = {};
        translation[key] = value;
        success = {...success, ...super.compileTranslations(translation, lang)};
      } catch (e) {
        error[key] = {...e, ...{value}};
      }
    });
    return {success, error};
  }
}
