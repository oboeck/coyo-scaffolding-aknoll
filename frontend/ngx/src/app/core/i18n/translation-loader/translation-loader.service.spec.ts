import {inject, TestBed} from '@angular/core/testing';
import {Ng1TranslationRegistry} from '@root/typings';
import {NG1_COYO_TRANSLATION_LOADER, NG1_TRANSLATION_REGISTRY} from '@upgrade/upgrade.module';
import {Messages} from '../messages';
import {TranslationLoaderService} from './translation-loader.service';

describe('TranslationLoaderService', () => {
  let translationLoaderService: TranslationLoaderService;
  let messages: Messages[];
  let coyoTranslationLoader: jasmine.Spy;
  let translationRegistry: Ng1TranslationRegistry;
  const ng1Message = 'ng1 message';

  beforeEach(() => {
    messages = [{
      lang: 'en',
      messages: {
        'HELLO.WORLD': 'Hello World'
      }
    }, {
      lang: 'de',
      messages: {
        'HELLO.WORLD': 'Hallo Welt'
      }
    }, {
      lang: 'de',
      messages: {
        TEST: 'test'
      }
    }];

    TestBed.configureTestingModule({
      providers: [
        TranslationLoaderService,
        {provide: NG1_COYO_TRANSLATION_LOADER, useValue: jasmine.createSpy('coyoTranslationLoader')},
        {provide: 'messages', useValue: messages},
        {provide: NG1_TRANSLATION_REGISTRY, useValue: jasmine.createSpyObj('translationRegistry', ['addTranslations'])}
      ]
    });
    translationLoaderService = TestBed.get(TranslationLoaderService);
    translationRegistry = TestBed.get(NG1_TRANSLATION_REGISTRY);
    coyoTranslationLoader = TestBed.get(NG1_COYO_TRANSLATION_LOADER);
  });

  it('should be created', inject([TranslationLoaderService], (service: TranslationLoaderService) => {
    // then
    expect(service).toBeTruthy();
    expect(translationRegistry.addTranslations).toHaveBeenCalledWith(messages[0].lang, messages[0].messages);
    expect(translationRegistry.addTranslations).toHaveBeenCalledWith(messages[1].lang, messages[1].messages);
    expect(translationRegistry.addTranslations).toHaveBeenCalledWith(messages[2].lang, messages[2].messages);
  }));

  it('should get translations', (done: DoneFn) => {
    // given
    coyoTranslationLoader.and.returnValue(Promise.resolve({TEST: ng1Message}));

    // when
    const result = translationLoaderService.getTranslation('de');

    // then
    result.subscribe(data => {
      expect(data).toBeTruthy();
      expect(data['HELLO.WORLD']).toBe('Hallo Welt');
      expect(data['TEST']).toBe(ng1Message);
      done();
    });
  });
});
