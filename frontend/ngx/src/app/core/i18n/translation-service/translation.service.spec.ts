import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {NG1_LOCAL_STORAGE} from '@upgrade/upgrade.module';
import {EMPTY, of, throwError} from 'rxjs';
import {TranslationService} from './translation.service';

describe('TranslationService', () => {
  let translationService: TranslationService;
  let translateServiceMock: jasmine.SpyObj<TranslateService>;
  let authServiceMock: jasmine.SpyObj<AuthService>;

  const defaultLanguage = 'en';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TranslationService, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('TranslateService', ['setDefaultLang', 'use'])
      }, {
        provide: NG1_LOCAL_STORAGE,
        useValue: {userLanguage: defaultLanguage}
      }, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['isAuthenticated$', 'getUser'])
      }]
    });

    translationService = TestBed.get(TranslationService);
    translateServiceMock = TestBed.get(TranslateService);
    authServiceMock = TestBed.get(AuthService);
  });

  it('should be created', inject([TranslationService], (service: TranslationService) => {
    expect(service).toBeTruthy();
    expect(translateServiceMock.setDefaultLang).toHaveBeenCalledWith(defaultLanguage);
  }));

  it('should set the interface language', () => {
    // given
    translateServiceMock.use.and.returnValue(EMPTY);

    // when
    translationService.setNgXInterfaceLanguage('en');

    // then
    expect(translateServiceMock.use).toHaveBeenCalledWith('en');
  });

  it('should update the user language on auth change', fakeAsync(() => {
    // given
    const observable = of(true);
    authServiceMock.isAuthenticated$.and.returnValue(observable);
    authServiceMock.getUser.and.returnValue(of({language: 'cs'}));
    translateServiceMock.use.and.returnValue(of({}));

    // when
    translationService.updateUserLanguageOnAuthChange();
    tick();

    // then
    expect(translateServiceMock.use).toHaveBeenCalledWith('cs');
  }));

  it('should update the user language to default when auth change and user endpoint returns error', fakeAsync(() => {
    // given
    authServiceMock.isAuthenticated$.and.returnValue(of(false));
    authServiceMock.getUser.and.returnValue(throwError('not authenticated'));
    translateServiceMock.use.and.returnValue(of({}));

    // when
    translationService.updateUserLanguageOnAuthChange();
    tick();

    // then
    expect(translateServiceMock.use).toHaveBeenCalledWith(defaultLanguage);
  }));
});
