/**
 * Interface for storages used by the coyo application
 */
export interface CoyoStorage {

  /**
   * Sets a value to the local storage. Uses JSON.stringify internally for the value, so don't use circular objects.
   *
   * @param key The key where to store the value. Will be prefixed with 'LocalStorageService.PREFIX'
   * @param value The value to store. Will be stringified with 'JSON.stringify'
   */
  setValue(key: string, value: any): void;

  /**
   * Gets a value from the local storage.
   *
   * @param key The Key to read the value from
   * @returns The value
   */
  getValue<T>(key: string): T;

  /**
   * Deletes a local storage entry
   * @param key the key of the entry to delete
   */
  deleteEntry(key: string): void;
}
