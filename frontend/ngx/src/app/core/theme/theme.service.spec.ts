import {DOCUMENT} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {async, inject, TestBed} from '@angular/core/testing';
import {BrowserService} from '@core/browser/browser.service';
import {UrlService} from '@core/http/url/url.service';
import {Theme} from '@domain/theme/theme';
import {CSS_VARS} from '@root/injection-tokens';
import {Ng1MobileEventsService} from '@root/typings';
import {
  NG1_ADMIN_THEME_CONFIG,
  NG1_COYO_CONFIG,
  NG1_DEFAULT_THEME_COLORS,
  NG1_LOCAL_STORAGE, NG1_MOBILE_EVENTS_SERVICE
} from '@upgrade/upgrade.module';
import {ThemeService} from './theme.service';

describe('ThemeService', () => {
  let urlService: jasmine.SpyObj<UrlService>;
  let mobileEventsService: jasmine.SpyObj<Ng1MobileEventsService>;
  let browserService: jasmine.SpyObj<BrowserService>;
  let cssVars: jasmine.Spy;
  let document: Document;
  let appendedElements: string[] = [];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThemeService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['getBackendUrl', 'isBackendUrlSet', 'join'])
      }, {
        provide: NG1_MOBILE_EVENTS_SERVICE,
        useValue: jasmine.createSpyObj('mobileEventsService', ['propagate'])
      }, {
        provide: NG1_COYO_CONFIG,
        useValue: {
          version: {
            major: '90',
            minor: '0',
            patch: '0',
            qualifier: 'SNAPSHOT'
          },
          versionString: () => '90.0.0-SNAPSHOT'
        }
      }, {
        provide: NG1_ADMIN_THEME_CONFIG,
        useValue: {
          imageConfigs: []
        }
      }, {
        provide: NG1_DEFAULT_THEME_COLORS,
        useValue: {}
      }, {
        provide: NG1_LOCAL_STORAGE,
        useValue: {}
      }, {
        provide: BrowserService,
        useValue: jasmine.createSpyObj('browserService', ['isInternetExplorer', 'supportsCSSVariables'])
      }, {
        provide: CSS_VARS,
        useValue: jasmine.createSpy('cssVars')
      }],
      imports: [HttpClientModule,
        HttpClientTestingModule
      ]
    });

    urlService = TestBed.get(UrlService);
    mobileEventsService = TestBed.get(NG1_MOBILE_EVENTS_SERVICE);
    browserService = TestBed.get(BrowserService);
    cssVars = TestBed.get(CSS_VARS);
    document = TestBed.get(DOCUMENT);

    browserService.supportsCSSVariables.and.returnValue(true);
    browserService.isInternetExplorer.and.returnValue(false);
    urlService.isBackendUrlSet.and.returnValue(true);
    urlService.getBackendUrl.and.returnValue('');
    urlService.join.and.returnValue('/web/themes/public');
    cssVars.and.callFake((options: any) => {
      options.onComplete('', undefined, {});
    });
    appendedElements = [];
    spyOn(document.head, 'appendChild').and.callFake((elem: HTMLElement) => {
      appendedElements.push(elem.outerHTML);
    });
    spyOn(document.head, 'insertBefore').and.callFake((newElem: HTMLElement, parent: HTMLElement) => {
      appendedElements.push(newElem.outerHTML);
    });
    spyOn(document, 'getElementById').and.callFake((id: string) => {
      const themeHtml = '<style id="customTheme" type="text/css">body { border: 1px solid red;}</style>';
      const parser = new DOMParser();
      const html = parser.parseFromString(themeHtml, 'text/html');
      return html.body.firstChild;
    });
  });

  it('should apply theme', async(inject([ThemeService, HttpTestingController],
    (service: ThemeService, backend: HttpTestingController) => {
      // given
      const expectedElements = ['<style id="customVars" type="text/css">:root {--color-primary: red;}</style>',
        '<style id="customTheme" type="text/css">body { border: 1px solid red;}</style>',
        '<meta name="theme-color" content="red">', '<meta name="msapplication-navbutton-color" content="red">',
        '<meta name="apple-mobile-web-app-status-bar-style" content="red">',
        '<link rel="shortcut icon" type="image/x-icon" href="teest">'];

      const theme = {
        modified: new Date(),
        images: {
          'image-coyo-front': 'https://someurl.com',
          'height-image-front': '123px',
          'image-coyo-favicon': 'teest'
        },
        colors: {'color-primary': 'red'},
        css: 'body { border: 1px solid red;}'
      } as Theme;

      // when
      service.applyTheme().then(() => {
        expect(cssVars).toHaveBeenCalled();
        expect(mobileEventsService.propagate).toHaveBeenCalledWith('theme:applied', {...theme.colors, ...theme.images});
        expect(document.head.appendChild).toHaveBeenCalled();
        expect(document.head.insertBefore).toHaveBeenCalled();
        expectedElements.forEach(elem => expect(appendedElements).toContain(elem));
      });

      // then
      backend.expectOne('/web/themes/public').flush(theme, {status: 200, statusText: 'Ok'});
    })));

  it('should apply ponyfill', async(inject([ThemeService, HttpTestingController],
    (service: ThemeService, backend: HttpTestingController) => {
      // given
      const expectedElements = ['<style id="customTheme" type="text/css">body { border: 1px solid red;}</style>',
        '<meta name="theme-color" content="red">', '<meta name="msapplication-navbutton-color" content="red">',
        '<meta name="apple-mobile-web-app-status-bar-style" content="red">',
        '<style type="text/css" data-css-ponyfill="true"></style>'];

      browserService.supportsCSSVariables.and.returnValue(false);
      browserService.isInternetExplorer.and.returnValue(true);

      const theme = {
        modified: new Date(),
        images: {
          'image-coyo-front': 'https://someurl.com',
          'height-image-front': '123px',
          'image-coyo-favicon': 'teest'
        },
        colors: {'color-primary': 'red'},
        css: 'body { border: 1px solid red;}'
      } as Theme;

      // when
      service.applyTheme().then(() => {
        expect(cssVars).toHaveBeenCalled();
        expect(mobileEventsService.propagate).toHaveBeenCalledWith('theme:applied', {...theme.colors, ...theme.images});
        expect(document.head.appendChild).toHaveBeenCalled();
        expect(document.head.insertBefore).toHaveBeenCalled();
        expectedElements.forEach(elem => expect(appendedElements).toContain(elem));
      });

      // then
      backend.expectOne('/web/themes/public').flush(theme, {status: 200, statusText: 'Ok'});
    })));
});
