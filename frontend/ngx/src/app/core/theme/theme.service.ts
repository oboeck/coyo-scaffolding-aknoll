import {DOCUMENT} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {BrowserService} from '@core/browser/browser.service';
import {UrlService} from '@core/http/url/url.service';
import {CachedTheme} from '@domain/theme/cached-theme';
import {Theme} from '@domain/theme/theme';
import {CSS_VARS} from '@root/injection-tokens';
import {
  Ng1AdminThemeConfig,
  Ng1AdminThemeImageConfig,
  Ng1CoyoConfig,
  Ng1DefaultThemeColors,
  Ng1MobileEventsService
} from '@root/typings';
import {
  NG1_ADMIN_THEME_CONFIG,
  NG1_COYO_CONFIG,
  NG1_DEFAULT_THEME_COLORS,
  NG1_LOCAL_STORAGE, NG1_MOBILE_EVENTS_SERVICE
} from '@upgrade/upgrade.module';
import * as lodash from 'lodash';
import {ngStorage} from 'ngstorage';
import {Observable, of, Subject} from 'rxjs';
import {first} from 'rxjs/operators';

const _ = lodash;

/**
 * Service to manage the custom css theme
 */
@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  constructor(private http: HttpClient, private urlService: UrlService, private browserService: BrowserService,
              @Inject(DOCUMENT) private document: Document,
              @Inject(CSS_VARS) private cssVars: Function,
              @Inject(NG1_MOBILE_EVENTS_SERVICE) private mobileEventsService: Ng1MobileEventsService,
              @Inject(NG1_LOCAL_STORAGE) private localStorage: ngStorage.StorageService,
              @Inject(NG1_COYO_CONFIG) private coyoConfig: Ng1CoyoConfig,
              @Inject(NG1_ADMIN_THEME_CONFIG) private adminThemeConfig: Ng1AdminThemeConfig,
              @Inject(NG1_DEFAULT_THEME_COLORS) private defaultThemeColors: Ng1DefaultThemeColors) {
  }

  /**
   * Applies the appropriate theme for the user.
   *
   * @returns Promise that completes when the theme is applied
   */
  applyTheme(): Promise<void> {
    const themePromise: Promise<Theme> = this.urlService.isBackendUrlSet()
      ? this.getTheme().toPromise()
      : of({css: '', colors: {}, images: {}} as Theme).toPromise();

    return themePromise.then(theme => {
      this.removeTheme();
      this.applyCSS(theme.css);
      this.applyMetaTags(theme.colors);
      this.applyFavicon(theme.images);
      return this.getDefaultVariables()
        .then(defaultVariables => this.applyColorsAndImages(defaultVariables, theme));
    })
      .then(theme => this.mobileEventsService.propagate('theme:applied', {...theme.colors, ...theme.images}))
      .catch(() => {
        this.applyMetaTags();
        this.onCustomThemeChanged();
        return;
      })
      .then(() => this.onCustomThemeChanged());
  }

  private getDefaultVariables(): Promise<{ [key: string]: string }> {
    const subject = new Subject<{ [key: string]: string }>();
    const cachedTheme: CachedTheme = this.localStorage.theme;
    if (!this.isDefaultVariablesCacheUpToDate(cachedTheme)) {
      const promise = subject.asObservable().pipe(first()).toPromise();
      this.cssVars({
        onlyLegacy: false,
        updateDOM: false,
        reduceCalc: true,
        reduceCalcPrecision: 4,
        variables: {},
        onComplete: (css: string, node: HTMLElement, vars: { [key: string]: string }) => {
          const result = {};
          Object.keys(vars).forEach(key => {
            const newKey = key.startsWith('--') ? key.slice(2) : key;
            result[newKey] = vars[key];
          });
          this.localStorage.theme = {
            ...this.localStorage.theme,
            ...{defaultVariables: result, version: this.coyoConfig.versionString(), defaultVariablesOnly: true}
          };
          subject.next(result);
          subject.complete();
        }
      });
      return promise;
    }
    return of(cachedTheme.defaultVariables).toPromise();
  }

  private isDefaultVariablesCacheUpToDate(cachedTheme: CachedTheme): boolean {
    if (!cachedTheme) {
      return false;
    }
    return cachedTheme.defaultVariables && cachedTheme.version === this.coyoConfig.versionString();
  }

  private getTheme(): Observable<Theme> {
    const url = this.urlService.join(this.urlService.getBackendUrl(), '/web/themes/public');
    return this.http.get<Theme>(url);
  }

  private removeTheme(): void {
    const customTheme = this.document.getElementById('customTheme');
    if (customTheme) {
      customTheme.remove();
    }

    const customVars = this.document.getElementById('customVars');
    if (customVars) {
      customVars.remove();
    }
    this.removePonyfill();
  }

  private applyCSS(css?: string): void {
    this.document.head.appendChild(this.createStyleTag('customTheme', css, false));
  }

  private applyMetaTags(colors?: { [key: string]: string }): void {
    const primaryColor = _.get(colors, 'color-primary', this.defaultThemeColors['color-primary']);
    const metaTags = ['theme-color', 'msapplication-navbutton-color', 'apple-mobile-web-app-status-bar-style'];
    metaTags.forEach((metaTag: string) => {
      const metaNode = this.document.querySelector('meta[name=' + metaTag + ']');
      if (metaNode) {
        metaNode.remove();
      }
      this.document.head.appendChild(this.createMetaTag(metaTag, primaryColor));
    });
  }

  private applyFavicon(images: { [key: string]: string }): void {
    const icon = images['image-coyo-favicon'];
    if (icon && !this.browserService.isInternetExplorer()) { // IE can't handle it...
      const linkElement = this.document.querySelector('link[rel="shortcut icon"]');
      if (linkElement) {
        linkElement.remove();
      }
      const newElement = this.document.createElement('link');
      newElement.setAttribute('rel', 'shortcut icon');
      newElement.setAttribute('type', 'image/x-icon');
      newElement.setAttribute('href', icon.split('\'').join(''));
      this.document.head.appendChild(newElement);
    }
  }

  private applyColorsAndImages(defaultVariables: { [key: string]: string }, theme: Theme = {} as Theme): Promise<Theme> {
    const imagesMapped = theme.images ? this.createImageCssVars(theme.images) : {};
    const themeVariables = {...theme.colors, ...imagesMapped};
    const allVariables = {...defaultVariables, ...themeVariables};

    const css = _.map(allVariables, (color: string, name: string) => '--' + name + ': ' + color + ';').join(' ');
    const varsElement = this.createStyleTag('customVars', `:root {${css}}`, false);
    const customThemeElem = this.document.getElementById('customTheme') || null;
    this.document.head.insertBefore(varsElement, customThemeElem);

    const cachedTheme = this.localStorage.theme as CachedTheme;
    const defered = new Subject<Theme>();
    const promise = defered.asObservable().toPromise();  // must be declared here due to the synchronous css ponyfill callback

    if (!this.browserService.supportsCSSVariables() && this.isCachedThemeUpToDate(cachedTheme, theme)) {
      this.applyPonyfillFromCache(cachedTheme);
      this.applyPonyfillWatch();
      return of(theme).toPromise();
    } else if (!this.browserService.supportsCSSVariables()) {
      this.applyPonyfill(theme, defered, defaultVariables);
    }
    return this.browserService.supportsCSSVariables() ? of(theme).toPromise() : promise;
  }

  private applyPonyfillFromCache(cachedTheme: CachedTheme): void {
    this.removePonyfill();
    const newElement = this.createStyleTag('css-vars-ponyfill', cachedTheme.css);
    this.document.head.appendChild(newElement);
  }

  private applyPonyfill(theme: Theme, subject: Subject<Theme>, defaultVariables: { [key: string]: string }): void {
    this.cssVars({
      onlyLegacy: true,
      onlyVars: true,
      updateURLs: true,
      updateDOM: true,
      reduceCalc: true,
      reduceCalcPrecision: 4,
      onComplete: (css: string, node: any, vars: { [key: string]: string }) => {
        this.localStorage.theme = {
          ...this.localStorage.theme, ...{
            version: this.coyoConfig.versionString(),
            modified: theme.modified,
            defaultVariables: defaultVariables,
            defaultVariablesOnly: false,
            variables: vars,
            css: css
          }
        };
        this.removePonyfill();
        this.applyPonyfillWatch();
        setTimeout(() => {
          subject.next(theme);
          subject.complete();
        }, 250);
      }
    });
  }

  private applyPonyfillWatch(): void {
    this.cssVars({
      include: 'style:not([type="text/css"]),#customVars',
      exclude: 'style[type="text/css"],link[rel="stylesheet"]',
      updateDOM: false,
      reduceCalc: true,
      reduceCalcPrecision: 4,
      watch: true,
      variables: (this.localStorage.theme as CachedTheme).variables,
      onComplete: (css: string) => {
        this.document.head.appendChild(this.createStyleTag('', css));
      }
    });
  }

  private removePonyfill(): void {
    Array.from(this.document
      .querySelectorAll('style[data-css-ponyfill="true"]'))
      .forEach((element: HTMLElement) => {
        element.remove();
      });
  }

  private isCachedThemeUpToDate(cachedTheme: CachedTheme, theme: Theme): boolean {
    if (!cachedTheme) {
      return false;
    }
    const isThemeUpToDate = cachedTheme.modified === theme.modified;

    // Check if the version is still the same, never use cached theme on snapshots
    const isVersionUpToDate = cachedTheme.version === this.coyoConfig.versionString() &&
      this.coyoConfig.version.qualifier !== 'SNAPSHOT';

    return isThemeUpToDate && isVersionUpToDate && !cachedTheme.defaultVariablesOnly;
  }

  private createStyleTag(id: string, css: string, ponyfill: boolean = true): HTMLElement {
    const styleNode = this.document.createElement('style');
    if (id) {
      styleNode.setAttribute('id', id);
    }
    styleNode.setAttribute('type', 'text/css');
    if (ponyfill) {
      styleNode.setAttribute('data-css-ponyfill', 'true');
    }
    const text = this.document.createTextNode(css);
    styleNode.appendChild(text);
    return styleNode;
  }

  private createMetaTag(name: string, content: string): HTMLElement {
    const newChild = this.document.createElement('meta');
    newChild.setAttribute('name', name);
    newChild.setAttribute('content', content);
    return newChild;
  }

  private createImageCssVars(themeCustomVars: { [key: string]: string }): { [key: string]: string } {
    function _mapImage(image: string): string {
      return 'url(' + image + ')';
    }

    const adminThemeConfig = this.adminThemeConfig;
    const images = {};
    adminThemeConfig.imageConfigs.forEach((config: Ng1AdminThemeImageConfig) => {
      if (_.has(themeCustomVars, config.key)) {
        images[config.key] = _mapImage(themeCustomVars[config.key]);
      }
      if (config.retinaKey && _.has(themeCustomVars, config.retinaKey)) {
        images[config.retinaKey] = _mapImage(themeCustomVars[config.retinaKey]);
      }
      _(config.variables)
        .keys()
        .filter((variable: string) => themeCustomVars[variable] !== undefined)
        .forEach((variable: string) => {
          images[variable] = themeCustomVars[variable];
        });
    });
    return images;
  }

  private onCustomThemeChanged(): void {
    this.document.body.classList.remove('custom-theme-pending');
  }
}
