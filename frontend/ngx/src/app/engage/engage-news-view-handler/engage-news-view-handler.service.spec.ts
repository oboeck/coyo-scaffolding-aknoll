import {TestBed} from '@angular/core/testing';
import {NewsViewParams} from '@app/engage/news/news-view-params';
import {StateService} from '@uirouter/core';
import {EngageNewsHandlerService} from './engage-news-view-handler.service';

describe('EngageNewsHandlerService', () => {
  let service: EngageNewsHandlerService;
  let stateService: jasmine.SpyObj<StateService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EngageNewsHandlerService, {
        provide: StateService, useValue: jasmine.createSpyObj('stateService', ['go'])
      }]
    });
    service = TestBed.get(EngageNewsHandlerService);
    stateService = TestBed.get(StateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open the news state', () => {
    // given
    const params = {
      senderId: 'sender-id',
      appId: 'app-id',
      id: 'id',
      hideHeadline: true,
      hideTeaserImg: true,
      hideTeaserText: true
    } as NewsViewParams;

    // when
    service.showNews(params);

    // then
    expect(stateService.go).toHaveBeenCalledWith('news', params);
  });
});
