import {TestBed} from '@angular/core/testing';
import {Ng1MobileEventService} from '@root/typings';
import {TransitionService} from '@uirouter/core';
import {NG1_MOBILE_EVENTS_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import {EngageTransitionHandlerService} from './engage-transition-handler.service';

describe('EngageTransitionHandlerService', () => {
  let service: EngageTransitionHandlerService;
  let transitionService: jasmine.SpyObj<TransitionService>;
  let mobileEventsService: jasmine.SpyObj<Ng1MobileEventService>;
  let stateService: jasmine.SpyObj<IStateService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EngageTransitionHandlerService, {
        provide: TransitionService,
        useValue: jasmine.createSpyObj('transitionService', ['onStart'])
      }, {
        provide: NG1_MOBILE_EVENTS_SERVICE,
        useValue: jasmine.createSpyObj('mobileEventsService', ['propagate'])
      }, {
        provide: NG1_STATE_SERVICE,
        useValue: jasmine.createSpyObj('$state', ['href'])
      }]
    });

    service = TestBed.get(EngageTransitionHandlerService);
    transitionService = TestBed.get(TransitionService);
    mobileEventsService = TestBed.get(NG1_MOBILE_EVENTS_SERVICE);
    stateService = TestBed.get(NG1_STATE_SERVICE);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add transition listener', () => {
    // when
    service.watchTransitions();

    // then
    expect(transitionService.onStart).toHaveBeenCalled();
  });

  it('should cancel unhandled transitions', () => {
    // given
    service.watchTransitions();
    const callback = transitionService.onStart.calls.mostRecent().args[1];

    // when
    const result = callback({
      targetState: () => ({
        name: () => 'test',
        params: () => ({})
      })
    });

    // then
    expect(result).toBeFalsy();
  });

  it('should allow news transitions', () => {
    // given
    service.watchTransitions();
    const callback = transitionService.onStart.calls.mostRecent().args[1];

    // when
    const result = callback({
      targetState: () => ({
        name: () => 'news',
        params: () => ({})
      })
    });

    // then
    expect(result).toBeTruthy();
  });

  it('should allow login transitions', () => {
    // given
    service.watchTransitions();
    const callback = transitionService.onStart.calls.mostRecent().args[1];

    // when
    const result = callback({
      targetState: () => ({
        name: () => 'front.login',
        params: () => ({})
      })
    });

    // then
    expect(result).toBeTruthy();
  });

  it('should notify when user profile transition', () => {
    // given
    const url = '/user/user-id';
    service.watchTransitions();
    stateService.href.and.returnValue(url);
    const callback = transitionService.onStart.calls.mostRecent().args[1];

    // when
    const result = callback({
      targetState: () => ({
        name: () => 'main.profile.test',
        params: () => ({userId: 'user-id'}),
        url: url
      })
    });

    // then
    expect(result).toBeFalsy();
    expect(mobileEventsService.propagate).toHaveBeenCalledWith('transition', {
      name: 'main.profile.test',
      params: {userId: 'user-id'},
      url: url
    });
  });
});
