import {TestBed} from '@angular/core/testing';
import {EngageNewsHandlerService} from '@app/engage/engage-news-view-handler/engage-news-view-handler.service';
import {Ng1DeviceRegistrationHandler, Ng1MobileStorageHandler} from '@root/typings';
import {NG1_DEVICE_REGISTRATION_HANDLER, NG1_MOBILE_STORAGE_HANDLER} from '@upgrade/upgrade.module';
import {EngageTransitionHandlerService} from '../engage-transition-handler/engage-transition-handler.service';
import {MobileRequestHandlerRegistryService} from './mobile-request-handler-registry.service';

describe('MobileRequestHandlerRegistryService', () => {
  let service: MobileRequestHandlerRegistryService;
  let mobileStorageHandler: jasmine.SpyObj<Ng1MobileStorageHandler>;
  let deviceRegistrationHandler: jasmine.SpyObj<Ng1DeviceRegistrationHandler>;
  let showNewsViewHandlerService: jasmine.SpyObj<EngageNewsHandlerService>;
  let engageTransitionHandler: jasmine.SpyObj<EngageTransitionHandlerService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MobileRequestHandlerRegistryService, {
        provide: NG1_MOBILE_STORAGE_HANDLER,
        useValue: jasmine.createSpyObj('mobileStorageHandler', ['setStorageValue', 'getStorageValue'])
      }, {
        provide: NG1_DEVICE_REGISTRATION_HANDLER,
        useValue: jasmine.createSpyObj('deviceRegistrationHandler', ['registerPushDevice'])
      }, {
        provide: EngageNewsHandlerService,
        useValue: jasmine.createSpyObj('showNewsViewHandlerService', ['showNews'])
      }, {
        provide: EngageTransitionHandlerService,
        useValue: jasmine.createSpyObj('engageTransitionHandler', ['watchTransitions'])
      }]
    });

    service = TestBed.get(MobileRequestHandlerRegistryService);
    mobileStorageHandler = TestBed.get(NG1_MOBILE_STORAGE_HANDLER);
    deviceRegistrationHandler = TestBed.get(NG1_DEVICE_REGISTRATION_HANDLER);
    showNewsViewHandlerService = TestBed.get(EngageNewsHandlerService);
    engageTransitionHandler = TestBed.get(EngageTransitionHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call the setStorageValue function', () => {
    // given
    const param = {};

    // when
    service.getHandler('setValue')(param);

    // then
    expect(mobileStorageHandler.setStorageValue).toHaveBeenCalledWith(param);
  });

  it('should call the getStorageValue function', () => {
    // given
    const param = {};

    // when
    service.getHandler('getValue')(param);

    // then
    expect(mobileStorageHandler.getStorageValue).toHaveBeenCalledWith(param);
  });

  it('should call the deviceRegistration function (DEPRECATED)', () => {
    // given
    const param = {};

    // when
    service.getHandler('deviceRegistration')(param);

    // then
    expect(deviceRegistrationHandler.registerPushDevice).toHaveBeenCalledWith(param);
  });

  it('should call the registerPushDevice function', () => {
    // given
    const param = {};

    // when
    service.getHandler('registerPushDevice')(param);

    // then
    expect(deviceRegistrationHandler.registerPushDevice).toHaveBeenCalledWith(param);
  });

  it('should call the showNews function', () => {
    // given
    const param = {};

    // when
    service.getHandler('showNews')(param);

    // then
    expect(showNewsViewHandlerService.showNews).toHaveBeenCalledWith(param);
  });

  it('should call the engageHello function (DEPRECATED)', () => {
    // given
    const param = {};

    // when
    service.getHandler('engageHello')(param);

    // then
    expect(engageTransitionHandler.watchTransitions).toHaveBeenCalled();
  });

  it('should call the initialize function', () => {
    // given
    const param = {};

    // when
    service.getHandler('initialize')(param);

    // then
    expect(engageTransitionHandler.watchTransitions).toHaveBeenCalled();
  });

  it('should create a arrow function for new angular calls', () => {
    // when
    const initializeHandler = service.getHandler('initialize');
    const engageHelloHandler = service.getHandler('engageHello');
    const showNewsHandler = service.getHandler('showNews');

    // then
    expect(initializeHandler).not.toBe(showNewsViewHandlerService.showNews);
    expect(engageHelloHandler).not.toBe(engageTransitionHandler.watchTransitions);
    expect(showNewsHandler).not.toBe(engageTransitionHandler.watchTransitions);
  });
});
