import {BlogArticleViewComponent} from '@app/engage/news/blog-article/blog-article-view.component';
import {NewsViewParams} from '@app/engage/news/news-view-params';
import {LocalStorageService} from '@core/storage/local-storage/local-storage.service';
import {BlogArticle} from '@domain/blog-article/blog-article';
import {NgHybridStateDeclaration} from '@uirouter/angular-hybrid';
import {ParamType, Transition} from '@uirouter/core';

resolveBlogArticle.$inject = ['BlogArticleModel', '$transition$'];

/**
 * Function for resolving the blog article for the news state.
 *
 * @param blogArticleModel the angularJs blog article model
 * @param $transition$ the transition service
 * @return The blog article
 */
export function resolveBlogArticle(blogArticleModel: any, $transition$: any): BlogArticle {
  return blogArticleModel.getWithPermissions({
    id: $transition$.params().id,
    senderId: $transition$.params().senderId,
    appId: $transition$.params().appId
  }, {preferredLanguage: $transition$.params().preferredLanguage}, ['like', 'comment']);
}

resolveParams.$inject = ['$transition$'];

/**
 * Function for resolving the state params.
 *
 * @param $transition$ the transition serivce
 * @return the view params
 */
export function resolveParams($transition$: any): NewsViewParams {
  return $transition$.params();
}

setLocalStorage.$inject = ['$transition$'];

/**
 * Setting up the local storage because the ios webview sometimes loses it on new instances.
 *
 * @param $transition$ the transition information
 */
export function setLocalStorage($transition$: Transition): void {
  const localStorageService: LocalStorageService = $transition$.injector().get(LocalStorageService);
  localStorageService.setValue('userId', $transition$.params().userId);
  localStorageService.setValue('clientId', $transition$.params().clientId);
  localStorageService.setValue('isAuthenticated', true);
  localStorageService.setValue('backendUrl', decodeURIComponent($transition$.params().backendUrl));
}

export const newsState: NgHybridStateDeclaration = {
  name: 'news',
  url: '/news/:senderId/:appId/:id',
  component: BlogArticleViewComponent,
  data: {
    authenticate: true
  },
  params: {
    senderId: {type: 'path'},
    appId: {type: 'path'},
    id: {type: 'path'},
    hideTeaserText: null as ParamType,
    hideHeadline: null as ParamType,
    hideTeaserImg: null as ParamType,
    preferredLanguage: null as ParamType
  },
  resolve: {
    article: resolveBlogArticle,
    params: resolveParams
  }
};

export const engageEmptyState: NgHybridStateDeclaration = {
  name: 'engage',
  url: '/engage?clientId&userId&backendUrl',
  data: {},
  params: {
    clientId: {type: 'query'},
    userId: {type: 'query'},
    backendUrl: {type: 'query'}
  },
  onEnter: setLocalStorage
};
