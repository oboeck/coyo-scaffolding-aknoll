import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TargetService} from '@domain/sender/target/target.service';
import {EventAvatarComponent} from './event-avatar.component';

describe('EventAvatarComponent', () => {
  let component: EventAvatarComponent;
  let fixture: ComponentFixture<EventAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventAvatarComponent],
      providers: [{
        provide: TargetService,
        useValue: jasmine.createSpyObj('targetService', ['getLinkTo'])
      }]
    }).overrideTemplate(EventAvatarComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventAvatarComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
