import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EventService} from '@domain/event/event.service';
import {SenderEvent} from '@domain/event/SenderEvent';
import {Sender} from '@domain/sender/sender';
import {Ng1EventDateSyncService} from '@root/typings';
import {NG1_EVENT_DATE_SYNC_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import {of} from 'rxjs';
import {EventSettingsComponent} from './event-settings.component';

describe('EventSettingsComponent', () => {
  let component: EventSettingsComponent;
  let fixture: ComponentFixture<EventSettingsComponent>;
  let stateService: jasmine.SpyObj<IStateService>;
  let eventDateSyncService: jasmine.SpyObj<Ng1EventDateSyncService>;
  let eventService: jasmine.SpyObj<EventService>;
  let event: SenderEvent;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [EventSettingsComponent],
        providers: [{
          provide: NG1_STATE_SERVICE,
          useValue: jasmine.createSpyObj('stateService', ['go'])
        }, {
          provide: NG1_EVENT_DATE_SYNC_SERVICE,
          useValue: jasmine.createSpyObj('eventDateSyncService', [
            'updateStartDate', 'updateEndDate', 'updateStartTime', 'updateEndTime'
          ])
        }, {
          provide: EventService,
          useValue: jasmine.createSpyObj('eventService', [
            'getEvent', 'updateEvent', 'deleteEvent'
          ])
        }, FormBuilder]
      })
      .overrideTemplate(EventSettingsComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EventSettingsComponent);
    component = fixture.componentInstance;
    stateService = TestBed.get(NG1_STATE_SERVICE);
    eventDateSyncService = TestBed.get(NG1_EVENT_DATE_SYNC_SERVICE);
    eventService = TestBed.get(EventService);
  }));

  beforeEach(() => {
    eventService.getEvent.and.returnValue(of(event));
    const startDate =  new Date();
    const endDate = new Date(startDate);
    endDate.setHours(startDate.getHours() + 1, 0, 0, 0);
    event = {
      attendingCount: 4,
      creator: {id: 'event-creator-id'} as Sender,
      description: '<div>event description</div>',
      displayName: 'event-name',
      endDate: endDate,
      fullDay: false,
      id: 'event-id',
      limitedParticipants: {participantsLimit: 12},
      place: 'event-location',
      requestDefiniteAnswer: true,
      showParticipants: true,
      slug: 'event-slug',
      startDate: startDate,
    } as SenderEvent;
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init with given event', () => {
    // given
    component.event = event;

    // when
    fixture.detectChanges();

    // then
    validateEventSettingsForm();
  });

  it('should init with event id only', () => {
    // given
    const eventId = 'event-id';
    component.eventId = eventId;

    // when
    fixture.detectChanges();

    // then
    expect(eventService.getEvent).toHaveBeenCalledWith(eventId);
    validateEventSettingsForm();
  });

  it('should call updateStartDate', () => {
    // given
    component.event = event;
    fixture.detectChanges();

    // when
    component.updateStartDate();

    // then
    expect(eventDateSyncService.updateStartDate).toHaveBeenCalled();
  });

  it('should call updateEndDate', () => {
    // given
    component.event = event;
    fixture.detectChanges();

    // when
    component.updateEndDate();

    // then
    expect(eventDateSyncService.updateEndDate).toHaveBeenCalled();
  });

  it('should call updateStartTime', () => {
    // given
    component.event = event;
    fixture.detectChanges();

    // when
    component.updateStartTime();

    // then
    expect(eventDateSyncService.updateStartTime).toHaveBeenCalled();
  });

  it('should call updateEndTime', () => {
    // given
    component.event = event;
    fixture.detectChanges();

    // when
    component.updateEndTime();

    // then
    expect(eventDateSyncService.updateEndTime).toHaveBeenCalled();
  });

  it('should emit after form changes', () => {
    // given
    component.event = event;
    fixture.detectChanges();
    spyOn(component.formResult, 'emit');

    // when
    component.eventSettingsForm.patchValue({name: 'new-event-name'});
    fixture.detectChanges();

    // then
    expect(component.formResult.emit).toHaveBeenCalled();
  });

  it('should update event form if description changes', () => {
    // given
    component.event = event;
    fixture.detectChanges();
    spyOn(component.eventSettingsForm, 'patchValue');

    // when
    component.descriptionChanges('new content');

    // then
    expect(component.eventSettingsForm.patchValue).toHaveBeenCalledWith({description: 'new content'});
  });

  it('should cancel the form with default settings', () => {
    // given
    fixture.detectChanges();

    // when
    component.cancelForm();

    // then
    expect(stateService.go).toHaveBeenCalledWith('main.event.show.settings', {});
  });

  it('should cancel the form with a previous state', () => {
    // given
    stateService['previous'] = {
      name: 'state-name',
      params: {a: 1}
    };
    fixture.detectChanges();

    // when
    component.cancelForm();

    // then
    expect(stateService.go).toHaveBeenCalledWith('state-name', {a: 1});
  });

  it('should cleanup on destroy', () => {
    // given
    fixture.detectChanges();
    spyOn(component.formResult, 'complete');

    // when
    component.ngOnDestroy();

    // then
    expect(component.formResult.complete).toHaveBeenCalled();
  });

  function validateEventSettingsForm(): void {
    expect(component.eventDates.startDate instanceof Date).toBeTruthy();
    expect(component.eventDates.startTime instanceof Date).toBeTruthy();
    expect(component.eventDates.endDate instanceof Date).toBeTruthy();
    expect(component.eventDates.endDate instanceof Date).toBeTruthy();
    expect(component.eventSettingsForm instanceof FormGroup).toBeTruthy();
    expect(component.eventSettingsForm.get('name').value).toEqual(event.displayName);
    expect(component.eventSettingsForm.get('host').value).toEqual(event.creator);
    expect(component.eventSettingsForm.get('location').value).toEqual(event.place);
    expect(component.eventSettingsForm.get('slug').value).toEqual(event.slug);
    expect(component.eventSettingsForm.get('description').value).toEqual(event.description);
    expect(component.eventSettingsForm.get('fullDay').value).toEqual(event.fullDay);
    expect(component.eventSettingsForm.get('startDate').value.toISOString).toEqual(event.startDate.toISOString);
    expect(component.eventSettingsForm.get('endDate').value.toISOString).toEqual(event.endDate.toISOString);
    expect(component.eventSettingsForm.get('showParticipants').value).toEqual(event.showParticipants);
    expect(component.eventSettingsForm.get('requestDefiniteAnswer').value).toEqual(event.requestDefiniteAnswer);
    expect(component.eventSettingsForm.get('limitedParticipantsFlag').value)
      .toEqual(event.limitedParticipants.participantsLimit > 0);
    expect(component.eventSettingsForm.get(['limitedParticipants', 'participantsLimit']).value)
      .toEqual(event.limitedParticipants.participantsLimit);
  }

});
