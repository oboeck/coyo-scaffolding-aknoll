import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {rteOptions} from '@app/events/rte-options';
import {EventService} from '@domain/event/event.service';
import {SenderEvent} from '@domain/event/SenderEvent';
import {EventDates, Ng1EventDateSyncService} from '@root/typings';
import {CoyoValidators} from '@shared/forms/validators/validators';
import {NG1_EVENT_DATE_SYNC_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import * as moment from 'moment-timezone';
import {EMPTY, Observable, of} from 'rxjs';

@Component({
  selector: 'coyo-event-settings',
  templateUrl: './event-settings.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventSettingsComponent implements OnInit, OnDestroy {

  /**
   * The event id.
   * If only the event id is given the component will retrieve the full entire event.
   */
  @Input() eventId: string;

  /**
   * The event.
   */
  @Input() event?: SenderEvent;

  /**
   * The form result of the general settings.
   */
  @Output() formResult: EventEmitter<object> = new EventEmitter<object>();

  event$: Observable<SenderEvent>;
  eventSettingsForm: FormGroup;
  eventDates: EventDates = {
    startDate: null,
    startTime: null,
    endDate: null,
    endTime: null,
  };
  name: FormControl;
  host: FormControl;
  eventStart: FormControl;
  eventEnd: FormControl;
  readonly slugPrefixEvents: string = '/events/';
  readonly eventDescriptionRteOptions: object = rteOptions;
  readonly timeOffset: number = 1;
  private eventEndTouched: boolean = false;

  constructor(@Inject(NG1_STATE_SERVICE) private stateService: IStateService,
              @Inject(NG1_EVENT_DATE_SYNC_SERVICE) private eventDateSyncService: Ng1EventDateSyncService,
              private eventService: EventService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.event$ = this.event ? of(this.event) : this.eventId ? this.eventService.getEvent(this.eventId) : EMPTY;
    this.event$.subscribe(event => {
      this.initForm(event);
      this.eventId = event.id;
      this.event = event;
    });
  }

  ngOnDestroy(): void {
    this.formResult.complete();
  }

  /**
   * Submit the form.
   */
  submitForm(): void {
    if (this.eventSettingsForm.valid) {
      const eventData = {...this.event, ...this.eventSettingsForm.getRawValue()};
      moment(eventData.startDate).format('YYYY-MM-DDTHH:mm:ss');
      moment(eventData.endDate).format('YYYY-MM-DDTHH:mm:ss');
      this.eventService.updateEvent(eventData).subscribe(() => this.cancelForm());
    }
  }

  /**
   * Cancel form and return to previous state.
   */
  cancelForm(): void {
    let state = 'main.event.show.settings';
    let params = {};
    if (!!this.stateService['previous']) {
      const prevState = this.stateService['previous'];
      state = prevState.name;
      params = prevState.params;
    }
    this.stateService.go(state, params);
  }

  /**
   * Delete the current event and return to previous state.
   */
  deleteEvent(): void {
    this.eventService.deleteEvent(this.eventId);
    this.cancelForm();
  }

  /**
   * Sync start and end date.
   */
  updateStartDate(): void {
    this.eventDateSyncService.updateStartDate(this.eventDates, !this.eventEndTouched);
    this.updateEventDates();
  }

  /**
   * Sync start and end date.
   */
  updateEndDate(): void {
    this.eventEndTouched = true;
    this.eventDateSyncService.updateEndDate(this.eventDates);
    this.updateEventDates();
  }

  /**
   * Sync start and end time.
   */
  updateStartTime(): void {
    this.eventDateSyncService.updateStartTime(this.eventDates, !this.eventEndTouched);
    this.updateEventDates();
  }

  /**
   * Sync start and end time.
   */
  updateEndTime(): void {
    this.eventEndTouched = true;
    this.eventDateSyncService.updateEndTime(this.eventDates);
    this.updateEventDates();
  }

  /**
   * Called after rte content changes.
   *
   * @param content The new content of the rte.
   */
  descriptionChanges(content: any): void {
    this.eventSettingsForm.patchValue({description: content});
  }

  private updateEventDates(): void {
    this.eventSettingsForm
      .patchValue({startDate: this.setTimeForDate(this.eventDates.startDate, this.eventDates.startTime)});
    this.eventSettingsForm
      .patchValue({endDate: this.setTimeForDate(this.eventDates.endDate, this.eventDates.endTime)});
  }

  private setTimeForDate(dateToUpdate: Date, time: Date): Date {
    const date = new Date(dateToUpdate);
    date.setHours(time.getHours());
    date.setMinutes(time.getMinutes());
    date.setSeconds(0, 0);
    return date;
  }

  private initForm(eventData: SenderEvent): void {
    this.initDates(eventData.startDate, eventData.endDate);

    this.name = new FormControl(eventData.displayName, [CoyoValidators.notBlank, Validators.maxLength(255)]);
    this.host = new FormControl(eventData.creator, [Validators.required]);
    this.eventStart = new FormControl(moment(eventData.startDate).toDate(), [Validators.required]);
    this.eventEnd = new FormControl(moment(eventData.endDate).toDate(), [Validators.required]);

    this.eventSettingsForm = this.formBuilder.group({
      id: [eventData.id, Validators.required],
      name: this.name,
      host: this.host,
      location: [eventData.place],
      description: [eventData.description],
      fullDay: [eventData.fullDay],
      slug: [eventData.slug, CoyoValidators.notBlank],
      startDate: this.eventStart,
      endDate: this.eventEnd,
      showParticipants: [eventData.showParticipants],
      requestDefiniteAnswer: [eventData.requestDefiniteAnswer],
      limitedParticipantsFlag: [this.getParticipantsLimit(eventData) > 0],
      limitedParticipants: this.formBuilder.group({
        participantsLimit: [this.getParticipantsLimit(eventData),
          this.getParticipantsLimit(eventData) > 0 ? [Validators.required, Validators.min(1)] : null]
      })
    });

    this.eventSettingsForm.get('limitedParticipantsFlag').valueChanges.subscribe(flag => {
      if (flag) {
        this.eventSettingsForm.get(['limitedParticipants', 'participantsLimit'])
          .setValidators([Validators.required, Validators.min(1)]);
      } else {
        this.eventSettingsForm.get(['limitedParticipants', 'participantsLimit']).clearValidators();
      }
      this.eventSettingsForm.get(['limitedParticipants', 'participantsLimit']).updateValueAndValidity();
    });

    this.eventSettingsForm.get('fullDay').valueChanges.subscribe(fullDay => {
      if (!fullDay) {
        const startDate = this.eventSettingsForm.get('startDate');
        const startTime = new Date();
        startTime.setHours(startTime.getHours() + 1, 0, 0, 0);
        startTime.setMinutes(0, 0, 0);
        startDate.patchValue(this.setTimeForDate(startDate.value, startTime));

        const endDate = this.eventSettingsForm.get('endDate');
        const endTime = new Date();
        endTime.setHours(startDate.value.getHours() + this.timeOffset, 0, 0, 0);
        endDate.patchValue(this.setTimeForDate(endDate.value, endTime));

        this.initDates(startDate.value, endDate.value);
      }
      this.eventSettingsForm.updateValueAndValidity();
    });

    this.eventSettingsForm.valueChanges.subscribe(event => {
      const eventFormData = {...this.event, ...event};
      eventFormData.valid = this.eventSettingsForm.valid;
      this.formResult.emit(eventFormData);
    });
  }

  private initDates(startDate: Date, endDate: Date): void {
    const start = moment(startDate).toDate();
    this.eventDates.startDate = start;
    this.eventDates.startTime = start;
    const end = moment(endDate).toDate();
    this.eventDates.endDate = end;
    this.eventDates.endTime = end;
  }

  private getParticipantsLimit(eventData: SenderEvent): number {
    if (!eventData.limitedParticipants || !eventData.limitedParticipants.participantsLimit) {
      return 0;
    }
    return eventData.limitedParticipants.participantsLimit;
  }

}
