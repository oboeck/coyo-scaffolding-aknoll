import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {EventSettingsComponent} from '@app/events/event-settings/event-settings.component';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {RteModule} from '@shared/rte/rte.module';
import {SenderUIModule} from '@shared/sender-ui/sender-ui.module';
import {UpgradeModule} from '@upgrade/upgrade.module';
import {MomentModule} from 'ngx-moment';
import {messagesDe} from './de.events.messages';
import {messagesEn} from './en.events.messages';
import {EventAvatarComponent} from './event-avatar/event-avatar.component';
import './event-avatar/event-avatar.component.downgrade';
import {EventCreateComponent} from './event-create/event-create.component';
import './event-create/event-create.component.downgrade';
import {EventDateOverlayComponent} from './event-date-overlay/event-date-overlay.component';
import './event-settings/event-settings.component.downgrade';

/**
 * Event feature module
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    CoyoFormsModule,
    MomentModule,
    ReactiveFormsModule,
    RteModule,
    MatCheckboxModule,
    SenderUIModule,
    UpgradeModule
  ],
  declarations: [
    EventAvatarComponent,
    EventCreateComponent,
    EventDateOverlayComponent,
    EventSettingsComponent
  ],
  exports: [
    EventAvatarComponent,
    EventCreateComponent,
    EventSettingsComponent
  ],
  entryComponents: [
    EventAvatarComponent,
    EventCreateComponent,
    EventSettingsComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ],
})
export class EventsModule {
}
