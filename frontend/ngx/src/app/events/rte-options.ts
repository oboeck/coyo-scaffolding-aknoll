export const rteOptions: object = {
  pluginsEnabled: [
    'url',
    'link',
    'lists'
  ],
  toolbarButtons: [
    'bold',
    'italic',
    'underline',
    'strikeThrough',
    'align',
    'formatOL',
    'formatUL',
    'insertLink',
    'clearFormatting',
    'html',
    'fullscreen',
    'undo',
    'redo'
  ]
};
