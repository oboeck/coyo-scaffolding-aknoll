import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'FILEPICKER.BREADCRUMB.CONTEXT_MENU.TOOLTIP' : 'Show hidden folders',
    'FILEPICKER.BUTTON.SELECT': 'Select ({count})',
    'FILEPICKER.BUTTON.BACK': 'Go back',
    'FILEPICKER.BUTTON.BACK.ARIA': 'Go back',
    'FILEPICKER.COLUMN.FILENAME': 'Name',
    'FILEPICKER.COLUMN.SIZE': 'Size',
    'FILEPICKER.COLUMN.MODIFIED': 'Modified',
    'FILEPICKER.COLUMN.SELECT': 'Select/Deselect All',
    'FILEPICKER.FILE.ARIA': 'File {file}',
    'FILEPICKER.FILE.SELECT': 'Select {file}',
    'FILEPICKER.FOLDER.ARIA': 'Folder {folder}. Click to navigate into this folder.',
    'FILEPICKER.FOLDER.CURRENT.ARIA': 'Current folder "{folder}"',
    'FILEPICKER.FOLDER.EMPTY': 'This folder is empty',
    'FILEPICKER.HEADLINE': 'Select a file',
  }
};
