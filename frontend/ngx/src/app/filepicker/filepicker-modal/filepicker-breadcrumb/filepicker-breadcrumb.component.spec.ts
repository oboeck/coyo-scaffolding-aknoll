import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerBreadcrumbComponent} from './filepicker-breadcrumb.component';

describe('FilepickerBreadcrumbComponent', () => {
  let component: FilepickerBreadcrumbComponent;
  let fixture: ComponentFixture<FilepickerBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilepickerBreadcrumbComponent]
    }).overrideTemplate(FilepickerBreadcrumbComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilepickerBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use first item as root folder', () => {
    // given
    const folder1 = {name: 'folder1'} as FilepickerItem;
    const folder2 = {name: 'folder2'} as FilepickerItem;
    component.items = [folder1, folder2];

    // when
    const rootFolder = component.rootFolder;

    // then
    expect(rootFolder).toBe(folder1);
  });

  it('should use last item as current folder', () => {
    // given
    const folder1 = {name: 'folder1'} as FilepickerItem;
    const folder2 = {name: 'folder2'} as FilepickerItem;
    component.items = [folder1, folder2];

    // when
    const currentFolder = component.currentFolder;

    // then
    expect(currentFolder).toBe(folder2);
  });

  it('should use root folder as current folder, if it is the only one', () => {
    // given
    const folder1 = {name: 'folder1'} as FilepickerItem;
    component.items = [folder1];

    // when
    const currentFolder = component.currentFolder;

    // then
    expect(currentFolder).toBe(folder1);
  });

  it('should not use context menu with 2 items', () => {
    // given
    const folder1 = {name: 'folder1'} as FilepickerItem;
    const folder2 = {name: 'folder2'} as FilepickerItem;
    component.items = [folder1, folder2];

    // when
    const hasContextMenu = component.hasContextMenu();
    const contextMenuItems = component.contextMenuItems;

    // then
    expect(hasContextMenu).toBe(false);
    expect(contextMenuItems.length).toBe(0);
  });

  it('should use second item for context menu when 3 items or more', () => {
    // given
    const folder1 = {name: 'folder1'} as FilepickerItem;
    const folder2 = {name: 'folder2'} as FilepickerItem;
    const folder3 = {name: 'folder3'} as FilepickerItem;
    component.items = [folder1, folder2, folder3];

    // when
    const hasContextMenu = component.hasContextMenu();
    const contextMenuItems = component.contextMenuItems;

    // then
    expect(hasContextMenu).toBe(true);
    expect(contextMenuItems.length).toBe(1);
    expect(contextMenuItems).toContain(folder2);
  });
});
