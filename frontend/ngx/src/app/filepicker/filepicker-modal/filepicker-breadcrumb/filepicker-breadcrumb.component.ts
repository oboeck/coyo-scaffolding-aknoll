import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import * as _ from 'lodash';

@Component({
  selector: 'coyo-filepicker-breadcrumb',
  templateUrl: './filepicker-breadcrumb.component.html',
  styleUrls: ['./filepicker-breadcrumb.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilepickerBreadcrumbComponent {
  /**
   * The items that should be displayed
   */
  @Input() items: FilepickerItem[] = [];

  /**
   * This event gets fired when a item in the breadcrumb was clicked
   */
  @Output() folderClick: EventEmitter<FilepickerItem> = new EventEmitter<FilepickerItem>();

  /**
   * Returns the first folder of the breadcrumb
   * @returns the first folder
   */
  get rootFolder(): FilepickerItem {
    return _.first(this.items);
  }

  /**
   * Returns the currently opened folder i.e. the last breadcrumb item
   * @returns the currently opened folder
   */
  get currentFolder(): FilepickerItem {
    return _.last(this.items);
  }

  /**
   * Checks if the currently opened folder is the given folder
   * @param item The item to check
   * @returns true when currently opened folder is the given folder, otherwise it returns false
   */
  isCurrentFolder(item: FilepickerItem): boolean {
    return this.currentFolder === item;
  }

  /**
   * Checks if a context menu should be shown inside of the breadcrumb or not
   * @returns true when a context menu should be shown in the breadcrumb, otherwise it returns false
   */
  hasContextMenu(): boolean {
    return !!this.items && this.items.length > 2;
  }

  /**
   * Returns all items that are inside of the breadcrumbs context menu
   * @returns a list of items
   */
  get contextMenuItems(): FilepickerItem[] {
    return this.hasContextMenu()
      ? this.items.slice(1, this.items.length - 1)
      : [];
  }

  /**
   * Load the contents of the given folder item
   * @param item a folder item
   */
  openFolder(item: FilepickerItem): void {
    this.folderClick.emit(item);
  }
}
