import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerIconComponent} from './filepicker-icon.component';

describe('FilepickerIconComponent', () => {
  let component: FilepickerIconComponent;
  let fixture: ComponentFixture<FilepickerIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilepickerIconComponent]
    }).overrideTemplate(FilepickerIconComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilepickerIconComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use folder icon', () => {
    // given
    component.item = {isFolder: true} as FilepickerItem;

    // when
    fixture.detectChanges();

    // then
    expect(component.svgIcon).toBe('folder_mc');
  });

  it('should default to generic file icon', () => {
    // given
    component.item = {isFolder: false} as FilepickerItem;

    // when
    fixture.detectChanges();

    // then
    expect(component.svgIcon).toBe('generic-file');

  });

  it('should find icon by mime type', () => {
    // given
    component.item = {isFolder: false, mimeType: 'video/mp4'} as FilepickerItem;

    // when
    fixture.detectChanges();

    // then
    expect(component.svgIcon).toBe('video');
  });

  it('should find icon by extension', () => {
    // given
    component.item = {isFolder: false, name: 'test.pdf'} as FilepickerItem;

    // when
    fixture.detectChanges();

    // then
    expect(component.svgIcon).toBe('pdf');
  });

  it('should find icon by extension (case insensitive)', () => {
    // given
    component.item = {isFolder: false, name: 'TEST.PDF'} as FilepickerItem;

    // when
    fixture.detectChanges();

    // then
    expect(component.svgIcon).toBe('pdf');
  });
});
