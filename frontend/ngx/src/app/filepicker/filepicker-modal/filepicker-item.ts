import {storageType} from '@domain/attachment/storage';
import {Observable} from 'rxjs';

/**
 * This interface represents an item of the filepicker
 */
export interface FilepickerItem {
  id?: string;
  name: string;
  isFolder: boolean;
  sizeInBytes: number | null;
  lastModified: Date;
  mimeType: string | null;
  storageType: storageType;

  /**
   * Fetches an array of children (e.g. files or folders inside of a folder)
   * @returns an Observable with an array of FilePickerItems
   */
  getChildren(): Observable<FilepickerItem[]>;
}
