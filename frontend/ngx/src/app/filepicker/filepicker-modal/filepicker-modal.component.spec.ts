import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerSelection} from '@app/filepicker/filepicker-modal/filepicker-selection/filepicker-selection';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {of} from 'rxjs';
import {FilepickerModalComponent} from './filepicker-modal.component';

describe('FilepickerModalComponent', () => {
  let component: FilepickerModalComponent;
  let fixture: ComponentFixture<FilepickerModalComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<FilepickerModalComponent>>;

  function createFolder(name: string, children: FilepickerItem[]): jasmine.SpyObj<FilepickerItem> {
    const folder = jasmine.createSpyObj(name, ['getChildren']);
    folder.isFolder = true;
    folder.getChildren.and.returnValue(of(children));
    return folder;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilepickerModalComponent],
      providers: [
        {provide: MatDialogRef, useValue: jasmine.createSpyObj('dialogRef', ['close'])},
        {provide: MAT_DIALOG_DATA, useValue: jasmine.createSpyObj('data', ['rootFolder'])},
        {provide: WindowSizeService, useValue: jasmine.createSpyObj('windowSizeService', ['isXs', 'isSm'])}
      ]
    }).overrideTemplate(FilepickerModalComponent, '')
      .compileComponents();

    dialogRef = TestBed.get(MatDialogRef);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilepickerModalComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch the children of the root folder on init', () => {
    // given
    const rootFolder = createFolder('root', []);
    component.data.rootFolder = rootFolder;

    // when
    fixture.detectChanges();

    // then
    expect(rootFolder.getChildren).toHaveBeenCalled();
  });

  it('should fetch the children of the "first open folder" on init', () => {
    // given
    const rootFolder = createFolder('root', []);
    const firstOpenFolder = createFolder('first open folder', []);
    component.data.rootFolder = rootFolder;
    component.data.firstOpenFolder = firstOpenFolder;

    // when
    fixture.detectChanges();

    // then
    expect(rootFolder.getChildren).toHaveBeenCalledTimes(0);
    expect(component.data.firstOpenFolder.getChildren).toHaveBeenCalled();
  });

  it('should add root folder to breadcrumb on init', () => {
    // given
    const rootFolder = createFolder('root', []);
    component.data.rootFolder = rootFolder;

    // when
    fixture.detectChanges();

    // then
    expect(component.breadcrumbItems.length).toBe(1);
    expect(component.breadcrumbItems).toContain(rootFolder);
  });

  it('should add root folder and "first open folder" to breadcrumb on init', () => {
    // given
    const rootFolder = createFolder('root', []);
    const firstOpenFolder = createFolder('first open folder', []);
    component.data.rootFolder = rootFolder;
    component.data.firstOpenFolder = firstOpenFolder;

    // when
    fixture.detectChanges();

    // then
    expect(component.breadcrumbItems.length).toBe(2);
    expect(component.breadcrumbItems[0]).toBe(rootFolder);
    expect(component.breadcrumbItems[1]).toBe(firstOpenFolder);
  });

  it('should add the folder to the breadcrumb while opening a folder', () => {
    // given
    const subFolder = createFolder('folder', []);
    const rootFolder = createFolder('root', [subFolder]);
    component.data.rootFolder = rootFolder;
    fixture.detectChanges();
    component.breadcrumbItems = [rootFolder];

    // when
    component.openFolder(subFolder);

    // then
    expect(component.breadcrumbItems.length).toBe(2);
    expect(component.breadcrumbItems).toContain(rootFolder);
    expect(component.breadcrumbItems).toContain(subFolder);
  });

  it('should remove subfolders of the breadcrumb while opening a folder', () => {
    // given
    const folder2 = createFolder('folder2', []);
    const folder1 = createFolder('folder1', [folder2]);
    const rootFolder = createFolder('root', [folder1]);
    component.data.rootFolder = rootFolder;
    fixture.detectChanges();
    component.breadcrumbItems = [rootFolder, folder1, folder2];

    // when
    component.openFolder(folder1);

    // then
    expect(component.breadcrumbItems.length).toBe(2);
    expect(component.breadcrumbItems).toContain(rootFolder);
    expect(component.breadcrumbItems).toContain(folder1);
    expect(component.breadcrumbItems).not.toContain(folder2);
  });

  it('should return selected files on click', () => {
    // given
    const selectedFiles = [{id: 'ITEM-1'}, {id: 'ITEM-2'}] as FilepickerItem[];
    component.fileSelection = {selectedFiles} as FilepickerSelection;

    // when
    component.onSubmitSelectedFiles();

    // expect
    expect(dialogRef.close).toHaveBeenCalledWith(selectedFiles);
  });
});
