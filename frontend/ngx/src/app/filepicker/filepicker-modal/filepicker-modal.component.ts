import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {Subject} from 'rxjs';
import {FilepickerSelection} from './filepicker-selection/filepicker-selection';

export interface FilePickerModalData {
  rootFolder: FilepickerItem;
  firstOpenFolder?: FilepickerItem;
}

/**
 * This component contains the filepicker inside of a modal
 */
@Component({
  selector: 'coyo-filepicker-modal',
  templateUrl: './filepicker-modal.component.html',
  styleUrls: ['./filepicker-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilepickerModalComponent implements OnInit {
  breadcrumbItems: FilepickerItem[] = [];
  loading: boolean = true;
  items$: Subject<FilepickerItem[]>;
  fileSelection: FilepickerSelection;

  constructor(public dialogRef: MatDialogRef<FilepickerModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: FilePickerModalData,
              private windowSizeService: WindowSizeService) {}

  ngOnInit(): void {
    this.items$ = new Subject();
    if (!!this.data.firstOpenFolder) {
      this.setCurrentBreadcrumbFolder(this.data.rootFolder);
    }
    this.openFolder(this.data.firstOpenFolder || this.data.rootFolder);
    this.fileSelection = new FilepickerSelection(this.items$);
  }

  /**
   * Handles the click event on filepicker items
   * Navigates inside the folder for folders, toggles the selection for files
   *
   * @param item the filepicker item that has been clicked
   * @param $event the click event dispatched by the browser
   */
  filePickerItemClicked(item: FilepickerItem, $event?: Event): void {
    item.isFolder ? this.openFolder(item) : this.toggleFileSelection(item, $event);
  }

  /**
   * Load the contents of the given folder item
   * @param item a folder item
   * @param $event row click event
   */
  openFolder(item: FilepickerItem, $event?: Event): void {
    this.loading = true;
    this.items$.next([]);
    this.setCurrentBreadcrumbFolder(item);
    item.getChildren().subscribe({
      next: items => this.items$.next(items),
      complete: () => this.loading = false
    });
  }

  /**
   * Checks for mobile view.
   *
   * @returns true if the window size is typical for mobile.
   */
  isMobile(): boolean {
    return this.windowSizeService.isXs() || this.windowSizeService.isSm();
  }

  private toggleFileSelection(item: FilepickerItem, $event?: Event): void {
    $event.preventDefault();
    this.fileSelection.toggleFileSelection(item);
  }

  private setCurrentBreadcrumbFolder(item: FilepickerItem): void {
    const index = this.breadcrumbItems.indexOf(item);
    if (index >= 0) {
      this.breadcrumbItems.length = index;
    }

    this.breadcrumbItems = [...this.breadcrumbItems, item];
  }

  /**
   * Submits the selected files
   */
  onSubmitSelectedFiles(): void {
    this.dialogRef.close(this.fileSelection.selectedFiles);
  }

  /**
   * Goes back to previous folder.
   */
  goBackToPreviousFolder(): void {
    const latestPath = this.breadcrumbItems[this.breadcrumbItems.length - 2];
    this.openFolder(latestPath);
  }
}
