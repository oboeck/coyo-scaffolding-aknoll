import {TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerModalComponent} from '@app/filepicker/filepicker-modal/filepicker-modal.component';
import {Observable, of} from 'rxjs';
import {FilepickerService} from './filepicker.service';

describe('FilepickerService', () => {
  let service: FilepickerService;
  let dialog: jasmine.SpyObj<MatDialog>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: MatDialog,
        useValue: jasmine.createSpyObj('MatDialog', ['open'])
      }]
    });

    service = TestBed.get(FilepickerService);
    dialog = TestBed.get(MatDialog);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open modal with root folder context and provide on close Observable', () => {
    // given
    const onClose: Observable<FilepickerItem[]> = of();
    const rootFolder = {id: 'ROOT-FOLDER'} as FilepickerItem;
    const firstOpenFolder = {id: 'FIRST-OPEN-FOLDER'} as FilepickerItem;
    dialog.open.and.returnValue({afterClosed: () => onClose});

    // when
    const returnValue = service.openFilepicker(rootFolder, firstOpenFolder);

    // then
    expect(returnValue).toBe(onClose);
    expect(dialog.open).toHaveBeenCalledWith(
      FilepickerModalComponent,
      {
        data: {rootFolder, firstOpenFolder},
        width: jasmine.any(String),
        height: jasmine.any(String)
      }
    );
  });
});
