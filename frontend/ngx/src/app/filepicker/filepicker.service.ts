import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerModalComponent} from '@app/filepicker/filepicker-modal/filepicker-modal.component';
import {MatDialogSize} from '@coyo/ui';
import {Observable} from 'rxjs';

/**
 * This service handles the filepicker
 */
@Injectable({
  providedIn: 'root'
})
export class FilepickerService {
  constructor(private dialog: MatDialog) {
  }

  /**
   * Opens a filepicker
   * @param rootFolder The folder that is the entrypoint of the filepicker
   * @param firstOpenFolder Optional - The first folder that should be opened. If not set, it will be the root folder.
   * @return Observable of selected items
   */
  openFilepicker(rootFolder: FilepickerItem, firstOpenFolder?: FilepickerItem): Observable<FilepickerItem[]> {
    return this.dialog.open(FilepickerModalComponent, {
      data: {rootFolder, firstOpenFolder},
      width: MatDialogSize.Large,
      height: 'calc(65vh + 171px)'
    }).afterClosed();
  }
}
