import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'GOOGLE.DRIVE.RECENT': 'Zuletzt'
  }
};
