import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'GOOGLE.DRIVE.RECENT': 'Recent'
  }
};
