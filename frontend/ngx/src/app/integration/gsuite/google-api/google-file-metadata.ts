import {GoogleApiDriveUser} from '@app/integration/gsuite/google-api/google-api-drive-user';
import {GoogleApiDrivePermission} from '@app/integration/gsuite/google-api/google-file-permission';

/**
 * Interface for google drive api file metadata response
 */
export interface GoogleFileMetaData {
  kind?: 'drive#file';
  id?: string;
  name?: string;
  mimeType?: string;
  description?: string;
  starred?: boolean;
  trashed?: boolean;
  explicitlyTrashed?: boolean;
  trashingUser?: GoogleApiDriveUser;
  trashedTime?: string;
  parents?: string[];
  properties?: Map;
  appProperties?: Map;
  spaces?: string[];
  version?: number;
  webContentLink?: string;
  webViewLink?: string;
  iconLink?: string;
  hasThumbnail?: boolean;
  thumbnailLink?: string;
  thumbnailVersion?: number;
  viewedByMe?: boolean;
  viewedByMeTime?: string;
  createdTime?: string;
  modifiedTime?: string;
  modifiedByMeTime?: string;
  modifiedByMe?: boolean;
  sharedWithMeTime?: string;
  sharingUser?: GoogleApiDriveUser;
  owners?: GoogleApiDriveUser[];
  teamDriveId?: string;
  driveId?: string;
  lastModifyingUser?: GoogleApiDriveUser;
  shared?: boolean;
  ownedByMe?: boolean;
  capabilities?: {
    canAddChildren?: boolean;
    canChangeCopyRequiresWriterPermission?: boolean;
    canChangeViewersCanCopyContent?: boolean;
    canComment?: boolean;
    canCopy?: boolean;
    canDelete?: boolean;
    canDeleteChildren?: boolean;
    canDownload?: boolean;
    canEdit?: boolean;
    canListChildren?: boolean;
    canMoveChildrenOutOfTeamDrive?: boolean;
    canMoveChildrenOutOfDrive?: boolean;
    canMoveChildrenWithinTeamDrive?: boolean;
    canMoveChildrenWithinDrive?: boolean;
    canMoveItemIntoTeamDrive?: boolean;
    canMoveItemOutOfTeamDrive?: boolean;
    canMoveItemOutOfDrive?: boolean;
    canMoveItemWithinTeamDrive?: boolean;
    canMoveItemWithinDrive?: boolean;
    canMoveTeamDriveItem?: boolean;
    canReadRevisions?: boolean;
    canReadTeamDrive?: boolean;
    canReadDrive?: boolean;
    canRemoveChildren?: boolean;
    canRename?: boolean;
    canShare?: boolean;
    canTrash?: boolean;
    canTrashChildren?: boolean;
    canUntrash?: boolean
  };
  viewersCanCopyContent?: boolean;
  copyRequiresWriterPermission?: boolean;
  writersCanShare?: boolean;
  permissions?: GoogleApiDrivePermission[];
  permissionIds?: string[];
  hasAugmentedPermissions?: boolean;
  folderColorRgb?: string;
  originalFilename?: string;
  fullFileExtension?: string;
  fileExtension?: string;
  md5Checksum?: string;
  size?: number;
  quotaBytesUsed?: number;
  headRevisionId?: string;
  contentHints?: {
    thumbnail?: {
      image?: any;
      mimeType?: string
    };
    indexableText?: string
  };
  imageMediaMetadata?: {
    width?: number;
    height?: number;
    rotation?: number;
    location?: {
      latitude?: number;
      longitude?: number;
      altitude?: number
    };
    time?: string;
    cameraMake?: string;
    cameraModel?: string;
    exposureTime?: number;
    aperture?: number;
    flashUsed?: boolean;
    focalLength?: number;
    isoSpeed?: number;
    meteringMode?: string;
    sensor?: string;
    exposureMode?: string;
    colorSpace?: string;
    whiteBalance?: string;
    exposureBias?: number;
    maxApertureValue?: number;
    subjectDistance?: number;
    lens?: string;
  };
  videoMediaMetadata?: {
    width?: number;
    height?: number;
    durationMillis?: number;
  };
  isAppAuthorized?: boolean;
  exportLinks?: {
    'application/pdf': string;
    [key: string]: string;
  };
}

interface KeyValue {
  key: string;
  value: string;
}

interface Map {
  pair: KeyValue[];
}
