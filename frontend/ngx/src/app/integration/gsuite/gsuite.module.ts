import {NgModule} from '@angular/core';
import {messagesDe} from '@app/integration/gsuite/de.g-suite.messages';
import {messagesEn} from '@app/integration/gsuite/en.g-suite.messages';
import './g-drive-picker/g-drive-picker.service.downgrade';
import './google-api/google-api.service.downgrade';

/**
 * Module for providing the functionality around the G Suite integration.
 */
@NgModule({
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ]
})
export class GsuiteModule {}
