import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'RECENT_FOLDER.NAME' : 'Zuletzt bearbeitet'
  }
};
