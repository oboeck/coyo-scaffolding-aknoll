import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'RECENT_FOLDER.NAME' : 'Recent'
  }
};
