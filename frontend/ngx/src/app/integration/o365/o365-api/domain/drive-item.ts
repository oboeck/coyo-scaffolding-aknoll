export interface DriveItem {
  id: string;
  name: string;
  file?: {
    mimeType: string;
  };
  folder?: {
    childCount: number;
  };
  parentReference: {
    driveId: string;
  };
  size?: number;
  lastModifiedDateTime: string;
  webUrl?: string;
}
