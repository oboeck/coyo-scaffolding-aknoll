import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {Observable} from 'rxjs';
import {flatMap, map} from 'rxjs/operators';

export type apiVersion = 'v1.0' | 'beta';

export const V1: apiVersion = 'v1.0';
export const BETA: apiVersion = 'beta';

/**
 * This service makes calls to the Microsoft GraphAPI
 */
@Injectable({
  providedIn: 'root'
})
export class GraphApiService {
  private static readonly GRAPH_URL: string = 'https://graph.microsoft.com/';

  constructor(private integrationApiService: IntegrationApiService, private httpClient: HttpClient) {
  }

  /**
   * Creates a GET request to the Microsoft GraphAPI and adds the needed headers
   * @param endpoint the endpoint of the GraphAPI (with leading slash)
   * @param version the version of the GraphAPI to query against
   * @returns an Observable with the data in the defined type
   */
  get<T>(endpoint: string, version: apiVersion = V1): Observable<T> {
    return this.integrationApiService.getToken().pipe(
      flatMap(token => {
        const url = GraphApiService.GRAPH_URL + version + endpoint;
        const options = {headers: {Authorization: 'Bearer ' + token.token}};
        return this.httpClient.get<{value: T}>(url, options);
      }),
      map((response: any) => response.value ? response.value : response)
    );
  }
}
