import {fakeAsync, TestBed} from '@angular/core/testing';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {BehaviorSubject, of} from 'rxjs';
import {GraphApiService} from './graph-api.service';
import {O365ApiService} from './o365-api.service';

describe('O365ApiService', () => {
  let service: O365ApiService;
  let integrationApiService: jasmine.SpyObj<IntegrationApiService>;
  let activeStateObservable: BehaviorSubject<boolean>;
  let graphApiService: jasmine.SpyObj<GraphApiService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: IntegrationApiService,
        useValue: jasmine.createSpyObj('IntegrationApiService', ['updateAndGetActiveState'])
      },
      {
        provide: GraphApiService,
        useValue: jasmine.createSpyObj('GraphApiService', ['get'])
      }]
    });
    activeStateObservable = new BehaviorSubject(true);
    integrationApiService = TestBed.get(IntegrationApiService);
    integrationApiService.updateAndGetActiveState.and.returnValue(activeStateObservable);
    graphApiService = TestBed.get(GraphApiService);
    graphApiService.get.and.returnValue(of());

    service = TestBed.get(O365ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update active state', fakeAsync(() => {
    // given
    const activeState = jasmine.createSpy();

    // when
    service.isApiActive().subscribe(activeState);
    activeStateObservable.next(false);

    // then
    expect(activeState).toHaveBeenCalledTimes(2);
    expect(activeState).toHaveBeenCalledWith(true);
    expect(activeState).toHaveBeenCalledWith(false);
  }));

  it('should get sharePoint site info', fakeAsync(() => {
    // given
    // when
    service.getDefaultSite();
    // then
    expect(graphApiService.get).toHaveBeenCalled();
  }));

  it('should fetch a drive item', fakeAsync(() => {
    // given
    const driveId = 'DRIVE_ID';
    const driveItemId = 'ITEM_ID';

    // when
    service.getDriveItem(driveId, driveItemId);

    // then
    expect(graphApiService.get).toHaveBeenCalledWith('/drives/DRIVE_ID/items/ITEM_ID');
  }));
});
