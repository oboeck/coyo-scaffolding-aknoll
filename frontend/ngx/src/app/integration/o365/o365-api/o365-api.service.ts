import {Injectable} from '@angular/core';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {Drive} from '@app/integration/o365/o365-api/domain/drive';
import {DriveItem} from '@app/integration/o365/o365-api/domain/drive-item';
import {Site} from '@app/integration/o365/o365-api/domain/site';
import {BETA, GraphApiService} from '@app/integration/o365/o365-api/graph-api.service';
import * as _ from 'lodash';
import * as mime from 'mime';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Insight} from './domain/insight';

/**
 * Service for interactions with the O365 integration.
 */
@Injectable({
  providedIn: 'root'
})
export class O365ApiService {

  private readonly INTEGRATION_TYPE: string = 'OFFICE_365';

  constructor(private integrationApiService: IntegrationApiService,
              private graphApi: GraphApiService) {
  }

  /**
   * This method provides the state of the O365 integration availability as
   * stream to subscribe.
   *
   * @returns an `Observable` holding the emitted O365 integration activation status.
   */
  isApiActive(): Observable<boolean> {
    return this.integrationApiService.updateAndGetActiveState(this.INTEGRATION_TYPE);
  }

  /**
   * This method returns all visible sites of the SharePoint
   *
   * @returns an `Observable` with an array of site information ordered asc by displayName
   */
  getSites(): Observable<Site[]> {
    return this.graphApi.get('/sites?search=*')
      .pipe(map(sites => _.orderBy(sites as Site[], ['displayName'], ['asc'])));
  }

  /**
   * This method returns recent used files
   *
   * @returns an `Observable` with an array of drive items
   */
  getRecentFiles(): Observable<DriveItem[]> {
    return this.graphApi.get('/me/insights/used?$filter=ResourceVisualization/containerType eq \'Site\'', BETA)
      .pipe(map(items => _.map(items as Insight[], item => {
        const driveId = /drives\/(.*)\/items/g.exec(item.resourceReference.id)[1];
        const id = /items\/(.*)/g.exec(item.resourceReference.id)[1];
        return {
          id,
          name: `${item.resourceVisualization.title}.${mime.getExtension(item.resourceVisualization.mediaType)}`,
          file: {
            mimeType: item.resourceVisualization.mediaType
          },
          parentReference: {
            driveId
          },
          size: -1,
          lastModifiedDateTime: item.lastUsed.lastModifiedDateTime
        } as DriveItem;
      })));
  }

  /**
   * This method returns the user organization default SharePoint site
   *
   * @returns an `Observable` with an object with the site information
   */
  getDefaultSite(): Observable<Site> {
    return this.graphApi.get('/sites/root');
  }

  /**
   * This method returns all drives of a site
   *
   * @param siteId the ID of the site
   * @returns an `Observable` with an array of drives sorted asc by name
   */
  getDrivesBySiteId(siteId: string): Observable<Drive[]> {
    return this.graphApi.get(`/sites/${siteId}/drives`);
  }

  /**
   * This method returns all driveItems of a drive
   *
   * @param driveId the ID of the drive
   * @param driveItemId the ID of the drive item. The default is 'root'.
   * @returns an `Observable` with an array of drivesItems sorted first folder asc by name then files asc by name
   */
  getDriveItems(driveId: string, driveItemId: string = 'root'): Observable<DriveItem[]> {
    return this.graphApi.get(`/drives/${driveId}/items/${driveItemId}/children`);
  }

  /**
   * This method returns a drive item by id
   *
   * @param driveId The ID of the drive the file is located in
   * @param driveItemId the ID of the item to return
   * @return A observable with the fetched item
   */
  getDriveItem(driveId: string, driveItemId: string): Observable<DriveItem> {
    return this.graphApi.get(`/drives/${driveId}/items/${driveItemId}`);
  }
}
