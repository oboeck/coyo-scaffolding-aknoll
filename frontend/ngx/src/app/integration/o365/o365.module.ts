import {NgModule} from '@angular/core';
import {FilepickerModule} from '@app/filepicker/filepicker.module';
import {messagesDe} from '@app/integration/o365/de.integration.o365.messages';
import {messagesEn} from '@app/integration/o365/en.integration.o365.messages';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import './o365-api/o365-api.service.downgrade';
import './share-point-filepicker/share-point-filepicker.service.downgrade';

/**
 * Module for providing the functionality around the O365 integration.
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    FilepickerModule
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ]
})
export class O365Module {
}
