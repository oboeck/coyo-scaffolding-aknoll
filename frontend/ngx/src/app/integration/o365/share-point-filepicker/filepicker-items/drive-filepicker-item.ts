import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {Drive} from '@app/integration/o365/o365-api/domain/drive';
import {DriveItemFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-item-filepicker-item';
import {FetchDriveItems} from '@app/integration/o365/share-point-filepicker/share-point-filepicker.service';
import {OFFICE365, storageType} from '@domain/attachment/storage';
import {Observable} from 'rxjs';

/**
 * This class creates a FilepickerItem based on a SharePoint Drive entity
 */
export class DriveFilepickerItem implements FilepickerItem {
  name: string;
  isFolder: boolean;
  sizeInBytes: number | null;
  lastModified: Date;
  mimeType: string | null;
  storageType: storageType;

  constructor(private drive: Drive, private fetchDriveItems: FetchDriveItems) {
    this.name = drive.name;
    this.isFolder = true;
    this.sizeInBytes = null;
    this.lastModified = new Date(this.drive.lastModifiedDateTime);
    this.mimeType = null;
    this.storageType = OFFICE365;
  }

  static fromArray(drives: Drive[], fetchDriveItems: FetchDriveItems): DriveFilepickerItem[] {
    return drives.map(drive => new DriveFilepickerItem(drive, fetchDriveItems));
  }

  getChildren(): Observable<DriveItemFilepickerItem[]> {
    return this.fetchDriveItems(this.drive.id);
  }
}
