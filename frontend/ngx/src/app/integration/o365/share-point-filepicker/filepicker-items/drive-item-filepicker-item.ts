import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {DriveItem} from '@app/integration/o365/o365-api/domain/drive-item';
import {FetchDriveItems} from '@app/integration/o365/share-point-filepicker/share-point-filepicker.service';
import {OFFICE365, storageType} from '@domain/attachment/storage';
import {Observable} from 'rxjs';

/**
 * This class creates a FilepickerItem based on a SharePoint DriveItem entity
 */
export class DriveItemFilepickerItem implements FilepickerItem {
  id: string;
  name: string;
  isFolder: boolean;
  sizeInBytes: number | null;
  lastModified: Date;
  mimeType: string | null;
  storageType: storageType;

  constructor(private driveItem: DriveItem, private fetchDriveItems: FetchDriveItems) {
    this.id = driveItem.parentReference.driveId + '|' + driveItem.id;
    this.name = driveItem.name;
    this.isFolder = driveItem.hasOwnProperty('folder');
    this.sizeInBytes = this.isFolder ? null : this.driveItem.size;
    this.lastModified = new Date(this.driveItem.lastModifiedDateTime);
    this.mimeType = driveItem.hasOwnProperty('file') ? this.driveItem.file.mimeType : null;
    this.storageType = OFFICE365;
  }

  static fromArray(driveItems: DriveItem[], fetchDriveItems: FetchDriveItems): DriveItemFilepickerItem[] {
    return driveItems.map(driveItem => new DriveItemFilepickerItem(driveItem, fetchDriveItems));
  }

  getChildren(): Observable<DriveItemFilepickerItem[]> {
    return this.fetchDriveItems(this.driveItem.parentReference.driveId, this.driveItem.id);
  }
}
