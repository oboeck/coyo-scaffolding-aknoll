import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

/**
 * The header of the launchpad.
 */
@Component({
  selector: 'coyo-launchpad-header',
  templateUrl: './launchpad-header.component.html',
  styleUrls: ['./launchpad-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LaunchpadHeaderComponent {

  /**
   * Flag indicating if the user can add new links.
   */
  @Input() canAddLink: boolean;

  /**
   * Emits an event when a new link should be created.
   */
  @Output() addLink: EventEmitter<void> = new EventEmitter();

  /**
   * Emits an event when the launchpad was closed.
   */
  @Output() close: EventEmitter<void> = new EventEmitter();
}
