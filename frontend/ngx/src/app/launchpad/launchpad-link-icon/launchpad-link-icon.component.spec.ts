import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LaunchpadLinkIconComponent} from './launchpad-link-icon.component';

describe('LaunchpadLinkIconComponent', () => {
  let component: LaunchpadLinkIconComponent;
  let fixture: ComponentFixture<LaunchpadLinkIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LaunchpadLinkIconComponent]
    }).overrideTemplate(LaunchpadLinkIconComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchpadLinkIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the configured color as background color without icon source', () => {
    // given
    component.color = '#0073E6';
    component.iconSrc = null;

    // when
    fixture.detectChanges();

    // then
    expect(component.backgroundColor).toBe('#0073E6');
  });

  it('should get null as the background color with icon source', () => {
    // given
    component.color = '#0073E6';
    component.iconSrc = 'src';

    // when
    fixture.detectChanges();

    // then
    expect(component.backgroundColor).toBeNull();
  });

  it('should get the background URL from the icon source', () => {
    // given
    component.iconSrc = 'src';

    // when
    fixture.detectChanges();

    // then
    expect(component.backgroundUrl['changingThisBreaksApplicationSecurity']).toEqual('url(src)');
  });
});
