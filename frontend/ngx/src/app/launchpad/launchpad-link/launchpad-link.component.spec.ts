import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LaunchpadLink} from '@domain/launchpad/launchpad-link';
import {LaunchpadLinkComponent} from './launchpad-link.component';

describe('LaunchpadLinkComponent', () => {
  let component: LaunchpadLinkComponent;
  let fixture: ComponentFixture<LaunchpadLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LaunchpadLinkComponent]
    }).overrideTemplate(LaunchpadLinkComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchpadLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should prepend https:// to link URLs', () => {
    // given
    component.link = {url: 'www.coyoapp.com'} as LaunchpadLink;

    // then
    expect(component.href).toEqual('https://www.coyoapp.com');
  });
});
