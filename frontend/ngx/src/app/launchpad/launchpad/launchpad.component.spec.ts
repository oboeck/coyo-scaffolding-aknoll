import {FocusTrapFactory} from '@angular/cdk/a11y';
import {OverlayRef} from '@angular/cdk/overlay';
import {ElementRef, QueryList} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {O365ApiService} from '@app/integration/o365/o365-api/o365-api.service';
import {LaunchpadCategory} from '@domain/launchpad/launchpad-category';
import {LaunchpadCategoryService} from '@domain/launchpad/launchpad-category.service';
import {LaunchpadLink} from '@domain/launchpad/launchpad-link';
import {LaunchpadLinkService} from '@domain/launchpad/launchpad-link.service';
import {OverlayService} from '@shared/overlay/overlay.service';
import {of, Subject} from 'rxjs';
import {
  CATEGORY,
  LaunchpadLinkManagerComponent,
  LINK
} from '../launchpad-link-manager/launchpad-link-manager.component';
import {LaunchpadComponent} from './launchpad.component';

describe('LaunchpadComponent', () => {
  let component: LaunchpadComponent;
  let fixture: ComponentFixture<LaunchpadComponent>;
  let launchpadCategoryService: jasmine.SpyObj<LaunchpadCategoryService>;
  let launchpadLinkService: jasmine.SpyObj<LaunchpadLinkService>;
  let o365apiService: jasmine.SpyObj<O365ApiService>;
  let overlayRef: jasmine.SpyObj<OverlayRef>;
  let focusTrapFactory: jasmine.SpyObj<FocusTrapFactory>;
  let overlayService: jasmine.SpyObj<OverlayService>;
  let onCloseSubject: Subject<[LaunchpadCategory, LaunchpadLink]>;
  let categories: LaunchpadCategory[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LaunchpadComponent],
      providers: [{
        provide: OverlayRef,
        useValue: jasmine.createSpyObj('OverlayRef', ['keydownEvents', 'dispose'])
      }, {
        provide: FocusTrapFactory,
        useValue: jasmine.createSpyObj('FocusTrapFactory', ['create'])
      }, {
        provide: OverlayService,
        useValue: jasmine.createSpyObj('OverlayService', ['open'])
      }, {
        provide: LaunchpadCategoryService,
        useValue: jasmine.createSpyObj('launchpadCategoryService', ['getAll'])
      }, {
        provide: LaunchpadLinkService,
        useValue: jasmine.createSpyObj('launchpadLinkService', ['delete'])
      },
      {
        provide: O365ApiService,
        useValue: jasmine.createSpyObj('o365apiService', ['getDefaultSite', 'isApiActive'])
      }]
    }).overrideTemplate(LaunchpadComponent, '')
      .compileComponents();

    categories = [{
      id: 'catId1',
      name: 'cat1',
      links: []
    } as LaunchpadCategory, {
      id: 'catId2',
      name: 'cat2',
      links: [],
      _permissions: {manage: true}
    } as LaunchpadCategory];

    onCloseSubject = new Subject<[LaunchpadCategory, LaunchpadLink]>();
    launchpadCategoryService = TestBed.get(LaunchpadCategoryService);
    launchpadCategoryService.getAll.and.returnValue(of(categories));
    launchpadLinkService = TestBed.get(LaunchpadLinkService);
    launchpadLinkService.delete.and.returnValue(of({}));
    o365apiService = TestBed.get(O365ApiService);
    o365apiService.isApiActive.and.returnValue(of(true));
    o365apiService.getDefaultSite.and.returnValue(of({webUrl: 'https://mindmail.sharepoint.com'}));
    overlayRef = TestBed.get(OverlayRef);
    overlayRef.keydownEvents.and.returnValue(of({}));
    (overlayRef as any).overlayElement = jasmine.createSpyObj('HTMLElement', ['querySelector']);
    focusTrapFactory = TestBed.get(FocusTrapFactory);
    focusTrapFactory.create.and.returnValue(jasmine.createSpyObj('FocusTrap', ['focusInitialElementWhenReady']));
    overlayService = TestBed.get(OverlayService);
    overlayService.open.and.returnValue(onCloseSubject);
    (overlayService as any).scrollStrategies = {block: () => 'BLOCK'};
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchpadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    onCloseSubject.complete();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get categories on init', () => {
    // then
    component.active$.subscribe(id => expect(id).toEqual('catId1'));

    // when
    component.ngOnInit();

    // then
    expect(launchpadCategoryService.getAll).toHaveBeenCalled();
    component.state$.subscribe(state => {
      expect(state.isLoading).toBe(false);
      expect(state.categories.length).toBe(2);
      expect(state.managableCategories.length).toBe(1);
    });
  });

  it('should track scrolling', () => {
    // given
    const categoryComponents = jasmine.createSpyObj<QueryList<ElementRef<HTMLElement>>>('QueryList', ['toArray']);
    categoryComponents.toArray.and.returnValue([
      {nativeElement: {offsetTop: 0, offsetHeight: 0, dataset: {id: 'catId1'}}},
      {nativeElement: {offsetTop: 20, offsetHeight: 20, dataset: {id: 'catId2'}}}
    ]);
    component.categoryComponents = categoryComponents;

    // then
    component.active$.subscribe(id => expect(id).toEqual('catId2'));

    // when
    component.onScroll({target: {offsetTop: 5, scrollTop: 20}});

    // then
    expect(component.scrollOffset).toBe(20);
  });

  it('should navigate to a category', () => {
    // given
    const launchpadBodyWrapperElement = jasmine.createSpyObj<HTMLElement>('HTMLElement', ['scroll']);
    (launchpadBodyWrapperElement as any).offsetTop = 5;
    const categoryComponents = jasmine.createSpyObj<QueryList<ElementRef<HTMLElement>>>('QueryList', ['toArray']);
    categoryComponents.toArray.and.returnValue([
      {nativeElement: {offsetTop: 0, dataset: {id: 'catId1'}}},
      {nativeElement: {offsetTop: 20, dataset: {id: 'catId2'}}}
    ]);
    component.categoryComponents = categoryComponents;
    component.launchpadBodyWrapper = {nativeElement: launchpadBodyWrapperElement};

    // then
    component.active$.subscribe(id => expect(id).toEqual('catId2'));

    // when
    component.navigate(categories[1]);

    // then
    expect(launchpadBodyWrapperElement.scroll).toHaveBeenCalledWith({top: 20 - 5 - 12, behavior: 'smooth'});
  });

  it('should open add link modal', () => {
    // given
    const category = categories[0];

    // when
    component.addLink(category);

    // then
    expect(overlayService.open).toHaveBeenCalledWith(LaunchpadLinkManagerComponent, {
      scrollStrategy: 'BLOCK',
      panelClass: ['launchpad', 'launchpad-manager'],
      hasBackdrop: false,
      height: '100%',
      width: '100%'
    }, jasmine.any(WeakMap));
    expect(overlayService.open.calls.mostRecent().args[2].get(CATEGORY)).toEqual(category);
  });

  it('should add link to category', () => {
    // given
    const link = {id: 'linkId', url: 'url1'} as LaunchpadLink;

    // when
    component.addLink();
    onCloseSubject.next([categories[0], link]);

    // then
    component.state$.subscribe(state => {
      expect(state.categories[0].links).toContain(link);
    });
  });

  it('should open edit link modal', () => {
    // given
    const category = categories[0];
    const link = {id: 'linkId'} as LaunchpadLink;

    // when
    component.editLink(category, link);

    // then
    expect(overlayService.open).toHaveBeenCalledWith(LaunchpadLinkManagerComponent, {
      scrollStrategy: 'BLOCK',
      panelClass: ['launchpad', 'launchpad-manager'],
      hasBackdrop: false,
      height: '100%',
      width: '100%'
    }, jasmine.any(WeakMap));
    expect(overlayService.open.calls.mostRecent().args[2].get(LINK)).toEqual(link);
    expect(overlayService.open.calls.mostRecent().args[2].get(CATEGORY)).toEqual(category);
  });

  it('should modify link in category', () => {
    // given
    const category = categories[0];
    const link = {id: 'linkId', url: 'url1'} as LaunchpadLink;
    const linkEdited = {id: 'linkId', url: 'url2'} as LaunchpadLink;
    category.links.push(link);

    // when
    component.editLink(category, link);
    onCloseSubject.next([category, linkEdited]);

    // then
    component.state$.subscribe(state => {
      expect(state.categories[0].links).toContain(linkEdited);
      expect(state.categories[0].links).not.toContain(link);
    });
  });

  it('should delete a link', () => {
    // given
    const category = categories[0];
    const link = {id: 'linkId', url: 'url1'} as LaunchpadLink;
    category.links.push(link);

    // when
    component.deleteLink(category, link);

    // then
    expect(launchpadLinkService.delete).toHaveBeenCalledWith(link.id, {
      context: {categoryId: category.id}
    });
    component.state$.subscribe(state => {
      expect(state.categories[0].links).not.toContain(link);
    });
  });
});
