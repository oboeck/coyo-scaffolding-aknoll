import {ElementRef} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '@core/auth/auth.service';
import {UserStorageService} from '@core/storage/user-storage/user-storage.service';
import {Attachment} from '@domain/attachment/attachment';
import {LOCAL_FILE_LIBRARY} from '@domain/attachment/storage';
import {of} from 'rxjs';
import {MessageFormComponent} from './message-form.component';

describe('MessageFormComponent', () => {
  let component: MessageFormComponent;
  let fixture: ComponentFixture<MessageFormComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let userStorageService: jasmine.SpyObj<UserStorageService>;
  let nativeElement: jasmine.SpyObj<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [MessageFormComponent],
      providers: [{
        provide: AuthService, useValue: jasmine.createSpyObj('authService', ['getUser'])
      }, {
        provide: UserStorageService,
        useValue: jasmine.createSpyObj('userStorageService', ['getValue', 'setValue', 'deleteEntry'])
      }]
    }).overrideTemplate(MessageFormComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    authService.getUser.and.returnValue(of({}));
    userStorageService = TestBed.get(UserStorageService);
    nativeElement = jasmine.createSpyObj('message-element', ['focus']);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageFormComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    expect(component.form.get('attachments').value).toEqual([]);
    expect(component.form.get('message').value).toBe('');
  });

  it('should init from local storage after user is loaded', () => {
    // given
    const channelId = 'channel-id';
    const message = 'message';
    const attachments = [{id: 'id'}];
    component.channelId = channelId;

    userStorageService.getValue.and.returnValue({message, attachments});
    component.message = {nativeElement: nativeElement} as ElementRef;

    // when
    component.ngOnInit();
    component.currentUser$.subscribe();

    // then
    expect(userStorageService.getValue).toHaveBeenCalledWith('NEW_MESSAGE-' + channelId);
    expect(component.form.get('attachments').value).toEqual(attachments);
    expect(component.form.get('message').value).toBe(message);
    expect(nativeElement.focus).toHaveBeenCalled();
  });

  it('should store the current form value on changes to the local storage debounced', fakeAsync(() => {
    // given
    const channelId = 'channel-id';
    component.channelId = channelId;
    fixture.detectChanges();
    const newFormValue = {message: 'test-message', attachments: [{id: 'id'}]};

    // when
    component.form.setValue(newFormValue);

    // then
    expect(userStorageService.setValue).not.toHaveBeenCalled();
    tick(500);
    expect(userStorageService.setValue).toHaveBeenCalledWith('NEW_MESSAGE-' + channelId, newFormValue);
  }));

  it('should remove attachments', () => {
    // given
    const attachmentOne = {id: '1'};
    const attachmentThree = {id: '3'};
    const attachmentToRemove = {id: '2'} as Attachment;
    const attachments = [attachmentOne, attachmentToRemove, attachmentThree];
    fixture.detectChanges();
    component.form.get('attachments').setValue(attachments);

    // when
    component.removeAttachment(attachmentToRemove);

    // then
    expect(component.form.get('attachments').value).toEqual([attachmentOne, attachmentThree]);
  });

  it('should store the current form value on destroy', fakeAsync(() => {
    // given
    const channelId = 'channel-id';
    component.channelId = channelId;
    fixture.detectChanges();
    const newFormValue = {message: 'test-message', attachments: [{id: 'id'}]};
    component.form.setValue(newFormValue);

    // when
    component.ngOnDestroy();

    // then
    expect(userStorageService.setValue).toHaveBeenCalledWith(MessageFormComponent.MESSAGE_PREFIX + channelId,
      newFormValue);
  }));

  it('should submit the form', () => {
    // given
    let called = false;
    const channelId = 'channel-id';
    component.channelId = channelId;
    fixture.detectChanges();
    component.message = {nativeElement: nativeElement} as ElementRef;
    const newFormValue = {
      message: 'test-message',
      attachments: [
        {id: 'id', storage: LOCAL_FILE_LIBRARY},
        {id: 'id2', storage: 'BLOB_STORAGE'}
        ]
    };
    component.form.setValue(newFormValue);
    component.submit.subscribe((value: any) => {
      called = true;
      expect(value.data.message).toBe(newFormValue.message);
      expect(value.attachments).toEqual([newFormValue.attachments[1]]);
      expect(value.fileLibraryAttachments).toEqual([newFormValue.attachments[0]]);
    });

    // when
    component.onSubmit();

    // then
    expect(called).toBeTruthy('submit output did not emit form value');
    expect(component.form.getRawValue()).toEqual({message: '', attachments: []});
    expect(nativeElement.focus).toHaveBeenCalled();
    expect(userStorageService.deleteEntry).toHaveBeenCalledWith(MessageFormComponent.MESSAGE_PREFIX + channelId);
  });

  it('should submit the form on enter', () => {
    const channelId = 'channel-id';
    component.channelId = channelId;
    fixture.detectChanges();
    component.onSubmit = jasmine.createSpy();
    const event = {
      preventDefault: jasmine.createSpy(),
      shiftKey: false
    } as any as KeyboardEvent;

    // when
    component.onEnter(event);

    // then
    expect(component.onSubmit).toHaveBeenCalled();
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should submit the form on enter', () => {
    const channelId = 'channel-id';
    component.channelId = channelId;
    fixture.detectChanges();
    component.onSubmit = jasmine.createSpy();
    const event = {
      preventDefault: jasmine.createSpy(),
      shiftKey: true
    } as any as KeyboardEvent;

    // when
    component.onEnter(event);

    // then
    expect(component.onSubmit).not.toHaveBeenCalled();
    expect(event.preventDefault).toHaveBeenCalled();
  });
});
