import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {EmojiModule} from '@shared/emoji/emoji.module';
import {FileModule} from '@shared/files/file.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {AutosizeModule} from 'ngx-autosize';
import {TooltipModule} from 'ngx-bootstrap';
import {MessageFormComponent} from './message-form/message-form.component';
import './message-form/message-form.component.downgrade';

/**
 * Module containing all messaging components
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    CoyoFormsModule,
    EmojiModule,
    FileModule,
    TooltipModule,
    AutosizeModule
  ],
  declarations: [MessageFormComponent],
  entryComponents: [MessageFormComponent]
})
export class MessagingModule { }
