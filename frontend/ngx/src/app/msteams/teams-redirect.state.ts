import {UrlService} from '@core/http/url/url.service';
import {LocalStorageService} from '@core/storage/local-storage/local-storage.service';
import {TokenService} from '@core/token/token.service';
import {NgHybridStateDeclaration} from '@uirouter/angular-hybrid';
import {Transition} from '@uirouter/core';

redirect.$inject = ['$transition$'];

/**
 * Setting up the local storage because the ios webview sometimes loses it on new instances.
 *
 * @param $transition$ the transition information
 */
export function redirect($transition$: Transition): void {
  const tokenService: TokenService = $transition$.injector().get(TokenService);
  const localStorageService: LocalStorageService = $transition$.injector().get(LocalStorageService);
  const urlService: UrlService = $transition$.injector().get(UrlService);
  tokenService.getSessionInfo().subscribe(sessionInfo => {
    const backendUrl = urlService.getBackendUrl();
    const urlParams = {
      sessionId: sessionInfo.sessionId,
      tenantId: sessionInfo.tenantId,
      userId: localStorageService.getValue<string>('userId').split('"').join(''),
      clientId: localStorageService.getValue<string>('clientId').split('"').join(''),
      teamsUserId: $transition$.params().userId,
      tenantUrl: sessionInfo.tenantUrl
    };
    const urlParamString = Object.keys(urlParams).map(key => key + '=' + urlParams[key]).join('&');
    const urlPath = '/web/teams/auth/success?' + urlParamString;
    document.location.href = urlService.join(backendUrl, urlPath);
  });
}

export const teamsRedirectState: NgHybridStateDeclaration = {
  name: 'teamsRedirect',
  url: '/teams/redirect/:userId',
  params: {
    userId: {type: 'path'},
  },
  data: {
    authenticate: true
  },
  onEnter: redirect
};
