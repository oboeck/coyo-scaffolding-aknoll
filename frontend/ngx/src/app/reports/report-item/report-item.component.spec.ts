import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TargetService} from '@domain/sender/target/target.service';
import {ReportItemComponent} from './report-item.component';

describe('ReportItemComponent', () => {
  let component: ReportItemComponent;
  let fixture: ComponentFixture<ReportItemComponent>;
  let targetService: jasmine.SpyObj<TargetService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportItemComponent],
      providers: [{
        provide: TargetService,
        useValue: jasmine.createSpyObj('targetService', ['getLinkTo'])
      }]
    }).overrideTemplate(ReportItemComponent, '')
      .compileComponents();

    targetService = TestBed.get(TargetService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportItemComponent);
    component = fixture.componentInstance;
  });

  it('should set link on init', () => {
    const link = '/link/to/something';
    component.report = {target: {target: {}}} as any;
    targetService.getLinkTo.and.returnValue(link);
    fixture.detectChanges();
    expect(component.link).toBe(link);
  });
});
