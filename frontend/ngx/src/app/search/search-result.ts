/**
 * Representation of one single external search result.
 */
export interface SearchResult {
  id: string;
  name: string;
  originalFilename: string;
  createdTime: string;
  modifiedTime: number;
  webViewLink: string;
  mimeType: string;
}
