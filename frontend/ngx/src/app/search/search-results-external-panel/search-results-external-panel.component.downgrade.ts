import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {SearchResultsExternalPanelComponent} from '@app/search/search-results-external-panel/search-results-external-panel.component';

getAngularJSGlobal()
  .module('coyo.search')
  .directive('coyoSearchResultsExternalPanel', downgradeComponent({
    component: SearchResultsExternalPanelComponent
  }));
