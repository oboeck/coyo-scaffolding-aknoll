import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {FileModule} from '@shared/files/file.module';
import {TimeModule} from '@shared/time/time.module';
import {messagesDe} from './de.search.messages';
import {messagesEn} from './en.search.messages';
import {SearchResultExternalComponent} from './search-result-external/search-result-external.component';
import './search-result-external/search-result-external.component.downgrade';
import {SearchResultsExternalPanelComponent} from './search-results-external-panel/search-results-external-panel.component';
import './search-results-external-panel/search-results-external-panel.component.downgrade';

/**
 * This module provides access to the COYO search as well as to external search providers.
 */
@NgModule({
  imports: [
    CommonModule,
    FileModule,
    TimeModule,
    TranslateModule
  ],
  declarations: [
    SearchResultExternalComponent,
    SearchResultsExternalPanelComponent
  ],
  entryComponents: [
    SearchResultExternalComponent,
    SearchResultsExternalPanelComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn},
  ]
})
export class SearchModule {}
