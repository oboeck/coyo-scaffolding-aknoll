import {AnchorDirective} from './anchor.directive';

describe('AnchorDirective', () => {

  it('should create an instance', () => {
    // when
    const directive = new AnchorDirective(null);

    // then
    expect(directive).toBeTruthy();
  });

  it('should reset location hash', () => {
    // given
    const window = {location: {hash: '#top'}} as Window;
    const directive = new AnchorDirective(window);

    // when
    directive.onClick({target: {hash: '#bottom'}} as any as MouseEvent);

    // then
    expect(window.location.hash).toEqual('#bottom');
  });
});
