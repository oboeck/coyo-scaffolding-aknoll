import {Directive, HostListener, Inject} from '@angular/core';
import {WINDOW} from '@root/injection-tokens';

/**
 * Directive to make anchor tags clickable every time.
 */
@Directive({
  selector: 'a[coyoAnchor]'
})
export class AnchorDirective {

  constructor(@Inject(WINDOW) private window: Window) {
  }

  /**
   * Handles click events on this directive.
   *
   * @param $event the click event
   */
  @HostListener('click', ['$event'])
  onClick($event: MouseEvent): void {
    this.window.location.hash = '';
    this.window.location.hash = ($event.target as HTMLAnchorElement).hash;
  }
}
