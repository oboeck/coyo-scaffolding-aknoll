import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'CONTEXT_MENU.SHOW': 'Show options'
  }
};
