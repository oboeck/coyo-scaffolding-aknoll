import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormControl, NgControl} from '@angular/forms';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {GDrivePickerService} from '@app/integration/gsuite/g-drive-picker/g-drive-picker.service';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {O365ApiService} from '@app/integration/o365/o365-api/o365-api.service';
import {SharePointFilepickerService} from '@app/integration/o365/share-point-filepicker/share-point-filepicker.service';
import {AuthService} from '@core/auth/auth.service';
import {UrlService} from '@core/http/url/url.service';
import {OFFICE365} from '@domain/attachment/storage';
import {WINDOW} from '@root/injection-tokens';
import {Ng1CsrfService, Ng1FileLibraryModalService} from '@root/typings';
import {NG1_CSRF_SERVICE, NG1_FILE_LIBRARY_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {FileItem} from 'ng2-file-upload';
import {of} from 'rxjs';
import {AttachmentBtnComponent} from './attachment-btn.component';

describe('AttachmentsComponent', () => {
  let component: AttachmentBtnComponent;
  let fixture: ComponentFixture<AttachmentBtnComponent>;
  let urlService: jasmine.SpyObj<UrlService>;
  let authService: jasmine.SpyObj<AuthService>;
  let o365ApiService: jasmine.SpyObj<O365ApiService>;
  let sharePointFilepickerService: jasmine.SpyObj<SharePointFilepickerService>;
  let gDrivePickerService: jasmine.SpyObj<GDrivePickerService>;
  let googleApiService: jasmine.SpyObj<GoogleApiService>;
  let csrfService: jasmine.SpyObj<Ng1CsrfService>;
  let fileLibraryModalService: jasmine.SpyObj<Ng1FileLibraryModalService>;
  let onChangeSpyFn: jasmine.Spy;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [
          AttachmentBtnComponent
        ],
        providers: [{
          provide: UrlService,
          useValue: jasmine.createSpyObj('urlService', ['getBackendUrl'])
        }, {
          provide: AuthService,
          useValue: jasmine.createSpyObj('authService', ['getUser'])
        }, {
          provide: O365ApiService,
          useValue: jasmine.createSpyObj('o365ApiService', ['isApiActive'])
        }, {
          provide: SharePointFilepickerService,
          useValue: jasmine.createSpyObj('sharePointFilepickerService', ['openFilepicker'])
        }, {
          provide: GDrivePickerService,
          useValue: jasmine.createSpyObj('gDrivePickerService', ['open'])
        }, {
          provide: GoogleApiService,
          useValue: jasmine.createSpyObj('googleApiService', ['isGoogleApiActive', 'isFilePublicVisible'])
        }, {
          provide: NG1_CSRF_SERVICE,
          useValue: jasmine.createSpyObj('csrfService', ['getToken'])
        }, {
          provide: NG1_FILE_LIBRARY_MODAL_SERVICE,
          useValue: jasmine.createSpyObj('fileLibraryModalService', ['open'])
        }, {
          provide: WINDOW,
          useValue: jasmine.createSpy('windowSizeService')
        }, NgControl]
      })
      .overrideTemplate(AttachmentBtnComponent, '')
      .compileComponents();

    urlService = TestBed.get(UrlService);
    authService = TestBed.get(AuthService);
    o365ApiService = TestBed.get(O365ApiService);
    sharePointFilepickerService = TestBed.get(SharePointFilepickerService);
    gDrivePickerService = TestBed.get(GDrivePickerService);
    googleApiService = TestBed.get(GoogleApiService);
    csrfService = TestBed.get(NG1_CSRF_SERVICE);
    fileLibraryModalService = TestBed.get(NG1_FILE_LIBRARY_MODAL_SERVICE);
  }));

  beforeEach(() => {
    urlService.getBackendUrl.and.returnValue('http://localhost:8080');
    o365ApiService.isApiActive.and.returnValue(of(true));
    googleApiService.isGoogleApiActive.and.returnValue(of(true));
    googleApiService.isFilePublicVisible.and.returnValue(Promise.resolve(true));
    csrfService.getToken.and.returnValue(Promise.resolve('csrf-token'));
    authService.getUser.and.returnValue(of({globalPermissions: ['ACCESS_FILES']}));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentBtnComponent);
    component = fixture.componentInstance;
    (component as any).ngControl = new FormControl();
    onChangeSpyFn = jasmine.createSpy('onChangeSpyFn');
    component.registerOnChange(onChangeSpyFn);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should show file library option', () => {
    // when
    component.ngOnInit();

    // then
    expect(component.fileLibraryActivated).toBeTruthy();
  });

  it('should set up the uploader', fakeAsync(() => {
    // given

    // when
    fixture.detectChanges();
    tick();

    // then
    expect(googleApiService.isGoogleApiActive).toHaveBeenCalled();
    expect(csrfService.getToken).toHaveBeenCalled();
    expect(component.uploader).toBeDefined();
    expect(component.uploader.onSuccessItem).toBeDefined();
    expect(component.uploader.onErrorItem).toBeDefined();
  }));

  it('should process files from local hdd', fakeAsync(() => {
    // given
    const fileItem = {
      isUploading: false,
      isUploaded: false,
      upload: () => {
      }
    } as FileItem;
    spyOn(fileItem, 'upload');
    fixture.detectChanges();
    tick();
    component.uploader.queue = [fileItem, fileItem];

    // when
    component.uploadFiles();
    fixture.detectChanges();

    // then
    expect(fileItem.upload).toHaveBeenCalledTimes(2);
  }));

  it('should open coyo file library picker', () => {
    // given
    fileLibraryModalService.open.and.returnValue(Promise.resolve());

    // when
    component.openCoyoFileLibrary();

    // then
    expect(fileLibraryModalService.open).toHaveBeenCalled();
  });

  it('should process coyo file library data', fakeAsync(() => {
    // given
    const coyoLibraryResult = {
      id: 'coyo-id',
      senderId: 'sender-id',
      displayName: 'display-name'
    };
    const coyoLibraryExpected = {
      fileId: coyoLibraryResult.id,
      senderId: coyoLibraryResult.senderId,
      name: coyoLibraryResult.displayName,
      displayName: coyoLibraryResult.displayName,
      storage: 'LOCAL_FILE_LIBRARY'
    };
    fileLibraryModalService.open.and.returnValue(Promise.resolve([coyoLibraryResult]));
    // when
    component.openCoyoFileLibrary();
    tick();

    // then
    expect(onChangeSpyFn).toHaveBeenCalledWith([coyoLibraryExpected]);
  }));

  it('should open google suite file picker', () => {
    // given
    gDrivePickerService.open.and.returnValue(Promise.resolve());

    // when
    component.openGSuitePicker();

    // then
    expect(gDrivePickerService.open).toHaveBeenCalled();
  });

  it('should process google suite file data', fakeAsync(() => {
    // given
    const googlePickerResult = {
      id: 'google-id',
      displayName: 'display-name',
      mimeType: 'mime-type'
    };
    const googlePickerExpected = {
      uid: googlePickerResult.id,
      name: googlePickerResult.displayName,
      displayName: googlePickerResult.displayName,
      contentType: googlePickerResult.mimeType,
      storage: 'G_SUITE',
      visibility: 'PUBLIC'
    };
    gDrivePickerService.open.and.returnValue(Promise.resolve([googlePickerResult]));

    // when
    component.openGSuitePicker();
    tick();

    // then
    expect(onChangeSpyFn).toHaveBeenCalledWith([googlePickerExpected]);
  }));

  it('should set visibility to private for protected google drive files', fakeAsync(() => {
    // given
    const googlePickerResult = {
      id: 'google-id',
      displayName: 'display-name',
      mimeType: 'mime-type'
    };
    const googlePickerExpected = {
      uid: googlePickerResult.id,
      name: googlePickerResult.displayName,
      displayName: googlePickerResult.displayName,
      contentType: googlePickerResult.mimeType,
      storage: 'G_SUITE',
      visibility: 'PRIVATE'
    };
    gDrivePickerService.open.and.returnValue(Promise.resolve([googlePickerResult]));
    googleApiService.isFilePublicVisible.and.returnValue(Promise.resolve(false));

    // when
    component.openGSuitePicker();
    tick();

    // then
    expect(onChangeSpyFn).toHaveBeenCalledWith([googlePickerExpected]);
  }));

  it('should default visibility to PRIVATE when state could not be determined.', fakeAsync(() => {
    // given
    const googlePickerResult = {
      id: 'google-id',
      displayName: 'display-name',
      mimeType: 'mime-type'
    };
    const googlePickerExpected = {
      uid: googlePickerResult.id,
      name: googlePickerResult.displayName,
      displayName: googlePickerResult.displayName,
      contentType: googlePickerResult.mimeType,
      storage: 'G_SUITE',
      visibility: 'PRIVATE'
    };
    gDrivePickerService.open.and.returnValue(Promise.resolve([googlePickerResult]));
    googleApiService.isFilePublicVisible.and.returnValue(Promise.reject());

    // when
    component.openGSuitePicker();
    tick();

    // then
    expect(onChangeSpyFn).toHaveBeenCalledWith([googlePickerExpected]);
  }));

  it('should enable file library, when the user has the ACCESS_FILES permission', () => {
    // given
    // permission ACCESS_FILES is already granted by a beforeEach()

    // when
    fixture.detectChanges();

    // then
    expect(fixture.componentInstance.fileLibraryActivated).toBe(true);
  });

  it('should disable file library, when the user does not have the ACCESS_FILES permission', () => {
    // given
    authService.getUser.and.returnValue(of({globalPermissions: []}));

    // when
    fixture.detectChanges();

    // then
    expect(fixture.componentInstance.fileLibraryActivated).toBe(false);
  });

  it('should open sharepoint filepicker', () => {
    // given
    sharePointFilepickerService.openFilepicker.and.returnValue(of());

    // when
    component.openSharePointOnlinePicker();

    // then
    expect(sharePointFilepickerService.openFilepicker).toHaveBeenCalledWith();
  });

  it('should add selected files from sharepoint filepicker', () => {
    // given
    const jpeg = {
      id: 'JPEG-1',
      name: 'JPEG',
      mimeType: 'image/jpeg'
    } as FilepickerItem;

    const pdf = {
      id: 'PDF-1',
      name: 'PDF',
      mimeType: 'application/pdf'
    } as FilepickerItem;

    const items = [jpeg, pdf];

    sharePointFilepickerService.openFilepicker.and.returnValue(of(items));

    // when
    component.openSharePointOnlinePicker();

    // then
    expect(component.allSelectedAttachments.length).toBe(2);
    expect(component.allSelectedAttachments).toContain({
      uid: 'JPEG-1',
      name: 'JPEG',
      displayName: 'JPEG',
      storage: OFFICE365,
      visibility: null,
      contentType: 'image/jpeg'
    });

    expect(component.allSelectedAttachments).toContain({
      uid: 'PDF-1',
      name: 'PDF',
      displayName: 'PDF',
      storage: OFFICE365,
      visibility: null,
      contentType: 'application/pdf'
    });
  });
});
