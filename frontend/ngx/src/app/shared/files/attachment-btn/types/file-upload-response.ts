/**
 * File upload response from backend.
 */
export interface FileUploadResponse {
  uid: string;
  groupId: string;
  contentType: string;
}
