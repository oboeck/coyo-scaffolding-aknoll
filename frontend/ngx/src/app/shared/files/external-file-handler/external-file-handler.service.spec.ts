import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {O365ApiService} from '@app/integration/o365/o365-api/o365-api.service';
import {AuthService} from '@core/auth/auth.service';
import {Attachment} from '@domain/attachment/attachment';
import {GSUITE, OFFICE365} from '@domain/attachment/storage';
import {ExternalFileDetails} from '@shared/files/external-file-handler/external-file-details';
import {NGXLogger} from 'ngx-logger';
import {of} from 'rxjs';
import {ExternalFileHandlerService} from './external-file-handler.service';

describe('ExternalFileHandlerService', () => {

  const googleApiService = jasmine.createSpyObj('googleApiService', ['getFileMetadata']);
  const o365Service = jasmine.createSpyObj('o365Service', ['getDriveItem']);
  const logger = jasmine.createSpyObj('NGXLogger', ['error']);

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [{
      provide: GoogleApiService,
      useValue: googleApiService
    }, {
      provide: AuthService
    }, {
      provide: NGXLogger,
      useValue: logger
    }, {
      provide: O365ApiService,
      useValue: o365Service
    }]
  }));

  it('should be created', () => {
    const service: ExternalFileHandlerService = TestBed.get(ExternalFileHandlerService);
    expect(service).toBeTruthy();
  });

  it('should recognize G Suite vendor media types', () => {
    const service: ExternalFileHandlerService = TestBed.get(ExternalFileHandlerService);

    expect(service.isGoogleMediaType('application/vnd.google-apps.document')).toBe(true);
    expect(service.isGoogleMediaType('application/vnd.google-apps.drawing')).toBe(true);
    expect(service.isGoogleMediaType('application/vnd.google-apps.file')).toBe(true);
    expect(service.isGoogleMediaType('application/vnd.google-apps.presentation')).toBe(true);
    expect(service.isGoogleMediaType('application/vnd.google-apps.spreadsheet')).toBe(true);
    expect(service.isGoogleMediaType('application/vnd.google-apps.video')).toBe(true);
  });

  it('should not recognize other media types as G Suite media types', () => {
    const service: ExternalFileHandlerService = TestBed.get(ExternalFileHandlerService);

    expect(service.isGoogleMediaType('application/pdf')).toBe(false);
    expect(service.isGoogleMediaType('application/zip')).toBe(false);
    expect(service.isGoogleMediaType('image/jpg')).toBe(false);
    expect(service.isGoogleMediaType('image/png')).toBe(false);
  });

  it('should consider file as non Google media type for missing contentType property', () => {
    const service: ExternalFileHandlerService = TestBed.get(ExternalFileHandlerService);

    expect(service.isGoogleMediaType('')).toBe(false);
    expect(service.isGoogleMediaType(null)).toBe(false);
    expect(service.isGoogleMediaType(undefined)).toBe(false);
  });

  it('should recognize g suite files as external', () => {
    // given
    const service = TestBed.get(ExternalFileHandlerService);
    const file = {storage: 'G_SUITE'};

    // when
    const result = service.isExternalFile(file);

    // then
    expect(result).toBeTruthy();
  });

  it('should recognize office 365 files as external', () => {
    // given
    const service = TestBed.get(ExternalFileHandlerService);
    const file = {storage: 'OFFICE_365'};

    // when
    const result = service.isExternalFile(file);

    // then
    expect(result).toBeTruthy();
  });

  it('should not recognize locally stored files as external', () => {
    // given
    const service = TestBed.get(ExternalFileHandlerService);
    const file = {storage: 'LOCAL_BLOB_STORAGE'};

    // when
    const result = service.isExternalFile(file);

    // then
    expect(result).toBeFalsy();
  });

  it('should not recognize files with no storage information as external', () => {
    // given
    const service = TestBed.get(ExternalFileHandlerService);
    const file = {};

    // when
    const result = service.isExternalFile(file);

    // then
    expect(result).toBeFalsy();
  });

  it('should return file metadata for gsuite file', () => {
    // given
    const service = TestBed.get(ExternalFileHandlerService);
    const fileDetails = {
      webViewLink: 'externalUrl',
      thumbnailLink: 'previewUrl',
      size: 1024,
      capabilities: {canEdit: true}
    };
    googleApiService.getFileMetadata.and.returnValue(Promise.resolve(fileDetails));

    // when, then
    service.getExternalFileDetails({storage: GSUITE} as Attachment).then((result: ExternalFileDetails) => {
      // then
      expect(result.externalUrl).toBe(fileDetails.webViewLink);
      expect(result.previewUrl).toBe(fileDetails.thumbnailLink);
      expect(result.fileSize).toBe(fileDetails.size);
      expect(result.canEdit).toBe(fileDetails.capabilities.canEdit);
    }).catch(() => {
      fail();
    });
  });

  it('should return file metadata for sharepoint file', () => {
    // given
    const service = TestBed.get(ExternalFileHandlerService);
    const fileDetails = {
      webUrl: 'externalUrl',
      size: 1024
    };
    o365Service.getDriveItem.and.returnValue(of(fileDetails));

    // when, then
    service.getExternalFileDetails({
      storage: OFFICE365,
      fileId: 'DRIVE_ID|ITEM_ID'
    } as Attachment).then((result: ExternalFileDetails) => {
      // then
      expect(result.externalUrl).toBe(fileDetails.webUrl);
      expect(result.previewUrl).toBe(fileDetails.webUrl);
      expect(result.fileSize).toBe(fileDetails.size);
      expect(result.canEdit).toBeFalsy();
      expect(o365Service.getDriveItem).toHaveBeenCalledWith('DRIVE_ID', 'ITEM_ID');
    }).catch(() => {
      fail();
    });
  });

  it('should reject for unknown storage types', () => {
    // given
    const service = TestBed.get(ExternalFileHandlerService);

    // when, then
    service.getExternalFileDetails({storage: 'unknown'}).then(() => {
      fail();
    }).catch(() => {
      expect().nothing();
    });
  });
});
