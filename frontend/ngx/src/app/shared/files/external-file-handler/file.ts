import {storageType} from '@domain/attachment/storage';

/**
 * A file that contains the properties required to retrieve the external file details.
 */
export interface File {
  fileId: string;
  storage?: storageType;
}
