import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CheckboxComponent} from './checkbox.component';

describe('CheckboxComponent', () => {
  let component: CheckboxComponent;
  let fixture: ComponentFixture<CheckboxComponent>;

  const TRUE_VALUE = true;
  const FALSE_VALUE = false;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxComponent ]
    })
      .overrideTemplate(CheckboxComponent, '')
      .compileComponents();
  }));

  function setModelValue(value: any): void {
    component.ngModel = value;
    fixture.detectChanges();
  }

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxComponent);
    component = fixture.componentInstance;
    component.trueValue = TRUE_VALUE;
    component.falseValue = FALSE_VALUE;
    component.ngModel = TRUE_VALUE;
    fixture.detectChanges();
  });

  it('should be checked when true value is set', () => {
    // given
    setModelValue(TRUE_VALUE);

    // then
    expect(component.isChecked).toBe(true);
  });

  it('should not be checked when false value is set', () => {
    // given
    setModelValue(FALSE_VALUE);

    // then
    expect(component.isChecked).toBe(false);
  });

  it('should toggle value from true value to false value', () => {
    // given
    let calledOnTouched = false;
    let calledOnChange = false;
    setModelValue(TRUE_VALUE);
    component.registerOnTouched(() => calledOnTouched = true);
    component.registerOnChange(() => calledOnChange = true);

    // when
    component.toggle();
    fixture.detectChanges();

    // then
    expect(component.ngModel).toBe(FALSE_VALUE);
    expect(calledOnTouched).toBe(true);
    expect(calledOnChange).toBe(true);
  });

  it('should toggle value on click', () => {
    // given
    spyOn(component, 'toggle');

    // when
    component.onInputClick();

    // then
    expect(component.toggle).toHaveBeenCalled();
  });

  it('should register onTouched function', () => {
    function onTouched(): void {
    }

    expect(component['onTouched']).toBeUndefined();
    component.registerOnTouched(onTouched);
    expect(component['onTouched']).toBeDefined();

  });

  it('should register onChange function', () => {
    const onChange = () => {};
    expect(component['onChange']).toBeUndefined();
    component.registerOnChange(onChange);
    expect(component['onChange']).toBeDefined();
  });

  it('should set disabled state', () => {
    component.setDisabledState(true);
    expect(component.disabled).toBe(true);
    component.setDisabledState(false);
    expect(component.disabled).toBe(false);
  });

  it('should write value', () => {
    component.writeValue(FALSE_VALUE);
    expect(component.ngModel).toBe(FALSE_VALUE);
  });
});
