import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {DatePickerComponent} from '@shared/forms/date-picker/date-picker.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoDatePicker', downgradeComponent({
    component: DatePickerComponent,
    propagateDigest: false
  }));
