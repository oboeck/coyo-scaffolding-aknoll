import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslationService} from '@core/i18n/translation-service/translation.service';
import {BsLocaleService} from 'ngx-bootstrap';
import {DatePickerComponent} from './date-picker.component';

describe('DatePickerComponent', () => {
  let component: DatePickerComponent;
  let fixture: ComponentFixture<DatePickerComponent>;
  let translationService: jasmine.SpyObj<TranslationService>;
  let localeService: jasmine.SpyObj<BsLocaleService>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ DatePickerComponent ],
        providers: [{
          provide: BsLocaleService,
          useValue: jasmine.createSpyObj('BsLocaleService', ['use'])
        }, {
          provide: TranslationService,
          useValue: jasmine.createSpyObj('TranslationService', ['getActiveLanguage'])
        }]
      })
      .overrideTemplate(DatePickerComponent, '')
      .compileComponents();

    translationService = TestBed.get(TranslationService);
    localeService = TestBed.get(BsLocaleService);
  }));

  beforeEach(() => {
    translationService.getActiveLanguage.and.returnValue('en');
    localeService.use.and.callThrough();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatePickerComponent);
    component = fixture.componentInstance;
    component.datePickerInput = {
      nativeElement: {
        value: 'initial value',
        dispatchEvent: () => {}
      }
    };
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(translationService.getActiveLanguage).toHaveBeenCalled();
    expect(localeService.use).toHaveBeenCalled();
    expect(component.datePickerInput.nativeElement.value).toBe('initial value');
  });

  it('should emit value if value changed', () => {
    // given
    const expectedDate = new Date('2018-10-18T13:37:00');
    spyOn(component.dateChange, 'emit');

    // when
    component.onBsValueChange(expectedDate);

    // then
    expect(component.dateChange.emit).toHaveBeenCalledWith(expectedDate);
  });

  it('should reset input value', () => {
    // given
    fixture.detectChanges();
    spyOn(component.datePickerInput.nativeElement, 'dispatchEvent');

    // when
    component.datePickerInput.nativeElement.value = '';
    component.resetInputValue();

    // then
    expect(component.datePickerInput.nativeElement.value).toBe('initial value');
    expect(component.datePickerInput.nativeElement.dispatchEvent).toHaveBeenCalledWith(new Event('change'));
  });
});
