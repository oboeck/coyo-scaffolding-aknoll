import {Duration, DurationFormatter, DurationUnit} from './duration';

describe('Duration', () => {

  it('should verify zero', () => {
    // given/when
    const zeroDuration = Duration.ZERO;

    // then
    // noinspection JSIgnoredPromiseFromCall
    assertDuration(zeroDuration, 0, 0, 0, 0, 0, 0, 0);
    expect(zeroDuration.toMilliseconds()).toEqual(0);
    expect(DurationFormatter.formatIso8601(zeroDuration)).toEqual('PT0S');
  });

  it('should parse an ISO 8601 duration', () => {
    // given
    const isoDuration = 'P2Y3M4DT23H59M30.123S';

    // when
    const actualDuration = DurationFormatter.parseIso8601(isoDuration);

    // then
    assertDuration(actualDuration, 2, 3, 4, 23, 59, 30, 123);
  });

  it('should parse a zero duration', () => {
    // given
    const isoDuration = 'PT0S';

    // when
    const actualDuration = DurationFormatter.parseIso8601(isoDuration);

    // then
    assertDuration(actualDuration, 0, 0, 0, 0, 0, 0, 0);
  });

  it('should parse date parts only', () => {
    // given
    const isoDuration = 'P1Y2M3D';

    // when
    const actualDuration = DurationFormatter.parseIso8601(isoDuration);

    // then
    assertDuration(actualDuration, 1, 2, 3, 0, 0, 0, 0);
  });

  it('should parse time parts only', () => {
    // given
    const isoDuration = 'PT2H3M4.789S';

    // when
    const actualDuration = DurationFormatter.parseIso8601(isoDuration);

    // then
    assertDuration(actualDuration, 0, 0, 0, 2, 3, 4, 789);
  });

  it('should not parse an invalid duration', () => {
    // given
    const illegalFormatDuration = '1Y2M3D'; // 'P' is missing

    // when
    expect(() => DurationFormatter.parseIso8601(illegalFormatDuration))

    // then
      .toThrow(new Error('Illegal duration format "1Y2M3D"'));
  });

  it('should normalize', () => {
    // given
    const duration = DurationFormatter.parseIso8601('P1Y13M34DT25H59M61S');

    // when
    const actualDuration = duration.normalize();

    // then
    assertDuration(actualDuration, 2, 2, 4, 2, 0, 1, 0);
  });

  it('should parse a non-normalized duration', () => {
    // given
    const isoDuration = 'P1Y13M34DT25H59M61S';

    // when
    const actualDuration = DurationFormatter.parseIso8601(isoDuration);

    // then
    assertDuration(actualDuration, 1, 13, 34, 25, 59, 61, 0);
  });

  it('should render an ISO 8601 duration', () => {
    assertToString('P2Y3M4DT23H59M30.123S', 'P2Y3M4DT23H59M30.123S');
    assertToString('P2Y3M4DT23H59M30.12S', 'P2Y3M4DT23H59M30.12S');
    assertToString('P2Y3M4DT23H59M30.1S', 'P2Y3M4DT23H59M30.1S');
    assertToString('P2Y3M4DT23H59M30.0S', 'P2Y3M4DT23H59M30S');
    assertToString('P2Y3M4DT0H59M0S', 'P2Y3M4DT59M');
    assertToString('P0Y3M0DT0H0M0S', 'P3M');
    assertToString('P0Y0M0DT1H0M0S', 'PT1H');
    assertToString('P0Y0M0DT0H0M0S', 'PT0S');
  });

  it('should evaluate milliseconds from time duration', () => {
    expect(DurationFormatter.parseIso8601('PT3H62M1.345S').toMilliseconds()).toEqual(14521345);
  });

  it('should disable units in descending order of units', () => {
    // given
    const duration = DurationFormatter.parseIso8601('P2Y3M4DT23H59M30.123S');

    // when
    const actualDuration = duration.disable([DurationUnit.DAYS, DurationUnit.MINUTES, DurationUnit.SECONDS]);

    // then
    assertDuration(actualDuration, 2, 3, 0, 119, 0, 0, 3570123);
  });

  it('should disable units multiple times in any order of units', () => {
    // given
    const duration = DurationFormatter.parseIso8601('PT2H3M4S');

    // when
    const actualDuration = duration
      .disable([DurationUnit.MINUTES, DurationUnit.HOURS, DurationUnit.MILLISECONDS])
      .disable([DurationUnit.MILLISECONDS, DurationUnit.HOURS])
      .disable([DurationUnit.HOURS]);

    // then
    assertDuration(actualDuration, 0, 0, 0, 0, 0, 7384, 0);
  });

  it('should fail to disable smallest enabled unit with carry', () => {
    // given
    const duration = DurationFormatter.parseIso8601('PT1S');

    // when
    expect(() => duration.disable([DurationUnit.SECONDS, DurationUnit.MILLISECONDS]))

    // then
      .toThrow(new Error('Illegal state while disabling units.'
        + ' Tried to disable smallest enabled unit having a value greater than zero.'));
  });

  it('should add a value to a unit', () => {
    // given
    const duration = DurationFormatter.parseIso8601('PT2M1S');

    // when
    const actualDuration = duration.plus(DurationUnit.MINUTES, 3);

    // then
    assertDuration(actualDuration, 0, 0, 0, 0, 5, 1, 0);
  });

  it('should not normalize unit when higher unit is disabled', () => {
    // given
    const duration = DurationFormatter.parseIso8601('PT2M1S').disable([DurationUnit.MINUTES]);

    // when
    const actualDuration = duration.normalize();

    // then
    assertDuration(actualDuration, 0, 0, 0, 0, 0, 121, 0);
  });

  it('should normalize to next available unit when next higher unit is disabled', () => {
    // given
    const duration = DurationFormatter.parseIso8601('PT2M1S')
      .disable([DurationUnit.MINUTES]).plus(DurationUnit.SECONDS, 3500);

    // when
    const actualDuration = duration.normalize();

    // then
    assertDuration(actualDuration, 0, 0, 0, 1, 0, 21, 0);
  });

  function assertDuration(
    zeroDuration: Duration,
    years: number,
    months: number,
    days: number,
    hours: number,
    minutes: number,
    seconds: number,
    milliseconds: number
  ): void {
    expect(zeroDuration.years).toEqual(years);
    expect(zeroDuration.months).toEqual(months);
    expect(zeroDuration.days).toEqual(days);
    expect(zeroDuration.hours).toEqual(hours);
    expect(zeroDuration.minutes).toEqual(minutes);
    expect(zeroDuration.seconds).toEqual(seconds);
    expect(zeroDuration.milliseconds).toEqual(milliseconds);
  }

  function assertToString(parsed: string, expected: string): void {
    const actual = DurationFormatter.formatIso8601(DurationFormatter.parseIso8601(parsed));
    expect(actual).toEqual(expected);
  }
});
