import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {NgSelectModule} from '@ng-select/ng-select';
import {TranslateModule} from '@ngx-translate/core';
import {ContextMenuModule} from '@shared/context-menu/context-menu.module';
import {ExtractUrlsDirective} from '@shared/forms/extract-urls/extract-urls.directive';
import {FileUploadModule} from 'ng2-file-upload';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {PopoverModule} from 'ngx-bootstrap/popover';
import {TimepickerModule} from 'ngx-bootstrap/timepicker';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {CoyoCommonsModule} from '../commons/commons.module';
import {CheckboxComponent} from './checkbox/checkbox.component';
import {DatePickerComponent} from './date-picker/date-picker.component';
import './date-picker/date-picker.component.downgrade';
import {messagesDe} from './de.forms.messages';
import {DurationPickerComponent} from './duration-picker/duration-picker.component';
import './duration-picker/duration-picker.component.downgrade';
import {messagesEn} from './en.forms.messages';
import {TimePickerComponent} from './time-picker/time-picker.component';
import './time-picker/time-picker.component.downgrade';
import {ValidationErrorsComponent} from './validation-errors/validation-errors.component';

/**
 * Module exporting components that are needed for forms
 */
@NgModule({
  imports: [
    BsDatepickerModule,
    BsDropdownModule,
    ContextMenuModule,
    CoyoCommonsModule,
    FileUploadModule,
    FormsModule,
    MatCheckboxModule,
    NgSelectModule,
    PopoverModule,
    ReactiveFormsModule,
    TimepickerModule,
    TooltipModule,
    TranslateModule
  ],
  declarations: [
    CheckboxComponent,
    DatePickerComponent,
    DurationPickerComponent,
    ExtractUrlsDirective,
    TimePickerComponent,
    ValidationErrorsComponent
  ],
  exports: [
    CheckboxComponent,
    DatePickerComponent,
    DurationPickerComponent,
    ExtractUrlsDirective,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    TimePickerComponent,
    ValidationErrorsComponent
  ],
  entryComponents: [
    DatePickerComponent,
    DurationPickerComponent,
    TimePickerComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ],
})
export class CoyoFormsModule {}
