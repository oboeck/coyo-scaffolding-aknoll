import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {TimePickerComponent} from '@shared/forms/time-picker/time-picker.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoTimePicker', downgradeComponent({
    component: TimePickerComponent,
    propagateDigest: false
  }));
