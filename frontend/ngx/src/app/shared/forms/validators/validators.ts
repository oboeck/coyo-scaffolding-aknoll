import {AbstractControl, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {JitTranslationSettingsService} from '@app/admin/settings/jit-translation-settings/jit-translation-settings.service';
import * as _ from 'lodash';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';

/**
 * Provides a set of custom validators that can be used by form controls.
 */
export class CoyoValidators {

  /**
   * Validator that requires the control's value to not only contain whitespace.
   * The validator exists only as a function and not as a directive.
   *
   * @example
   * ```typescript
   * const control = new FormControl('   ', CoyoValidators.notBlank());
   *
   * console.log(control.errors); // {notBlank: true}
   * ```
   *
   * @param control the underlying form control
   * @returns An error map with the `notBlank` property
   * if the validation check fails, otherwise `null`.
   */
  static notBlank(control: AbstractControl): ValidationErrors | null {
    return _.isEmpty(control.value) || (_.isString(control.value) && _.isEmpty(control.value.trim())) ? {notBlank: true} : null;
  }

  /**
   * Validator that requires the control's value to be one of the provided options.
   * The validator exists only as a function and not as a directive.
   *
   * @example
   * ```typescript
   * const control = new FormControl(4, Validators.options(1, 3, 5));
   *
   * console.log(control.errors); // {options: {options: [1, 3, 5], actual: 4}}
   * ```
   *
   * @param options the list of valid options
   * @returns A validator function that returns an error map with the
   * `options` if the validation check fails, otherwise `null`.
   */
  static options<T>(...options: T[]): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null =>
      !_.includes(options, control.value) ? {options: {options: options, actual: control.value}} : null;
  }

  /**
   * Validator that requires a field to be not blank when an other field has a true value.
   *
   * @example
   * ```typescript
   * const control = new FormGroup({
   *    checkbox: [true], requiredControl: ['']
   * }, {
   *    validators: [requiredIfTrue('checkbox', 'requiredControl', control => control.value)]
   * }));
   *
   * console.log(control.errors); // {requiredControl:{notBlank: true}}
   * ```
   *
   * @param requiredTrueControl The name of the checkbox like form control which must have the true value
   * @param requiredControl The name of the control which is required if the other control is truthy
   * @param trueProvider A function which should be considered true x
   *
   * @returns A Validator function returning an error map with the 'notBlank' property if the validator check failed
   */
  static requiredIfTrue(requiredTrueControl: string, requiredControl: string, trueProvider: (requiredControl: AbstractControl) => boolean): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const result = trueProvider(control.get(requiredTrueControl)) && this.notBlank(control.get(requiredControl));
      const errorMap = {};
      errorMap[requiredControl] = result;
      return errorMap[requiredControl] ? errorMap : null;
    };
  }

  static url(control: AbstractControl): ValidationErrors | null {
    // look for a better RegEx
    const reg = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    return control.value ? Validators.pattern(reg)(control) : null;
  }

  /**
   * Validator that requires one of the provided control values to be non empty.
   *
   * @example
   * ```typescript
   * const form = this.formBuilder.group({
   *   attachments: [],
   *   message: ['']
   *  }, {validator: CoyoValidators.anyNotBlank('attachments', 'message')}
   * );
   *
   * console.log(form.errors); // {anyNotBlank: true}
   * ```
   *
   * @param controlNames a list of control paths to be checked for blankness
   * @returns A validator function that returns an error map with the
   * `anyNotBlank` property set if the validation check fails, otherwise `null`.
   */
  static anyNotBlank(...controlNames: string[]): ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      const controls = _(controlNames).map(path => control.get(path)).filter(value => !!value).value();
      return _.some(controls, elem => !this.notBlank(elem)) ? null : {anyNotBlank: true};
    };
  }

  /**
   * Validator that requires none of the control's values is in uploading state.
   *
   * @example
   * ```typescript
   * const control = new FormControl([{name: 'test-file', isUploading: true}], CoyoValidators.notUploading);
   *
   * console.log(control.errors); // {uploading: true}
   * ```
   *
   * @param control the underlying form control
   *
   * @returns null if no value is in uploading state else an error map with uploading flag
   */
  static notUploading(control: AbstractControl): ValidationErrors | null {
    return _.some(control.value, attachment => attachment.isUploading) ? {uploading: true} : null;
  }

  /**
   * Creates an asynchronous validator to check if the given api key is valid to access the configured translation
   * provider.
   * @param settingsService The service
   *
   * @return an async validator function
   */
  static createApiKeyValidator(settingsService: JitTranslationSettingsService): (control: AbstractControl) =>
    Observable<ValidationErrors | null> {
    return (control: AbstractControl) => {
      if (!control.parent) {
        return of({invalidKey: true});
      } else if (!control.parent.get('activeProvider').value) {
        return of(null);
      }
      return settingsService
        .validateApiKey({activeProvider: control.parent.get('activeProvider').value, apiKey: control.value})
        .pipe(map(valid => valid ? null : {invalidKey: true}));
    };
  }
}
