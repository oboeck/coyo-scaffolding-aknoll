import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

/**
 * Modal for showing help information.
 */
@Component({
  selector: 'coyo-help-modal',
  templateUrl: './help-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HelpModalComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public text: string) {
  }
}
