import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {MatDialogSize} from '@coyo/ui';
import {TooltipDirective} from 'ngx-bootstrap/tooltip';
import {Subscription} from 'rxjs';
import {HelpModalComponent} from '../help-modal/help-modal.component';

/**
 * Renders a help icon with an info text in a tooltip.
 */
@Component({
  selector: 'coyo-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HelpComponent implements OnDestroy, OnInit {

  /**
   * Translation node which is read by screen readers when
   * focusing the info link. Defaults to 'READ_INFO_TEXT'.
   */
  @Input() aria?: string = 'READ_INFO_TEXT';

  /**
   * The tooltip text.
   */
  @Input() tip: string;

  /**
   * The tooltip text which is read by screen readers.
   */
  @Input() tipAria?: string;

  /**
   * The placement of the tooltip. Defaults to 'left'.
   */
  @Input() placement?: 'top' | 'right' | 'bottom' | 'left' | 'auto' = 'left';

  /**
   * Open a modal when button is clicked
   */
  @Input() modalText?: string;

  @ViewChild('tooltip', {
    static: true
  }) tooltip: TooltipDirective;

  ariaMessage: string;

  triggers: string;

  disabled: boolean;

  private subscription: Subscription;

  constructor(private matDialog: MatDialog,
              private windowSizeService: WindowSizeService) {
  }

  ngOnInit(): void {
    this.subscription = this.windowSizeService.observeScreenChange().subscribe(screenSize => {
      if (screenSize === ScreenSize.XS) {
        this.disabled = !!this.modalText;
        this.triggers = 'click';
      } else {
        this.disabled = false;
        this.triggers = 'hover';
      }
    });
  }

  /**
   * Opens a modal. If there is no modal text given it will set the aria message for live assertion.
   *
   * @param $event The click event
   */
  openModal($event: Event): void {
    $event.preventDefault();
    if (this.modalText) {
      this.matDialog.open<HelpModalComponent, string>(HelpModalComponent, {
        width: MatDialogSize.Large,
        data: this.modalText
      });
    } else {
      this.setAriaMessage();
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private setAriaMessage(): void {
    this.ariaMessage = this.tipAria || this.tip;
    // Screen readers will read content of the live region once it changes.
    // The change recognition takes a while but when the screen reader starts reading,
    // we can reset the message. This is needed in terms of repeated reading (the value needs
    // to change to be read out by screen readers).
    setTimeout(() => this.ariaMessage = null, 500);
  }
}
