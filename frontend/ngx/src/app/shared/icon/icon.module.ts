import {APP_INITIALIZER, NgModule} from '@angular/core';
import {AngularSvgIconModule, SvgIconRegistryService} from 'angular-svg-icon';
import {IconComponent} from './icon.component';
import './icon.component.downgrade';

/**
 * Registers all SVG icons used within COYO.
 *
 * @param iconRegistry the SVG icon registry
 * @return a factory function
 */
export function registerIcons(iconRegistry: SvgIconRegistryService): Function {
  return () => {
    iconRegistry.addSvg('star', require('!svg-inline-loader?classPrefix=!@coyo/icons/assets/star.svg'));
    iconRegistry.addSvg('star-crossed', require('!svg-inline-loader?classPrefix=!@coyo/icons/assets/star-crossed.svg'));
    iconRegistry.addSvg('page', require('!svg-inline-loader?classPrefix=!@coyo/icons/assets/page.svg'));
    iconRegistry.addSvg('workspace', require('!svg-inline-loader?classPrefix=!@coyo/icons/assets/workspace.svg'));
    iconRegistry.addSvg('chevron-down', require('!svg-inline-loader?classPrefix=!@coyo/icons/assets/chevron-down.svg'));
    iconRegistry.addSvg('pin', require('!svg-inline-loader?classPrefix=!@coyo/icons/assets/pin.svg'));
    iconRegistry.addSvg('close', require('!svg-inline-loader?classPrefix=!@coyo/icons/assets/close.svg'));
    iconRegistry.addSvg('outline-bookmark-plus', require('!svg-inline-loader?classPrefix=!@coyo/icons/assets/outline-bookmark-plus.svg'));
    iconRegistry.addSvg('folder', require('!svg-inline-loader?classPrefix=!@coyo/icons/assets/folder.svg'));
    iconRegistry.addSvg('o365-excel', require('!svg-inline-loader?classPrefix=!assets/icons/O365/icon-excel.svg'));
    iconRegistry.addSvg('o365-one-note', require('!svg-inline-loader?classPrefix=!assets/icons/O365/icon-one-note.svg'));
    iconRegistry.addSvg('o365-outlook', require('!svg-inline-loader?classPrefix=!assets/icons/O365/icon-outlook.svg'));
    iconRegistry.addSvg('o365-powerpoint', require('!svg-inline-loader?classPrefix=!assets/icons/O365/icon-powerpoint.svg'));
    iconRegistry.addSvg('o365-sharepoint', require('!svg-inline-loader?classPrefix=!assets/icons/O365/icon-sharepoint.svg'));
    iconRegistry.addSvg('o365-teams', require('!svg-inline-loader?classPrefix=!assets/icons/O365/icon-teams.svg'));
    iconRegistry.addSvg('o365-word', require('!svg-inline-loader?classPrefix=!assets/icons/O365/icon-word.svg'));
    iconRegistry.addSvg('o365-one-drive', require('!svg-inline-loader?classPrefix=!assets/icons/O365/icon-onedrive.svg'));
    iconRegistry.addSvg('o365-planner', require('!svg-inline-loader?classPrefix=!assets/icons/O365/icon-office-planner.svg'));
    iconRegistry.addSvg('volume-off', require('!svg-inline-loader?classPrefix=!assets/icons/volume-off.svg'));
    iconRegistry.addSvg('filepicker-folder', require('!svg-inline-loader?classPrefix=!assets/icons/icon-mc-folder.svg'));
    iconRegistry.addSvg('filepicker-filetype-generic', require('!svg-inline-loader?classPrefix=!assets/icons/icon-mc-filetype-generic.svg'));
    iconRegistry.addSvg('caret-right', require('!svg-inline-loader?classPrefix=!assets/icons/icon-caret-right.svg'));
  };
}

/**
 * Module exporting a icon component.
 */
@NgModule({
  imports: [
    AngularSvgIconModule
  ],
  declarations: [
    IconComponent
  ],
  exports: [
    IconComponent
  ],
  entryComponents: [
    IconComponent
  ],
  providers: [
    {provide: APP_INITIALIZER, useFactory: registerIcons, deps: [SvgIconRegistryService], multi: true}
  ]
})
export class IconModule {}
