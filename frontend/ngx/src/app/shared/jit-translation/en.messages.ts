import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'TRANSLATIONS.JIT.TRANSLATION.SHOW': 'Translate',
    'TRANSLATIONS.JIT.TRANSLATION.HIDE': 'Hide',
    'TRANSLATIONS.JIT.ERROR.NOTAVAILABLE': 'Translations currently not available'
  }
};
