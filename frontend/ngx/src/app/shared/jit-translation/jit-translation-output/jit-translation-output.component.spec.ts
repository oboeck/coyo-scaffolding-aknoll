import {ComponentFixture, TestBed} from '@angular/core/testing';

import {JitTranslationOutputComponent} from './jit-translation-output.component';

describe('JitTranslationOutputComponent', () => {
  let component: JitTranslationOutputComponent;
  let fixture: ComponentFixture<JitTranslationOutputComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JitTranslationOutputComponent]
    }).overrideTemplate(JitTranslationOutputComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JitTranslationOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
