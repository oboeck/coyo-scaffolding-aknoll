import {APP_INITIALIZER, NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule, MatIconRegistry} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {DomSanitizer} from '@angular/platform-browser';
import {MaterialModule as MaterialUiModule} from '@coyo/ui';

export function registerIcons(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer): Function {
  return () => iconRegistry.addSvgIconSet(sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/sprite.defs.svg'));
}

/**
 * Module exporting used angular modules and directives.
 */
@NgModule({
  imports: [
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    MatSnackBarModule,
    MaterialUiModule
  ],
  exports: [
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    MatSnackBarModule,
    MaterialUiModule
  ],
  providers: [{
    provide: APP_INITIALIZER, useFactory: registerIcons,
    deps: [MatIconRegistry, DomSanitizer],
    multi: true
  }]
})
export class MaterialModule {
}
