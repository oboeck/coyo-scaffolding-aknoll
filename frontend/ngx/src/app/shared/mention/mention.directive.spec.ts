import {ChangeDetectorRef, ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import {fakeAsync, tick} from '@angular/core/testing';
import {Caret} from '@core/caret/caret';
import {CaretService} from '@core/caret/caret.service';
import {MentionItem} from '@domain/mention/mention-item';
import {MentionService} from '@domain/mention/mention.service';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {User} from '@domain/user/user';
import {MentionComponent} from '@shared/mention/mention.component';
import {EMPTY, of, Subject} from 'rxjs';
import {MentionDirective} from './mention.directive';

describe('MentionDirective', () => {
  let directive: MentionDirective;
  let document: jasmine.SpyObj<Document>;
  let nativeElement: jasmine.SpyObj<HTMLInputElement | HTMLTextAreaElement>;
  let viewRef: jasmine.SpyObj<ViewContainerRef>;
  let componentFactoryResolver: jasmine.SpyObj<ComponentFactoryResolver>;
  let caretService: jasmine.SpyObj<CaretService>;
  let mentionService: jasmine.SpyObj<MentionService>;
  let cd: jasmine.SpyObj<ChangeDetectorRef>;

  let page: Page<MentionItem>;
  let instance: jasmine.SpyObj<MentionComponent>;
  let caret: Caret;

  beforeEach(() => {
    nativeElement = jasmine.createSpyObj('nativeElement', ['addEventListener', 'dispatchEvent', 'setSelectionRange', 'focus']);
    document = jasmine.createSpyObj('document', ['createEvent']);
    viewRef = jasmine.createSpyObj('viewRef', ['createComponent', 'clear']);
    componentFactoryResolver = jasmine.createSpyObj('componentFactoryResolver', ['resolveComponentFactory']);
    caretService = jasmine.createSpyObj('caretService', ['getCoordinates']);
    mentionService = jasmine.createSpyObj('mentionService', ['getItems']);
    cd = jasmine.createSpyObj('changeDetectorRef', ['detectChanges']);

    directive = new MentionDirective(document, {nativeElement}, viewRef, componentFactoryResolver, caretService, mentionService, cd);
    directive.ngOnInit();
  });

  beforeEach(() => {
    instance = jasmine.createSpyObj('instance', ['select']);
    (instance as any).clickedOutside = EMPTY;
    (instance as any).selected = EMPTY;
    viewRef.createComponent.and.returnValue({instance});

    caret = {top: 12, left: 12, height: 12};
    caretService.getCoordinates.and.returnValue(caret);
    page = {content: [{}]} as Page<MentionItem>;
    mentionService.getItems.and.returnValue(of(page));

    (nativeElement as any).scrollTop = 10;
    (nativeElement as any).scrollLeft = 10;
    (nativeElement as any).value = 'Hello @';
    (nativeElement as any).selectionStart = 7;
    (nativeElement as any).selectionEnd = 7;
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should open a dropdown on "@"', fakeAsync(() => {
    // when
    directive.onKeyUp({key: '@'} as KeyboardEvent);
    tick(MentionDirective.THROTTLE);

    // then
    expect(viewRef.createComponent).toHaveBeenCalled();
    expect(instance.top).toEqual('2px');
    expect(instance.left).toEqual('2px');
    expect(instance.height).toEqual('12px');
    expect(instance.page).toBe(page);
    expect(instance.active).toBe(0);
    expect(mentionService.getItems).toHaveBeenCalledWith('', new Pageable(0, 8));
  }));

  it('should select an mention item on click', fakeAsync(() => {
    const subject = new Subject<string>();
    (instance as any).selected = subject;

    // when
    directive.onKeyUp({key: '@'} as KeyboardEvent);
    tick(MentionDirective.THROTTLE);
    subject.next('slug');
    tick();

    expect(nativeElement.value).toEqual('Hello @slug ');
    expect(nativeElement.dispatchEvent).toHaveBeenCalled();
    expect(nativeElement.setSelectionRange).toHaveBeenCalledWith(12, 12);
    expect(nativeElement.focus).toHaveBeenCalled();
  }));

  it('should navigate down', fakeAsync(() => {
    // given
    page.content = [{} as MentionItem, {} as MentionItem];
    const event = jasmine.createSpyObj('event', ['preventDefault', 'stopPropagation']);
    event.key = 'ArrowDown';

    // when
    directive.onKeyUp({key: '@'} as KeyboardEvent);
    tick(MentionDirective.THROTTLE);
    directive.onKeyDown(event as KeyboardEvent);

    // then
    expect(instance.active).toBe(1);
    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopPropagation).toHaveBeenCalled();
  }));

  it('should navigate up', fakeAsync(() => {
    // given
    (instance as any).active = 1;
    page.content = [{} as MentionItem, {} as MentionItem];
    const event = jasmine.createSpyObj('event', ['preventDefault', 'stopPropagation']);
    event.key = 'ArrowUp';

    // when
    directive.onKeyUp({key: '@'} as KeyboardEvent);
    tick(MentionDirective.THROTTLE);
    directive.onKeyDown(event as KeyboardEvent);

    // then
    expect(instance.active).toBe(0);
    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopPropagation).toHaveBeenCalled();
  }));

  function testSelect(key: string): void {
    // given
    (instance as any).active = 0;
    const user = {displayName: 'user'} as User;
    page.content = [{user} as MentionItem];
    const event = jasmine.createSpyObj('event', ['preventDefault', 'stopPropagation']);
    event.key = key;

    // when
    directive.onKeyUp({key: '@'} as KeyboardEvent);
    tick(MentionDirective.THROTTLE);
    directive.onKeyDown(event as KeyboardEvent);

    // then
    expect(instance.select).toHaveBeenCalledWith(user);
    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopPropagation).toHaveBeenCalled();
  }

  it('should select item on enter', fakeAsync(() => testSelect('Enter')));
  it('should select item on tab', fakeAsync(() => testSelect('Tab')));

  it('should close the dropdown on Escape', fakeAsync(() => {
    // given
    const event = jasmine.createSpyObj('event', ['preventDefault', 'stopPropagation']);
    event.key = 'Escape';

    // when
    directive.onKeyUp({key: '@'} as KeyboardEvent);
    tick(MentionDirective.THROTTLE);
    directive.onKeyDown(event as KeyboardEvent);

    // then
    expect(viewRef.clear).toHaveBeenCalled();
    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopPropagation).toHaveBeenCalled();
  }));

  it('should call keydown listener on the element', fakeAsync(() => {
    // given
    const keydownHandler = nativeElement.addEventListener.calls.argsFor(0)[1];
    const event = jasmine.createSpyObj('$event', ['preventDefault', 'stopImmediatePropagation']);
    event['timeStamp'] = new Date();
    directive['eventStopped'] = event;

    // when
    keydownHandler(event);

    // then
    expect(nativeElement.addEventListener).toHaveBeenCalled();
    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopImmediatePropagation).toHaveBeenCalled();
  }));
});
