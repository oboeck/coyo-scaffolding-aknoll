import {Overlay} from '@angular/cdk/overlay';
import {TestBed} from '@angular/core/testing';
import {OverlayService} from './overlay.service';

describe('OverlayService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{
      provide: Overlay,
      useValue: jasmine.createSpyObj('Overlay', ['create'])
    }]
  }));

  it('should be created', () => {
    const service: OverlayService = TestBed.get(OverlayService);
    expect(service).toBeTruthy();
  });
});
