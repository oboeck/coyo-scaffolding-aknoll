import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'PDFVIEWER.OF': 'of',
    'PDFVIEWER.AUTOZOOM': 'Automatic zoom',
    'PDFVIEWER.ORIGINALSIZE': 'Original size'
  }
};
