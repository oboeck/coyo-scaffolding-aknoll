import {SimpleChange} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {WINDOW} from '@root/injection-tokens';
import {PDFDocumentProxy} from 'pdfjs-dist';
import {SettingPreset} from '../setting-preset';
import {settingPresets} from '../setting-presets';
import {PdfViewerComponent} from './pdf-viewer.component';

describe('PdfViewerComponent', () => {
  let component: PdfViewerComponent;
  let fixture: ComponentFixture<PdfViewerComponent>;
  let urlService: jasmine.SpyObj<UrlService>;
  let windowService: jasmine.SpyObj<Window>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PdfViewerComponent],
      providers: [{
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['isRelativePath', 'isAbsoluteBackendUrl']),
      }, {
        provide: WINDOW,
        useValue: jasmine.createSpyObj('windowService', ['setTimeout', 'clearTimeout'])
      }]
    }).overrideTemplate(PdfViewerComponent, '<div></div>')
      .compileComponents();

    urlService = TestBed.get(UrlService);
    windowService = TestBed.get(WINDOW);
  });

  beforeEach(() => {
    (windowService as any)['PDFJS'] = {};
    fixture = TestBed.createComponent(PdfViewerComponent);
    component = fixture.componentInstance;
    component.loading = true;
    component.src = 'some/path/to/internal/pdf';
    fixture.detectChanges();
  });

  afterEach(() => {
    (windowService as any)['PDFJS'] = undefined;
  });

  it('should init', () => {
    // given
    urlService.isRelativePath.and.returnValue(true);
    urlService.isAbsoluteBackendUrl.and.returnValue(false);

    // when
    fixture.detectChanges();
    component.ngOnInit();

    // then
    expect(component.loading).toBeTruthy();
    expect(component.currentPage).toBe(1);
    expect(component.maxPages).toBe(1);
    expect(component.rotation).toBe(0);
    expect(component.settingPresets).toEqual(settingPresets);
    expect(component.activeSettings).toEqual(settingPresets[0]);
    expect(component.pdfSrc).toEqual({url: 'some/path/to/internal/pdf', withCredentials: true});
    expect((windowService as any)['PDFJS'].workerSrc).toBeTruthy();
  });

  it('should set pdf src on change', () => {
    // given
    urlService.isRelativePath.and.returnValue(true);
    urlService.isAbsoluteBackendUrl.and.returnValue(false);
    component.src = 'some/path/to/internal/pdf';

    // when
    component.ngOnChanges({src: new SimpleChange(null, component.src, true)});
    fixture.detectChanges();

    // then
    expect(component.pdfSrc).toEqual({url: component.src, withCredentials: true});
  });

  it('should expect credentials included in url for external pdf source', () => {
    // given
    urlService.isRelativePath.and.returnValue(false);
    urlService.isAbsoluteBackendUrl.and.returnValue(false);
    component.src = 'some/path/to/external/pdf';

    // when
    fixture.detectChanges();
    component.ngOnInit();

    // then
    expect(component.pdfSrc).toEqual({url: component.src, withCredentials: false});
  });

  it('should set properties after loading the pdf', () => {
    const loadedPdf: any = {
      numPages: 200,
      fingerprint: 'id'
    };

    component.afterLoadComplete(loadedPdf as PDFDocumentProxy);
    expect(component.maxPages).toBe(loadedPdf.numPages);
  });

  it('should emit loading completed event when pdf.js finished loading', () => {
    // given
    component.loading = true;
    spyOn(component.loadingCompleted, 'emit');

    // when
    component.afterLoadComplete({} as any);

    // then
    expect(component.loading).toBe(false);
    expect(component.loadingCompleted.emit).toHaveBeenCalled();
  });

  it('should stop loading external pdf when error occurred', () => {
    // given
    const sampleTimeoutId = 123;
    component.loading = true;
    spyOn(component.loadingFailed, 'emit');

    // when
    component.onError();

    // then
    expect(component.loading).toBe(false);
    expect(component.loadingFailed.emit).toHaveBeenCalled();
  });

  it('should change page', () => {
    const desiredPage = 10;
    expect(component.currentPage).toBe(1);
    component.pageChange(desiredPage);
    expect(component.currentPage).toBe(desiredPage);
  });

  it('should update rotation', () => {
    const desiredRotation = 90;
    expect(component.rotation).toBe(0);
    component.updateRotation(desiredRotation);
    expect(component.rotation).toBe(desiredRotation);
  });

  it('should select setting', () => {
    const desiredSetting = {} as SettingPreset;
    expect(component.rotation).toBe(0);
    component.settingSelected(desiredSetting);
    expect(component.activeSettings).toBe(desiredSetting);
  });
});
