import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'FILE_DETAILS.EDIT_IN_G_SUITE': 'In G Suite bearbeiten',
    'FILE_DETAILS.GENERATING_PREVIEW': 'Eine Vorschau für dieses Dokument wird erstellt',
    'FILE_DETAILS.OPEN_IN_G_SUITE': 'In G Suite öffnen',
    'FILE_DETAILS.OPEN_IN_G_SUITE.ARIA': 'Diese Datei in Google G Suite ansehen oder bearbeiten.'
  }
};
