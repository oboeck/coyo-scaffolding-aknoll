import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'coyo-file-non-preview',
  templateUrl: './file-non-preview.component.html',
  styleUrls: ['./file-non-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileNonPreviewComponent {

  /**
   * Attachment attached on timeline post
   */
  @Input() attachment: any;

}
