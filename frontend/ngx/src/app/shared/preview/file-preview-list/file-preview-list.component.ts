import {ChangeDetectionStrategy, Component, Inject, Input, OnInit} from '@angular/core';
import {OFFICE365} from '@domain/attachment/storage';
import {CapabilitiesService} from '@domain/capability/capabilities/capabilities.service';
import {Sender} from '@domain/sender/sender';
import {WINDOW} from '@root/injection-tokens';
import {Ng1fileDetailsModalService} from '@root/typings';
import {ExternalFileHandlerService} from '@shared/files/external-file-handler/external-file-handler.service';
import {NG1_FILE_DETAILS_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {Attachment} from 'app/core/domain/attachment/attachment';
import {FilePreview} from 'app/core/domain/preview/file-preview';
import * as _ from 'lodash';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

/**
 * Component for the list of previews on timeline form
 */
@Component({
  selector: 'coyo-file-preview-list',
  templateUrl: './file-preview-list.component.html',
  styleUrls: ['./file-preview-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilePreviewListComponent implements OnInit {

  /**
   * List of attachments attached on timeline post
   */
  @Input() attachments: Attachment[];
  /**
   * Id of the group
   */
  @Input() groupId: string;
  /**
   * Author of the timeline post
   */
  @Input() author: Sender;
  /**
   * Attachment preview Url
   */
  @Input() previewUrl: string;
  /**
   * Attachment view size
   */
  @Input() size: string;
  /**
   * If all attachments have the same view size
   */
  @Input() sameSize: boolean;
  /**
   * Attachment title for preview
   */
  @Input() previewTitle: string;
  /**
   * Attachment title
   */
  @Input() nonPreviewTitle: string;

  url: string = '/web/senders/{{groupId}}/documents/{{id}}';
  previewFiles$: Observable<FilePreview[]>;
  nonPreviewFiles$: Observable<FilePreview[]>;
  filePreviews$: Observable<FilePreview[]>;

  private cannotProcess: string[] = [];

  constructor(
    @Inject(NG1_FILE_DETAILS_MODAL_SERVICE) private fileDetailsModalService: Ng1fileDetailsModalService,
    private capabilitiesService: CapabilitiesService,
    private externalFileHandlerService: ExternalFileHandlerService,
    @Inject(WINDOW) private windowService: Window) {
  }

  ngOnInit(): void {
    this.size = this.size || 'xl';
    this.mapAttachments();
  }

  private mapAttachments(): void {
    this.filePreviews$ = forkJoin(this.attachments.map((attachment: Attachment) =>
      this.capabilitiesService.imgAvailable(attachment.contentType).pipe(map((available: boolean) => {
        const filePreview = this.toFilePreview(attachment);
        filePreview.previewAvailable = this.isLocalStoredAttachment(attachment) ? available : false;
        filePreview.showSize = this.isLocalStoredAttachment(attachment);
        filePreview.deleted = this.isDeletedAttachment(attachment);
        return filePreview;
      })))
    ).pipe(map(previews => _.sortBy(previews, [(preview: FilePreview) => preview.name.toLowerCase(), 'created'])));

    this.previewFiles$ = this.filePreviews$
      .pipe(map(previews => previews.filter(preview => preview.previewAvailable && this.cannotProcess.indexOf(preview.id) === -1)));
    this.nonPreviewFiles$ = this.filePreviews$
      .pipe(map(previews => previews.filter(preview => !preview.previewAvailable || this.cannotProcess.indexOf(preview.id) !== -1)));
  }

  /**
   * Opens attached file details
   *
   * @param attachment Attached file
   * @param $event click event
   */
  showDetails(attachment: Attachment, $event: Event): void {
    $event.preventDefault();
    if (attachment.storage === OFFICE365) {
      this.openExternalFileInNewTab(attachment);
    } else {
      this.openAttachmentInFileDetailsModal(attachment);
    }
  }

  private openAttachmentInFileDetailsModal(attachment: Attachment): void {
    const files = this.getFilePreviewsSortedByName();
    const index = this.getAttachmentIndex(attachment);
    this.fileDetailsModalService.open(files, index, true);
  }

  private openExternalFileInNewTab(attachment: Attachment): void {
    this.externalFileHandlerService
      .getExternalFileDetails(attachment)
      .then(details => this.windowService.open(details.externalUrl, '_blank'));
  }

  private getFilePreviewsSortedByName(): FilePreview[] {
    const filePreviews = this.attachments.map(file => this.toFilePreview(file));
    return _.sortBy(filePreviews, preview => preview.name.toLowerCase());
  }

  private getAttachmentIndex(attachment: Attachment): number {
    const sortedByName = _.sortBy(this.attachments, preview => preview.name.toLowerCase());
    return _.findIndex(sortedByName, file => file.fileId === attachment.fileId || file.id === attachment.id);
  }

  private toFilePreview(attachment: Attachment): FilePreview {
    if (attachment.storage === 'LOCAL_FILE_LIBRARY') {
      return {
        ...attachment,
        id: attachment.fileId,
        attachment: false,
        groupId: attachment.modelId,
        senderId: attachment.modelId,
      };
    } else {
      return {
        ...attachment,
        attachment: true,
        groupId: this.groupId,
        previewUrl: this.previewUrl,
        author: this.author
      };
    }
  }

  /**
   * Maps the attachment in the nonPreviewFiles when preview file cannot be processed
   *
   * @param fileId File Id
   */
  onCannotProcess(fileId: string): void {
    this.cannotProcess.push(fileId);
    this.mapAttachments();
  }

  private isLocalStoredAttachment(attachment: Attachment): boolean {
    return !this.externalFileHandlerService.isExternalFile(attachment);
  }

  private isDeletedAttachment(attachment: Attachment): boolean {
    return !attachment.length && this.isLocalStoredAttachment(attachment);
  }
}
