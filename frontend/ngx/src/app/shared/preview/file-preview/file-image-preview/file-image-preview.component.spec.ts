import {SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {SocketService} from '@core/socket/socket.service';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {GSUITE} from '@domain/attachment/storage';
import {CapabilitiesService} from '@domain/capability/capabilities/capabilities.service';
import {FileService} from '@domain/file/file/file.service';
import {FilePreview} from '@domain/preview/file-preview';
import {of} from 'rxjs';
import {FilePreviewOptions} from '../file-preview-options';
import {FileImagePreviewComponent} from './file-image-preview.component';

describe('FileImagePreviewComponent', () => {
  let component: FileImagePreviewComponent;
  let fixture: ComponentFixture<FileImagePreviewComponent>;
  let windowSizeServiceMock: jasmine.SpyObj<WindowSizeService>;
  let urlServiceMock: jasmine.SpyObj<UrlService>;
  let capabilitiesServiceMock: jasmine.SpyObj<CapabilitiesService>;
  let fileServiceMock: jasmine.SpyObj<FileService>;
  let socketServiceMock: jasmine.SpyObj<SocketService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileImagePreviewComponent],
      providers: [{
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['getScreenSize', 'isXs', 'isSm', 'isMd', 'isLg'])
      }, {
        provide: CapabilitiesService,
        useValue: jasmine.createSpyObj('capabilitiesService', ['previewImageFormat', 'imgAvailable'])
      }, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['join', 'getBackendUrl', 'insertPathVariablesIntoUrl'])
      }, {
        provide: FileService,
        useValue: jasmine.createSpyObj('fileService', ['getPreviewStatus', 'getImagePreviewUrl'])
      }, {
        provide: SocketService,
        useValue: jasmine.createSpyObj('socketService', ['listenTo$'])
      }]
    }).overrideTemplate(FileImagePreviewComponent, '')
      .compileComponents();

    windowSizeServiceMock = TestBed.get(WindowSizeService);
    urlServiceMock = TestBed.get(UrlService);
    urlServiceMock.join.and.callFake((parts: string) => parts.slice(1));
    urlServiceMock.getBackendUrl.and.returnValue('http://localhost:8080');
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');
    capabilitiesServiceMock = TestBed.get(CapabilitiesService);
    capabilitiesServiceMock.previewImageFormat.and.callFake((contentType: string) => 'application/pdf').and.returnValue(of('application/pdf'));
    capabilitiesServiceMock.imgAvailable.and.returnValue(of(true));
    fileServiceMock = TestBed.get(FileService);
    fileServiceMock.getPreviewStatus.and.returnValue(of(''));
    fileServiceMock.getImagePreviewUrl.and
      .returnValue(of('test-url'));
    socketServiceMock = TestBed.get(SocketService);
    socketServiceMock.listenTo$.and.returnValue(of({ content: { status: 'SUCCESS'}}));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileImagePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should gif playback be allowed', () => {
    // given
    component.file = {contentType: 'image/gif'} as FilePreview;
    component.options = {allowGifPlayback: true} as FilePreviewOptions;
    // when
    const result = component.isGifPlaybackAllowed();
    // then
    expect(result).toBe(true);
  });

  it('should image be loaded', () => {
    // when
    component.imageLoaded();
    // then
    expect(component.loading).toBe(false);
    expect(component.loadingGif).toBe(false);
  });

  it('should reset preview on change', () => {
    // given
    const previousFile = {contentType: 'image/png', previewUrl: '/web/timeline-items/{{groupId}}/attachments/{{id}}', id: '54321'} as FilePreview;
    const file = {contentType: 'application/pdf', previewUrl: '/web/timeline-items/{{groupId}}/c/{{id}}', id: '54321'} as FilePreview;
    component.file = file;
    component.groupId = '12345';
    component.size = '';
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/timeline-items/12345/attachments/54321');
    fileServiceMock.getImagePreviewUrl.and
      .returnValue(of('http://localhost:8080/web/timeline-items/12345/attachments/54321?format=application/pdf'));

    // when
    component.ngOnChanges({
      file: new SimpleChange(previousFile, component.file, true)
    });
    fixture.detectChanges();
    // then
    expect(component.isSuccess).toBe(false);
    expect(component.conversionError).toBe(false);
    expect(component.previewAvailable).toBe(true);
    expect(component.isProcessing).toBe(false);
    expect(component.loading).toBe(true);
    expect(component.imageSrc).toBe('http://localhost:8080/web/timeline-items/12345/attachments/54321?format=application/pdf');
    expect(component.imageLoadingHelper.src).toBe(component.imageSrc);

    component.imageLoadingHelper.onload(undefined);
    expect(component.loading).toBe(false);
    expect(component.isProcessing).toBe(false);

    component.imageLoadingHelper.onerror(undefined);
    expect(component.isSuccess).toBe(true);
    expect(component.loading).toBe(false);
    expect(component.conversionError).toBe(false);
    expect(component.isProcessing).toBe(false);
    expect(fileServiceMock.getImagePreviewUrl).toHaveBeenCalledWith(file.previewUrl, component.groupId, file, '');
  });

  it('should set proper google preview image url on mobile screens', () => {
    // given
    const previousFile = {contentType: 'image/png', previewUrl: '/web/timeline-items/{{groupId}}/attachments/{{id}}', id: '54321'} as FilePreview;
    const file = {
      contentType: 'image/jpg',
      previewUrl: 'http://external-google-url.com/image=s220',
      id: '54321',
      storage: GSUITE
    } as FilePreview;
    component.file = file;
    component.size = '';
    windowSizeServiceMock.isXs.and.returnValue(true);
    // when
    component.ngOnChanges({
      file: new SimpleChange(previousFile, component.file, true)
    });
    fixture.detectChanges();
    // then
    expect(component.imageSrc).toBe('http://external-google-url.com/image=w400');
    expect(component.imageLoadingHelper.src).toBe('http://external-google-url.com/image=w400');
  });

  it('should set proper google preview image url on desktop', () => {
    // given
    const previousFile = {contentType: 'image/png', previewUrl: '/web/timeline-items/{{groupId}}/attachments/{{id}}', id: '54321'} as FilePreview;
    const file = {
      contentType: 'image/jpg',
      previewUrl: 'http://external-google-url.com/image=s220',
      id: '54321',
      storage: GSUITE
    } as FilePreview;
    component.file = file;
    component.size = '';
    windowSizeServiceMock.isXs.and.returnValue(false);
    windowSizeServiceMock.isSm.and.returnValue(false);
    // when
    component.ngOnChanges({
      file: new SimpleChange(previousFile, component.file, true)
    });
    fixture.detectChanges();
    // then
    expect(component.imageSrc).toBe('http://external-google-url.com/image=w800');
    expect(component.imageLoadingHelper.src).toBe('http://external-google-url.com/image=w800');
  });

  it('should set cannot process onError ', () => {
    // given
    capabilitiesServiceMock.imgAvailable.and.returnValue(of(false));
    component.file = {contentType: undefined, previewUrl: '/web/timeline-items/{{groupId}}/attachments/{{id}}', id: '54321'} as FilePreview;
    component.groupId = '12345';
    component.size = '';
    fileServiceMock.getPreviewStatus.and.returnValue(of('CANNOT_PROCESS'));

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();
    component.imageLoadingHelper.onerror(undefined);
    // then
    expect(component.loading).toBe(false);
    expect(component.conversionError).toBe(false);
    expect(component.isProcessing).toBe(false);
    expect(component.previewAvailable).toBe(false);
  });

  it('should play GIF', () => {
    // given
    const event = jasmine.createSpyObj('event', ['preventDefault', 'stopPropagation']);
    component.file = {downloadUrl: 'http://localhost:8080/web/timeline-items/12345/attachments/54321?format=image/gif&modified=1549623196982'} as FilePreview;
    component.gifIsRunning = false;
    component.imageSrc = 'http://localhost:8080/web/timeline-items/12345/attachments/54321?format=image/gif&modified=1549623196982';
    // when
    component.onClickGifPlayButton(event);
    // then
    expect(component.gifIsRunning).toBe(true);
    expect(component.imageSrcBackup).toEqual(component.imageSrc);
    expect(component.loadingGif).toBe(true);
    expect(component.imageSrc).toEqual(component.file.downloadUrl);
    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopPropagation).toHaveBeenCalled();

    component.gifIsRunning = true;
    component.onClickGifPlayButton(event);
    expect(component.gifIsRunning).toBe(false);
    expect(component.imageSrc).toBe(component.imageSrcBackup);
  });

  it('should detect that full pdf is shown on mobile', () => {
    // given
    component.options.showPdfMobile = true;
    component.options.showPdfDesktop = false;
    windowSizeServiceMock.isXs.and.returnValue(true);
    // when
    const result = component.isFullPdfShown();
    // then
    expect(result).toBe(true);
  });

  it('should detect that full pdf is shown on desktop', () => {
    // given
    component.options.showPdfMobile = false;
    component.options.showPdfDesktop = true;
    windowSizeServiceMock.isMd.and.returnValue(true);
    // when
    const result = component.isFullPdfShown();
    // then
    expect(result).toBe(true);
  });

  it('should detect that full pdf is not shown on mobile', () => {
    // given
    component.options.showPdfMobile = false;
    component.options.showPdfDesktop = true;
    windowSizeServiceMock.isXs.and.returnValue(true);
    // when
    const result = component.isFullPdfShown();
    // then
    expect(result).toBe(false);
  });

  it('should detect that full pdf is not shown on desktop', () => {
    // given
    component.options.showPdfMobile = true;
    component.options.showPdfDesktop = false;
    windowSizeServiceMock.isMd.and.returnValue(true);
    // when
    const result = component.isFullPdfShown();
    // then
    expect(result).toBe(false);
  });

  it('should not be pdf', () => {
    // given
    component.file = {contentType: 'image/gif'} as FilePreview;
    // when
    const result = component.isPdf();
    // then
    expect(result).toBe(false);
  });

  it('should be pdf', () => {
    // given
    component.file = {contentType: 'application/pdf'} as FilePreview;
    // when
    const result = component.isPdf();
    // then
    expect(result).toBe(true);
  });

  it('should show image preview', () => {
    // given
    component.options.backgroundImage = false;
    component.imageSrc = 'src';
    component.loading = false;
    component.previewAvailable = true;
    component.isProcessing = false;
    // when
    const result = component.shouldShowImgPreview();
    // then
    expect(result).toBe(true);
  });

  it('should not show image preview (backgroundImage true)', () => {
    // given
    component.options.backgroundImage = true;
    component.imageSrc = 'src';
    component.loading = false;
    component.previewAvailable = true;
    component.isProcessing = false;
    // when
    const result = component.shouldShowImgPreview();
    // then
    expect(result).toBe(false);
  });

  it('should not show image preview (loading)', () => {
    // given
    component.options.backgroundImage = false;
    component.imageSrc = 'src';
    component.loading = true;
    component.previewAvailable = true;
    component.isProcessing = false;
    // when
    const result = component.shouldShowImgPreview();
    // then
    expect(result).toBe(false);
  });

  it('should not show image preview (previewAvailable false)', () => {
    // given
    component.options.backgroundImage = false;
    component.imageSrc = 'src';
    component.loading = false;
    component.previewAvailable = false;
    component.isProcessing = false;
    // when
    const result = component.shouldShowImgPreview();
    // then
    expect(result).toBe(false);
  });

  it('should not show image preview (isProcessing)', () => {
    // given
    component.options.backgroundImage = false;
    component.imageSrc = 'src';
    component.loading = false;
    component.previewAvailable = true;
    component.isProcessing = true;
    // when
    const result = component.shouldShowImgPreview();
    // then
    expect(result).toBe(false);
  });

  it('should show background image', () => {
    // given
    component.options.backgroundImage  = true;
    component.imageSrc = 'src';
    component.previewAvailable = true;
    component.isProcessing = false;
    // when
    const result = component.shouldShowBackgroundImg();
    // then
    expect(result).toBe(true);
  });

  it('should not show background image (backgroundImage false)', () => {
    // given
    component.options.backgroundImage  = false;
    component.imageSrc = 'src';
    component.previewAvailable = true;
    component.isProcessing = false;
    // when
    const result = component.shouldShowBackgroundImg();
    // then
    expect(result).toBe(false);
  });

  it('should not show background image (preview not available)', () => {
    // given
    component.options.backgroundImage  = true;
    component.imageSrc = 'src';
    component.previewAvailable = false;
    component.isProcessing = false;
    // when
    const result = component.shouldShowBackgroundImg();
    // then
    expect(result).toBe(false);
  });

  it('should not show background image (isProcessing)', () => {
    // given
    component.options.backgroundImage  = true;
    component.imageSrc = 'src';
    component.previewAvailable = true;
    component.isProcessing = true;
    // when
    const result = component.shouldShowBackgroundImg();
    // then
    expect(result).toBe(false);
  });
});
