/**
 * Contains all relevant information about the options of a file preview
 */
export interface FilePreviewOptions {
  backgroundImage?: boolean;
  allowGifPlayback?: boolean;
  showPdfDesktop?: boolean;
  showPdfMobile?: boolean;
  hideNoPreviewAvailableMsg?: boolean;
  hidePreviewGenerationInformation?: boolean;
}
