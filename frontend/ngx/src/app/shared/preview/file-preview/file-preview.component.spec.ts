import {SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {BrowserService} from '@core/browser/browser.service';
import {UrlService} from '@core/http/url/url.service';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {GSUITE} from '@domain/attachment/storage';
import {CapabilitiesService} from '@domain/capability/capabilities/capabilities.service';
import {FileService} from '@domain/file/file/file.service';
import {FilePreview} from '@domain/preview/file-preview';
import {WINDOW} from '@root/injection-tokens';
import {ExternalFileHandlerService} from '@shared/files/external-file-handler/external-file-handler.service';
import {of} from 'rxjs';
import {FilePreviewOptions} from './file-preview-options';
import {FilePreviewStatus} from './file-preview-status';
import {FilePreviewComponent} from './file-preview.component';

describe('FilePreviewComponent', () => {
  let component: FilePreviewComponent;
  let fixture: ComponentFixture<FilePreviewComponent>;
  let windowSizeServiceMock: jasmine.SpyObj<WindowSizeService>;
  let urlServiceMock: jasmine.SpyObj<UrlService>;
  let externalFileHandlerServiceMock: jasmine.SpyObj<ExternalFileHandlerService>;
  let googleApiService: jasmine.SpyObj<GoogleApiService>;
  let windowService: jasmine.SpyObj<Window>;
  let capabilitiesService: jasmine.SpyObj<CapabilitiesService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilePreviewComponent],
      providers: [
        {
          provide: WindowSizeService,
          useValue: jasmine.createSpyObj('windowSizeService', ['getScreenSize', 'isXs', 'isSm', 'isMd', 'isLg', 'isRetina'])
        },
        {
          provide: UrlService,
          useValue: jasmine.createSpyObj('urlService', ['join', 'getBackendUrl', 'insertPathVariablesIntoUrl'])
        },
        {
          provide: BrowserService,
          useValue: jasmine.createSpyObj('browserService', ['supportsQuicktimeVideo'])
        },
        {
          provide: ExternalFileHandlerService,
          useValue: jasmine.createSpyObj('externalFileHandlerService', ['isGoogleMediaType'])
        },
        {
          provide: GoogleApiService,
          useValue: jasmine.createSpyObj('googleApiService', ['appendAccessTokenParam', 'getFileContentRequestUrl'])
        },
        {
          provide: WINDOW,
          useValue: jasmine.createSpyObj('windowService', ['setTimeout', 'clearTimeout'])
        },
        {
          provide: CapabilitiesService,
          useValue: jasmine.createSpyObj('capabilitiesService', ['imgAvailable'])
        },
        {
          provide: FileService,
          useValue: jasmine.createSpyObj('fileService', ['getImagePreviewUrl'])
        }
      ]
    }).overrideTemplate(FilePreviewComponent, '<div></div>')
      .compileComponents();

    windowSizeServiceMock = TestBed.get(WindowSizeService);
    urlServiceMock = TestBed.get(UrlService);
    urlServiceMock.join.and.callFake((parts: string) => parts.slice(1));
    urlServiceMock.getBackendUrl.and.returnValue('http://localhost:8080');
    externalFileHandlerServiceMock = TestBed.get(ExternalFileHandlerService);
    externalFileHandlerServiceMock.isGoogleMediaType.and.returnValue(false);
    googleApiService = TestBed.get(GoogleApiService);
    windowService = TestBed.get(WINDOW);
    capabilitiesService = TestBed.get(CapabilitiesService);
    capabilitiesService.imgAvailable.and.returnValue(of(false));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update status', () => {
    // given
    const status = {
      loading: false,
      isProcessing: false,
      conversionError: false,
      previewAvailable: false
    } as FilePreviewStatus;

    component.options = {hidePreviewGenerationInformation: false} as FilePreviewOptions;
    // when
    component.onStatusUpdated(status);
    // then
    expect(component.loading).toBe(false);
    expect(component.isProcessing).toBe(false);
    expect(component.conversionFailed).toBe(false);
    expect(component.previewAvailable).toBe(false);
  });

  it('should fallback to preview unavailable status for unsupported file types', () => {
    // given
    component.url = '/web/senders/{{groupId}}/documents/{{id}}';
    component.groupId = '12345';
    component.file = {contentType: 'application/zip', id: '54321', fileId: '54321'} as FilePreview;
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.previewAvailable).toBe(false);
  });

  it('should fallback to preview unavailable status for unsupported files stored at Google', () => {
    // given
    component.url = '/web/senders/{{groupId}}/documents/{{id}}';
    component.groupId = '12345';
    component.file = {contentType: 'application/zip', id: '54321', fileId: '54321', storage: GSUITE} as FilePreview;
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.previewAvailable).toBe(false);
  });

  it('should enable cover image for gsuite pdfs on mobile', () => {
    // given
    component.url = '/link/to/google/pdf';
    component.file = {contentType: 'application/pdf', storage: GSUITE, previewUrl: '/pdf/preview/url'} as FilePreview;
    component.options = {showPdfMobile: false};
    spyOn(component, 'shouldShowPdfOnDesktop').and.returnValue(false);
    spyOn(component, 'shouldShowPdfOnMobile').and.returnValue(false);
    windowSizeServiceMock.isXs.and.returnValue(true);
    googleApiService.getFileContentRequestUrl.and.returnValue(of('http://someExportUrl?access_token=WITH_TOKEN'));
    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.previewAvailable).toBe(true);
  });

  it('should show image previews for image type', () => {
    // given
    spyOn(component, 'previewIsProcessing').and.returnValue(false);
    component.previewAvailable = true;
    component.file = {contentType: 'image/png', id: '54321', fileId: '54321', length: 123} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when
    const result = component.showImagePreview();

    // then
    let called = false;
    result.subscribe(show => {
      expect(show).toBeTruthy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should show image previews for pdf type (used for cover image of the pdf preview)', () => {
    // given
    spyOn(component, 'previewIsProcessing').and.returnValue(false);
    component.previewAvailable = true;
    component.file = {contentType: 'application/pdf', id: '54321', fileId: '54321', length: 123} as FilePreview;

    // when
    const result = component.showImagePreview();

    // then
    expect(result).toBeTruthy();
  });

  it('should not show image previews for image type when preview is unavailable', () => {
    // given
    component.previewAvailable = false;
    component.file = {contentType: 'image/png', id: '54321', fileId: '54321', length: 123} as FilePreview;

    // when
    const result = component.showImagePreview();

    // then
    let called = false;
    result.subscribe(show => {
      expect(show).toBeFalsy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should not show image previews for image type when preview is processing', () => {
    // given
    component.previewAvailable = true;
    spyOn(component, 'previewIsProcessing').and.returnValue(true);
    component.file = {contentType: 'image/png', id: '54321', fileId: '54321', length: 123} as FilePreview;

    // when
    const result = component.showImagePreview();

    // then
    let called = false;
    result.subscribe(show => {
      expect(show).toBeFalsy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should enable preview for images', () => {
    // given
    component.url = '/web/senders/{{groupId}}/documents/{{id}}';
    component.groupId = '12345';
    component.file = {contentType: 'image/png', id: '54321', fileId: '54321', length: 123} as FilePreview;
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.previewAvailable).toBe(true);
  });

  it('should show video preview', () => {
    // given
    component.previewAvailable = true;
    component.videoType = true;

    // when
    const result = component.showVideoPreview();

    // then
    expect(result).toBeTruthy();
  });

  it('should not show video preview for non-video types', () => {
    // given
    component.previewAvailable = true;
    component.videoType = false;

    // when
    const result = component.showVideoPreview();

    // then
    expect(result).toBeFalsy();
  });

  it('should not show video preview if preview is unavailable', () => {
    // given
    component.previewAvailable = false;
    component.videoType = true;

    // when
    const result = component.showVideoPreview();

    // then
    expect(result).toBeFalsy();
  });

  it('should enable preview for videos of type mp4', () => {
    // given
    component.url = '/web/senders/{{groupId}}/documents/{{id}}';
    component.groupId = '12345';
    component.file = {contentType: 'video/mp4', id: '54321', fileId: '54321', length: 123} as FilePreview;
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.previewAvailable).toBe(true);
    expect(component.videoType).toBe(true);
  });

  it('should enable preview for videos of type quicktime when browser supports quicktime', () => {
    // given
    component.url = '/web/senders/{{groupId}}/documents/{{id}}';
    component.groupId = '12345';
    component.file = {contentType: 'video/quicktime', id: '54321', fileId: '54321', length: 123} as FilePreview;
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');
    const browserService = TestBed.get(BrowserService);
    browserService.supportsQuicktimeVideo.and.returnValue(true);
    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.previewAvailable).toBe(true);
    expect(component.videoType).toBe(true);
  });

  it('should disable preview for videos of type quicktime when browser does not support quicktime', () => {
    // given
    component.url = '/web/senders/{{groupId}}/documents/{{id}}';
    component.groupId = '12345';
    component.file = {contentType: 'video/quicktime', id: '54321', fileId: '54321', length: 123} as FilePreview;
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');
    const browserService = TestBed.get(BrowserService);
    browserService.supportsQuicktimeVideo.and.returnValue(false);

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.previewAvailable).toBe(false);
    expect(component.videoType).toBe(false);
  });

  it('should stop loading when PDF viewer has completed loading', () => {
    // when
    component.pdfLoadingCompleted();

    // then
    expect(component.loading).toBe(false);
  });

  it('should stop loading when PDF viewer failed loading', () => {
    // given
    component.loading = true;

    // when
    component.pdfLoadingFailed();

    // then
    expect(component.loading).toBe(false);
  });

  it('should generate pdf url on change', () => {
    // given
    component.url = '/web/senders/{{groupId}}/documents/{{id}}';
    component.groupId = '12345';
    component.file = {contentType: 'application/pdf', id: '54321', fileId: '54321'} as FilePreview;
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.pdfSrc).toBe('http://localhost:8080/web/senders/12345/documents/54321?');
  });

  it('should use pdf export url with access token for GSuite files on change', () => {
    // given
    externalFileHandlerServiceMock.isGoogleMediaType.and.returnValue(true);
    component.file = {
      storage: GSUITE,
      contentType: 'someGoogleMediaType',
      exportLinks: {'application/pdf': 'http://someExportUrl'},
      previewUrl: 'http://somePreviewUrl'
    } as FilePreview;
    googleApiService.appendAccessTokenParam.and.returnValue(of('http://someExportUrl?access_token=WITH_TOKEN'));

    // when
    component.ngOnChanges({file: new SimpleChange(null, component.file, true)});
    fixture.detectChanges();

    // then
    expect(component.pdfSrc).toBe('http://someExportUrl?access_token=WITH_TOKEN');
  });

  it('should show PDF on desktop', () => {
    // given
    component.previewAvailable = true;
    component.options = {showPdfMobile: true};
    component.file = {contentType: 'application/pdf'} as FilePreview;
    component.pdfPreviewAvailable = true;
    spyOn(component, 'shouldShowPdfOnDesktop').and.returnValue(true);
    spyOn(component, 'shouldShowPdfOnMobile').and.returnValue(false);

    // when
    const shouldShowPdfPreview = component.showPdfPreview();

    // then
    expect(shouldShowPdfPreview).toBeTruthy();
  });

  it('should show PDF on mobile', () => {
    // given
    component.previewAvailable = true;
    component.options = {showPdfMobile: true};
    component.file = {contentType: 'application/pdf'} as FilePreview;
    component.pdfPreviewAvailable = true;
    spyOn(component, 'shouldShowPdfOnDesktop').and.returnValue(false);
    spyOn(component, 'shouldShowPdfOnMobile').and.returnValue(true);

    // when
    const shouldShowPdfPreview = component.showPdfPreview();

    // then
    expect(shouldShowPdfPreview).toBeTruthy();
  });

  it('should not show PDF', () => {
    // given
    component.previewAvailable = false;
    component.options = {showPdfMobile: false};
    component.file = {contentType: 'application/pdf'} as FilePreview;
    component.pdfPreviewAvailable = true;
    spyOn(component, 'shouldShowPdfOnDesktop').and.returnValue(false);
    spyOn(component, 'shouldShowPdfOnMobile').and.returnValue(false);

    // when
    const shouldShowPdfPreview = component.showPdfPreview();

    // then
    expect(shouldShowPdfPreview).toBeFalsy();
  });

  it('should show PDF on desktop', () => {
    // given
    component.options = {showPdfDesktop: true} as FilePreviewOptions;
    windowSizeServiceMock.isMd.and.returnValue(true);
    component.pdfPreviewAvailable = true;
    component.file = {contentType: 'application/pdf'} as FilePreview;
    // when
    const result = component.shouldShowPdfOnDesktop();
    // then
    expect(result).toBe(true);
  });

  it('should not show PDF on desktop (file is not PDF)', () => {
    // given
    component.options = {showPdfDesktop: true} as FilePreviewOptions;
    windowSizeServiceMock.isMd.and.returnValue(true);
    component.pdfPreviewAvailable = false;
    component.file = {contentType: 'image/jpeg'} as FilePreview;
    // when
    const result = component.shouldShowPdfOnDesktop();
    // then
    expect(result).toBe(false);
  });

  it('should not show PDF on desktop (option false)', () => {
    // given
    component.options = {showPdfDesktop: false} as FilePreviewOptions;
    windowSizeServiceMock.isMd.and.returnValue(true);
    component.pdfPreviewAvailable = true;
    component.file = {contentType: 'application/pdf'} as FilePreview;
    // when
    const result = component.shouldShowPdfOnDesktop();
    // then
    expect(result).toBe(false);
  });

  it('should not show PDF on desktop (screenSize not valid)', () => {
    // given
    component.options = {showPdfDesktop: true} as FilePreviewOptions;
    windowSizeServiceMock.isMd.and.returnValue(false);
    windowSizeServiceMock.isLg.and.returnValue(false);
    component.pdfPreviewAvailable = true;
    component.file = {contentType: 'application/pdf'} as FilePreview;
    // when
    const result = component.shouldShowPdfOnDesktop();
    // then
    expect(result).toBe(false);
  });

  it('should show PDF on mobile', () => {
    // given
    component.options = {showPdfMobile: true} as FilePreviewOptions;
    windowSizeServiceMock.isXs.and.returnValue(true);
    component.pdfPreviewAvailable = true;
    component.file = {contentType: 'application/pdf'} as FilePreview;
    // when
    const result = component.shouldShowPdfOnMobile();
    // then
    expect(result).toBe(true);
  });

  it('should not show PDF on mobile (file is not PDF)', () => {
    // given
    component.options = {showPdfMobile: true} as FilePreviewOptions;
    windowSizeServiceMock.isXs.and.returnValue(true);
    component.pdfPreviewAvailable = false;
    component.file = {contentType: 'image/jpeg'} as FilePreview;
    // when
    const result = component.shouldShowPdfOnMobile();
    // then
    expect(result).toBe(false);
  });

  it('should not show PDF on mobile (option false)', () => {
    // given
    component.options = {showPdfMobile: false} as FilePreviewOptions;
    windowSizeServiceMock.isXs.and.returnValue(true);
    component.pdfPreviewAvailable = true;
    component.file = {contentType: 'application/pdf'} as FilePreview;
    // when
    const result = component.shouldShowPdfOnMobile();
    // then
    expect(result).toBe(false);
  });

  it('should not show PDF on mobile (screenSize not valid)', () => {
    // given
    component.options = {showPdfMobile: true} as FilePreviewOptions;
    windowSizeServiceMock.isXs.and.returnValue(false);
    windowSizeServiceMock.isSm.and.returnValue(false);
    component.pdfPreviewAvailable = true;
    component.file = {contentType: 'application/pdf'} as FilePreview;
    // when
    const result = component.shouldShowPdfOnMobile();
    // then
    expect(result).toBe(false);
  });

  it('should pdf be ready', () => {
    // given
    component.pdfSrc = 'src';
    component.pdfPreviewAvailable = true;
    component.file = {contentType: 'application/pdf'} as FilePreview;
    component.isProcessing = false;
    // when
    const result = component.pdfShouldBeReady();
    // then
    expect(result).toBe(true);
  });

  it('should pdf not be ready', () => {
    // given
    component.pdfSrc = 'src';
    component.pdfPreviewAvailable = false;
    component.file = {contentType: 'image/png'} as FilePreview;
    component.isProcessing = false;
    // when
    const result = component.pdfShouldBeReady();
    // then
    expect(result).toBe(false);
  });

  it('should pdf be processing', () => {
    // given
    component.pdfSrc = 'src';
    component.pdfPreviewAvailable = true;
    component.file = {contentType: 'application/pdf'} as FilePreview;
    component.isProcessing = true;
    // when
    const result = component.pdfShouldBeReady();
    // then
    expect(result).toBe(false);
  });

  it('should be processing', () => {
    // given
    component.options = {hidePreviewGenerationInformation: false} as FilePreviewOptions;
    component.conversionFailed = false;
    component.loading = false;
    component.isProcessing = true;
    component.previewAvailable = true;
    // when
    const result = component.previewIsProcessing();
    // then
    expect(result).toBe(true);
  });

  it('should not be processing (hidePreviewGenerationInformation)', () => {
    // given
    component.options = {hidePreviewGenerationInformation: true} as FilePreviewOptions;
    component.conversionFailed = false;
    component.loading = false;
    component.isProcessing = true;
    component.previewAvailable = true;
    // when
    const result = component.previewIsProcessing();
    // then
    expect(result).toBe(false);
  });

  it('should not be processing (conversionFailed)', () => {
    // given
    component.options = {hidePreviewGenerationInformation: false} as FilePreviewOptions;
    component.conversionFailed = true;
    component.loading = false;
    component.isProcessing = true;
    component.previewAvailable = true;
    // when
    const result = component.previewIsProcessing();
    // then
    expect(result).toBe(false);
  });

  it('should not be processing (loading)', () => {
    // given
    component.options = {hidePreviewGenerationInformation: false} as FilePreviewOptions;
    component.conversionFailed = false;
    component.loading = true;
    component.isProcessing = true;
    component.previewAvailable = true;
    // when
    const result = component.previewIsProcessing();
    // then
    expect(result).toBe(false);
  });

  it('should not be processing (is not processing)', () => {
    // given
    component.options = {hidePreviewGenerationInformation: false} as FilePreviewOptions;
    component.conversionFailed = false;
    component.loading = false;
    component.isProcessing = false;
    component.previewAvailable = true;
    // when
    const result = component.previewIsProcessing();
    // then
    expect(result).toBe(false);
  });

  it('should not be processing (preview not available)', () => {
    // given
    component.options = {hidePreviewGenerationInformation: false} as FilePreviewOptions;
    component.conversionFailed = false;
    component.loading = false;
    component.isProcessing = true;
    component.previewAvailable = false;
    // when
    const result = component.previewIsProcessing();
    // then
    expect(result).toBe(false);
  });

  it('should not fail (preview not available)', () => {
    // given
    component.previewAvailable = false;
    component.conversionFailed = true;
    // when
    const result = component.previewError();
    // then
    expect(result).toBe(false);
  });

  it('should not fail (no error)', () => {
    // given
    component.previewAvailable = true;
    component.conversionFailed = false;
    // when
    const result = component.previewError();
    // then
    expect(result).toBe(false);
  });

  it('should return true if preview is available', () => {
    // given
    component.previewAvailable = true;
    component.loading = false;
    component.file = {} as FilePreview;
    // when
    const result = component.isPreviewAvailable();
    // then
    expect(result).toBe(true);
  });

  it('should return false if preview is not available', () => {
    // given
    component.previewAvailable = false;
    component.loading = false;
    component.file = {} as FilePreview;
    // when
    const result = component.isPreviewAvailable();
    // then
    expect(result).toBe(false);
  });

  it('should return false if loading', () => {
    // given
    component.previewAvailable = true;
    component.loading = true;
    component.file = {} as FilePreview;
    // when
    const result = component.isPreviewAvailable();
    // then
    expect(result).toBe(false);
  });

  it('should return false if no file present', () => {
    // given
    component.previewAvailable = true;
    component.loading = false;
    component.file = undefined;
    // when
    const result = component.isPreviewAvailable();
    // then
    expect(result).toBe(false);
  });

  it('should show preview generation information', () => {
    // given
    component.options = {hidePreviewGenerationInformation: false} as FilePreviewOptions;
    // when
    const result = component.showPreviewGenerationInformation();
    // then
    expect(result).toBe(true);
  });

  it('should not show preview generation information', () => {
    // given
    component.options = {hidePreviewGenerationInformation: true} as FilePreviewOptions;
    // when
    const result = component.showPreviewGenerationInformation();
    // then
    expect(result).toBe(false);
  });

  it('should get preview size lg as default', () => {
    // given
    component.size = null;
    windowSizeServiceMock.isRetina.and.returnValue(false);

    // when
    const result = component.getPreviewSize();

    // then
    expect(result).toBe('L');
  });

  it('should show emit cannotProcess when preview generation info is hidden and a conversion error occurs', () => {
    // given
    const id = 'id';
    const spy = jasmine.createSpy('subscribe spy');
    component.file = {id: id} as FilePreview;
    component.options = {hidePreviewGenerationInformation: true} as FilePreviewOptions;
    component.cannotProcess.subscribe(spy);

    // when
    component.onStatusUpdated({conversionError: true} as FilePreviewStatus);

    // then
    expect(spy).toHaveBeenCalledWith(id);
  });

  it('should set loading timeout for external files with thumbnail', () => {
    // given
    component.file = {
      storage: GSUITE,
      contentType: 'some google media type',
      previewUrl: 'http://somePreviewUrl'
    } as FilePreview;
    windowService.setTimeout.and.callFake(() =>
      123);

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(windowService.setTimeout).toHaveBeenCalledWith(jasmine.any(Function), 60000);
    expect(component.externalFileLoadingTimeout).toBeDefined();
  });

  it('should clear loading timeout for external files on destroy', () => {
    // given
    component.externalFileLoadingTimeout = 123;

    // when
    component.ngOnDestroy();

    // then
    expect(windowService.clearTimeout).toHaveBeenCalled();
  });

  it('should return false for GSuite images', () => {
    // given
    component.interactiveImageView = true;
    component.file = {
      contentType: 'image',
      id: '54321',
      fileId: '54321',
      length: 1231,
      storage: 'G_SUITE'
    } as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when
    const result = component.isFullResImageOverlayAvailable();

    // then
    let called = false;
    result.subscribe(available => {
      expect(available).toBeFalsy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should return true for image file', () => {
    // given
    component.interactiveImageView = true;
    component.file = {
      contentType: 'image',
      id: '54321',
      fileId: '54321',
      length: 1231,
      storage: 'LOCAL_BLOB_STORAGE'
    } as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when
    const result = component.isFullResImageOverlayAvailable();

    // then
    let called = false;
    result.subscribe(available => {
      expect(available).toBeTruthy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should return true if is not mobile', () => {
    // given
    component.interactiveImageView = true;
    component.file = {
      contentType: 'image',
      id: '54321',
      fileId: '54321',
      length: 1231,
      storage: 'LOCAL_BLOB_STORAGE'
    } as FilePreview;
    windowSizeServiceMock.isXs.and.returnValue(false);
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when
    const result = component.showFullResImageOverlay();

    // then
    let called = false;
    result.subscribe(available => {
      expect(available).toBeTruthy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should return true if overlay already open', () => {
    // given
    component.interactiveImageView = true;
    component.fullResImageOverlayIsOpen = true;
    component.file = {
      contentType: 'image',
      id: '54321',
      fileId: '54321',
      length: 1231,
      storage: 'LOCAL_BLOB_STORAGE'
    } as FilePreview;
    windowSizeServiceMock.isXs.and.returnValue(true);
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when
    const result = component.showFullResImageOverlay();

    // then
    let called = false;
    result.subscribe(available => {
      expect(available).toBeTruthy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should ask capability service for deciding if it is a image type', () => {
    // given
    const contentType = 'document/word';
    spyOn(component, 'previewIsProcessing').and.returnValue(false);
    component.file = {contentType: contentType, id: '54321', fileId: '54321', length: 123} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.previewAvailable).toBeTruthy();
    expect(capabilitiesService.imgAvailable).toHaveBeenCalledWith(contentType);
  });

  it('should not set image overlay as available on conversion error', () => {
    // given
    component.interactiveImageView = true;
    component.conversionFailed = true;
    component.file = {
      contentType: 'image',
      id: '54321',
      fileId: '54321',
      length: 1231,
      storage: 'LOCAL_BLOB_STORAGE'
    } as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when
    const result = component.isFullResImageOverlayAvailable();

    // then
    let called = false;
    result.subscribe(available => {
      expect(available).toBeFalsy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should not show image preview for pdf files', () => {
    // given
    const contentType = 'application/pdf';
    spyOn(component, 'previewIsProcessing').and.returnValue(false);
    component.file = {contentType: contentType, id: '54321', fileId: '54321', length: 123} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));
    component.previewAvailable = true;
    component.options = {showPdfDesktop: true};
    windowSizeServiceMock.isMd.and.returnValue(true);

    // when
    const result = component.showImagePreview();

    // then
    let called = false;
    result.subscribe(showImagePreview => {
      expect(showImagePreview).toBeFalsy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should not set image overlay as available for pdf files', () => {
    // given
    component.interactiveImageView = true;
    component.conversionFailed = false;
    component.file = {
      contentType: 'application/pdf',
      id: '54321',
      fileId: '54321',
      length: 1231,
      storage: 'LOCAL_BLOB_STORAGE'
    } as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when
    const result = component.isFullResImageOverlayAvailable();

    // then
    let called = false;
    result.subscribe(available => {
      expect(available).toBeFalsy();
      called = true;
    });
    expect(called).toBeTruthy();
  });
});
