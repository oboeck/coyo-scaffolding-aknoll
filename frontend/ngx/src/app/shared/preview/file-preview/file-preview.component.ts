import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges
} from '@angular/core';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {BrowserService} from '@core/browser/browser.service';
import {UrlService} from '@core/http/url/url.service';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {GSUITE} from '@domain/attachment/storage';
import {CapabilitiesService} from '@domain/capability/capabilities/capabilities.service';
import {FileService} from '@domain/file/file/file.service';
import {FilePreview} from '@domain/preview/file-preview';
import {WINDOW} from '@root/injection-tokens';
import {ExternalFileHandlerService} from '@shared/files/external-file-handler/external-file-handler.service';
import {Observable, of, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {FilePreviewOptions} from './file-preview-options';
import {FilePreviewStatus} from './file-preview-status';

/**
 * This Component handles the preview for pdf and video files.
 *
 * ChangeDetection.onPush has been removed in order to detect changes on the
 * Input "file" even if the reference has not changed.
 */
@Component({
  selector: 'coyo-file-preview',
  templateUrl: './file-preview.component.html',
  styleUrls: ['./file-preview.component.scss']
})
export class FilePreviewComponent implements OnDestroy, OnChanges {

  static readonly TIMEOUT_FOR_EXTERNAL_FILE_LOADING: number = 60000;

  /**
   * Attached file
   */
  @Input() file: FilePreview;
  /**
   * File preview url
   */
  @Input() url: string;
  /**
   * File groupId
   */
  @Input() groupId: string;
  /**
   * File preview size
   */
  @Input() size: string = 'lg';
  /**
   * File options
   */
  @Input() options: FilePreviewOptions;
  /**
   * Enable fullscreen pan/zoom view on click
   */
  @Input() interactiveImageView: boolean = false;
  /**
   * If true, the file preview animation (coyo-file-preview-generating-animation) will be displayed while loading the preview.
   * Otherwise the default spinner (coyo-spinner) will be shown.
   */
  @Input() usePreviewGeneratingAnimation: boolean = false;
  /**
   * Event emitted by image-preview when preview cannot be processed
   */
  @Output() cannotProcess: EventEmitter<string> = new EventEmitter<string>();

  loading: boolean = true;
  conversionFailed: boolean = false;
  previewAvailable: boolean = false;
  isProcessing: boolean = false;
  videoType: boolean = false;
  pdfPreviewAvailable: boolean = false;
  pdfSrc: string;
  fullResImageOverlayIsOpen: boolean = false;
  externalFileLoadingTimeout: number;

  /**
   * Defines the translation from preview sizes (bootstrap style) to available image sizes in backend.
   */
  readonly imageSizes: {
    [key: string]: {
      sd: 'XS' | 'S' | 'M' | 'L' | 'XL' | 'XXL' | '',
      retina: 'XS' | 'S' | 'M' | 'L' | 'XL' | 'XXL' | ''
    }
  } = {
    xs: {sd: 'XS', retina: 'S'},
    sm: {sd: 'S', retina: 'M'},
    md: {sd: 'M', retina: 'L'},
    lg: {sd: 'L', retina: 'XL'},
    xl: {sd: 'XL', retina: 'XXL'},
    original: {sd: '', retina: ''}
  };

  private statusSubscription: Subscription;

  constructor(private windowSizeService: WindowSizeService,
              private urlService: UrlService,
              private browserService: BrowserService,
              private externalFileHandlerService: ExternalFileHandlerService,
              private googleApiService: GoogleApiService,
              private fileService: FileService,
              @Inject(WINDOW) private windowService: Window,
              private capabilitiesService: CapabilitiesService,
              private cd: ChangeDetectorRef) {
  }

  get downloadUrl(): string {
    if (!this.file) {
      return null;
    }
    const url = this.file.downloadUrl || this.urlService.insertPathVariablesIntoUrl(this.urlService.getBackendUrl() + (this.url || this.file.previewUrl), {
      groupId: this.file.groupId || this.file.senderId,
      id: this.file.id
    });
    return url;
  }

  ngOnDestroy(): void {
    if (this.statusSubscription) {
      this.statusSubscription.unsubscribe();
    }
    if (this.externalFileLoadingTimeout) {
      this.windowService.clearTimeout(this.externalFileLoadingTimeout);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const currentFile = changes.file && changes.file.currentValue;
    if (this.shouldShowPdfOnMobile() && this.isMobile()) {
      this.previewAvailable = true;
      this.pdfPreviewAvailable = true;
      this.loading = true;
    }
    if (!!currentFile) {
      this.reset();
      if (this.isPdfFile(currentFile) && !this.isGSuiteFile(currentFile)) {
        this.handleCoyoPdfFiles();
      } else if (this.isVideoTypeSupported(currentFile.contentType) || this.isGSuiteVideo(currentFile)) {
        this.handleVideoFiles();
      } else if (this.isGSuiteFile(currentFile) && currentFile.previewUrl) {
        this.setExternalFileTimeout();
        if (this.mayCreatePreviewForGSuiteFile(currentFile)) {
          this.googleApiService.appendAccessTokenParam(currentFile.exportLinks['application/pdf'])
            .subscribe(url => {
              this.pdfSrc = url;
            });
          this.pdfPreviewAvailable = true;
          this.previewAvailable = true;
        } else if (this.isGsuiteImageFile(currentFile)) {
          this.previewAvailable = true;
        } else if (this.isPdfFile(currentFile)) {
          this.handleGSuitePdfFiles(currentFile);
        } else {
          this.setPreviewUnavailable();
        }
      } else {
        this.isImageFile(currentFile).subscribe(isImage => {
          if (isImage) {
            this.previewAvailable = true;
          } else {
            this.setPreviewUnavailable();
          }
          this.cd.detectChanges();
        });
      }
    }
  }

  /**
   * Updates component status variables
   *
   * @param status FilePreviewStatus interface
   */
  onStatusUpdated(status: FilePreviewStatus): void {
    this.loading = status.loading;
    this.isProcessing = status.isProcessing;
    this.previewAvailable = status.previewAvailable;
    this.conversionFailed = status.conversionError;
    if (this.options.hidePreviewGenerationInformation && status.conversionError) {
      this.cannotProcess.emit(this.file.id);
    }
  }

  /**
   * Returns the image size (retina or sd).
   *
   * @returns size The image size (retina or sd)
   */
  getPreviewSize(): 'XS' | 'S' | 'M' | 'L' | 'XL' | 'XXL' | '' {
    const imageSize = this.imageSizes[this.size] || this.imageSizes.lg;
    return this.windowSizeService.isRetina() ? imageSize.retina : imageSize.sd;
  }

  /**
   * Opens a full resolution overlay for images stored in COYO on mobile
   */
  openFullResImageOverlayOnMobile(): void {
    if (this.isImageFile(this.file) && this.isMobile() && !this.isGSuiteFile(this.file)) {
      this.fullResImageOverlayIsOpen = true;
    }
  }

  /**
   * Closes the full resolution overlay
   */
  closeFullResImageOverlay(): void {
    this.fullResImageOverlayIsOpen = false;
  }

  /**
   * Check if the interactive full resolution image view should be displayed:
   * Always on desktop, only if it was opened on mobile
   *
   * @returns true or false
   */
  showFullResImageOverlay(): Observable<boolean> {
    return this.isFullResImageOverlayAvailable().pipe(map(available => (!this.isMobile() ||
      this.fullResImageOverlayIsOpen) && available));
  }

  /**
   * Check if the interactive full resolution image view is available
   * Currently only available for uploaded images
   *
   * @returns true or false
   */
  isFullResImageOverlayAvailable(): Observable<boolean> {
    return this.isImageFile(this.file)
      .pipe(map(imageAvailable => this.interactiveImageView && imageAvailable && !this.conversionFailed &&
        !this.isGSuiteFile(this.file) && !this.previewError() && !this.isPdfFile(this.file)));
  }

  /**
   * Check if pdf can be shown on desktop
   *
   * @returns true or false
   */
  shouldShowPdfOnDesktop(): boolean {
    return (this.windowSizeService.isMd() || this.windowSizeService.isLg()) &&
      (this.options.showPdfDesktop && this.isPdfPreview());
  }

  /**
   * Check if pdf can be show on mobile
   *
   * @returns true or false
   */
  shouldShowPdfOnMobile(): boolean {
    return this.isMobile() && this.options.showPdfMobile && this.isPdfPreview();
  }

  /**
   * Check if pdf file is ready to be displayed
   *
   * @returns true or false
   */
  pdfShouldBeReady(): boolean {
    return this.pdfSrc && this.isPdfPreview() && !this.isProcessing;
  }

  /**
   * Check if preview is processing
   *
   * @returns true or false
   */
  previewIsProcessing(): boolean {
    return this.showPreviewGenerationInformation() && !this.conversionFailed && !this.loading && this.isProcessing && this.previewAvailable;
  }

  /**
   * Check if preview conversion failed
   *
   * @returns true or false
   */
  previewError(): boolean {
    return this.previewAvailable && this.conversionFailed;
  }

  /**
   * Check if the preview is not available
   *
   * @returns true or false
   */
  isPreviewAvailable(): boolean {
    return !!this.file && this.previewAvailable && !this.loading;
  }

  /**
   * Check if the preview generation information should be displayed
   *
   * @returns true or false
   */
  showPreviewGenerationInformation(): boolean {
    return !this.options.hidePreviewGenerationInformation;
  }

  /**
   * Gets triggered when the pdf viewer component has finished loading the pdf.
   */
  pdfLoadingCompleted(): void {
    this.loading = false;
  }

  /**
   * Gets triggered when the pdf viewer component could not load the pdf.
   */
  pdfLoadingFailed(): void {
    this.setPreviewUnavailable();
  }

  /**
   * Check if file is of type image or pdf (for rendering pdf cover image) and full resolution view is not opened
   *
   * @returns true or false
   */
  showImagePreview(): Observable<boolean> {
    return this.isImageFile(this.file).pipe(map(isImage => (this.previewAvailable && !this.previewIsProcessing()
      && (isImage || this.showPdfCoverImage()) && !this.showPdfPreview())));
  }

  /**
   * Check if file is of type video and the preview is ready to show
   *
   * @returns true or false
   */
  showVideoPreview(): boolean {
    return this.previewAvailable && this.videoType;
  }

  /**
   * Check if file is of type pdf and ready to load
   *
   * @returns true or false
   */
  showPdfPreview(): boolean {
    return this.previewAvailable && (this.shouldShowPdfOnMobile() || this.shouldShowPdfOnDesktop());
  }

  getImagePreviewUrl(): Observable<string> {
    return this.fileService.getImagePreviewUrl(this.file.previewUrl || this.url, this.groupId, this.file,
      this.getPreviewSize());
  }

  private createPdfUrl(url: string, groupId: string, fileId: string, modified: Date): string {
    const baseUrl = this.urlService.getBackendUrl()
      + this.urlService.insertPathVariablesIntoUrl(url, {groupId, id: fileId});
    const _modified = modified ? ('modified=' + modified) : '';
    const _amp = modified && (this.file.contentType !== 'application/pdf') ? '&' : '';
    const _format = (this.file.contentType !== 'application/pdf') ? ('format=application/pdf') : '';
    return baseUrl + (baseUrl.indexOf('?') < 0 ? '?' : '&') + _format + _amp + _modified;
  }

  private isVideoTypeSupported(contentType: string): boolean {
    if (contentType === 'video/quicktime') {
      return this.browserService.supportsQuicktimeVideo();
    } else {
      return contentType === 'video/mp4';
    }
  }

  private isGSuiteVideo(file: FilePreview): boolean {
    return this.isGSuiteFile(file) && this.isVideoType(file.contentType);
  }

  private isVideoType(contentType: string): boolean {
    return contentType.startsWith('video/');
  }

  private reset(): void {
    this.closeFullResImageOverlay();
    this.videoType = false;
    this.previewAvailable = false;
    this.loading = true;
    this.pdfSrc = undefined;
    this.pdfPreviewAvailable = false;
  }

  private mayCreatePreviewForGSuiteFile(currentFile: FilePreview): boolean {
    return this.externalFileHandlerService.isGoogleMediaType(currentFile.contentType)
      && currentFile.exportLinks && !!currentFile.exportLinks['application/pdf'];
  }

  private setPreviewUnavailable(): void {
    this.loading = false;
    this.previewAvailable = false;
    this.videoType = false;
  }

  private isMobile(): boolean {
    return this.windowSizeService.isXs() || this.windowSizeService.isSm();
  }

  private isPdfPreview(): boolean {
    return this.pdfPreviewAvailable || this.isPdfFile(this.file);
  }

  private isImageFile(file: FilePreview): Observable<boolean> {
    if (!file || !file.length) {
      return of(false);
    }
    return this.capabilitiesService.imgAvailable(file.contentType);
  }

  private isGsuiteImageFile(file: FilePreview): boolean {
    return file && file.length && file.contentType.startsWith('image');
  }

  private isPdfFile(file: FilePreview): boolean {
    return !!file && (file.contentType === 'application/pdf' || this.hasPDFExportLink(file));
  }

  private hasPDFExportLink(file: FilePreview): boolean {
    return !!file && !!file.exportLinks && file.exportLinks.hasOwnProperty('application/pdf');
  }

  private isGSuiteFile(file: FilePreview): boolean {
    return !!file && file.storage === GSUITE;
  }

  private showPdfCoverImage(): boolean {
    return this.isPdfFile(this.file) && !this.showPdfPreview();
  }

  private handleGSuitePdfFiles(file: FilePreview): void {
    this.googleApiService.getFileContentRequestUrl(file.fileId).subscribe(url => {
      this.pdfSrc = url;
    });
    if (!this.options.showPdfMobile && this.isMobile()) {
      this.previewAvailable = true;
    } else {
      this.pdfPreviewAvailable = true;
      this.previewAvailable = true;
    }
  }

  private handleVideoFiles(): void {
    this.videoType = true;
    this.previewAvailable = true;
    this.loading = false;
  }

  private handleCoyoPdfFiles(): void {
    this.pdfSrc = this.createPdfUrl(this.url, this.groupId, this.file.id, this.file.modified);
    this.previewAvailable = true;
  }

  private setExternalFileTimeout(): void {
    this.externalFileLoadingTimeout = this.windowService.setTimeout(() => {
      if (this.loading && !this.isPreviewAvailable()) {
        this.setPreviewUnavailable();
      }
    }, FilePreviewComponent.TIMEOUT_FOR_EXTERNAL_FILE_LOADING);
  }
}
