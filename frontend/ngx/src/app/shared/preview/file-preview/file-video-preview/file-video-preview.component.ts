import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {GSUITE} from '@domain/attachment/storage';
import {FilePreview} from '@domain/preview/file-preview';

/**
 * This Components handles the video preview
 */
@Component({
  selector: 'coyo-file-video-preview',
  templateUrl: './file-video-preview.component.html',
  styleUrls: ['./file-video-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileVideoPreviewComponent implements OnChanges {

  /**
   * Attached file
   */
  @Input() file: FilePreview;
  /**
   * File preview url
   */
  @Input() url: string;
  /**
   * File groupId
   */
  @Input() groupId: string;

  videoUrl: string;

  constructor(private urlService: UrlService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.videoUrl = !!this.file ? this.createVideoUrl() : null;
  }

  isGSuiteVideo(): boolean {
    return !!this.file && this.file.storage === GSUITE;
  }

  private createVideoUrl(): string {
    return this.isGSuiteVideo()
      ? this.createGSuitePreviewUrl()
      : this.createInternalVideoUrl();
  }

  private createInternalVideoUrl(): string {
    const baseUrl = this.urlService.getBackendUrl()
      + this.urlService.insertPathVariablesIntoUrl(this.url, {groupId: this.groupId, id: this.file.id});
    return baseUrl + '/stream';
  }

  private createGSuitePreviewUrl(): string {
    return 'https://drive.google.com/file/d/' + this.file.fileId + '/preview';
  }
}
