import {Vec2} from './vec2';

describe('Vec2', () => {

  it('should correctly calculate the magnitude of a vector', () => {
    // given
    const v1 = new Vec2(2, 2);
    const v2 = new Vec2(-2, 3.5);

    // when
    const magnitude1 = v1.magnitude;
    const magnitudes1 = v1.magnitudeSquared;

    const magnitude2 = v2.magnitude;
    const magnitudes2 = v2.magnitudeSquared;

    // then
    expect(magnitude1).toBe(Math.sqrt(2 * 2 * 2));
    expect(magnitudes1).toBe(8);

    expect(magnitude2).toBe(Math.sqrt((-2 * -2) + (3.5 * 3.5)));
    expect(magnitudes2).toBe((-2 * -2) + (3.5 * 3.5));
  });

  it('should correctly return the max and min values for two vector2', () => {
    // given
    const v1 = new Vec2(1, 2);
    const v2 = new Vec2(-2, 3.5);

    // when
    const min = Vec2.min(v1, v2);
    const max = Vec2.max(v1, v2);

    // then
    expect(min).toEqual(new Vec2(-2, 2));
    expect(max).toEqual(new Vec2(1, 3.5));
  });

  it('should correctly add and subtract two vectors', () => {
    // given
    const v1 = new Vec2(1, 2);
    const v2 = new Vec2(-2, 3.5);

    // when
    const sub = v1.subtract(v2);
    const add = v1.add(v2);

    // then
    expect(sub).toEqual(new Vec2(3, -1.5));
    expect(add).toEqual(new Vec2(-1, 5.5));
  });

  it('should correctly multiply and divide two vectors', () => {
    // given
    const v1 = new Vec2(1, 2);
    const v2 = new Vec2(-2, 3.5);

    // when
    const mul = v1.multiply(v2);
    const div = v1.divide(v2);

    // then
    expect(mul).toEqual(new Vec2(-2, 7));
    expect(div).toEqual(new Vec2(-0.5, 2 / 3.5));
  });

  it('should correctly scale a vector', () => {
    // given
    const v1 = new Vec2(3, 2.24);
    const scalar = 7.4;

    // when
    const sca = v1.scale(scalar);
    const sci = v1.scaleInverse(scalar);

    // then
    expect(sca).toEqual(new Vec2(3 * scalar, 2.24 * scalar));
    expect(sci).toEqual(new Vec2(3 / scalar, 2.24 / scalar));
  });
});
