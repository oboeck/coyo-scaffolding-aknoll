import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {LinkPreview} from '@domain/preview/link-preview';
import {LinkPreviewComponent} from './link-preview.component';

describe('LinkPreviewComponent', () => {
  let component: LinkPreviewComponent;
  let fixture: ComponentFixture<LinkPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LinkPreviewComponent],
      providers: [{
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['getBackendUrl', 'join'])
      }]
    }).overrideTemplate(LinkPreviewComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a preview image when internal and image URL is assigned', () => {
    // given
    component.preview = {internal: true, imageUrl: 'test-url'} as LinkPreview;

    // when
    const result = component.hasPreviewImage();

    // then
    expect(result).toBeTruthy();
  });

  it('should have a preview image when not internal, content type and image blob uid is assigned', () => {
    // given
    component.preview = {internal: false, imageBlobUid: 'test-uid', contentType: 'image/png'} as LinkPreview;

    // when
    const result = component.hasPreviewImage();

    // then
    expect(result).toBeTruthy();
  });

  it('should not have a preview image when internal but no image URL', () => {
    // given
    component.preview = {internal: true} as LinkPreview;

    // when
    const result = component.hasPreviewImage();

    // then
    expect(result).toBeFalsy();
  });

  it('should not have a preview image when not internal but image URL is given', () => {
    // given
    component.preview = {imageUrl: 'test-url'} as LinkPreview;

    // when
    const result = component.hasPreviewImage();

    // then
    expect(result).toBeFalsy();
  });

  it('should not have a preview image when not internal, and no content type is assigned', () => {
    // given
    component.preview = {internal: false, imageBlobUid: 'test-uid'} as LinkPreview;

    // when
    const result = component.hasPreviewImage();

    // then
    expect(result).toBeFalsy();
  });

  it('should not have a preview image when not internal, and no image blob uid is assigned', () => {
    // given
    component.preview = {internal: false, contentType: 'image/png'} as LinkPreview;

    // when
    const result = component.hasPreviewImage();

    // then
    expect(result).toBeFalsy();
  });

  it('should have an internal icon when internal and icon URL is set', () => {
    // given
    component.preview = {internal: true, iconUrl: 'test-url'} as LinkPreview;

    // when
    const result = component.hasInternalIcon();

    // then
    expect(result).toBeTruthy();
  });

  it('should not have an internal content type and blobid is set', () => {
    // given
    component.preview = {
      internal: true,
      iconUrl: 'test-url',
      contentType: 'imagePng',
      imageBlobUid: 'blobuid'
    } as LinkPreview;

    // when
    const result = component.hasInternalIcon();

    // then
    expect(result).toBeFalsy();
  });

  it('should have an google icon if the tdl contains .google.', () => {
    // given
    component.preview = {
      internal: false,
      tld: 'https://docs.google.com/document/id',
      iconUrl: 'document'
    } as LinkPreview;

    // when
    const result = component.hasGoogleIcon();

    // then
    expect(result).toBeTruthy();
  });

  it('should not have an google icon if the tld does not contain .google.', () => {
    // given
    component.preview = {internal: false, tld: 'https://docs.not-google.com/document/id', iconUrl: ''} as LinkPreview;

    // when
    const result = component.hasGoogleIcon();

    // then
    expect(result).toBeFalsy();
  });

  it('should get the absolute image url', () => {
    // given
    component.preview = {
      imageUrl: 'test-url'
    } as LinkPreview;

    const urlService = TestBed.get(UrlService) as jasmine.SpyObj<UrlService>;
    urlService.getBackendUrl.and.returnValue('http://backend.url/');
    urlService.join.and.callFake((a: string, b: string) => a + b);

    // when
    const result = component.getAbsoluteImageUrl();

    // then
    expect(result).toBe('http://backend.url/test-url');
  });
});
