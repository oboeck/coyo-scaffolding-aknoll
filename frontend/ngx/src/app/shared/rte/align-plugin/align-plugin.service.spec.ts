import {inject, TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import {AlignPluginService} from 'app/shared/rte/align-plugin/align-plugin.service';

describe('AlignPluginService', () => {
  let froala: jasmine.SpyObj<any>;
  let translateService: jasmine.SpyObj<TranslateService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlignPluginService, {
        provide: FROALA_EDITOR,
        useValue: jasmine.createSpyObj('FroalaEditor', ['DefineIcon', 'RegisterCommand'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }]
    });

    froala = TestBed.get(FROALA_EDITOR);
    translateService = TestBed.get(TranslateService);
  });

  it('should be created', inject([AlignPluginService], (service: AlignPluginService) => {
    expect(service).toBeTruthy();
  }));

  it('should register icons', inject([AlignPluginService], (service: AlignPluginService) => {
    // given
    const icons = [
      'coyoAlignIcon',
      'coyoAlignBlockLeftIcon',
      'coyoAlignBlockCenterIcon',
      'coyoAlignBlockRightIcon',
      'coyoAlignInlineLeftIcon',
      'coyoAlignInlineRightIcon'];

    // when
    service.initialize({} as RteSettings);

    // then
    icons.forEach(icon => expect(froala.DefineIcon).toHaveBeenCalledWith(icon, jasmine.any(Object)));
  }));

  it('should register commands', inject([AlignPluginService], (service: AlignPluginService) => {
    // given
    const commands = [
      'coyoImageAlign',
      'coyoVideoAlign',
      'coyoTableAlign'];
    translateService.instant.and.returnValue('Alignment');

    // when
    service.initialize({} as RteSettings);

    // then
    commands.forEach(command => expect(froala.RegisterCommand).toHaveBeenCalledWith(command, {
      title: 'Alignment',
      icon: 'coyoAlignIcon',
      plugin: 'coyoAlignPlugin',
      type: 'dropdown',
      undo: true,
      focus: false,
      showOnMobile: true,
      refreshAfterCallback: false,
      html: jasmine.any(Function),
      callback: jasmine.any(Function),
      refresh: jasmine.any(Function),
      refreshOnShow: jasmine.any(Function)
    }));
  }));

  it('should use correct HTML', inject([AlignPluginService], (service: AlignPluginService) => {
    // given
    const editor = {
      icon: jasmine.createSpyObj('icon', ['create'])
    };

    translateService.instant.and.returnValue('[TITLE]');
    editor['icon'].create.and.returnValue('[ICON]');

    // when
    service.initialize({} as RteSettings);
    const html = froala.RegisterCommand.calls.first().args[1].html.apply(editor, []);

    // then
    expect(html).toEqual('<ul class="fr-dropdown-list" role="presentation">' +
      '<li role="presentation"><a class="fr-command fr-title" tabIndex="-1" role="option" data-cmd="coyoImageAlign" data-param1="fr-cdbl" title="[TITLE]">' +
        '[ICON]<span class="fr-sr-only">[TITLE]</span></a></li>' +
      '<li role="presentation"><a class="fr-command fr-title" tabIndex="-1" role="option" data-cmd="coyoImageAlign" data-param1="fr-cdbc" title="[TITLE]">' +
        '[ICON]<span class="fr-sr-only">[TITLE]</span></a></li>' +
      '<li role="presentation"><a class="fr-command fr-title" tabIndex="-1" role="option" data-cmd="coyoImageAlign" data-param1="fr-cdbr" title="[TITLE]">' +
        '[ICON]<span class="fr-sr-only">[TITLE]</span></a></li>' +
      '<li role="presentation"><a class="fr-command fr-title" tabIndex="-1" role="option" data-cmd="coyoImageAlign" data-param1="fr-cdil" title="[TITLE]">' +
        '[ICON]<span class="fr-sr-only">[TITLE]</span></a></li>' +
      '<li role="presentation"><a class="fr-command fr-title" tabIndex="-1" role="option" data-cmd="coyoImageAlign" data-param1="fr-cdir" title="[TITLE]">' +
        '[ICON]<span class="fr-sr-only">[TITLE]</span></a></li>' +
      '</ul>');
  }));

  it('should apply alignment class', inject([AlignPluginService], (service: AlignPluginService) => {
    // given
    const elem = jasmine.createSpyObj('elem', ['css', 'addClass', 'removeClass']);
    const editor = {
      image: jasmine.createSpyObj('image', ['getEl', 'display'])
    };

    elem.css.and.returnValue(elem);
    elem.addClass.and.returnValue(elem);
    elem.removeClass.and.returnValue(elem);
    editor['image'].getEl.and.returnValue(elem);

    // when
    service.initialize({} as RteSettings);
    froala.RegisterCommand.calls.first().args[1].callback.apply(editor, ['cmd', 'fr-cdil']);

    // then
    expect(editor['image'].getEl).toHaveBeenCalled();
    expect(elem.removeClass).toHaveBeenCalledWith('fr-cdbl');
    expect(elem.removeClass).toHaveBeenCalledWith('fr-cdbc');
    expect(elem.removeClass).toHaveBeenCalledWith('fr-cdbr');
    expect(elem.removeClass).toHaveBeenCalledWith('fr-cdil');
    expect(elem.removeClass).toHaveBeenCalledWith('fr-cdir');
    expect(elem.addClass).toHaveBeenCalledWith('fr-cdil');
    expect(elem.css).toHaveBeenCalledWith('margin-left', '');
    expect(elem.css).toHaveBeenCalledWith('margin-right', '');
    expect(editor['image'].display).toHaveBeenCalledWith();
  }));

  it('should refresh', inject([AlignPluginService], (service: AlignPluginService) => {
    // given
    const first = jasmine.createSpyObj('first', ['replaceWith']);
    const children = jasmine.createSpyObj('children', ['first']);
    const $btn = jasmine.createSpyObj('$btn', ['children']);
    children.first.and.returnValue(first);
    $btn.children.and.returnValue(children);

    const elem = jasmine.createSpyObj('elem', ['hasClass']);
    const editor = {
      image: jasmine.createSpyObj('image', ['getEl']),
      icon: jasmine.createSpyObj('icon', ['create'])
    };
    editor['image'].getEl.and.returnValue(elem);
    editor['icon'].create.and.returnValue('icon');

    // when
    service.initialize({} as RteSettings);
    froala.RegisterCommand.calls.first().args[1].refresh.apply(editor, [$btn]);

    // then
    expect(editor['image'].getEl).toHaveBeenCalled();
    expect(first.replaceWith).toHaveBeenCalledWith('icon');
  }));

  it('should refreshOnShow', inject([AlignPluginService], (service: AlignPluginService) => {
    // given
    const $dropdown = jasmine.createSpyObj('$dropdown', ['find']);
    const elem = jasmine.createSpyObj('elem', ['hasClass']);
    const editor: object = {
      dataset: {},
      image: jasmine.createSpyObj('image', ['getEl']),
      classList: jasmine.createSpyObj('classList', ['add', 'remove']),
      setAttribute: jasmine.createSpy()
    };
    $dropdown.find.and.returnValue({each: (fn: any) => fn.bind(editor)()});
    elem.hasClass.and.returnValue(true);
    editor['image'].getEl.and.returnValue(elem);

    // when
    service.initialize({} as RteSettings);
    froala.RegisterCommand.calls.first().args[1].refreshOnShow.apply(editor, [{}, $dropdown]);

    // then
    expect(editor['image'].getEl).toHaveBeenCalled();
    expect($dropdown.find).toHaveBeenCalledWith('.fr-command');
    expect(editor['classList'].add).toHaveBeenCalledWith('fr-active');
    expect(editor['setAttribute']).toHaveBeenCalledWith('aria-selected', true);
  }));
});
