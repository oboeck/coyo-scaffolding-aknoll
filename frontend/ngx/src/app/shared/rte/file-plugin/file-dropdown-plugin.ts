import {Inject, Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {FileFromFileLibraryPlugin} from '@shared/rte/file-plugin/file-from-file-library-plugin';
import {FileFromGSuitePlugin} from '@shared/rte/file-plugin/file-from-g-suite-plugin';
import {RtePlugin} from '@shared/rte/rte-plugin';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';

/**
 * A custom RTE dropdown plugin that bundles the active commands into a drop down for file selection.
 */
@Injectable()
export class FileDropdownPlugin extends RtePlugin {

  static readonly KEY: string = 'coyoInsertFile';

  constructor(@Inject(FROALA_EDITOR) private froala: any,
              private translateService: TranslateService) {
    super();
  }

  protected doInitialize(settings: RteSettings): void {
    this.registerCommand(this.getDropdownOptions(this.froala,
      FileFromFileLibraryPlugin.KEY,
      FileFromGSuitePlugin.KEY
    ));
  }

  private registerCommand(options: { [key: string]: string; }): void {
    const plugin = this;
    const keys = Object.keys(options);
    if (keys.length === 1) {
      // Push command to the toolbar
      Object.defineProperty(this.froala.COMMANDS, FileDropdownPlugin.KEY,
        Object.getOwnPropertyDescriptor(this.froala.COMMANDS, keys[0]));
    } else if (keys.length > 1) {
      // Register dropdown command
      this.froala.RegisterCommand(FileDropdownPlugin.KEY, {
        title: plugin.translateService.instant('RTE.INSERT_FILE'),
        type: 'dropdown',
        options: options,
        plugin: 'coyoFilePlugin',
        icon: 'insertFile',
        undo: false,
        focus: false,
        showOnMobile: true,
        refreshAfterCallback: false,
        callback: function(cmd: string, opt: string): void {
          plugin.froala.COMMANDS[opt].callback.bind(this)(cmd, opt);
        }
      });
    }
  }
}
