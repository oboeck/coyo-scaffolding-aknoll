import {TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {ImageDropdownPlugin} from '@shared/rte/image-plugin/image-dropdown-plugin';
import {ImageFromFileLibraryPlugin} from '@shared/rte/image-plugin/image-from-file-library-plugin';
import {ImageFromGSuitePlugin} from '@shared/rte/image-plugin/image-from-g-suite-plugin';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';

describe('ImageDropdownPlugin', () => {
  let imageDropdownPlugin: ImageDropdownPlugin;
  let froala: jasmine.SpyObj<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImageDropdownPlugin, {
        provide: FROALA_EDITOR,
        useValue: jasmine.createSpyObj('froala', ['RegisterCommand'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }]
    });

    imageDropdownPlugin = TestBed.get(ImageDropdownPlugin);
    froala = TestBed.get(FROALA_EDITOR);
  });

  it('should be created', () => {
    expect(imageDropdownPlugin).toBeTruthy();
  });

  it('should initialize image button command', () => {
    // given
    froala.COMMANDS = {};
    froala.COMMANDS[ImageFromFileLibraryPlugin.KEY] = {title: 'COYO'};

    // when
    imageDropdownPlugin.initialize({} as RteSettings);

    // then
    expect(froala.RegisterCommand).not.toHaveBeenCalledWith();
    expect(froala.COMMANDS['coyoInsertImage']).toEqual(jasmine.objectContaining({
      title: 'COYO'
    }));
  });

  it('should initialize image dropdown command', () => {
    // given
    froala.COMMANDS = {};
    froala.COMMANDS[ImageFromFileLibraryPlugin.KEY] = {title: 'COYO'};
    froala.COMMANDS[ImageFromGSuitePlugin.KEY] = {title: 'Google'};

    // when
    imageDropdownPlugin.initialize({} as RteSettings);

    // then
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertImage', jasmine.objectContaining({
      type: 'dropdown',
      options: {
        coyoInsertImageFromFileLibrary: 'COYO',
        coyoInsertImageFromGSuite: 'Google'
      }
    }));
  });
});
