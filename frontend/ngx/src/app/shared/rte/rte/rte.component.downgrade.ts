import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {youtubeNoCookie} from '../video-plugin/rte-youtube-regex';
import {RteComponent} from './rte.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoRte', downgradeComponent({
    component: RteComponent
  }))
  .constant('youtubeEmbedUrlRegEx', youtubeNoCookie.regex)
  .constant('youtubeEmbedUrlReplacement', youtubeNoCookie.replacement);
