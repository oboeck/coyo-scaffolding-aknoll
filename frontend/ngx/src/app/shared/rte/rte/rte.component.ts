import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Inject,
  Input,
  NgZone,
  OnInit,
  Optional,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {ControlValueAccessor} from '@angular/forms';
import {AuthService} from '@core/auth/auth.service';
import {App} from '@domain/apps/app';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {TranslateService} from '@ngx-translate/core';
import {JQUERY} from '@root/injection-tokens';
import {RTE_PLUGINS, RtePlugin} from '@shared/rte/rte-plugin';
import * as _ from 'lodash';
import {of, zip} from 'rxjs';
import {finalize, tap} from 'rxjs/operators';
import {RTE_OPTIONS, RteOptions} from '../rte-options';
import {RteSettings} from './rte-settings/rte-settings';
import {RteSettingsService} from './rte-settings/rte-settings.service';

/**
 * A rich text editor component.
 *
 * This component supports AngularJS usage with ng-model.
 */
@Component({
  selector: 'coyo-rte',
  templateUrl: './rte.component.html',
  encapsulation: ViewEncapsulation.None
})
export class RteComponent implements OnInit, ControlValueAccessor {

  constructor(private ngZone: NgZone,
              private translate: TranslateService,
              private settingsService: RteSettingsService,
              private senderService: SenderService,
              private authService: AuthService,
              private cd: ChangeDetectorRef,
              @Inject(JQUERY) private jQuery: any,
              @Optional() @Inject(RTE_PLUGINS) private plugins: RtePlugin[],
              @Optional() @Inject(RTE_OPTIONS) private optionDefaults: RteOptions) {
  }

  loading: boolean = true;
  options: { [key: string]: any; } = {};
  serverSettings: RteSettings;
  sender: Sender;
  app: App;
  onChangeCallback: any;
  onTouchedCallback: any;
  editor: any;
  focused: boolean;

  /**
   * A map of RTE options overriding the default options.
   */
  @Input() optionOverrides: { [key: string]: any; };

  /**
   * The default height of the RTE.
   */
  @Input() height: number;

  /**
   * The content of the RTE.
   */
  @Input() content: any;

  /**
   * Event emitted when the contend of the RTE is changed.
   */
  @Output() contentUpdated: EventEmitter<string> = new EventEmitter<string>();

  ngOnInit(): void {
    this.initSender();
    this.settingsService.getSettings()
      .pipe(tap(settings => this.serverSettings = settings))
      .pipe(finalize(() => {
        this.options = this.buildOptions();
        this.loading = false;
      })).subscribe(settings => {
        this.initPlugins(settings);
        this.cd.markForCheck();
    });
  }

  private initSender(): void {
    const senderId = this.senderService.getCurrentIdOrSlug();
    zip(
      senderId ? this.senderService.get(senderId) : this.authService.getUser(),
      senderId ? this.senderService.getCurrentApp(senderId) : of(null)
    ).subscribe(([sender, app]) => {
      this.sender = sender;
      this.app = app;
    });
  }

  private initPlugins(settings: RteSettings, force: boolean = false): void {
    if (this.plugins) {
      this.plugins.forEach(plugin => plugin.initialize(settings, force));
    }
  }

  /**
   * Handles ESC clicks and closes the fullscreen mode of the editor.
   */
  @HostListener('document:keydown.escape', ['$event'])
  handleEscape(): void {
    if (this.editor && this.editor.fullscreen.isActive()) {
      this.editor.fullscreen.toggle();
    }
  }

  /**
   * Emits an content updated event and calls the onChangeCallback.
   *
   * @param $event the Froala updated event
   */
  emitContentUpdatedEvent($event: any): void {
    this.contentUpdated.emit($event);
    if (this.onChangeCallback) {
      this.onChangeCallback($event);
    }
  }

  private buildOptions(): { [key: string]: any; } {
    const component = this;
    const removeImg = ['fr-dib', 'fr-dii', 'fr-fil', 'fr-fir'].join(' ');
    const removeVid = ['fr-dvb', 'fr-dvi', 'fr-fvl', 'fr-fvr'].join(' ');

    const extendedOptions: { [key: string]: any; } = _.extend({
      events: {
        'initialized': function(): void {
          component.editor = this;
          component.editor.getSender = () => component.sender;
          component.editor.getApp = () => component.app;
          component.jQuery('.fr-view img').removeClass(removeImg);
          component.jQuery('.fr-view .fr-video').removeClass(removeVid)
            .attr('draggable', true)
            .attr('contentEditable', false);
          component.jQuery('.fr-view span[coyo-download]').replaceWith(function(): JQuery {
            let link = component.jQuery(this).attr('coyo-download');
            link = link ? link.replace(/\'/g, '') : link;
            const innerHtml = component.jQuery(this).html();
            return component.jQuery(`<a href="${link}" target="_self">${innerHtml}</a>`);
          });
        },
        'image.inserted': function($image: any): void {
          $image.addClass('fr-cdbc').removeClass(removeImg);
        },
        'video.inserted': function($video: any): void {
          $video.addClass('fr-cdbc').removeClass(removeVid);
        },
        'table.inserted': function(table: HTMLTableElement): void {
          table.classList.add('fr-table');
          table.classList.add('fr-cdbl');
          table.classList.add('rte-table-bordered');
        },
        'table.resized': function(table: HTMLTableElement): void {
          if (table.classList.contains('fr-cdil') || table.classList.contains('fr-cdir')) {
            table.style.setProperty('margin-left', null);
            table.style.setProperty('margin-right', null);
          }
        },
        'focus': function(): void {
          component.ngZone.run(() => {
            component.focused = true;
            if (component.onTouchedCallback) {
              component.onTouchedCallback();
            }
          });
        },
        'blur': function(): void {
          component.ngZone.run(() => {
            component.focused = false;
            if (this.codeview && this.codeView.isActive()) {
              this.codeView.toggle();
            }
            if (component.onChangeCallback) {
              component.onChangeCallback(component.content);
            }
          });
        },
        'paste.before': function(event: ClipboardEvent): void {
          component.triggerWordPasteForNoRTFDocuments(this, event);
        }
      }
    }, RteOptions.DEFAULT_OPTIONS,
      this.optionDefaults ? this.optionDefaults.get() : {},
      {key: _.get(this.serverSettings, 'license')},
      this.optionOverrides);

    return _.extend(extendedOptions, {
      paragraphFormat: _.mapValues(extendedOptions.paragraphFormat, key => this.translate.instant(key)),
      paragraphStyles: _.mapValues(extendedOptions.paragraphStyles, key => this.translate.instant(key)),
      tableStyles: _.mapValues(extendedOptions.tableStyles, key => this.translate.instant(key)),
      imageStyles: _.mapValues(extendedOptions.imageStyles, key => this.translate.instant(key))
    });
  }

  /* tslint:disable-next-line:completed-docs */
  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  /* tslint:disable-next-line:completed-docs */
  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  /* tslint:disable-next-line:completed-docs */
  writeValue(obj: any): void {
    this.content = obj;
  }

  /**
   * Called when pasting content into the RTE. Fixes the Froala issue that word documents
   * which do not create a rich text format in the clipboard are not pasted into the editor.
   *
   * @param editor the editor instance
   * @param event the original clipboard event
   * @return false when word paste event is triggered manually, true otherwise
   */
  triggerWordPasteForNoRTFDocuments(editor: any, event: ClipboardEvent): boolean {
    if (event.clipboardData) {
      const html = event.clipboardData.getData('text/html') || '';
      const rtf = event.clipboardData.getData('text/rtf');
      const isWord = html.match(/(class=\"?Mso|class=\'?Mso|class="?Xl|class='?Xl|class=Xl|style=\"[^\"]*\bmso\-|style=\'[^\']*\bmso\-|w:WordDocument)/gi);
      if (html && !rtf && isWord) {
        editor.events.trigger('paste.wordPaste', [html], true);
        return false;
      }
      return true;
    }
  }
}
