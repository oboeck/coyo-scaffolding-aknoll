export const youtubeNoCookie = {
  regex: /((?:https?:\/\/)?(?:www\.)?youtube)(-nocookie)?(\.com\/embed\/((\w|-){11}))/g,
  replacement: '$1-nocookie$3'
};
