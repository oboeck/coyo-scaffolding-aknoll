import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {App} from '@domain/apps/app';
import {Ng1AppRegistry} from '@root/typings';
import {StateService} from '@uirouter/core';
import {NG1_APP_REGISTRY} from '@upgrade/upgrade.module';
import {AppHrefComponent} from './app-href.component';

describe('AppHrefComponent', () => {
  let component: AppHrefComponent;
  let fixture: ComponentFixture<AppHrefComponent>;
  let appRegistry: jasmine.SpyObj<Ng1AppRegistry>;
  let stateService: jasmine.SpyObj<StateService>;
  const app = {
    key: 'wiki',
    id: '54321',
    senderType: 'page',
    senderId: '88888'
  } as App;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppHrefComponent ],
      providers: [
        {provide: NG1_APP_REGISTRY, useValue: jasmine.createSpyObj('appRegistry', ['getDefaultStateName'])},
        {provide: StateService, useValue: jasmine.createSpyObj('stateService', ['href'])}
      ]
    }).overrideTemplate(AppHrefComponent, '<div></div>')
    .compileComponents();

    appRegistry = TestBed.get(NG1_APP_REGISTRY);
    stateService = TestBed.get(StateService);
    appRegistry.getDefaultStateName.and.returnValue('main.page.show.apps.wiki.main');
    stateService.href.and.returnValue('/pages/88888/apps/wiki/wiki');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppHrefComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given
    component.app = app;
    // when
    component.ngOnInit();
    // then
    expect(component.link).toBeDefined();
    expect(component.link).toBe('/pages/88888/apps/wiki/wiki');
  });
});
