import {HttpParams} from '@angular/common/http';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Pageable} from '@domain/pagination/pageable';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {of} from 'rxjs';
import {SelectSenderComponent} from './select-sender.component';

describe('SelectSenderComponent', () => {
  let component: SelectSenderComponent;
  let fixture: ComponentFixture<SelectSenderComponent>;
  let senderService: jasmine.SpyObj<SenderService>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [SelectSenderComponent],
        providers: [{
          provide: SenderService,
          useValue: jasmine.createSpyObj('SenderService', ['getPage'])
        }]
      })
      .overrideTemplate(SelectSenderComponent, '<div></div>')
      .compileComponents();

    senderService = TestBed.get(SenderService);
  }));

  beforeEach(() => {
    const sender = {} as Sender;
    const pageResult = {
      number: 0,
      last: false,
      size: 10,
      totalPages: 2,
      numberOfElements: 10,
      totalElements: 12,
      content: [sender, sender]
    };
    senderService.getPage.and.returnValue(of(pageResult));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSenderComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', fakeAsync(() => {
    fixture.detectChanges();
    expect(component.senderFilterInput$).toBeDefined();
    expect(component.pageNumber).toBeDefined();
    expect(component.loading).toBeFalsy();
    expect(component.disabled).toBeFalsy();
    component.senders$.subscribe(senders => {
      expect(senderService.getPage).toHaveBeenCalled();
      expect(senders.length).toEqual(2);
    });
    tick();
  }));

  it('should find managed senders', fakeAsync(() => {
    component.parameters = {
      findOnlyManagedSenders: true,
      filters: 'type=user&type=page&type=workspace&type=event'
    };
    const expected = setupExpectedProperties('/search/managed');

    fixture.detectChanges();

    component.senders$.subscribe(() => {
      expect(senderService.getPage).toHaveBeenCalledWith(expected.pageable, expected.params);
    });
    tick();
  }));

  it('should find sharing recipients', fakeAsync(() => {
    component.parameters = {
      findSharingRecipients: true,
      filters: 'type=user&type=page&type=workspace'
    };
    const expected = setupExpectedProperties('/search/sharing-recipients');

    fixture.detectChanges();

    component.senders$.subscribe(() => {
      expect(senderService.getPage).toHaveBeenCalledWith(expected.pageable, expected.params);
    });
    tick();
  }));

  it('should find senders with default settings', fakeAsync(() => {
    component.parameters = {};
    const expected = setupExpectedProperties('/search');

    fixture.detectChanges();

    component.senders$.subscribe(() => {
      expect(senderService.getPage).toHaveBeenCalledWith(expected.pageable, expected.params);
    });
    tick();
  }));

  it('should filter senders', fakeAsync(() => {
    // given
    component.parameters = {};
    const expected = setupExpectedProperties('/search');

    fixture.detectChanges();

    component.senders$.subscribe(() => {
      expect(senderService.getPage).toHaveBeenCalledWith(expected.pageable, expected.params);
    });
    component.senderFilterInput$.next('Robert');
    expected.params.params.set('term', 'Robert');

    // when
    tick(250);
    fixture.detectChanges();

    // then
    component.senders$.subscribe(() => {
      expect(senderService.getPage).toHaveBeenCalledWith(expected.pageable, expected.params);
    });
  }));

  it('should load more sender on scroll event', fakeAsync(() => {
    component.parameters = {};
    const expected = setupExpectedProperties('/search');

    fixture.detectChanges();
    component.senders$.subscribe(() => {
      expect(senderService.getPage).toHaveBeenCalledWith(expected.pageable, expected.params);
    });
    tick();

    const pageResult = {
      number: 1,
      numberOfElements: 2,
      last: true,
      size: 10,
      totalPages: 2,
      totalElements: 12,
      content: [{} as Sender, {} as Sender]
    };
    senderService.getPage.and.returnValue(of(pageResult));
    component.onScroll({start: 0, end: 8});
    fixture.detectChanges();
    component.senders$.subscribe(() => {
      expect(senderService.getPage).toHaveBeenCalledWith(new Pageable(1, 10), expected.params);
    });
    tick();
  }));

  it('should change ng model value on change event', () => {
    const expectedSender = {id: 'sender-id'} as Sender;
    component.parameters = {};
    component.registerOnChange(() => {
    });
    spyOn(component.ngModelChange, 'emit');

    component.onChange(expectedSender);
    fixture.detectChanges();

    expect(component.ngModelChange.emit).toHaveBeenCalledWith(expectedSender);

  });

  function setupExpectedProperties(path: string): any {
    const expectedPageable = new Pageable(0, 10);
    const expectedParams = {
      params: new HttpParams({fromObject: component.parameters as any}).set('term', ''),
      path: path
    };
    return {
      pageable: expectedPageable,
      params: expectedParams
    };
  }

});
