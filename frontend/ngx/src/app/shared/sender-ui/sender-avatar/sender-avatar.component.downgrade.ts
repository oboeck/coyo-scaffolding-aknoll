import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {SenderAvatarComponent} from './sender-avatar.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoSenderAvatar', downgradeComponent({
    component: SenderAvatarComponent,
    propagateDigest: false
  }));
