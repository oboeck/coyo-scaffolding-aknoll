import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SenderListItemComponent} from './sender-list-item.component';

describe('SenderListItemComponent', () => {
  let component: SenderListItemComponent;
  let fixture: ComponentFixture<SenderListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderListItemComponent ]
    }).overrideTemplate(SenderListItemComponent, '')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
