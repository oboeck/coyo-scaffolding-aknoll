import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {UserFollowComponent} from './user-follow.component';

getAngularJSGlobal()
  .module('commons.sender')
  .directive('coyoUserFollow', downgradeComponent({
    component: UserFollowComponent,
    propagateDigest: false
  }));
