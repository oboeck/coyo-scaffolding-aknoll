import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Comment} from '@domain/comment/comment';
import {CommentService} from '@domain/comment/comment.service';
import {NG1_AUTH_SERVICE} from '@upgrade/upgrade.module';
import {of} from 'rxjs';
import {CommentContextMenuComponent} from './comment-context-menu.component';

describe('CommentContextMenuComponent', () => {
  let component: CommentContextMenuComponent;
  let fixture: ComponentFixture<CommentContextMenuComponent>;
  let commentService: jasmine.SpyObj<CommentService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentContextMenuComponent],
      providers: [{
        provide: NG1_AUTH_SERVICE,
        useValue: jasmine.createSpyObj('authService', ['getCurrentUserId'])
      }, {
        provide: CommentService,
        useValue: jasmine.createSpyObj('commentService', ['get'])
      }]
    }).overrideTemplate(CommentContextMenuComponent, '')
      .compileComponents();

    commentService = TestBed.get(CommentService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the show original author event', fakeAsync(() => {
    // when
    component.showAuthor.subscribe((showAuthor: boolean) => {
      expect(showAuthor).toBeTruthy();
    });

    component.onShowOriginalAuthor();

    // then --> observable
    tick();
  }));

  it('should show delete entry', () => {
    // given
    component.comment = {
      _permissions: {
        delete: true
      }
    } as Comment;

    // then
    expect(component.canShowDelete).toBeTruthy();
    expect(component.canShowMenuOptions).toBeTruthy();
    expect(component.canShowMenu).toBeTruthy();
  });

  it('should show edit entry', () => {
    // given
    component.editable = true;
    component.comment = {
      _permissions: {
        edit: true
      }
    } as Comment;

    // then
    expect(component.canShowEdit).toBeTruthy();
    expect(component.canShowMenuOptions).toBeTruthy();
    expect(component.canShowMenu).toBeTruthy();
  });

  it('should show edit entry', () => {
    // given
    component.comment = {
      author: {
        id: 'other-id'
      },
      _permissions: {
        accessoriginalauthor: true
      }
    } as Comment;

    // then
    expect(component.canShowOriginalAuthor).toBeTruthy();
    expect(component.canShowMenuOptions).toBeTruthy();
    expect(component.canShowMenu).toBeTruthy();
  });

  it('should show nothing when no permission is set', () => {
    // given
    component.comment = {
      _permissions: {}
    } as Comment;

    // then
    expect(component.canShowMenuOptions).toBeFalsy();
    expect(component.canShowMenu).toBeFalsy();
  });

  it('should request the comment permissions when context menu is open and comment was send over websocket', () => {
    // given
    component.comment = {
      id: 'comment-id',
      partiallyLoaded: true,
      _permissions: {}
    } as Comment;

    const emitSpy = spyOn(component.changed, 'emit');

    const comment = {
      _permissions: {
        delete: true
      }
    } as Comment;

    commentService.get.and.returnValue(of(comment));

    // when
    component.onOpen();

    // then
    expect(commentService.get).toHaveBeenCalledWith('comment-id', {params: {_permissions: '*'}, handleErrors: false});
    expect(emitSpy).toHaveBeenCalledWith(comment);
    expect(comment.partiallyLoaded).toBeTruthy();
  });
});
