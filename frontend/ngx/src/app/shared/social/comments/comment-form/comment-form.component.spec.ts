import {SimpleChange} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '@core/auth/auth.service';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {CommentService} from '@domain/comment/comment.service';
import {Sender} from '@domain/sender/sender';
import {TimelineFormAttachment} from '@shared/files/attachment-btn/types/timeline-form-attachment';
import {CommentAttachment} from '@shared/social/comments/comment-form/comment-attachment';
import {CommentFormStatus} from '@shared/social/comments/comment-form/comment-form-status';
import {of, throwError} from 'rxjs';
import {delay} from 'rxjs/operators';
import {CommentFormComponent} from './comment-form.component';

describe('CommentFormComponent', () => {
  let component: CommentFormComponent;
  let fixture: ComponentFixture<CommentFormComponent>;

  let windowSizeService: jasmine.SpyObj<WindowSizeService>;
  let commentService: jasmine.SpyObj<CommentService>;
  const author: Sender = {id: 'some-author'} as any;
  const target: any = {id: 'some-target', typeName: 'timeline-item'};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [CommentFormComponent],
      providers: [
        {provide: AuthService, useValue: jasmine.createSpyObj('authService', [''])},
        {provide: CommentService, useValue: jasmine.createSpyObj('commentService', ['post', 'put'])},
        {provide: WindowSizeService, useValue: jasmine.createSpyObj('windowSizeService', ['isXs', 'observeScreenChange'])}
      ]
    }).overrideTemplate(CommentFormComponent, '<textarea #message (focus)="onFocus()"></textarea>')
      .compileComponents();

    windowSizeService = TestBed.get(WindowSizeService);
    commentService = TestBed.get(CommentService);
  }));

  beforeEach(() => {
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.MD));

    fixture = TestBed.createComponent(CommentFormComponent);
    component = fixture.componentInstance;
    component.author = author;
    component.target = target;

    fixture.detectChanges();
  });

  it('should register keydown listener on after view init', fakeAsync(() => {
    component.messageRef.nativeElement.dispatchEvent(new Event('keydown'));

    component.isXs$.subscribe(isXs => expect(isXs).toBe(false));
    expect(component).toBeTruthy();
  }));

  it('should not handle event when screen size isXs', () => {
    // given
    const event: KeyboardEvent = new KeyboardEvent('');
    spyOn(event, 'preventDefault');
    windowSizeService.isXs.and.returnValue(true);

    // when
    const result = component.onKeydown(event);
    // then
    expect(component).toBeTruthy();
    expect(windowSizeService.isXs).toHaveBeenCalled();
    expect(event.preventDefault).not.toHaveBeenCalled();
    expect(result).toBe(undefined);
  });

  it('should handle event when pressing enter key', () => {
    // given
    const event: KeyboardEvent = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
      shiftKey: false,
      view: window
    });
    spyOn(event, 'preventDefault');
    windowSizeService.isXs.and.returnValue(false);
    component.formState$.next(CommentFormStatus.SUBMITTING);

    // when
    const result = component.onKeydown(event);

    // then
    expect(component).toBeTruthy();
    expect(event.preventDefault).toHaveBeenCalled();
    expect(result).toBe(false);
  });

  it('should handle event when pressing escape', () => {
    // given
    const event: KeyboardEvent = new KeyboardEvent('keydown', {
      code: 'Escape',
      key: 'Escape',
      shiftKey: false,
      view: window
    });
    spyOn(event, 'preventDefault');
    windowSizeService.isXs.and.returnValue(false);
    component.emojiPicker = jasmine.createSpyObj('emojiPicker', ['closePicker']);

    // when
    const result = component.onKeydown(event);

    // then
    expect(component).toBeTruthy();
    expect(event.preventDefault).not.toHaveBeenCalled();
    expect(component.form.getRawValue()).toEqual({message: '', attachments: []});
    expect(component.emojiPicker.closePicker).toHaveBeenCalled();
    expect(result).toBe(false);
  });

  it('should remove only one attachment', () => {
    // given
    const attachment = {uid: 'a'} as TimelineFormAttachment;
    const anotherAttachment = {uid: 'b'} as TimelineFormAttachment;
    const expectedAttachments = [anotherAttachment];
    component.form.setValue({
      message: '',
      attachments: [attachment, anotherAttachment]
    });
    fixture.detectChanges();

    // when
    component.removeAttachment(attachment);

    // then
    expect(component.form.getRawValue().attachments).toEqual(expectedAttachments);
  });

  it('should invalidate form when message is not set', () => {
    expect(component.form.valid).toBe(false);
  });

  it('should not submit form when form is invalid', fakeAsync(() => {
    // given
    expect(component.form.valid).toBe(false);
    let submitting;
    component.formState$.subscribe(value => submitting = value);

    // when
    component.submit();
    tick();

    // then
    expect(submitting).toBe(CommentFormStatus.START);

  }));

  it('should create a new comment', fakeAsync(() => {
    // given
    let createdEmitted = false;
    let editedEmitted = false;
    component.form.get('message').setValue('Some message');
    component.form.get('attachments').setValue([{storage: 'LOCAL_FILE_LIBRARY'}]);
    commentService.post.and.returnValue(of({}).pipe(delay(10)));
    component.created.subscribe(() => createdEmitted = true);
    component.edited.subscribe(() => editedEmitted = true);

    // when
    component.submit();

    // expect
    const commentRequest = commentService.post.calls.argsFor(0)[0];
    expect(commentRequest.attachments.length).toBe(0);
    expect(commentRequest.fileLibraryAttachments.length).toBe(1);

    expect(component.formState$.getValue()).toBe(CommentFormStatus.SUBMITTING);
    tick(10);
    expect(createdEmitted).toBe(true);
    expect(editedEmitted).toBe(false);
    expect(component.form.getRawValue()).toEqual({
      message: '', attachments: []
    });
    expect(component.formState$.getValue()).toBe(CommentFormStatus.START);
  }));

  it('should set to error state when backend request fails', fakeAsync(() => {
    // given
    component.form.get('message').setValue('Some message');
    commentService.post.and.returnValue(throwError({}));

    // when
    component.submit();

    // expect
    tick();
    expect(component.formState$.getValue()).toBe(CommentFormStatus.ERROR);
  }));

  it('should edit a comment', fakeAsync(() => {
    // given
    component.comment = {id: 'a-comment'} as any;
    let createdEmitted = false;
    let editedEmitted = false;
    component.form.get('message').setValue('Some message');
    commentService.put.and.returnValue(of({}));
    component.created.subscribe(() => createdEmitted = true);
    component.edited.subscribe(() => editedEmitted = true);

    // when
    component.submit(component.form.getRawValue());

    // expect
    expect(createdEmitted).toBe(false);
    expect(editedEmitted).toBe(true);
    expect(component.form.getRawValue()).toEqual({
      message: '', attachments: []
    });
    expect(component.formState$.getValue()).toBe(CommentFormStatus.START);
  }));

  it('should adjust attachment values for comment edit', fakeAsync(() => {
    // given
    const commentId = 'comment-id';

    const attachment: CommentAttachment = {
      fileId: '123',
      storage: 'LOCAL_BLOB_STORAGE'
    } as CommentAttachment;

    const fileLibraryAttachment: CommentAttachment = {
      modelId: '456',
      storage: 'LOCAL_FILE_LIBRARY'
    } as CommentAttachment;

    component.comment = {id: commentId} as any;
    component.form.get('attachments').setValue([attachment, fileLibraryAttachment]);
    component.form.get('message').setValue('Some message');
    commentService.put.and.returnValue(of({}));

    // when
    component.submit(component.form.getRawValue());

    // then
    const expectedAttachment: CommentAttachment = {
      uid: '123',
      fileId: '123',
      storage: 'LOCAL_BLOB_STORAGE'
    } as CommentAttachment;

    const expectedFileLibraryAttachment: CommentAttachment = {
      senderId: '456',
      modelId: '456',
      storage: 'LOCAL_FILE_LIBRARY'
    } as CommentAttachment;

    expect(commentService.put).toHaveBeenCalledWith(commentId, jasmine.objectContaining({
      attachments: [expectedAttachment]
    }), jasmine.any(Object));
    expect(commentService.put).toHaveBeenCalledWith(commentId, jasmine.objectContaining({
      fileLibraryAttachments: [expectedFileLibraryAttachment]
    }), jasmine.any(Object));
  }));

  it('should reset the form', () => {
    // given
    const attachment = {uid: 'a'} as TimelineFormAttachment;
    component.form.setValue({
      message: 'test message',
      attachments: [attachment]
    });

    // when
    component.reset();

    // then
    component.formState$.subscribe(status => expect(status).toBe(CommentFormStatus.START));
    expect(component.form.getRawValue()).toEqual({
      message: '',
      attachments: []
    });
  });

  it('should cancel the form', fakeAsync(() => {
    // given
    let eventsEmitted = 0;
    component.comment = {} as any;
    component.cancel.subscribe(() => {
      eventsEmitted++;
    });

    // when
    component.cancelForm();

    // then
    tick();
    expect(eventsEmitted).toBe(1);
    expect(component.comment).toBe(null);
  }));

  it('should set focus state on focus', () => {
    // given
    expect(component.formActionsActive).toBe(false);

    // when
    component.messageRef.nativeElement.dispatchEvent(new Event('focus'));

    // then
    expect(component.formActionsActive).toBe(true);
  });

  it('should react to change of comment input propery', fakeAsync(() => {
    // given
    component.attachments$ = null;
    const newComment = {message: '123', attachments: []} as any;
    component.comment = newComment;
    const changes = {
      comment: new SimpleChange(null, newComment, false)
    };

    // when
    component.ngOnChanges(changes);

    // then
    tick(50);
    expect(component.attachments$).toBeDefined();
    expect(component.form.getRawValue()).toEqual(newComment);
  }));

});
