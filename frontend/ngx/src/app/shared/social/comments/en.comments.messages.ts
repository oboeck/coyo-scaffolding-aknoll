import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'COMMENTS.ACTIONS.UPDATE': 'Update',
    'COMMENTS.ACTIONS.CANCEL': 'cancel',
    'COMMENTS.ACTIONS.SEND': 'send',
    'COMMENTS.ACTIONS.TRY_AGAIN': 'try again',
    'COMMENTS.ACTIONS.PRESS_ESCAPE': 'Press **ESC** to',
    'COMMENTS.ACTIONS.PRESS_ENTER': 'Press **ENTER** to',
    'COMMENTS.ACTIONS.ERROR': 'Comment can not be edited anymore',
    'COMMENTS.FORM.STATUS.SUBMITTING': 'Submitting…',
    'COMMENTS.FORM.PLACEHOLDER': 'Add a comment',
    'COMMENTS.FORM.PLACEHOLDER.SHORT': 'Comment…',
    'COMMENTS.LOAD_MORE': 'Load more comments',
    'COMMENTS.EDITED': 'Edited',
    'COMMENTS.NEW': 'New'
  }
};
