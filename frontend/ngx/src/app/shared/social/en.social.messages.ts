import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'COMMONS.SHARES.MESSAGE.HELP': 'The message for your post',
    'COMMONS.SHARES.MESSAGE.LABEL': 'Message',
    'COMMONS.SHARES.PLACEHOLDER': 'Select Target',
    'COMMONS.SHARES.SUBMIT': 'Share',
    'COMMONS.SHARES.SUCCESS': 'Successfully shared',
    'COMMONS.SHARES.TIMELINE.HELP': 'Select whom to share this element with',
    'COMMONS.SHARES.TIMELINE.LABEL': 'Share with',
    'COMMONS.SHARES.EXTENDED_OPTIONS': 'Extended options',
    'COMMONS.SHARES.FROM.HELP': 'Select as whom to share this element with',
    'COMMONS.SHARES.FROM.LABEL': 'Share as',
    'COMMONS.SHARES.STICKY.EXPIRY.HELP': 'Defines the time until which a post will be sticky',
    'COMMONS.SHARES.STICKY.EXPIRY.LABEL': 'Sticky period',
    'COMMONS.SHARES.CREATE.TITLE': 'Share',
    'COMMONS.SHARES.LIST.TITLE': 'Shares',
    'COMMONS.SHARES.WARNING.PRIVATE': 'You share a non public post, are you sure?',
    'EVENT.SHARE': 'Share event',
    'MODULE.TIMELINE.TOOLTIPS.SUBSCRIBE': 'Click to subscribe',
    'MODULE.TIMELINE.TOOLTIPS.UNSUBSCRIBE': 'Click to unsubscribe',
    'MODULE.SOCIAL.LIKE.TEXT': '{isLiked, select, true{You{othersCount, plural, offset:1 =0{} =1{ and {latest}} ' +
      '=2{, {latest} and # other} other{, {latest} and # others}}} ' +
      'other{{othersCount, plural, offset:1 =0{} =1{{latest}} =2{{latest} and # other} other{{latest} and # others}}}}',
    'PAGE.SHARE': 'Share page',
    'SHARE_BUTTON.SHARE_COUNT.LABEL': '{count, plural, one{1 share} other{# shares}}',
    'SHARE_BUTTON.SHARE_MODAL.OPEN.ARIA': 'Share content',
    'SHARE_BUTTON.SHARES_MODAL.OPEN.ARIA': '{count, plural, one{One share} other{# shares}}. View {count, plural, one{share} other{all shares}}.',
    'WORKSPACE.SHARE': 'Share workspace'
  }
};
