import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {LikeState} from '@domain/like/like-state';

/**
 * Component rendering the like button
 */
@Component({
  selector: 'coyo-like-button',
  templateUrl: './like-button.component.html',
  styleUrls: ['./like-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LikeButtonComponent {

  /**
   * The state of the button
   */
  @Input() state: LikeState;

  /**
   * Flag for showing condensed information ('Like' text only on timeline-items, Icon only on other occurrences)
   */
  @Input() condensed: boolean;

  /**
   * Emits when the like button is clicked and the state should toggle
   */
  @Output() toggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  /**
   * Called when the like button is clicked.
   */
  onToggle(): void {
    if (!this.state.isLoading) {
      this.toggle.emit(!this.state.isLiked);
    }
  }
}
