import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {BaseModel} from '@domain/base-model/base-model';
import {LikeState} from '@domain/like/like-state';
import {LikeService} from '@domain/like/like.service';
import {Likeable} from '@domain/like/likeable';
import {Sender} from '@domain/sender/sender';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {Subject} from 'rxjs';
import {LikeContainerComponent} from './like-container.component';

describe('LikeContainerComponent', () => {
  let component: LikeContainerComponent;
  let fixture: ComponentFixture<LikeContainerComponent>;
  let likeService: jasmine.SpyObj<LikeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LikeContainerComponent],
      providers: [{
        provide: LikeService,
        useValue: jasmine.createSpyObj('LikeService', ['getLikeTargetState$', 'like', 'unlike'])
      }]
    }).overrideTemplate(LikeContainerComponent, '<div></div>')
      .compileComponents();

    likeService = TestBed.get(LikeService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikeContainerComponent);
    component = fixture.componentInstance;
  });

  it('should init', fakeAsync(() => {
    // given
    const subject = new Subject<LikeState>();
    component.author = {id: 'author-id'} as BaseModel;
    component.target = {id: 'id', typeName: 'timeline-item'} as Likeable;
    likeService.getLikeTargetState$.and.returnValue(subject);

    // when
    component.ngOnInit();

    // then
    expect(likeService.getLikeTargetState$).toHaveBeenCalledWith('author-id', 'id', 'timeline-item', false);
    expect(component.state$).toBe(subject);
  }));

  it('should init without initial request', fakeAsync(() => {
    // given
    const subject = new Subject<LikeState>();
    component.author = {id: 'author-id'} as BaseModel;
    component.target = {id: 'id', typeName: 'timeline-item'} as Likeable;
    component.skipInitRequest = true;
    likeService.getLikeTargetState$.and.returnValue(subject);

    // when
    component.ngOnInit();

    // then
    expect(likeService.getLikeTargetState$).toHaveBeenCalledWith('author-id', 'id', 'timeline-item', true);
    expect(component.state$).toBe(subject);
  }));

  it('should get like target for other sender on author change', () => {
    const subject = new Subject<LikeState>();
    const author1 = {id: 'author-id1'} as Sender;
    const author2 = {id: 'author-id2'} as Sender;
    component.author = author1;
    component.target = {id: 'id', typeName: 'timeline-item'} as Likeable;
    likeService.getLikeTargetState$.and.returnValue(subject);
    fixture.detectChanges();
    likeService.getLikeTargetState$.calls.reset();

    // when
    component.author = author2;
    component.ngOnChanges({author: {firstChange: false, isFirstChange: () => false,
        previousValue: author1, currentValue: author2}});

    // then
    expect(likeService.getLikeTargetState$).toHaveBeenCalledWith(author2.id, 'id', 'timeline-item', false);
    expect(component.state$).toBe(subject);
  });

  it('should like a post', () => {
    // given
    const likeSubject = new Subject<LikeState>();
    likeService.like.and.returnValue(likeSubject);

    component.target = {
      id: 'id',
      typeName: 'timeline-item'
    } as TimelineItem;

    component.author = {
      id: 'author-id'
    } as Sender;

    // when
    component.toggle({isLiked: false} as LikeState);

    // then
    expect(likeService.like).toHaveBeenCalledWith('author-id', 'id', 'timeline-item');
    expect(likeSubject.observers.length).toBe(1);
  });

  it('should unlike a post', () => {
    // given
    const likeSubject = new Subject<LikeState>();
    likeService.unlike.and.returnValue(likeSubject);

    component.target = {
      id: 'id',
      typeName: 'timeline-item'
    } as TimelineItem;

    component.author = {
      id: 'author-id'
    } as Sender;

    // when
    component.toggle({isLiked: true} as LikeState);

    // then
    expect(likeService.unlike).toHaveBeenCalledWith('author-id', 'id', 'timeline-item');
    expect(likeSubject.observers.length).toBe(1);
  });
});
