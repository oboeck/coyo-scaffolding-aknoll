import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {LikeState} from '@domain/like/like-state';
import {Likeable} from '@domain/like/likeable';
import {Sender} from '@domain/sender/sender';
import {LikesModalService} from '@shared/social/likes/likes-modal/likes-modal.service';

interface LikeCountInfo {
  othersCount: number;
  latest: string;
  isLiked: boolean;
  count: number;
  others: Sender[];
  total: Sender[];
  totalCount: number;
}

/**
 * Component showing information on who liked a target.
 */
@Component({
  selector: 'coyo-like-info',
  templateUrl: './like-info.component.html',
  styleUrls: ['./like-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LikeInfoComponent implements OnChanges {

  /**
   * The like target.
   */
  @Input() target: Likeable;

  /**
   * Flag for condensed information (only the count will be shown).
   */
  @Input() condensed: boolean;

  /**
   * The like state.
   */
  @Input() state: LikeState;

  info: LikeCountInfo;

  constructor(private likesModalService: LikesModalService) {
  }

  /**
   * Open a modal showing all the likes for the target.
   */
  openLikes(): void {
    this.likesModalService.open(this.target);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.info = this.getCountAndLatest(this.state);
  }

  private getCountAndLatest(state: LikeState): LikeCountInfo {
    return state ? {
      count: state.totalCount,
      othersCount: state.othersCount,
      latest: state.others.length ? state.others[0].displayName : undefined,
      isLiked: state.isLiked,
      others: state.others,
      totalCount: state.totalCount,
      total: state.total
    } : null;
  }
}
