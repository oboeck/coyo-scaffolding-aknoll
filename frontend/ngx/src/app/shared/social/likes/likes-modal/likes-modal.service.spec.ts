import {TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {Likeable} from '@domain/like/likeable';
import {LikesModalComponent} from './likes-modal.component';
import {LikesModalService} from './likes-modal.service';

describe('LikesModalService', () => {
  let service: LikesModalService;
  let matDialog: jasmine.SpyObj<MatDialog>;

  beforeEach(() => TestBed.configureTestingModule({
    providers: [{
      provide: MatDialog,
      useValue: jasmine.createSpyObj('MatDialog', ['open'])
    }]
  }));

  beforeEach(() => {
    service = TestBed.get(LikesModalService);
    matDialog = TestBed.get(MatDialog);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open the like modal', () => {
    // given
    const likable = {
      id: 'id',
      typeName: 'timeline-item'
    } as Likeable;

    // when
    service.open(likable);

    // then
    expect(matDialog.open).toHaveBeenCalledWith(LikesModalComponent, {
      data: likable
    });
  });
});
