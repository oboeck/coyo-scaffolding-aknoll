import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Likeable} from '@domain/like/likeable';
import {LikesModalComponent} from './likes-modal.component';

/**
 * Modal service for opening the likes modal.
 */
@Injectable({
  providedIn: 'root'
})
export class LikesModalService {

  constructor(private dialog: MatDialog) {
  }

  /**
   * Opens a modal listing all the likes for the given target.
   *
   * @param target the like target
   * @return the modal reference
   */
  open(target: Likeable): MatDialogRef<LikesModalComponent, any> {
    return this.dialog.open(LikesModalComponent, {
      data: target
    });
  }
}
