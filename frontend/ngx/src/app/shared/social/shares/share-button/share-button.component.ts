import {ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AuthService} from '@core/auth/auth.service';
import {Share} from '@domain/share/share';
import {Shareable} from '@domain/share/shareable';
import {User} from '@domain/user/user';
import {Ng1NotificationService} from '@root/typings';
import {ShareModalComponent} from '@shared/social/shares/share-modal/share-modal.component';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {NgxPermissionsService} from 'ngx-permissions';
import {from} from 'rxjs';
import {ShareModalData} from '../share-modal/share-modal-data';

/**
 * Renders a share button and a share count from the perspective of the current user.
 */
@Component({
  selector: 'coyo-share-button',
  templateUrl: './share-button.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareButtonComponent implements OnInit {

  private currentUser: User;

  /**
   * Model with data.
   */
  @Input() target: Shareable;

  /**
   * Only show the icon.
   */
  @Input() condensed: boolean;

  /**
   * Event if a share was created.
   */
  @Output() shareCreated: EventEmitter<Share> = new EventEmitter<Share>();

  constructor(private authService: AuthService,
              private permissionService: NgxPermissionsService,
              private dialog: MatDialog,
              @Inject(NG1_NOTIFICATION_SERVICE) private notificationService: Ng1NotificationService) {
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(user => this.currentUser = user);
  }

  /**
   * Opens a modal with the share form.
   */
  openShareModal(): void {
    this.dialog.open<ShareModalComponent, ShareModalData>(ShareModalComponent, {
      data: {
        initialSender: this.currentUser,
        parentIsPublic: this.target.parentPublic,
        targetId: this.target.id,
        typeName: this.target.typeName,
        canCreateStickyShare: from(this.permissionService.hasPermission('CREATE_STICKY_TIMELINE_ITEM'))
      }
    }).afterClosed().subscribe((createdShare: Share) => {
      if (createdShare) {
        this.shareCreated.emit(createdShare);
        this.notificationService.success('COMMONS.SHARES.SUCCESS');
      }
    });
  }
}
