import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {ShareContainerComponent} from '@shared/social/shares/share-container/share-container.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoShareContainer', downgradeComponent({
    component: ShareContainerComponent
  }));
