import {Sender} from '@domain/sender/sender';
import {Observable} from 'rxjs';

/**
 * The data that needs to be provided to the ShareModalComponent.
 */
export interface ShareModalData {
  initialSender: Sender;
  parentIsPublic: boolean;
  targetId: string;
  typeName: string;
  canCreateStickyShare: Observable<boolean>;
}
