import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {StickyExpiryPeriods} from '@app/timeline/timeline-form/sticky-btn/sticky-expiry-periods';
import {UrlService} from '@core/http/url/url.service';
import {Sender} from '@domain/sender/sender';
import {ShareService} from '@domain/share/share.service';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {of} from 'rxjs';
import {ShareModalComponent} from './share-modal.component';

describe('ShareWithModalComponent', () => {
  let component: ShareModalComponent;
  let fixture: ComponentFixture<ShareModalComponent>;
  let shareService: jasmine.SpyObj<ShareService>;
  let urlService: jasmine.SpyObj<UrlService>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<ShareModalComponent>>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShareModalComponent],
      providers: [FormBuilder, {
        provide: ShareService,
        useValue: jasmine.createSpyObj('ShareService', ['post'])
      }, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['toUrlParamString'])
      }, {
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('MatDialogRef', ['close'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: {
          initialSender: {
            typeName: 'user',
            displayName: 'Robert Lang'
          } as Sender,
          parentIsPublic: true,
          targetId: 'target-id',
          typeName: 'type-name',
          canCreateStickyShare: of(true)
        }
      }, {
        provide: NG1_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('notificationService', ['error'])
      }]
    }).overrideTemplate(ShareModalComponent, '')
      .compileComponents();

    shareService = TestBed.get(ShareService);
    urlService = TestBed.get(UrlService);
    dialogRef = TestBed.get(MatDialogRef);
  }));

  beforeEach(() => {
    shareService.post.and.returnValue(of({key: 'value'}));
    urlService.toUrlParamString.and.callThrough();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    expect(component.shareForm).toBeTruthy();
    expect(component.shareForm.contains('shareWith')).toBeTruthy();
    expect(component.shareForm.contains('message')).toBeTruthy();
    expect(component.shareForm.contains('shareAs')).toBeTruthy();
    expect(component.shareForm.contains('stickyExpiry')).toBeTruthy();

    expect(component.showExtended).toBeFalsy();
    expect(urlService.toUrlParamString).toHaveBeenCalledWith('type', ['user', 'page', 'workspace']);
    expect(component.shareWithConfig.findSharingRecipients).toBeTruthy();
    expect(urlService.toUrlParamString).toHaveBeenCalledWith('type', ['user', 'page', 'workspace', 'event']);
    expect(component.shareAsConfig.findOnlyManagedSenders).toBeTruthy();
  });

  it('should disable sticky expiry field if type name is user', () => {
    component.shareForm.controls.shareWith.setValue({typeName: 'user'} as Sender);
    expect(component.shareForm.controls.stickyExpiry.disabled).toBeTruthy();
  });

  it('should provide form data to parent after submit', () => {
    // given
    component.shareForm.controls['shareWith'].setValue({id: 'share-with-id'});
    component.shareForm.controls['shareAs'].setValue({id: 'share-as-id'});
    component.shareForm.controls['message'].setValue('Message');
    component.shareForm.controls['stickyExpiry'].setValue(StickyExpiryPeriods.oneDay);
    spyOn(component.shareForm, 'disable');

    // when
    component.onSubmit();

    // then
    expect(component.shareForm.disable).toHaveBeenCalled();
    expect(shareService.post).toHaveBeenCalled();
    fixture.detectChanges();
    expect(dialogRef.close).toHaveBeenCalledWith({key: 'value'});
  });
});
