import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {AuthService} from '@core/auth/auth.service';
import {Share} from '@domain/share/share';
import {Ng1NotificationService} from '@root/typings';
import {ShareModalComponent} from '@shared/social/shares/share-modal/share-modal.component';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {NgxPermissionsService} from 'ngx-permissions';
import {of, Subject} from 'rxjs';
import {ShareNavBarButtonComponent} from './share-nav-bar-button.component';

describe('ShareLinkComponent', () => {
  let component: ShareNavBarButtonComponent;
  let fixture: ComponentFixture<ShareNavBarButtonComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let permissionService: jasmine.SpyObj<NgxPermissionsService>;
  let dialog: jasmine.SpyObj<MatDialog>;
  let notificationService: jasmine.SpyObj<Ng1NotificationService>;
  let onCloseSubject: Subject<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShareNavBarButtonComponent],
      providers: [{
        provide: AuthService,
        useValue: jasmine.createSpyObj('authService', ['getUser'])
      }, {
        provide: NgxPermissionsService,
        useValue: jasmine.createSpyObj('permissionService', ['hasPermission'])
      }, {
        provide: MatDialog,
        useValue: jasmine.createSpyObj('dialog', ['open'])
      }, {
        provide: NG1_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('notificationService', ['success'])
      }]
    }).overrideTemplate(ShareNavBarButtonComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    permissionService = TestBed.get(NgxPermissionsService);
    dialog = TestBed.get(MatDialog);
    notificationService = TestBed.get(NG1_NOTIFICATION_SERVICE);
    onCloseSubject = new Subject<any>();
  }));

  beforeEach(() => {
    authService.getUser.and.returnValue(of({}));
    permissionService.hasPermission.and.returnValue(of(true));
    dialog.open.and.returnValue({
      afterClosed: () => onCloseSubject.asObservable()
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareNavBarButtonComponent);
    component = fixture.componentInstance;

    // given data
    component.shareTarget = {
      id: 'target-id',
      typeName: 'timeline-item',
      public: true
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // when
    fixture.detectChanges();

    // then
    expect(authService.getUser).toHaveBeenCalled();
  });

  describe('Share modal', () => {

    it('should open', () => {
      // given
      fixture.detectChanges();

      // when
      component.openShareModal();

      // then
      expect(dialog.open).toHaveBeenCalled();
    });

    it('should open with initial state', () => {
      // given
      fixture.detectChanges();

      // when
      component.openShareModal();

      // then
      const expectedInitialState = {
        initialSender: {},
        parentIsPublic: true,
        canCreateStickyShare: true,
        targetId: 'target-id',
        typeName: 'timeline-item'
      };

      const args = dialog.open.calls.mostRecent().args;
      expect(args[0]).toEqual(ShareModalComponent);
      expect(args[1].data).toBeDefined();
      expect(args[1].data.initialSender).toEqual(expectedInitialState.initialSender);
      expect(args[1].data.parentIsPublic).toEqual(expectedInitialState.parentIsPublic);
      expect(args[1].data.targetId).toEqual(expectedInitialState.targetId);
      expect(args[1].data.typeName).toEqual(expectedInitialState.typeName);
      args[1].data.canCreateStickyShare.subscribe((canCreateStickyShare: boolean) =>
        expect(canCreateStickyShare).toEqual(expectedInitialState.canCreateStickyShare));
      expect(permissionService.hasPermission).toHaveBeenCalledWith('CREATE_STICKY_TIMELINE_ITEM');
    });

    it('should open and process result on close', () => {
      // when
      component.openShareModal();
      onCloseSubject.next({} as Share);

      // then
      expect(notificationService.success).toHaveBeenCalled();
    });
  });
});
