import {Share} from '@domain/share/share';
import {TimelineItemType} from '@domain/timeline-item/timeline-item-type';

/**
 * The data that needs to be provided to the SharesModalComponent.
 */
export interface SharesModalData {
  itemType: TimelineItemType;
  shares: Share[];
}
