import {SimpleChange, SimpleChanges} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {Share} from '@domain/share/share';
import {ShareService} from '@domain/share/share.service';
import {User} from '@domain/user/user';
import {of} from 'rxjs';
import {TimelineShareComponent} from './timeline-share.component';

describe('TimelineShareComponent', () => {
  let component: TimelineShareComponent;
  let fixture: ComponentFixture<TimelineShareComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let shareService: jasmine.SpyObj<ShareService>;
  let user: User;
  const initChanges = {
    share: {
      isFirstChange: () => true
    } as SimpleChange
  } as SimpleChanges;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineShareComponent],
      providers: [{
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['getUser'])
      }, {
        provide: ShareService,
        useValue: jasmine.createSpyObj('ShareService', ['getOriginalAuthor', 'post', 'delete'])
      }]
    }).overrideTemplate(TimelineShareComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    shareService = TestBed.get(ShareService);
  }));

  beforeEach(() => {
    user = {id: 'author-id'} as User;
    authService.getUser.and.returnValue(of(user));
    shareService.post.and.returnValue(of({} as Share));
    shareService.delete.and.returnValue(of(null));
    shareService.getOriginalAuthor.and.returnValue(of({id: 'original-author-id'} as User));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineShareComponent);
    component = fixture.componentInstance;
    component.itemType = 'page' as any;
    component.share = {
      id: 'share-id',
      author: {id: 'author-id'},
      recipient: {id: 'recipient-id'},
      stickyExpiry: 123,
      _permissions: {
        accessoriginalauthor: true,
        sticky: true,
        delete: true
      }
    } as Share;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create state', () => {
    // then
    const state = component.shareState.getValue();
    expect(state.isLoading).toEqual(true);
    expect(state.currentUser).toEqual(null);
    expect(state.shownAuthor).toEqual(null);
    expect(state.originalAuthor).toEqual(null);
    expect(state.i18nAuthor).toEqual('NONE');
    expect(state.i18nRecipient).toEqual('NONE');
    expect(state.canToggleAuthor).toEqual(false);
    expect(state.canMakeUnsticky).toEqual(false);
    expect(state.canDeleteShare).toEqual(false);
  });

  it('should init state', () => {
    // when
    component.ngOnChanges(initChanges);

    // then
    const state = component.shareState.getValue();
    expect(state.isLoading).toEqual(false);
    expect(state.currentUser).toEqual(user);
    expect(state.shownAuthor).toEqual(component.share.author);
    expect(state.originalAuthor).toEqual(null);
    expect(state.i18nAuthor).toEqual('YOU');
    expect(state.i18nRecipient).toEqual('OTHER');
    expect(state.canToggleAuthor).toEqual(false);
    expect(state.canMakeUnsticky).toEqual(true);
    expect(state.canDeleteShare).toEqual(true);
  });

  it('should show original author', () => {
    // when
    component.ngOnChanges(initChanges);
    component.toggleAuthor();

    // then
    const state = component.shareState.getValue();
    expect(shareService.getOriginalAuthor).toHaveBeenCalledWith('share-id');
    expect(state.shownAuthor).toEqual({id: 'original-author-id'} as User);
    expect(state.originalAuthor).toEqual({id: 'original-author-id'} as User);
    expect(state.i18nAuthor).toEqual('OTHER');
    expect(state.i18nRecipient).toEqual('OTHER');
  });

  it('should mark share as read', () => {
    // given
    const context = {id: 'share-id'};
    const permissions = ['*'];
    const path = '/{id}/read';

    // when
    component.ngOnChanges(initChanges);
    component.markShareAsRead();

    // then
    expect(shareService.post).toHaveBeenCalledWith(null, {context, permissions, path});
    expect(component.shareState.getValue().isLoading).toEqual(false);
  });

  it('should make share unsticky', () => {
    // given
    const context = {id: 'share-id'};
    const permissions = ['*'];
    const path = '/{id}/unsticky';

    // when
    component.ngOnChanges(initChanges);
    component.makeUnsticky();

    // then
    expect(shareService.post).toHaveBeenCalledWith(null, {context, permissions, path});
    expect(component.shareState.getValue().isLoading).toEqual(false);
  });

  it('should delete share', () => {
    // given
    spyOn(component.shareDeleted, 'emit');

    // when
    component.ngOnChanges(initChanges);
    component.deleteShare();

    // then
    expect(shareService.delete).toHaveBeenCalledWith('share-id');
    expect(component.shareState.getValue().isLoading).toEqual(false);
    expect(component.shareDeleted.emit).toHaveBeenCalledWith(component.share);
  });
});
