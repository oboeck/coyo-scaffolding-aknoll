import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'TIME_AGO.NOW': 'Just now',
    'TIME_AGO.NOW.SHORT': 'Just now',
    'TIME_AGO.MINUTES': '{minutes} minutes ago',
    'TIME_AGO.MINUTES.SHORT': '{minutes} min.',
    'TIME_AGO.TODAY': 'Today, {time}',
    'TIME_AGO.TODAY.SHORT': 'Today, {time}',
    'TIME_AGO.YESTERDAY': 'Yesterday, {time}',
    'TIME_AGO.YESTERDAY.SHORT': 'Yesterday, {time}',
    'TIME_AGO.EXACT': '{date}, {time}',
    'TIME_AGO.EXACT.SHORT': '{date}, {time}',
    'TIME_AGO.DATE': 'MMM D, YYYY',
    'TIME_AGO.DATE.SHORT': '{sameYear, select, true{MM/DD} other{MM/DD/YY}}',
    'TIME_AGO.TIME': 'hh:mm A',
    'TIME_AGO.TIME.SHORT': 'hh:mm A'
  }
};
