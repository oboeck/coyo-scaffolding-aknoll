import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {TimeService} from './time.service';

getAngularJSGlobal()
  .module('commons.ui')
  .factory('ngxTimeService', downgradeInjectable(TimeService));
