import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

/**
 * Marks a given resource URL as trusted.
 */
@Pipe({
  name: 'trustUrl'
})
export class TrustUrlPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {}

  /**
   * Bypasses the angular security check for the given value.
   *
   * @param value the trusted URL
   * @return the safe resource URL
   */
  transform(value: string): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(value);
  }
}
