import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {SenderEvent} from '@domain/event/SenderEvent';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {of} from 'rxjs';
import {EventShareItemComponent} from './event-share-item.component';

describe('EventShareItemComponent', () => {
  let component: EventShareItemComponent;
  let fixture: ComponentFixture<EventShareItemComponent>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  const sender = {
    description: '<p>Test <b>description</b></p>'
  } as SenderEvent;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventShareItemComponent],
      providers: [{
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['observeScreenChange'])
      }]
    }).overrideTemplate(EventShareItemComponent, '')
      .compileComponents();

    windowSizeService = TestBed.get(WindowSizeService);
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.XS));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventShareItemComponent);
    component = fixture.componentInstance;
    component.item = {
      itemType: 'event',
      data: {
        event: sender
      } as any
    } as TimelineItem;
    fixture.detectChanges();
  });

  it('should not strip description from html', () => {
    expect(component.description).toBe('<p>Test <b>description</b></p>');
  });

  it('should return same day', () => {
    // given
    component.sender.startDate = new Date(500000000000);
    component.sender.endDate = new Date(500000000005);

    // then
    expect(component.isSameDay).toBeTruthy();
  });

  it('should not return same day', () => {
    // given
    component.sender.startDate = new Date(500000000000);
    component.sender.endDate = new Date(400000000004);

    // then
    expect(component.isSameDay).toBeTruthy();
  });
});
