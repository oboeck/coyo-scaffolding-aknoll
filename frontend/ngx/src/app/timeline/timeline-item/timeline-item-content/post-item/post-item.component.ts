import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {TimelineItemContent} from '../timeline-item-content';

/**
 * Content component for a normal timeline post.
 */
@Component({
  selector: 'coyo-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostItemComponent extends TimelineItemContent {

  previewUrl: string = '/web/timeline-items/{{groupId}}/attachments/{{id}}';

  translation: string;

  constructor(cd: ChangeDetectorRef) {
    super(cd);
  }
}
