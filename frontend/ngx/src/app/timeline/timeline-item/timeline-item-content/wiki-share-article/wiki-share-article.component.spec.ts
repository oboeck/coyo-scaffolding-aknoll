import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {App} from '@domain/apps/app';
import {Sender} from '@domain/sender/sender';
import {Target} from '@domain/sender/target';
import {TargetService} from '@domain/sender/target/target.service';
import {SharedArticle} from '@domain/share/sharedArticle';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {WikiShareArticleComponent} from './wiki-share-article.component';

describe('WikiShareArticleComponent', () => {
  let component: WikiShareArticleComponent;
  let fixture: ComponentFixture<WikiShareArticleComponent>;
  let targetService: jasmine.SpyObj<TargetService>;
  let timelineItemService: jasmine.SpyObj<TimelineItemService>;
  const article = {
    author: {
      typeName: 'user'
    } as Sender,
    typeName: 'wiki-article',
    id: '12345',
    app: {
      id: '54321',
      slug: 'wiki',
      key: 'wiki'
    } as App,
    sender: {
      id: '88888',
      slug: 'company-news',
      target: {
        name: 'page'
      } as Target
    } as Sender
  } as SharedArticle;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WikiShareArticleComponent],
      providers: [
        {provide: TargetService, useValue: jasmine.createSpyObj('targetService', ['getLinkTo'])},
        {provide: TimelineItemService, useValue: jasmine.createSpyObj('timelineItemService', ['getAuthorIcon'])}
      ]
    }).overrideTemplate(WikiShareArticleComponent, '<div></div>')
      .compileComponents();

    targetService = TestBed.get(TargetService);
    timelineItemService = TestBed.get(TimelineItemService);
    targetService.getLinkTo.and.returnValue('/pages/company-news/apps/wiki/wiki/list/view/12345');
    timelineItemService.getAuthorIcon.and.returnValue('account');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiShareArticleComponent);
    component = fixture.componentInstance;
  });

  it('should init', () => {
    // given
    component.item = {
      data: {
        article: article
      }
    } as any;
    // when
    fixture.detectChanges();
    // then
    expect(component.articleLink).toBeDefined();
    expect(component.authorIcon).toBeDefined();
  });

  it('should init with articleTarget', () => {
    // given
    component.item = {
      data: {
        article: {
          ...article,
          articleTarget: {
            name: article.typeName,
            params: {
              id: article.id,
              appId: article.app.id,
              appSlug: article.app.slug,
              appKey: article.app.key,
              senderId: article.sender.id,
              senderSlug: article.sender.slug,
              senderType: article.sender.target.name
            }
          }
        }
      }
    } as any;

    // when
    fixture.detectChanges();

    // then
    expect(component.articleLink).toBeDefined();
    expect(component.authorIcon).toBeDefined();
  });

  it('should not be wiki article', () => {
    // given
    component.item = {
      data: {
        article: undefined,
        typeName: 'post'
      }
    } as any;

    // when
    fixture.detectChanges();

    // then
    expect(component.articleLink).not.toBeDefined();
    expect(component.authorIcon).not.toBeDefined();
  });
});
