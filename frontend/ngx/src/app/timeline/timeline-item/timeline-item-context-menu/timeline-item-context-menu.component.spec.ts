import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {WINDOW} from '@root/injection-tokens';
import {Ng1CoyoNotification} from '@root/typings';
import {NG1_AUTH_SERVICE, NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {Subject} from 'rxjs';
import {TimelineItemEditModalComponent} from '../timeline-item-edit-modal/timeline-item-edit-modal.component';
import {TimelineItemReportModalComponent} from '../timeline-item-report-modal/timeline-item-report-modal.component';
import {TimelineItemContextMenuComponent} from './timeline-item-context-menu.component';

describe('TimelineItemContextMenuComponent', () => {
  let component: TimelineItemContextMenuComponent;
  let fixture: ComponentFixture<TimelineItemContextMenuComponent>;
  let notificationService: jasmine.SpyObj<Ng1CoyoNotification>;
  let dialog: jasmine.SpyObj<MatDialog>;
  let onCloseSubject: Subject<boolean>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineItemContextMenuComponent],
      providers: [{
        provide: WINDOW,
        useValue: {
          location: {
            protocol: 'http:',
            host: 'test.host'
          }
        }
      }, {
        provide: NG1_AUTH_SERVICE,
        useValue: jasmine.createSpyObj('authService', ['getCurrentUserId'])
      }, {
        provide: NG1_COYO_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('notificationService', ['success'])
      }, {
        provide: MatDialog,
        useValue: jasmine.createSpyObj('dialog', ['open'])
      }]
    }).overrideTemplate(TimelineItemContextMenuComponent, '')
      .compileComponents();

    dialog = TestBed.get(MatDialog);
    dialog.open.and.returnValue({afterClosed: () => onCloseSubject.asObservable()});
    notificationService = TestBed.get(NG1_COYO_NOTIFICATION_SERVICE);
    onCloseSubject = new Subject<boolean>();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    onCloseSubject.complete();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the show original author event', fakeAsync(() => {
    // when
    component.showAuthor.subscribe((showAuthor: boolean) => {
      expect(showAuthor).toBeTruthy();
    });

    component.onClickShowOriginalAuthors();

    // then --> observable
    tick();
  }));

  it('should generate the deep link', () => {
    // given
    component.item = {id: 'testId'} as TimelineItem;

    // when
    const result = component.getTimelineItemLink();

    // then
    expect(result).toBe('http://test.host/timeline/item/testId');
  });

  it('should notify the user about clipboard changes', () => {
    // when
    component.copyLinkSuccessNotification();

    // then
    expect(notificationService.success).toHaveBeenCalledWith('MODULE.TIMELINE.COPY_LINK.SUCCESS');
  });

  it('should open edit modal', () => {
    // given
    component.item = {id: 'target-id'} as TimelineItem;

    // when
    component.openEditModal();

    // then
    expect(dialog.open).toHaveBeenCalledWith(TimelineItemEditModalComponent, {
      autoFocus: true,
      data: {
        itemId: component.item.id
      }
    });
  });

  it('should open edit modal and process result', () => {
    // given
    component.item = {id: 'target-id'} as TimelineItem;

    // when
    component.openEditModal();
    onCloseSubject.next(true);

    // then
    expect(notificationService.success).toHaveBeenCalled();
  });

  it('should open report modal', () => {
    // given
    component.item = {id: 'target-id'} as TimelineItem;

    // when
    component.openReportForm();

    // then
    expect(dialog.open).toHaveBeenCalledWith(TimelineItemReportModalComponent, {
      autoFocus: true,
      data: {
        targetId: component.item.id,
        targetType: component.item.typeName
      }
    });
  });

  it('should open report modal and process result', () => {
    // given
    component.item = {id: 'target-id'} as TimelineItem;

    // when
    component.openReportForm();
    onCloseSubject.next(true);

    // then
    expect(notificationService.success).toHaveBeenCalled();
  });
});
