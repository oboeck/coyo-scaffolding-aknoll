import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {LikeState} from '@domain/like/like-state';
import {LikeService} from '@domain/like/like.service';
import {Sender} from '@domain/sender/sender';
import {ShareService} from '@domain/share/share.service';
import {TimelineItemTarget} from '@domain/timeline-item/timeline-item-target';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {of, Subject} from 'rxjs';
import {skip} from 'rxjs/operators';
import {TimelineItemFooterComponent} from './timeline-item-footer.component';

describe('TimelineItemFooterComponent', () => {
  let component: TimelineItemFooterComponent;
  let fixture: ComponentFixture<TimelineItemFooterComponent>;
  let likeService: jasmine.SpyObj<LikeService>;
  let shareService: jasmine.SpyObj<ShareService>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineItemFooterComponent],
      providers: [{
        provide: LikeService,
        useValue: jasmine.createSpyObj('likeService', ['getLikeTargetState$', 'like', 'unlike'])
      }, {
        provide: TimelineItemService,
        useValue: jasmine.createSpyObj('timelineItemService', ['getShareCount'])
      }, {
        provide: ShareService,
        useValue: jasmine.createSpyObj('shareService', ['getShareCount'])
      }, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['observeScreenChange'])
      }]
    }).overrideTemplate(TimelineItemFooterComponent, '')
      .compileComponents();

    likeService = TestBed.get(LikeService);
    shareService = TestBed.get(ShareService);
    windowSizeService = TestBed.get(WindowSizeService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemFooterComponent);
    component = fixture.componentInstance;

    component.target = {
      id: 'id',
      typeName: 'timeline-item'
    } as TimelineItemTarget;

    component.author = {
      id: 'author-id'
    } as Sender;

    likeService.getLikeTargetState$.and.returnValue(of(LikeService.INITIAL_STATE));
    shareService.getShareCount.and.returnValue(of(5));
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.MD));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should like a post', () => {
    // given
    const likeSubject = new Subject<LikeState>();
    likeService.like.and.returnValue(likeSubject);

    component.target = {
      id: 'id',
      typeName: 'timeline-item'
    } as TimelineItemTarget;

    component.author = {
      id: 'author-id'
    } as Sender;

    // when
    component.toggleLike(true);

    // then
    expect(likeService.like).toHaveBeenCalledWith('author-id', 'id', 'timeline-item');
    expect(likeSubject.observers.length).toBe(1);
  });

  it('should unlike a post', () => {
    // given
    const likeSubject = new Subject<LikeState>();
    likeService.unlike.and.returnValue(likeSubject);

    component.target = {
      id: 'id',
      typeName: 'timeline-item'
    } as TimelineItemTarget;

    component.author = {
      id: 'author-id'
    } as Sender;

    // when
    component.toggleLike(false);

    // then
    expect(likeService.unlike).toHaveBeenCalledWith('author-id', 'id', 'timeline-item');
    expect(likeSubject.observers.length).toBe(1);
  });

  it('should create the footer state on init', fakeAsync(() => {
    // given

    // when
    component.ngOnInit();
    component.state$.subscribe(state => {
    });

    // then
    expect(likeService.getLikeTargetState$).toHaveBeenCalledWith('author-id', 'id', 'timeline-item', false);
    expect(shareService.getShareCount).toHaveBeenCalledWith(component.target);
  }));

  it('should create the footer state on init (skip init. request)', fakeAsync(() => {
    // given
    component.skipInitRequest = true;

    // when
    component.ngOnInit();
    component.state$.subscribe(state => {});

    // then
    expect(likeService.getLikeTargetState$).toHaveBeenCalledWith('author-id', 'id', 'timeline-item', true);
    expect(shareService.getShareCount).not.toHaveBeenCalledWith(component.item);
  }));

  it('should update the footer state on author change', fakeAsync(() => {
    // given
    let called = false;
    fixture.detectChanges();

    component.state$.pipe(skip(1)).subscribe(state => {
      called = true;
    });

    likeService.getLikeTargetState$.calls.reset();
    shareService.getShareCount.calls.reset();

    // when
    component.authorChanged.emit({id: 'author-id2'} as Sender);

    // then
    expect(called).toBeTruthy();
    expect(likeService.getLikeTargetState$).toHaveBeenCalledWith('author-id2', 'id', 'timeline-item', false);
    expect(shareService.getShareCount).toHaveBeenCalledWith(component.target);
  }));

  it('should change the state when like state changes', fakeAsync(() => {
    // given
    let called = false;
    const stateSubject = new Subject<LikeState>();

    likeService.getLikeTargetState$.and.returnValue(stateSubject);

    fixture.detectChanges();
    component.state$.pipe(skip(1)).subscribe(state => {
      called = true;
      expect(state.likeState.isLiked).toBeTruthy();
      expect(state.shareCount).toBe(5);
    });
    stateSubject.next(LikeService.INITIAL_STATE);

    // when
    stateSubject.next({isLiked: true} as LikeState);

    // then
    expect(called).toBeTruthy();
  }));
});
