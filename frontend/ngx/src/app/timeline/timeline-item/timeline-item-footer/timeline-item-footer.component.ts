import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {LikeState} from '@domain/like/like-state';
import {LikeService} from '@domain/like/like.service';
import {Sender} from '@domain/sender/sender';
import {Share} from '@domain/share/share';
import {ShareService} from '@domain/share/share.service';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {TimelineItemTarget} from '@domain/timeline-item/timeline-item-target';
import {combineLatest, merge, Observable, of} from 'rxjs';
import {map, startWith, switchMap, tap} from 'rxjs/operators';

interface TimelineItemFooterState {
  likeState: LikeState;
  shareCount: number;
}

/**
 * Footer component for timeline items. Showing all the social interaction items.
 */
@Component({
  selector: 'coyo-timeline-item-footer',
  templateUrl: './timeline-item-footer.component.html',
  styleUrls: ['./timeline-item-footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineItemFooterComponent implements OnInit {

  /**
   * The timeline item of the footer.
   */
  @Input() item: TimelineItem;

  /**
   * The context of the timeline used to calculate the possible functional users.
   */
  @Input() context: Sender;

  /**
   * The default author, mainly the current user.
   */
  @Input() author: Sender;

  /**
   * The target of the timeline item (e.g. a blog article when it is an blog-article share).
   */
  @Input() target: TimelineItemTarget;

  /**
   * Flag if the initial request should be skipped
   */
  @Input() skipInitRequest: boolean = false;

  /**
   * Emits when the author changed by the functional user chooser.
   */
  @Output() authorChanged: EventEmitter<Sender> = new EventEmitter<Sender>();

  /**
   * Emits when a share is created.
   */
  @Output() shareCreated: EventEmitter<Share> = new EventEmitter<Share>();

  /**
   * Emits when a share is deleted.
   */
  @Output() sharesDeleted: EventEmitter<Share[]> = new EventEmitter<Share[]>();

  state$: Observable<TimelineItemFooterState>;

  isXs$: Observable<boolean>;

  constructor(private likeService: LikeService,
              private shareService: ShareService,
              private windowSizeService: WindowSizeService) {
  }

  ngOnInit(): void {
    this.state$ = combineLatest([this.authorChanged, of(false)])
      .pipe(startWith([this.author, this.skipInitRequest]))
      .pipe(switchMap(([author, skipInitRequest]: [Sender, boolean]) =>
        combineLatest([this.getLikesObservable(author, skipInitRequest), this.getSharesObservable(skipInitRequest)])))
      .pipe(map(([likeState, shareCount]) => ({likeState, shareCount})));
    this.isXs$ = this.windowSizeService.observeScreenChange()
      .pipe(map(screenSize => screenSize === ScreenSize.XS));
  }

  /**
   * Toggles the like state and likes/unlikes the target.
   *
   * @param liked true when the target is liked
   */
  toggleLike(liked: boolean): void {
    const response = liked
      ? this.likeService.like(this.author.id, this.target.id, this.target.typeName)
      : this.likeService.unlike(this.author.id, this.target.id, this.target.typeName);
    response.subscribe();
  }

  private getLikesObservable(author: Sender, skipInitRequest: boolean): Observable<LikeState> {
    return this.likeService.getLikeTargetState$(author.id, this.target.id, this.target.typeName, skipInitRequest)
      .pipe(skipInitRequest ? startWith(LikeService.INITIAL_STATE) : tap());
  }

  private getSharesObservable(skipInitRequest: boolean): Observable<number> {
    return merge(this.shareCreated, this.sharesDeleted)
      // disable deprecation rule because the wrong signature is matched due to compiler config,
      // the function that we use is not really deprecated. This is recommended by rxjs
      // https://github.com/ReactiveX/rxjs/issues/4772
      // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
      .pipe(skipInitRequest ? tap() : startWith(null))
      .pipe(switchMap(() => this.shareService.getShareCount(this.target)))
      .pipe(skipInitRequest ? startWith(0) : tap());
  }
}
