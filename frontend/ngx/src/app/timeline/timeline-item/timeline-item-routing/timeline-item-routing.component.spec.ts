import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SocketService} from '@core/socket/socket.service';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {Ng1NotificationService} from '@root/typings';
import {NG1_NOTIFICATION_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import {of} from 'rxjs';
import {TimelineItemRoutingComponent} from './timeline-item-routing.component';

describe('TimelineItemRoutingComponent', () => {
  let component: TimelineItemRoutingComponent;
  let fixture: ComponentFixture<TimelineItemRoutingComponent>;
  let socketService: jasmine.SpyObj<SocketService>;
  let stateService: jasmine.SpyObj<IStateService>;
  let notificationService: jasmine.SpyObj<Ng1NotificationService>;

  const item = {id: 'item-id'} as TimelineItem;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineItemRoutingComponent],
      providers: [{
        provide: SocketService,
        useValue: jasmine.createSpyObj('socketService', ['listenTo$'])
      }, {
        provide: NG1_STATE_SERVICE,
        useValue: jasmine.createSpyObj('$state', ['go'])
      }, {
        provide: NG1_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('service', ['warning'])
      }]
    }).overrideTemplate(TimelineItemRoutingComponent, '')
      .compileComponents();

    socketService = TestBed.get(SocketService);
    stateService = TestBed.get(NG1_STATE_SERVICE);
    notificationService = TestBed.get(NG1_NOTIFICATION_SERVICE);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemRoutingComponent);
    component = fixture.componentInstance;
    component.item = item;
    socketService.listenTo$.and.returnValue(of({}));
    fixture.detectChanges();
  });

  it('should listen to websocket topic for deleted events', () => {
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.' + item.id, 'deleted');
    expect(notificationService.warning).toHaveBeenCalledWith('MODULE.TIMELINE.ITEM.DELETED');
    expect(stateService.go).toHaveBeenCalledWith('main');
  });
});
