import {SimpleChange, SimpleChanges} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {SocketService} from '@core/socket/socket.service';
import {Sender} from '@domain/sender/sender';
import {Share} from '@domain/share/share';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {TimelineItemTarget} from '@domain/timeline-item/timeline-item-target';
import {TimelineItemTargetService} from '@domain/timeline-item/timeline-item-target.service';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {User} from '@domain/user/user';
import {of, Subject} from 'rxjs';
import {TimelineItemComponent} from './timeline-item.component';

describe('TimelineItemComponent', () => {
  let component: TimelineItemComponent;
  let fixture: ComponentFixture<TimelineItemComponent>;
  let timelineItemService: jasmine.SpyObj<TimelineItemService>;
  let timelineItemTargetService: jasmine.SpyObj<TimelineItemTargetService>;
  let authService: jasmine.SpyObj<AuthService>;

  const item = {
    id: 'item-id',
    unread: true,
    isNew: true,
    recipients: []
  } as TimelineItem;
  const target = {} as TimelineItemTarget;
  const user = {} as User;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineItemComponent],
      providers: [ {
        provide: TimelineItemService,
        useValue: jasmine.createSpyObj('timelineItemService', ['markAsRead', 'unsticky', 'delete', 'getOriginalAuthor', 'getRelevantShare'])
      }, {
        provide: TimelineItemTargetService,
        useValue: jasmine.createSpyObj('timelineItemTargetService', ['determineTarget'])
      }, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('authService', ['getUser'])
      }]
    }).overrideTemplate(TimelineItemComponent, '')
      .compileComponents();

    timelineItemService = TestBed.get(TimelineItemService);
    timelineItemTargetService = TestBed.get(TimelineItemTargetService);
    authService = TestBed.get(AuthService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemComponent);
    component = fixture.componentInstance;
    component.timelineItem = item;
    timelineItemTargetService.determineTarget.and.returnValue(of(target));
    authService.getUser.and.returnValue(of(user));
    timelineItemService.markAsRead.and.returnValue(of({}));
    fixture.detectChanges();
  });

  it('should set item to the new timeline item on changes', () => {
    // given
    component.timelineItem = item;
    component.state$.next({...component.state$.getValue(), item: {} as TimelineItem});

    // when
    component.ngOnChanges({
      timelineItem: {} as SimpleChange
    } as SimpleChanges);

    // then
    expect(component.state$.getValue().item).toBe(component.timelineItem);
  });

  it('should set ribbon type to new', () => {
    // given
    const unread = false;
    const isNew = true;

    // when
    const ribbonType = component.getRibbonType(unread, isNew);

    // then
    expect(ribbonType).toBe('new');
  });

  it('should set ribbon type to sticky', () => {
    // given
    const unread = true;
    const isNew = true;

    // when
    const ribbonType = component.getRibbonType(unread, isNew);

    // then
    expect(ribbonType).toBe('sticky');
  });

  it('should mark a timeline item as read', () => {
    // given
    const readItem = {...item, unread: false, isNew: false};
    timelineItemService.markAsRead.and.returnValue(of(readItem));

    // when
    component.markAsRead();

    // then
    expect(component.state$.getValue().item).toEqual(readItem);
    expect(component.state$.getValue().ribbonType).toBeUndefined();
  });

  it('should not call mark a timeline item as read twice', () => {
    // given
    const subject = new Subject<any>();
    timelineItemService.markAsRead.and.returnValue(subject.asObservable());

    // when
    component.markAsRead();
    component.markAsRead();

    // then
    expect(timelineItemService.markAsRead).toHaveBeenCalledTimes(1);
  });

  it('should unsticky a timeline item', () => {
    // given
    const unstickiedItem = {...item, unread: false, isNew: false};
    timelineItemService.unsticky.and.returnValue(of(unstickiedItem));

    // when
    component.unsticky();

    // then
    expect(component.state$.getValue().item).toEqual(unstickiedItem);
    expect(component.state$.getValue().ribbonType).toBeUndefined();
  });

  it('should delete a timeline item', () => {
    // given
    timelineItemService.delete.and.returnValue(of({}));

    // when
    component.remove();

    // then
    expect(timelineItemService.delete).toHaveBeenCalledWith(item.id);
  });

  it('should set show original author to true', () => {
    // given
    const originalAuthor = {} as Sender;
    timelineItemService.getOriginalAuthor.and.returnValue(of(originalAuthor));

    // when
    component.setShowOriginalAuthor(true);

    // then
    expect(component.state$.getValue().originalAuthor).toEqual(originalAuthor);
  });

  it('should set show original author to true', () => {
    // when
    component.setShowOriginalAuthor(false);

    // then
    expect(component.state$.getValue().originalAuthor).toBeNull();
  });
  it('should emit the relevant share and set it', () => {
    // given
    const share1 = {
      id: 'share-1',
      recipient: {}
    } as Share;

    const share2 = {
      id: 'share-2',
      recipient: {}
    } as Share;
    component.setRelevantShare(share1);

    timelineItemService.getRelevantShare.and.returnValue(of(share2));

    // when
    component.emitRelevantShare();

    // then
    expect(component.state$.getValue().relevantShare).toBe(share2);
  });

  it('should set a new relevant share when the old one was deleted', () => {
    // given
    const share1 = {
      id: 'share-1',
      recipient: {}
    } as Share;

    const share2 = {
      id: 'share-2',
      recipient: {}
    } as Share;
    component.setRelevantShare(share1);

    timelineItemService.getRelevantShare.and.returnValue(of(share2));

    // when
    component.deleteShare([share1]);

    // then
    expect(component.state$.getValue().relevantShare).toBe(share2);
  });

  it('should set author', () => {
    // given
    const newAuthor = {} as User;

    // when
    component.setAuthor(newAuthor);

    // then
    expect(component.state$.getValue().author).toBe(newAuthor);
  });
});
