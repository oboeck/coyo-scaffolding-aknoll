import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {SocketService} from '@core/socket/socket.service';
import {Page} from '@domain/pagination/page';
import {Sender} from '@domain/sender/sender';
import {SubscriptionService} from '@domain/subscription/subscription.service';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {NG1_SOCKET_RECONNECT_DELAYS} from '@upgrade/upgrade.module';
import {NgxPermissionsService} from 'ngx-permissions';
import {EMPTY, of, Subject, throwError} from 'rxjs';
import {delay, skip, take} from 'rxjs/operators';
import {TimelineStreamComponent} from './timeline-stream.component';

describe('TimelineStreamComponent', () => {
  const TIMELINE_ITEMS_RELOAD_DELAY = 5;
  const CURRENT_USER_ID = 'current-user-id';
  const SENDER_ID = 'sender-id';

  let component: TimelineStreamComponent;
  let fixture: ComponentFixture<TimelineStreamComponent>;
  let permissionService: jasmine.SpyObj<NgxPermissionsService>;
  let socketService: jasmine.SpyObj<SocketService>;
  let timelineItemService: jasmine.SpyObj<TimelineItemService>;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;
  let authService: jasmine.SpyObj<AuthService>;
  const createdSubject: Subject<any> = new Subject();
  const deletedSubject: Subject<any> = new Subject();
  const sharedSubject: Subject<any> = new Subject();
  const reconnectSubject: Subject<any> = new Subject();
  const defaultPage = {content: [{id: 'a', created: 3}, {id: 'b', created: 2}, {id: 'c', created: 1}], last: false} as Page<TimelineItem>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ TimelineStreamComponent ],
        providers: [{
          provide: NgxPermissionsService, useValue: jasmine.createSpyObj('permissionService', ['hasPermission'])
        }, {
          provide: TimelineItemService, useValue: jasmine.createSpyObj('timelineItemService',
            ['getNewItems', 'getItems', 'getPage', 'get', 'getNewItemCount'])
        }, {
          provide: SocketService, useValue: jasmine.createSpyObj('socketService', ['listenToReconnect$', 'listenTo$'])
        }, {
          provide: AuthService, useValue: jasmine.createSpyObj('authService', ['getCurrentUserId'])
        }, {
          provide: NG1_SOCKET_RECONNECT_DELAYS, useValue: {TIMELINE_ITEMS_RELOAD_DELAY}
        }, {
          provide: SubscriptionService, useValue: jasmine.createSpyObj('subscriptionService', ['getSubscriptionsByType'])
        }]
      })
      .overrideTemplate(TimelineStreamComponent, '')
      .compileComponents();

    // add default values to dependencies
    permissionService = TestBed.get(NgxPermissionsService);
    permissionService.hasPermission.and.returnValue(of(true));

    socketService = TestBed.get(SocketService);
    socketService.listenToReconnect$.and.returnValue(reconnectSubject);
    socketService.listenTo$.and.callFake((route: string, type: string) => {
      if (route === '/topic/timeline.personal.' + SENDER_ID + '.item.created' ||
        route === '/topic/timeline.sender.' + SENDER_ID + '.item.created'
      ) {
        return createdSubject;
      } else if (route.startsWith('/topic/timeline.item') && type === 'deleted') {
        return deletedSubject;
      } else if (route === '/topic/timeline.personal.' + SENDER_ID + '.item.shared' ||
        route === '/topic/timeline.sender.' + SENDER_ID + '.item.shared') {
        return sharedSubject;
      } else {
        return EMPTY;
      }
    });

    timelineItemService = TestBed.get(TimelineItemService);
    timelineItemService.getNewItemCount.and.returnValue(of(0));
    timelineItemService.getNewItems.and.returnValue(of({data: {}, page: {}}));
    timelineItemService.getItems.and.returnValue(of({}));
    timelineItemService.get.and.returnValue(of({}));
    timelineItemService.getPage.and.returnValue(of(defaultPage));

    subscriptionService = TestBed.get(SubscriptionService);
    subscriptionService.getSubscriptionsByType.and.returnValue(of([{targetId: 'targetId'}]));

    authService = TestBed.get(AuthService);
    authService.getCurrentUserId.and.returnValue(CURRENT_USER_ID);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineStreamComponent);
    component = fixture.componentInstance;
    component.context = {id: SENDER_ID} as Sender;
    component.canPost = true;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // when
    fixture.detectChanges();

    // then
    expect(timelineItemService.getPage).toHaveBeenCalled();
    expect(timelineItemService.getNewItemCount).toHaveBeenCalledWith('personal', false);
    expect(socketService.listenTo$)
      .toHaveBeenCalledWith('/topic/timeline.personal.' + SENDER_ID + '.item.created');
    expect(socketService.listenTo$)
      .toHaveBeenCalledWith('/topic/timeline.personal.' + SENDER_ID + '.item.shared');
    expect(socketService.listenToReconnect$).toHaveBeenCalled();
    expect(subscriptionService.getSubscriptionsByType)
      .toHaveBeenCalledWith('user', 'workspace', 'page');
  });

  it('should set the visibility regarding the permissions', fakeAsync(() => {
    // given
    permissionService.hasPermission.and.returnValue(of(false));

    // when
    fixture.detectChanges();

    // then
    component.visible$.subscribe(visible => expect(visible).toBeFalsy());
    tick();
    expect(permissionService.hasPermission).toHaveBeenCalledWith('ACCESS_PERSONAL_TIMELINE');
  }));

  it('should hide the timeline form if the user has insufficient permissions', fakeAsync(() => {
    // given
    permissionService.hasPermission.and.returnValue(of(false));

    // when
    fixture.detectChanges();

    // then
    component.canPost$.subscribe(canPost => expect(canPost).toBeFalsy());
    tick();
    expect(permissionService.hasPermission).toHaveBeenCalledWith('CREATE_TIMELINE_ITEM');
  }));

  it('should hide the timeline form if the flag is set', fakeAsync(() => {
    // given
    permissionService.hasPermission.and.returnValue(of(true));
    component.canPost = false;

    // when
    fixture.detectChanges();

    // then
    component.canPost$.subscribe(canPost => expect(canPost).toBeFalsy());
    tick();
    expect(permissionService.hasPermission).toHaveBeenCalledWith('CREATE_TIMELINE_ITEM');
  }));

  it('should load more', fakeAsync(() => {
    // given
    const item = {id: 'id1', created: 1} as TimelineItem;
    const page = {content: [item], number: 0, size: 1, last: false} as Page<TimelineItem>;
    timelineItemService.getPage.and.returnValue(of(page).pipe(delay(1)));

    // when
    component.loadMore();

    // then
    expect(timelineItemService.getPage).toHaveBeenCalledWith(jasmine.objectContaining(
      {page: 0, pageSize: TimelineItemService.TIMELINE_PAGE_SIZE, offset: 0}),
      {
        params: {type: 'personal', senderId: SENDER_ID},
        headers: {etagEnabled: 'false'},
        permissions: TimelineItemService.TIMELINE_ITEM_PERMISSIONS
      });
    let calledLoading = false;
    let called = false;
    component.state$.pipe(take(1)).subscribe(state => {
      expect(state.loading).toBeTruthy();
      calledLoading = true;
    });
    component.state$.pipe(skip(1)).pipe(take(1)).subscribe(state => {
      expect(state.items).toEqual(page.content);
      expect(state.page).toBe(page);
      expect(state.loading).toBeFalsy();
      called = true;
    });
    tick(1);
    expect(calledLoading).toBeTruthy();
    expect(called).toBeTruthy();
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.id1', 'deleted');
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.id1', 'updated');

    // given - again
    const item2 = {id: 'id2', created: 0} as TimelineItem;
    const page2 =  {content: [item2], number: 0, size: 1, last: false} as Page<TimelineItem>;
    timelineItemService.getPage.and.returnValue(of(page2).pipe(delay(1)));

    // when - again
    component.loadMore();

    // then
    expect(timelineItemService.getPage).toHaveBeenCalledWith(jasmine.objectContaining(
      {page: 0, pageSize: TimelineItemService.TIMELINE_PAGE_SIZE, offset: 1}),
      {
        params: {type: 'personal', senderId: SENDER_ID},
        headers: {etagEnabled: 'false'},
        permissions: TimelineItemService.TIMELINE_ITEM_PERMISSIONS
      });
    called = false;
    component.state$.pipe(skip(1)).pipe(take(1)).subscribe(state => {
      expect(state.items).toEqual([item, item2]);
      expect(state.page).toBe(page2);
      expect(state.loading).toBeFalsy();
      called = true;
    });
    tick(1);
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.id2', 'deleted');
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.id2', 'updated');
    expect(called).toBeTruthy();
  }));

  it('should not load more when loading', fakeAsync(() => {
    // given
    timelineItemService.getPage.and.returnValue(of({content: []}).pipe(delay(1)));

    // when
    component.loadMore();
    component.loadMore();

    // then
    expect(timelineItemService.getPage).toHaveBeenCalledTimes(1);
    tick(1);
  }));

  it('should reset loading flag after error', () => {
    // given
    timelineItemService.getPage.and.returnValue(throwError('error'));

    let calledLoading = false;
    let calledAfterError = false;
    component.state$.pipe(skip(1)).pipe(take(1)).subscribe(state => {
      expect(state.loading).toBeTruthy();
      calledLoading = true;
    });
    component.state$.pipe(skip(2)).pipe(take(1)).subscribe(state => {
      expect(state.loading).toBeFalsy();
      calledAfterError = true;
    });

    // when
    component.loadMore();

    // then
    expect(timelineItemService.getPage).toHaveBeenCalledTimes(1);
    expect(calledLoading).toBeTruthy();
    expect(calledAfterError).toBeTruthy();
  });

  it('should load new', () => {
    // given
    const item = {id: 'new-id', created: 2, author: {id: 'other-user-id'}} as TimelineItem;
    const oldItem = {id: 'old-id', created: 1, author: {id: 'other-user-id'}} as TimelineItem;
    const page = {content: [item], number: 0, size: 1, last: true} as Page<TimelineItem>;
    const oldItems = {content: [oldItem], number: 0, size: 1, last: true};
    timelineItemService.getNewItems.and.returnValue(of({data: {lastRefreshDate: 1}, page: page}));
    timelineItemService.getPage.and.returnValue(of(oldItems));
    fixture.detectChanges();

    // when
    component.loadNew();

    // then
    component.state$.subscribe(state => {
      expect(state.items).toEqual([item, oldItem]);
      expect(state.items[0].isNew).toBeTruthy();
      expect(state.mostRecentItemId).toBe(oldItem.id);
      expect(state.loadingNew).toBeFalsy();
    });
    expect(timelineItemService.getNewItems)
      .toHaveBeenCalledWith(SENDER_ID, 'personal', TimelineItemService.TIMELINE_ITEM_PERMISSIONS);
  });

  it('should scroll to newest item after loading new', () => {
    // given
    const itemElement = jasmine.createSpyObj('element', ['scrollIntoView']);
    const getElementById = spyOn(document, 'getElementById').and.returnValue(itemElement);
    const newItem = {id: 'newItemId', created: 2, author: {id: 'other-user-id'}} as TimelineItem;
    const oldStickyItem = {
      id: 'oldStickyItemId', created: 1, stickyExpiry: 10, unread: true, author: {id: 'other-user-id'}
    } as TimelineItem & { stickyExpiry: number};
    const page = {content: [newItem], number: 0, size: 1, last: true} as Page<TimelineItem>;
    const oldItems = {content: [oldStickyItem], number: 0, size: 1, last: true};
    timelineItemService.getNewItems.and.returnValue(of({data: {lastRefreshDate: 1}, page: page}));
    timelineItemService.getPage.and.returnValue(of(oldItems));
    fixture.detectChanges();

    // when
    component.loadNew();

    // then
    expect(getElementById).toHaveBeenCalledWith('item-' + newItem.id);
    expect(itemElement.scrollIntoView).toHaveBeenCalled();
  });

  it('should not scroll after loading new when a new item is sticky', () => {
    // given
    const itemElement = jasmine.createSpyObj('element', ['scrollIntoView']);
    const getElementById = spyOn(document, 'getElementById').and.returnValue(itemElement);
    const newStickyItem = {
      id: 'newStickyItemId', created: 2, stickyExpiry: 10, unread: true, author: {id: 'other-user-id'}
    } as TimelineItem & { stickyExpiry: number};
    const newItem = {id: 'newItemId', created: 3, author: {id: 'other-user-id'}} as TimelineItem;
    const oldStickyItem = {
      id: 'newItemId', created: 1, stickyExpiry: 10, unread: true, author: {id: 'other-user-id'}
    } as TimelineItem & { stickyExpiry: number };
    const page = {content: [newStickyItem, newItem], number: 0, size: 1, last: true} as Page<TimelineItem>;
    const oldItems = {content: [oldStickyItem], number: 0, size: 1, last: true};
    timelineItemService.getNewItems.and.returnValue(of({data: {lastRefreshDate: 1}, page: page}));
    timelineItemService.getPage.and.returnValue(of(oldItems));
    fixture.detectChanges();

    // when
    component.loadNew();

    // then
    expect(getElementById).toHaveBeenCalledTimes(0);
    expect(itemElement.scrollIntoView).toHaveBeenCalledTimes(0);
  });

  it('should not scroll after loading new when newest item is at the top', () => {
    // given
    const itemElement = jasmine.createSpyObj('element', ['scrollIntoView']);
    const getElementById = spyOn(document, 'getElementById').and.returnValue(itemElement);
    const newItem = {id: 'newItemId', created: 3, author: {id: 'other-user-id'}} as TimelineItem;
    const oldItem = {id: 'newItemId', created: 1, author: {id: 'other-user-id'}} as TimelineItem;
    const page = {content: [newItem], number: 0, size: 1, last: true} as Page<TimelineItem>;
    const oldItems = {content: [oldItem], number: 0, size: 1, last: true};
    timelineItemService.getNewItems.and.returnValue(of({data: {lastRefreshDate: 1}, page: page}));
    timelineItemService.getPage.and.returnValue(of(oldItems));
    fixture.detectChanges();

    // when
    component.loadNew();

    // then
    expect(getElementById).toHaveBeenCalledTimes(0);
    expect(itemElement.scrollIntoView).toHaveBeenCalledTimes(0);
  });

  it('should reset timeline items if there are more than one page of new items', () => {
    // given
    const item = {id: 'new-id', created: 2} as TimelineItem;
    const oldItem = {id: 'old-id', created: 1} as TimelineItem;
    const page = {content: [item], number: 0, size: 1, last: false} as Page<TimelineItem>;
    const oldItems = {content: [oldItem], number: 0, size: 1, last: true};
    timelineItemService.getNewItems.and.returnValue(of({data: {lastRefreshDate: 1}, page: page}));
    timelineItemService.getPage.and.returnValue(of(oldItems));
    fixture.detectChanges();

    // when
    component.loadNew();

    // then
    component.state$.subscribe(state => {
      expect(state.items).toEqual([item]);
      expect(state.items[0].isNew).toBeTruthy();
      expect(state.loadingNew).toBeFalsy();
    });
    expect(timelineItemService.getNewItems)
      .toHaveBeenCalledWith(SENDER_ID, 'personal', TimelineItemService.TIMELINE_ITEM_PERMISSIONS);
  });

  it('should add item lazy on personal timeline', () => {
    // given
    const createdItemEvent = {content: {id: 'id'}};
    fixture.detectChanges();

    // when
    createdSubject.next(createdItemEvent);

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.newItemsCount).toBe(1);
      called = true;
    });
    expect(called).toBeTruthy();
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.id', 'deleted');
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.id', 'updated');
  });

  it('should load new items immediately when author is current user', () => {
    // given
    const createdItemEvent = {content: {id: 'id', authorId: CURRENT_USER_ID}};
    fixture.detectChanges();

    // when
    createdSubject.next(createdItemEvent);

    // then
    expect(timelineItemService.getNewItems).toHaveBeenCalled();
  });

  it('should load new items on sender timeline immediately', fakeAsync(() => {
    // given
    const createdItemEvent = {content: {id: 'id'}};
    const createdItem = {id: 'id', data: {message: 'test'}} as TimelineItem;
    component.type = 'sender';
    timelineItemService.get.and.returnValue(of(createdItem));
    fixture.detectChanges();

    // when
    createdSubject.next(createdItemEvent);
    tick();

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.items).toEqual([createdItem, ...defaultPage.content]);
      called = true;
    });
    expect(timelineItemService.get).toHaveBeenCalledWith(createdItemEvent.content.id, {
      params: {timelineType: 'SENDER', senderId: SENDER_ID},
      permissions: TimelineItemService.TIMELINE_ITEM_PERMISSIONS,
      handleErrors: false
    });
    expect(called).toBeTruthy();
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.id', 'deleted');
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.id', 'updated');
  }));

  it('should delete item', () => {
    // given
    const item = {id: 'id1', created: 1} as TimelineItem;
    const page = {content: [item], number: 0, size: 1, last: false} as Page<TimelineItem>;
    timelineItemService.getPage.and.returnValue(of(page));
    fixture.detectChanges();

    // when
    deletedSubject.next();

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.items.length).toEqual(0);
      called = true;
    });
    expect(deletedSubject.observers.length).toBe(0);
    expect(called).toBeTruthy();
  });

  it('should delete unloaded item', fakeAsync(() => {
    // given
    fixture.detectChanges();

    // when
    createdSubject.next({content: {id: 'new-id'}});
    deletedSubject.next();

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.newItemsCount).toEqual(0);
      called = true;
    });
    expect(deletedSubject.observers.length).toBe(0);
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.new-id', 'deleted');
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/timeline.item.new-id', 'updated');
    expect(called).toBeTruthy();
  }));

  it('should update personal timeline count on newly shared item', () => {
    // when
    fixture.detectChanges();
    sharedSubject.next({content: {id: 'd', recipient: 'targetId'}});

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.newItemsCount).toBe(1);
      called = true;
    });
    expect(called).toBeTruthy();
    expect(socketService.listenTo$)
                 .toHaveBeenCalledWith('/topic/timeline.item.d', 'deleted');
  });

  it('should update personal timeline count on item that got shared again', () => {
    // when
    fixture.detectChanges();
    sharedSubject.next({content: {id: 'b', recipient: 'targetId'}});

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.newItemsCount).toBe(1);
      expect(state.items).toEqual([
        {id: 'a', created: 3, isNew: false},
        {id: 'b', created: 2, isNew: false},
        {id: 'c', created: 1, isNew: false}] as TimelineItem[]);
      expect(deletedSubject.observers.length).toBe(state.items.length);
      called = true;
    });
    expect(called).toBeTruthy();
    expect(socketService.listenTo$)
      .toHaveBeenCalledWith('/topic/timeline.item.b', 'deleted');
  });

  it('should not place own share to top', () => {
    // when
    fixture.detectChanges();
    sharedSubject.next({
      content: {
        id: 'd',
        authorId: CURRENT_USER_ID,
        recipient: 'targetId'
      }
    });

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.newItemsCount).toBe(0);
      expect(state.items).toEqual([
        {id: 'a', created: 3, isNew: false},
        {id: 'b', created: 2, isNew: false},
        {id: 'c', created: 1, isNew: false}] as TimelineItem[]);
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should update personal timeline and place shares with me to top', () => {
    // when
    fixture.detectChanges();
    sharedSubject.next({content: {id: 'b', authorId: 'otherId', recipient: CURRENT_USER_ID}});

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.newItemsCount).toBe(1);
      expect(deletedSubject.observers.length).toBe(state.items.length);
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should update sender timeline and append relevant new share to top', () => {
    // given
    component.type = 'sender';
    timelineItemService.get.and.returnValue(of({id: 'd', created: 4}));

    // when
    fixture.detectChanges();
    sharedSubject.next({
      content: {
        id: 'd',
        recipient: SENDER_ID,
        authorId: 'otherId'
      }
    });

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.items).toEqual([
        {id: 'd', created: 4, isNew: true},
        {id: 'a', created: 3, isNew: false},
        {id: 'b', created: 2, isNew: false},
        {id: 'c', created: 1, isNew: false}] as TimelineItem[]);
      called = true;
    });
    expect(timelineItemService.get).toHaveBeenCalledWith('d', {
      params: {timelineType: 'SENDER', senderId: SENDER_ID},
      permissions: TimelineItemService.TIMELINE_ITEM_PERMISSIONS,
      handleErrors: false});
    expect(called).toBeTruthy();
  });

  it('should not update sender timeline when share is not relevant', () => {
    // given
    component.type = 'sender';
    const item = {id: 'b', created: 2, shares: [{created: 10}]};
    timelineItemService.get.and.returnValue(of(item));

    // when
    fixture.detectChanges();
    sharedSubject.next({
      content: {
        id: 'b',
        recipient: 'other-sender-id',
        authorId: 'otherId'
      }
    });

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.items).toEqual([
        {id: 'a', created: 3, isNew: false},
        {id: 'b', created: 2, isNew: false, shares: [{created: 10}]},
        {id: 'c', created: 1, isNew: false}] as TimelineItem[]);
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should update personal timeline on reconnect', fakeAsync(() => {
    // given
    const newItems = [{id: 'new-1', created: 5},
      {id: 'new-2', created: 4, data: {message: 'test'}},
      {id: 'b', created: 2,  data: {message: 'test'}, modified: 2}] as TimelineItem[];
    const updatedItem = {id: 'c', created: 1, modified: 2, data: {message: 'edited'}} as TimelineItem;
    const newItemCount = 20;
    fixture.detectChanges();
    timelineItemService.getPage.calls.reset();
    timelineItemService.getNewItemCount.calls.reset();
    timelineItemService.getPage.and.returnValue(of({content: newItems}));
    timelineItemService.getNewItemCount.and.returnValue(of(newItemCount));
    timelineItemService.getItems.and.returnValue(of({c: updatedItem}));

    // when
    reconnectSubject.next();
    tick(TIMELINE_ITEMS_RELOAD_DELAY);

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.items).toEqual([
        {id: 'new-1', created: 5, isNew: false},
        {id: 'new-2', created: 4, data: {message: 'test'}, isNew: false},
        {id: 'b', created: 2,  data: {message: 'test'}, isNew: false, modified: 2},
        {id: 'c', created: 1, isNew: false, modified: 2, data: {message: 'edited'}}
        ] as TimelineItem[] );
      expect(state.newItemsCount).toBe(newItemCount);
      called = true;
    });
    expect(called).toBeTruthy();
    expect(timelineItemService.getPage).toHaveBeenCalled();
    expect(timelineItemService.getNewItemCount).toHaveBeenCalled();
    expect(timelineItemService.getItems).toHaveBeenCalledWith(
      ['a', 'c'],
      SENDER_ID,
      'personal',
      TimelineItemService.TIMELINE_ITEM_PERMISSIONS);
  }));

  it('should remove duplicates when loading more', () => {

    // when
    component.loadMore();
    component.loadMore();

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.items).toEqual(defaultPage.content);
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should update existing items', () => {
    // given
    const item = {id: 'c', data: {message: 'test message'}, created: 1, isNew: false} as TimelineItem;
    fixture.detectChanges();

    // when
    component.updateItem(item);

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.items).toEqual([
        {id: 'a', created: 3, isNew: false},
        {id: 'b', created: 2, isNew: false},
        item
      ] as TimelineItem[]);
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should order items correctly', () => {
    // given
    const itemA = {id: 'a', created: 3, isNew: false};
    const itemB = {id: 'b', created: 1, unread: true, isNew: false};
    const itemC = {id: 'c', created: 2, relevantShare: {id: 1, created: 4}, isNew: false};
    const itemD = {id: 'd', created: 1, relevantShare: {id: 1, created: 2, unread: true}};
    const itemE = {id: 'e', created: 1, relevantShare: {id: 1, created: 2, unread: true, stickyExpiry: 3}};

    timelineItemService.getNewItems.and.returnValue(of({
      page: {content: [itemA, itemB, itemC, itemD, itemE], last: true},
      data: {}
    }));
    timelineItemService.getPage.and.returnValue(EMPTY);
    fixture.detectChanges();

    // when
    component.loadNew();

    // then
    let called = false;
    component.state$.subscribe(state => {
      expect(state.items).toEqual([
        itemE,
        itemD,
        itemB,
        itemC,
        itemA
      ] as TimelineItem[]);
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should track items by id', () => {
    // given
    const item = {
      id: 'item-id'
    } as TimelineItem;

    // when
    const result = component.trackItem(0, item);

    // then
    expect(result).toBe(item.id);
  });
});
