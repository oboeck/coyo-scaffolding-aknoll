import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {TimelineItemUpdatedMessage} from '@app/timeline/timeline-item/timeline-item/timeline-item-updated-message';
import {AuthService} from '@core/auth/auth.service';
import {EtagInterceptor} from '@core/http/etag-interceptor/etag-interceptor';
import {SocketService} from '@core/socket/socket.service';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {Sender} from '@domain/sender/sender';
import {SubscriptionService} from '@domain/subscription/subscription.service';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {Ng1SocketReconnectDelays} from '@root/typings';
import {NG1_SOCKET_RECONNECT_DELAYS} from '@upgrade/upgrade.module';
import * as _ from 'lodash';
import {NgxPermissionsService} from 'ngx-permissions';
import {BehaviorSubject, from, Observable, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';

interface TimelineStreamState {
  loadingNew: boolean;
  newItemsCount: number;
  loading: boolean;
  page: Page<TimelineItem>;
  items: TimelineItem[];
  mostRecentItemId: string;
}

/**
 * Renders the timeline stream. Consisting of the timeline form and a paginated list of items.
 *
 * The stream will not react on changes of the type or the context after initialization.
 */
@Component({
  selector: 'coyo-timeline-stream',
  templateUrl: './timeline-stream.component.html',
  styleUrls: ['./timeline-stream.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineStreamComponent implements OnInit, OnDestroy {

  /**
   * The timeline type. The default value is 'personal'.
   */
  @Input() type: 'sender' | 'personal' = 'personal';

  /**
   * Permission flag if the user is allowed to post.
   */
  @Input() canPost: boolean = true;

  /**
   * The context sender. If it is a personal timeline this should be the current user otherwise the sender containing
   * the timeline.
   */
  @Input() context: Sender;

  visible$: Observable<boolean>;

  canPost$: Observable<boolean>;

  state$: Observable<TimelineStreamState>;

  private stateSubject$: BehaviorSubject<TimelineStreamState> = new BehaviorSubject<TimelineStreamState>({
    loadingNew: false,
    newItemsCount: 0,
    loading: false,
    page: null,
    items: [],
    mostRecentItemId: null
  });

  private unloadedItems: string[] = [];

  private createSubscription: Subscription;

  private itemDeletedSubscriptions: { [key: string]: Subscription } = {};
  private itemUpdatedSubscriptions: { [key: string]: Subscription } = {};

  private sharedSubscription: Subscription;

  private reconnectSubscription: Subscription;

  private offset: number = 0;

  private newItemsReferenceDate: number;

  private contextSenders: string[];

  constructor(private permissionService: NgxPermissionsService, private timelineItemService: TimelineItemService,
              private socketService: SocketService, private authService: AuthService,
              @Inject(NG1_SOCKET_RECONNECT_DELAYS) private socketReconnectDelays: Ng1SocketReconnectDelays,
              private subscriptionService: SubscriptionService, private changeDetectorRef: ChangeDetectorRef) {
    this.state$ = this.stateSubject$.asObservable();
  }

  ngOnInit(): void {
    this.visible$ = from(this.permissionService.hasPermission('ACCESS_PERSONAL_TIMELINE'))
      .pipe(map(permission => permission || !this.isPersonal()));

    this.canPost$ = from(this.permissionService.hasPermission('CREATE_TIMELINE_ITEM'))
      .pipe(map(permission => permission && this.canPost));

    this.updateCount();

    const route = '/topic/timeline.' + this.type + '.' + this.context.id + '.item.created';
    this.createSubscription = this.socketService.listenTo$(route).subscribe(event =>
      this.addItem(event.content));

    const shareRoute = '/topic/timeline.' + this.type + '.' + this.context.id + '.item.shared';
    this.sharedSubscription = this.socketService.listenTo$(shareRoute).subscribe(event =>
      this.onItemShared(event.content));

    this.loadContext();

    this.loadMore();

    this.reconnectSubscription = this.socketService.listenToReconnect$()
      .subscribe(() => this.onWebsocketReconnect());
  }

  ngOnDestroy(): void {
    if (this.createSubscription && !this.createSubscription.closed) {
      this.createSubscription.unsubscribe();
    }

    if (this.sharedSubscription && !this.sharedSubscription.closed) {
      this.sharedSubscription.unsubscribe();
    }

    if (this.reconnectSubscription && !this.reconnectSubscription.closed) {
      this.reconnectSubscription.unsubscribe();
    }

    this.unsubscribeAllItemSubscriptions();

    this.stateSubject$.complete();
  }

  /**
   * Loads new timeline items if they are not already loading. If there are more then one page of new items,
   * the already loaded items will be removed from state and only the new page is shown. Otherwise the new items will be
   * prepended to the current items.
   */
  loadNew(): void {
    if (this.stateSubject$.getValue().loadingNew) {
      return;
    }
    this.setLoadingNew(true);
    const senderId = this.context ? this.context.id : null;
    this.timelineItemService.getNewItems(senderId, this.type, TimelineItemService.TIMELINE_ITEM_PERMISSIONS)
      .subscribe(newItems => {
        let lastMostRecentItemId = null;
        let items = null;
        this.newItemsReferenceDate = newItems.data.lastRefreshDate;
        const page = newItems.page;
        if (page.last) {
          items = this.setNew(this.mergeItems(page.content));
          this.offset += page.numberOfElements;
          lastMostRecentItemId = this.determineLastMostRecentItemId(items);
        } else {
          this.unsubscribeAllItemSubscriptions();
          this.offset = 0;
          items = this.setNew(page.content);
        }

        this.addState({
          mostRecentItemId: lastMostRecentItemId,
          newItemsCount: 0,
          items
        });

        _.forEach(page.content, item => this.subscribeItemDeleted(item.id));
        _.forEach(page.content, item => this.subscribeItemUpdated(item.id));
        this.unloadedItems = [];
        this.setLoadingNew(false);
        this.changeDetectorRef.detectChanges();
        this.scrollToMostRecentNewItem();
      }, () => this.setLoadingNew(false));
  }

  /**
   * Loads the next page of items if it is not already loading.
   */
  loadMore(): void {
    const state = this.stateSubject$.getValue();
    if (state.loading) {
      return;
    }
    this.setLoading(true);

    this.requestPage(false, state.page).subscribe(response => {
      const items = this.setNew(_.uniqBy(_.concat(this.stateSubject$.getValue().items, response.content), 'id'));
      _.forEach(response.content, item => this.subscribeItemDeleted(item.id));
      _.forEach(response.content, item => this.subscribeItemUpdated(item.id));
      const mostRecentItemId = this.determineLastMostRecentItemId(items);
      this.addState({page: response, loading: false, items, mostRecentItemId});
    }, error => {
      this.setLoading(false);
    });
  }

  /**
   * Tracks item by id. Prevents unnecessary rerendering when an item is updated.
   * @param index the index
   * @param item the current item
   * @returns a unique identifier for a timeline item
   */
  trackItem(index: number, item: TimelineItem): string {
    return item.id;
  }

  /**
   * Update a timeline item
   * @param item the new version of the timeline item
   */
  updateItem(item: TimelineItem): void {
    const items = this.stateSubject$.getValue().items;
    const index = _.findIndex(items, ['id', item.id]);
    if (index !== -1) {
      items[index] = {...items[index], ...item};
      this.addState({items: items});
    }
  }

  private onWebsocketReconnect(): void {
    setTimeout(() => this.updateItems(), this.socketReconnectDelays.TIMELINE_ITEMS_RELOAD_DELAY);
  }

  private updateItems(): void {
    this.updateCount(true);
    this.requestPage(true, null).subscribe(page => {
      const newItems = this.setNew(
        _.filter(page.content,
          item => !_.some(this.stateSubject$.getValue().items,
            oldItem => oldItem.id === item.id)));

      if (newItems.length >= TimelineItemService.TIMELINE_PAGE_SIZE) {
        this.addState({
          items: newItems,
          page
        });
      } else {
        _.forEach(newItems, item => this.subscribeItemDeleted(item.id));
        _.forEach(newItems, item => this.subscribeItemUpdated(item.id));
        this.refreshState(page, newItems);
      }
    });
  }

  private refreshState(page: Page<TimelineItem>, newItems: TimelineItem[]): void {
    const updateItemIds = _(this.stateSubject$.getValue().items)
      .filter(item => !_.some(page.content, newItem => newItem.id === item.id))
      .map(item => item.id).value();
    if (updateItemIds.length) {
      this.timelineItemService
        .getItems(updateItemIds, this.context.id, this.type, TimelineItemService.TIMELINE_ITEM_PERMISSIONS)
        .subscribe(items => {
          const refreshedItems: { [key: string]: TimelineItem } = _.keyBy(page.content, 'id');
          const updatedItems = this.updateItemsFromResult({...refreshedItems, ...items},
            this.stateSubject$.getValue().items);
          this.addState({
            items: [...newItems, ...updatedItems],
            page
          });
        });
    } else {
      this.addState({
        items: page.content,
        page
      });
    }
  }

  private updateItemsFromResult(updatedItems: { [key: string]: TimelineItem }, oldItems: TimelineItem[]): TimelineItem[] {
    return this.orderItems(_(oldItems).filter(item => _.has(updatedItems, item.id))
      .map(oldItem => {
        if (oldItem.modified !== updatedItems[oldItem.id].modified) {
          return {...oldItem, ...updatedItems[oldItem.id]};
        } else {
          return oldItem;
        }
      }).value());
  }

  private requestPage(useCache: boolean, previousPage: Page<TimelineItem>): Observable<Page<TimelineItem>> {
    const offset = (previousPage ? (previousPage.number + 1) * previousPage.size : 0) + this.offset;
    const pageable = new Pageable(0, TimelineItemService.TIMELINE_PAGE_SIZE, offset);
    const params = {type: this.type, senderId: this.context.id};
    const headers = {};
    if (!useCache) {
      headers[EtagInterceptor.ETAG_ENABLED] = 'false';
    }

    return this.timelineItemService.getPage(pageable, {
      params,
      headers,
      permissions: TimelineItemService.TIMELINE_ITEM_PERMISSIONS
    });
  }

  private mergeItems(items: TimelineItem[]): TimelineItem[] {
    const oldItems = [...this.stateSubject$.getValue().items];
    const removed = _.remove(oldItems, item => _.some(items, {id: item.id}));

    _.forEach(removed, item => this.unsubscribeItemDeleted(item.id));
    _.forEach(removed, item => this.unsubscribeItemUpdated(item.id));

    const newItems = this.orderItems(_.concat(this.setNew(items), oldItems));

    return newItems;
  }

  private unsubscribeAllItemSubscriptions(): void {
    const deletedSubscriptions = _.values(this.itemDeletedSubscriptions);
    const updatedSubscriptions = _.values(this.itemUpdatedSubscriptions);
    _.concat(deletedSubscriptions, updatedSubscriptions).forEach(subscription => {
      if (subscription && !subscription.closed) {
        subscription.unsubscribe();
      }
    });
  }

  private determineLastMostRecentItemId(items: TimelineItem[]): string | null {
    if (_.some(items, item => item.isNew && item.author.id !== this.authService.getCurrentUserId())) {
      const foundItem = _.find(items, item => !item.isNew &&
        !(item.unread || (item.relevantShare ? item.relevantShare.unread : false)));
      return foundItem ? foundItem.id : null;
    } else {
      return null;
    }
  }

  private orderItems(items: TimelineItem[]): TimelineItem[] {
    return _.orderBy(items, [item => (item.unread || (item.relevantShare ? item.relevantShare.unread : false)) ? 1 : 0,
      item => this.getSortDate(item)], ['desc', 'desc']);
  }

  private getSortDate(item: TimelineItem): number {
    const relevantShare = item.relevantShare;
    if (!_.isEmpty(relevantShare)) {
      if (relevantShare.unread && relevantShare.stickyExpiry) {
        return relevantShare.stickyExpiry;
      } else {
        return relevantShare.created;
      }
    } else {
      return item.created;
    }
  }

  private isNew(item: TimelineItem): boolean {
    return !!(this.newItemsReferenceDate && this.newItemsReferenceDate < this.getSortDate(item));
  }

  private setNew(items: TimelineItem[]): TimelineItem[] {
    return _.map(items, item => {
      item.isNew = this.isNew(item);
      return item;
    });
  }

  private setLoading(loading: boolean): void {
    this.addState({
      loading
    });
  }

  private setLoadingNew(loadingNew: boolean): void {
    this.addState({
      loadingNew
    });
  }

  private addState(newStateObj: Partial<TimelineStreamState>): void {
    this.stateSubject$.next({
      ...this.stateSubject$.getValue(),
      ...newStateObj
    });
  }

  private addItem(event: { id: string, authorId: string, recipient: string }): void {
    if (!this.isPersonal()) {
      const senderId = this.context.id;
      this.timelineItemService.get(event.id, {
        params: {timelineType: this.type.toUpperCase(), senderId},
        permissions: TimelineItemService.TIMELINE_ITEM_PERMISSIONS,
        handleErrors: false
      }).subscribe(item => {
        const items = this.mergeItems([item]);
        item.isNew = event.recipient === this.context.id;
        this.subscribeItemDeleted(item.id);
        this.subscribeItemUpdated(item.id);
        this.addState({items});
      });
    } else if (this.authService.getCurrentUserId() === event.authorId) {
      this.loadNew();
    } else if (this.unloadedItems.indexOf(event.id) === -1) {
      this.addUnloadedItem(event.id);
    }
  }

  private addUnloadedItem(id: string): void {
    this.unloadedItems.push(id);
    this.addState({newItemsCount: this.stateSubject$.getValue().newItemsCount + 1});
    this.subscribeItemDeleted(id);
    this.subscribeItemUpdated(id);
  }

  private subscribeItemUpdated(id: string): void {
    const route = '/topic/timeline.item.' + id;
    if (!this.itemUpdatedSubscriptions[id]) {
      this.itemUpdatedSubscriptions[id] = this.socketService.listenTo$(route, 'updated').subscribe(
        (item: TimelineItemUpdatedMessage) => this.updateItem(item.content)
      );
    }
  }

  private unsubscribeItemUpdated(id: string): void {
    const subscription = this.itemUpdatedSubscriptions[id];
    if (subscription && !subscription.closed) {
      subscription.unsubscribe();
    }
    this.itemUpdatedSubscriptions = _.omit(this.itemUpdatedSubscriptions, id);
  }

  private subscribeItemDeleted(id: string): void {
    const route = '/topic/timeline.item.' + id;
    if (!this.itemDeletedSubscriptions[id]) {
      this.itemDeletedSubscriptions[id] = this.socketService.listenTo$(route, 'deleted').subscribe(
        () => this.removeItem(id)
      );
    }
  }

  private unsubscribeItemDeleted(id: string): void {
    const subscription = this.itemDeletedSubscriptions[id];
    if (subscription && !subscription.closed) {
      subscription.unsubscribe();
    }
    this.itemDeletedSubscriptions = _.omit(this.itemDeletedSubscriptions, id);
  }

  private removeItem(id: string): void {
    let newItemsCount = this.stateSubject$.getValue().newItemsCount;
    let items = this.stateSubject$.getValue().items;
    let mostRecentItemId = this.stateSubject$.getValue().mostRecentItemId;
    if (this.unloadedItems.indexOf(id) >= 0) {
      this.unloadedItems = _.without(this.unloadedItems, id);
      newItemsCount--;
    }
    if (_.some(items, item => item.id === id)) {
      --this.offset;
      items = _.filter(this.stateSubject$.getValue().items, item => item.id !== id);
      mostRecentItemId = this.determineLastMostRecentItemId(items);
    }
    this.addState({newItemsCount, items, mostRecentItemId});
    this.unsubscribeItemDeleted(id);
    this.unsubscribeItemUpdated(id);
  }

  private isPersonal(): boolean {
    return this.type === 'personal';
  }

  private updateCount(useCache: boolean = false): void {
    if (this.isPersonal()) {
      this.timelineItemService
        .getNewItemCount(this.type, useCache).subscribe(newItemsCount => this.addState({newItemsCount}));
    }
  }

  private onItemShared(event: { id: string, recipient: string, authorId: string }): void {
    const state = this.stateSubject$.getValue();
    const isRelevant = _.includes(this.contextSenders, event.recipient) ||
      (_.some(state.items, {id: event.id}) && !this.isPersonal());
    if (isRelevant && event.authorId !== this.authService.getCurrentUserId()) {
      this.addItem(event);
    }
  }

  private loadContext(): void {
    if (this.isPersonal()) {
      this.subscriptionService.getSubscriptionsByType('user', 'workspace', 'page').subscribe(subscriptions =>
        this.contextSenders = _.concat(_.map(subscriptions, 'targetId'), this.authService.getCurrentUserId()));
    } else {
      this.contextSenders = [this.context.id];
    }
  }

  private scrollToMostRecentNewItem(): void {
    const items = this.stateSubject$.getValue().items;

    if (items.length < 1 || items[0].isNew) {
      return;
    }

    const mostRecentNewItem = _.find(items, item => item.isNew);
    if (!!mostRecentNewItem) {
      const element = document.getElementById('item-' + mostRecentNewItem.id);
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }
}
