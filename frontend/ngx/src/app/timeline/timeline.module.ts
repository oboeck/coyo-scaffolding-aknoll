import {ModuleWithProviders, NgModule} from '@angular/core';
import {EventsModule} from '@app/events/events.module';
import {NgSelectModule} from '@ng-select/ng-select';
import {CollapsibleContentModule} from '@shared/collapsible-content/collapsible-content.module';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {ContextMenuModule} from '@shared/context-menu/context-menu.module';
import {DividerModule} from '@shared/divider/divider.module';
import {EmojiModule} from '@shared/emoji/emoji.module';
import {FileModule} from '@shared/files/file.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {HashtagModule} from '@shared/hashtags/hashtag.module';
import {HelpModule} from '@shared/help/help.module';
import {JitTranslationModule} from '@shared/jit-translation/jit-translation.module';
import {MarkdownModule} from '@shared/markdown/markdown.module';
import {MentionModule} from '@shared/mention/mention.module';
import {PreviewModule} from '@shared/preview/preview.module';
import {RibbonModule} from '@shared/ribbon/ribbon.module';
import {SenderUIModule} from '@shared/sender-ui/sender-ui.module';
import {SocialModule} from '@shared/social/social.module';
import {TimeModule} from '@shared/time/time.module';
import {UIRouterUpgradeModule} from '@uirouter/angular-hybrid';
import {UpgradeModule} from '@upgrade/upgrade.module';
import {AutosizeModule} from 'ngx-autosize';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {ClipboardModule} from 'ngx-clipboard';
import {MomentModule} from 'ngx-moment';
import {messagesDe} from './de.timeline.messages';
import {messagesEn} from './en.timeline.messages';
import {messagesFormDe} from './timeline-form/de.timeline-form.messages';
import {messagesFormEn} from './timeline-form/en.timeline-form.messages';
import {FunctionalUserChooserComponent} from './timeline-form/functional-user-chooser/functional-user-chooser.component';
import {LockBtnComponent} from './timeline-form/lock-btn/lock-btn.component';
import {StickyBtnComponent} from './timeline-form/sticky-btn/sticky-btn.component';
import {StickyOptionsPipe} from './timeline-form/sticky-btn/sticky-options.pipe';
import {TimelineFormComponent} from './timeline-form/timeline-form/timeline-form.component';
import {TimelineItemContentOutletComponent} from './timeline-item/timeline-item-content-outlet/timeline-item-content-outlet.component';
import {BlogShareArticleComponent} from './timeline-item/timeline-item-content/blog-share-article/blog-share-article.component';
import {EventShareItemComponent} from './timeline-item/timeline-item-content/event-share-item/event-share-item.component';
import {PostItemComponent} from './timeline-item/timeline-item-content/post-item/post-item.component';
import {SenderShareItemComponent} from './timeline-item/timeline-item-content/sender-share-item/sender-share-item.component';
import {WikiShareArticleComponent} from './timeline-item/timeline-item-content/wiki-share-article/wiki-share-article.component';
import {TimelineItemContextMenuComponent} from './timeline-item/timeline-item-context-menu/timeline-item-context-menu.component';
import {TimelineItemEditModalComponent} from './timeline-item/timeline-item-edit-modal/timeline-item-edit-modal.component';
import {TimelineItemFooterComponent} from './timeline-item/timeline-item-footer/timeline-item-footer.component';
import {TimelineItemReportModalComponent} from './timeline-item/timeline-item-report-modal/timeline-item-report-modal.component';
import {TimelineItemRoutingComponent} from './timeline-item/timeline-item-routing/timeline-item-routing.component';
import {TimelineItemSkeletonComponent} from './timeline-item/timeline-item-skeleton/timeline-item-skeleton.component';
import {TimelineItemComponent} from './timeline-item/timeline-item/timeline-item.component';
import {TimelineStreamComponent} from './timeline-stream/timeline-stream.component';
import './timeline-stream/timeline-stream.component.downgrade';
import {timelineItemState} from './timeline.state';

export const uiRouterModuleChild: ModuleWithProviders =
  UIRouterUpgradeModule.forChild({states: [timelineItemState]});

/**
 * Feature module for all timeline related stuff
 */
@NgModule({
  imports: [
    AutosizeModule,
    BsDropdownModule,
    ClipboardModule,
    ContextMenuModule,
    CollapsibleContentModule,
    CoyoCommonsModule,
    CoyoFormsModule,
    DividerModule,
    EmojiModule,
    EventsModule,
    FileModule,
    HashtagModule,
    HelpModule,
    JitTranslationModule,
    MarkdownModule,
    MentionModule,
    MomentModule,
    NgSelectModule,
    PreviewModule,
    RibbonModule,
    SenderUIModule,
    SocialModule,
    TimeModule,
    TooltipModule,
    uiRouterModuleChild,
    UpgradeModule
  ],
  declarations: [
    BlogShareArticleComponent,
    EventShareItemComponent,
    FunctionalUserChooserComponent,
    LockBtnComponent,
    PostItemComponent,
    SenderShareItemComponent,
    StickyBtnComponent,
    StickyOptionsPipe,
    TimelineFormComponent,
    TimelineItemComponent,
    TimelineItemContentOutletComponent,
    TimelineItemContextMenuComponent,
    TimelineItemEditModalComponent,
    TimelineItemFooterComponent,
    TimelineItemReportModalComponent,
    TimelineItemRoutingComponent,
    TimelineItemSkeletonComponent,
    TimelineStreamComponent,
    WikiShareArticleComponent
  ],
  exports: [
    TimelineStreamComponent
  ],
  entryComponents: [
    BlogShareArticleComponent,
    EventShareItemComponent,
    PostItemComponent,
    SenderShareItemComponent,
    TimelineItemEditModalComponent,
    TimelineItemReportModalComponent,
    TimelineItemRoutingComponent,
    TimelineStreamComponent,
    WikiShareArticleComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn},
    {provide: 'messages', multi: true, useValue: messagesFormDe},
    {provide: 'messages', multi: true, useValue: messagesFormEn}
  ]
})

export class TimelineModule {
}
