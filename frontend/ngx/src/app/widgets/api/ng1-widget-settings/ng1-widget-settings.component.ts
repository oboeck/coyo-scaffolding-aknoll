import {Directive, ElementRef, EventEmitter, Inject, Injector, Input, OnChanges, OnInit, Output} from '@angular/core';
import {UpgradeComponent} from '@angular/upgrade/static';
import {Widget} from '@domain/widget/widget';
import {NG1_$TIMEOUT} from '@upgrade/upgrade.module';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetConfig} from '../widget-config';

/**
 * Directive for rendering the not migrated ng1 widget settings
 */
@Directive({
  selector: 'coyo-ng1-widget-settings' // tslint:disable-line:directive-selector
})
// tslint:disable-next-line:directive-class-suffix
export class Ng1WidgetSettingsComponent extends UpgradeComponent implements OnInit, OnChanges {

  /**
   * The widget
   */
  @Input() widget: any;

  /**
   * The configuration of the widget
   */
  @Input() config: WidgetConfig<Widget<WidgetSettings>>;

  /**
   * The callbacks to be called after saving
   */
  @Input() saveCallbacks: any;

  /**
   * Emits an event when the settings form is submitted
   */
  @Output() legacyFormSet: EventEmitter<any>;

  constructor(ref: ElementRef, inj: Injector, @Inject(NG1_$TIMEOUT) timeout: any) {
    super('oyocWidgetSettingsContainer', ref, inj);
    timeout(() => {
    });
  }
}
