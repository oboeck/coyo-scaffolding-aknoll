import {Injectable, NgZone} from '@angular/core';
import {environment} from '@root/environments/environment';
import {BsModalService} from 'ngx-bootstrap/modal';
import {first} from 'rxjs/operators';
import {WidgetChooserComponent} from './widget-chooser.component';

/**
 * A service that opens a {@link WidgetChooserComponent} modal to select and configure widgets.
 */
@Injectable()
export class WidgetChooserService {

  constructor(private ngZone: NgZone,
              private modalService: BsModalService) {
  }

  /**
   * Opens a {@link WidgetChooserComponent} to select and configure widgets.
   *
   * @returns a promise containing the widget data
   */
  open(): Promise<any> {
    return this.ngZone.run(() => this.modalService.show(WidgetChooserComponent, {
      animated: environment.enableAnimations,
      class: 'modal-lg',
      initialState: {
        widget: {
          isNew: () => true, settings: {}
        }
      }
    }).content.onClose.pipe(first()).toPromise());
  }
}
