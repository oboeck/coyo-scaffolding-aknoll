import {HostBinding, Input} from '@angular/core';
import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * A component that renders the inline settings form of a specific widget.
 */
export abstract class WidgetInlineSettingsComponent<WidgetType extends Widget<WidgetSettings>> {

  /**
   * The corresponding widget this settings component belongs to.
   */
  @Input() widget: WidgetType;

  @HostBinding('class') hostClasses: string = 'btn-group';
}
