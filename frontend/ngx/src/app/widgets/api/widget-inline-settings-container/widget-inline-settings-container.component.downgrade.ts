import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetInlineSettingsContainerComponent} from './widget-inline-settings-container.component';

getAngularJSGlobal()
  .module('coyo.widgets')
  .directive('coyoNgxWidgetInlineSettings', downgradeComponent({
    component: WidgetInlineSettingsContainerComponent
  }));
