import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetSettingsContainerComponent} from './widget-settings-container.component';

getAngularJSGlobal()
  .module('coyo.widgets')
  .directive('oyocNgxWidgetSettings', downgradeComponent({
    component: WidgetSettingsContainerComponent
  }));
