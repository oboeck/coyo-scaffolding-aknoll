import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetSettingsModalService} from './widget-settings-modal.service';

getAngularJSGlobal()
  .module('coyo.widgets.api')
  .factory('ngxWidgetSettingsModalService', downgradeInjectable(WidgetSettingsModalService) as any);
