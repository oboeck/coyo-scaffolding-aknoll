import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {BsModalService} from 'ngx-bootstrap/modal';
import {Subject} from 'rxjs';
import {WidgetRegistryService} from '../widget-registry/widget-registry.service';
import {WidgetSettingsModalComponent} from './widget-settings-modal.component';
import {WidgetSettingsModalService} from './widget-settings-modal.service';

describe('WidgetSettingsModalService', () => {
  let modalService: jasmine.SpyObj<BsModalService>;
  let widgetRegistry: jasmine.SpyObj<WidgetRegistryService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WidgetSettingsModalService, {
        provide: BsModalService,
        useValue: jasmine.createSpyObj('BsModalService', ['show'])
      }, {
        provide: WidgetRegistryService,
        useValue: jasmine.createSpyObj('WidgetRegistryService', ['get'])
      }]
    });

    modalService = TestBed.get(BsModalService);
    widgetRegistry = TestBed.get(WidgetRegistryService);
  });

  it('should be created', inject([WidgetSettingsModalService], (service: WidgetSettingsModalService) => {
    expect(service).toBeTruthy();
  }));

  it('should open a modal', fakeAsync(inject([WidgetSettingsModalService], (service: WidgetSettingsModalService) => {
    // given
    const widgetConfig = {key: 'widget', data: {}};
    const widget = {key: 'widget', name: 'WidgetComponent'};
    widgetRegistry.get.and.returnValue(widgetConfig);

    const subject = new Subject<any>();
    const modal = {content: {onClose: subject}};
    modalService.show.and.returnValue(modal);

    // when
    let args: any;
    service.open(widget).then(result => {
      args = result;
    });
    subject.next('result');
    tick();

    // then
    expect(args).toEqual('result');
    expect(modal.content['config']).toEqual(widgetConfig);
    expect(modal.content['widget']).toEqual(widget);
    expect(modalService.show).toHaveBeenCalledWith(WidgetSettingsModalComponent, {
      animated: false,
      class: 'modal-lg'
    });
  })));
});
