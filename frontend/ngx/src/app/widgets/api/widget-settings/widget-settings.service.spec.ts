import {TestBed} from '@angular/core/testing';
import {WidgetSettingsService} from './widget-settings.service';

describe('WidgetSettingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WidgetSettingsService = TestBed.get(WidgetSettingsService);
    expect(service).toBeTruthy();
  });
});
