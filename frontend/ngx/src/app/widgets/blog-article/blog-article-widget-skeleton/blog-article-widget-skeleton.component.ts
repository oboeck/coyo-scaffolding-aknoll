import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'coyo-blog-article-widget-skeleton',
  template: '<div></div>',
  styleUrls: ['./blog-article-widget-skeleton.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BlogArticleWidgetSkeletonComponent {
}
