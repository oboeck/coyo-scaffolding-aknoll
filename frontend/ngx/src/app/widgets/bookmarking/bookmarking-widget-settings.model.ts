import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {Bookmark} from '@widgets/bookmarking/bookmark.model';

/**
 * The entity model for the settings of a bookmarking widget
 */
export interface BookmarkingWidgetSettings extends WidgetSettings {
  _bookmarks: Bookmark[];
  _validBookmarkList: boolean;
}
