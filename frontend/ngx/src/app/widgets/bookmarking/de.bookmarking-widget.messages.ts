import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGETS.BOOKMARKING.ADD_BOOKMARK': 'Lesezeichen hinzufügen',
    'WIDGETS.BOOKMARKING.DESCRIPTION': 'Zeigt eine Liste von Lesezeichen an.',
    'WIDGETS.BOOKMARKING.NAME': 'Lesezeichen',
    'WIDGETS.BOOKMARKING.NO_BOOKMARKS': 'Es gibt aktuell noch keine Lesezeichen',
    'WIDGETS.BOOKMARKING.TITLE.PLACEHOLDER': 'Gib einen Titel an',
    'WIDGETS.BOOKMARKING.TITLE.ARIA': 'Lesezeichen Titel',
    'WIDGETS.BOOKMARKING.URL.ARIA': 'Lesezeichen URL',
    'WIDGETS.BOOKMARKING.URL.INVALID': 'Die eingegebene URL ist ungültig',
    'WIDGETS.BOOKMARKING.EDIT.TOGGLE_FROM.URL': 'Zum Lesezeichen-Titel wechseln',
    'WIDGETS.BOOKMARKING.EDIT.TOGGLE_FROM.TITLE': 'Zur Lesezeichen-URL wechseln',
    'WIDGETS.BOOKMARKING.EDIT.NEXT_FROM.URL': 'Weiter zur Eingabe des Lesezeichen-Titels',
    'WIDGETS.BOOKMARKING.EDIT.NEXT_FROM.TITLE': 'Lesezeichen hinzufügen',
    'WIDGETS.BOOKMARKING.EDIT.REMOVE': 'Lesezeichen löschen',
    'WIDGETS.BOOKMARKING.EDIT.DRAG_REORDER': 'Lesezeichen umsortieren (Maustaste gedrückt halten und an neue Stelle verschieben)'
  }
};
