import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {SettingsService} from '@domain/settings/settings.service';
import {WidgetSettingsComponent} from '@widgets/api/widget-settings-component';
import {ButtonWidget} from '@widgets/button/button-widget';
import * as _ from 'lodash';

/**
 * The button widget's settings component.
 *
 * This component is used only in the context of the widget settings modal dialog.
 */
@Component({
  templateUrl: './button-widget-settings.component.html',
  styleUrls: ['./button-widget-settings.component.scss']
})
export class ButtonWidgetSettingsComponent extends WidgetSettingsComponent<ButtonWidget> implements OnInit {

  styles: {
    clazz: string,
    name: string
  }[] = [
    {clazz: 'btn-default', name: 'WIDGET.BUTTON.SETTINGS.STYLE.DEFAULT'},
    {clazz: 'btn-primary', name: 'WIDGET.BUTTON.SETTINGS.STYLE.PRIMARY'},
    {clazz: 'btn-success', name: 'WIDGET.BUTTON.SETTINGS.STYLE.SUCCESS'},
    {clazz: 'btn-info', name: 'WIDGET.BUTTON.SETTINGS.STYLE.INFO'},
    {clazz: 'btn-warning', name: 'WIDGET.BUTTON.SETTINGS.STYLE.WARNING'},
    {clazz: 'btn-danger', name: 'WIDGET.BUTTON.SETTINGS.STYLE.DANGER'}
  ];

  constructor(private formBuilder: FormBuilder,
              private settingsService: SettingsService) {
    super();
  }

  ngOnInit(): void {
    const target = _.get(this.widget.settings, '_linkTarget', '_blank');
    const style = _.get(this.widget.settings, '_button.btnClass', this.styles[0].clazz);

    // set initial form controls
    this.parentForm.addControl('text', new FormControl(this.widget.settings.text,
      [Validators.required, Validators.maxLength(255)]));
    this.parentForm.addControl('_url', new FormControl(this.widget.settings._url, [Validators.required]));
    this.parentForm.addControl('_linkTarget', new FormControl(target));
    this.parentForm.addControl('_button', this.formBuilder.group({
      btnClass: this.formBuilder.control(style)
    }));

    // update form validators after URL pattern has been resolved
    this.settingsService.retrieveByKey('linkPattern').subscribe(pattern => {
      this.parentForm.get('_url').setValidators(
        [Validators.required, Validators.pattern(pattern)]);
      this.parentForm.updateValueAndValidity();
    });
  }
}
