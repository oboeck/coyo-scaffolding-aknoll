import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Page} from '@test/util/page';
import {ButtonWidgetComponent} from './button-widget.component';

class ComponentPage extends Page<ButtonWidgetComponent> {
  get link(): HTMLElement {
    return this.query<HTMLElement>('a');
  }
}

describe('ButtonWidgetComponent', () => {
  let component: ButtonWidgetComponent;
  let fixture: ComponentFixture<ButtonWidgetComponent>;
  let page: ComponentPage;

  let widget: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonWidgetComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    widget = {settings: {}};
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonWidgetComponent);
    page = new ComponentPage(fixture);
    component = fixture.componentInstance;
    component.widget = widget;
    fixture.detectChanges();
  });

  it('should use the configured text', () => {
    widget.settings['text'] = 'Link text';
    fixture.detectChanges();

    expect(page.link.textContent).toContain(widget.settings['text']);
  });

  it('should use the configured link settings', () => {
    widget.settings['_url'] = 'http://example.com/';
    widget.settings['_button'] = {btnClass: 'btn-default'};
    widget.settings['_linkTarget'] = '_self';
    fixture.detectChanges();

    expect(page.link.getAttribute('href')).toEqual(widget.settings['_url']);
    expect(page.link.getAttribute('target')).toBeNull();
    expect(page.link.className).toContain(widget.settings['_button']['btnClass']);
  });

  it('should use "_blank" as a default target', () => {
    expect(page.link.getAttribute('target')).toEqual('_blank');
  });
});
