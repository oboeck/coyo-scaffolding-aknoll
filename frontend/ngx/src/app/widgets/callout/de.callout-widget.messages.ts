import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.CALLOUT.DESCRIPTION': 'Zeigt eine Hinweisbox an.',
    'WIDGET.CALLOUT.NAME': 'Hinweis',
    'WIDGET.CALLOUT.SETTINGS.TEXT': 'Text',
    'WIDGET.CALLOUT.SETTINGS.STYLE': 'Stil',
    'WIDGET.CALLOUT.SETTINGS.STYLE.SUCCESS': 'Erfolg',
    'WIDGET.CALLOUT.SETTINGS.STYLE.INFO': 'Info',
    'WIDGET.CALLOUT.SETTINGS.STYLE.WARNING': 'Warnung',
    'WIDGET.CALLOUT.SETTINGS.STYLE.DANGER': 'Achtung'
  }
};
