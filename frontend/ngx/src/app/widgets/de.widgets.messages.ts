import {Messages} from '@core/i18n/messages';

/* tslint:disable no-trailing-whitespace */
export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGETS.WIDGET_CHOOSER.EMPTY': 'Es sind keine Widgets verfügbar.',
    'WIDGETS.WIDGET_CHOOSER.LOADING': 'Widgets werden geladen...',
    'WIDGETS.WIDGET_CHOOSER.TITLE': 'Widget auswählen',
    'WIDGETS.WIDGET_CHOOSER.SEARCH': 'Suche…',
    'WIDGETS.WIDGET_CHOOSER.CATEGORY.ALL': 'Alle',
    'WIDGETS.WIDGET_CHOOSER.CATEGORY.DYNAMIC': 'Dynamisch',
    'WIDGETS.WIDGET_CHOOSER.CATEGORY.STATIC': 'Statisch',
    'WIDGETS.WIDGET_CHOOSER.CATEGORY.PERSONAL': 'Persönlich',
    'WIDGETS.HELP.MODAL': `
**Widgets sind in COYO die wichtigste Content-Management-Funktion. Du verwendest sie, um die von dir verwendeten Apps im
 erweiterten Modus anzupassen.**

### Apps UND Widgets?
Der Unterschied zwischen Apps und Widgets besteht darin, dass Widgets in erster Linie Erweiterungen für die Apps 
darstellen. Du kannst mit ihrer Hilfe weitere Funktionen in Apps einbinden und so noch mehr Flexibilität und Kreativität
 in dein COYO bringen.

### Berechtigungen
Um Widgets hinzuzufügen, musst du zunächst sicherstellen, dass du die dafür benötigten Rechte besitzt. Sollte dies nicht
 der Fall sein, müsste dir ein Admin zunächst die entsprechende Rolle zuweisen, die es dir ermöglicht Widgets zu einer 
 App hinzuzufügen.

**Tipp**: Wenn du neu mit COYO startest, kann es sein, dass einige Widgets deaktiviert sind. Über die 
[Administration](/docs/guide/user/administration_apps-widgets?lang=de) kannst du dies ändern!

### Arten von Widgets
Widgets unterscheiden grundsätzlich zwische statischen, dynamischen und persönlichen Content.
Du kannst sie an verschiedenen Stellen in COYO verwenden, so z.B. in Startseiten, auf Seiten oder in Workspaces.

#### Statische Inhalte
Mit statischen Content-Widgets kannst du benutzerdefinierte, feste Inhalte erstellen.

#### Dynamische Inhalte
Zusätzlich zu statischen Content-Widgets kannst du mit COYO Widgets erstellen, die ihre Informationen aus anderen 
Quellen beziehen. Sie sind dynamisch, weil sie sich verändern und/oder aus anderen Quellen stammen.

#### Persönliche Inhalte
Ergänzend hast du die Möglichkeit personalisierte Inhalte für die User hinzuzufügen.

### Widget API
COYO hat eine einfache Front-End Widget API, die es extrem einfach für dich oder deine Partner macht, neue Widgets 
für spezielle Anforderungen zu schaffen.

Schau für mehr Informationen in unserem [Developer Guide](/docs/guide/dev/widget-architecture) vorbei.

    `
  }
};
