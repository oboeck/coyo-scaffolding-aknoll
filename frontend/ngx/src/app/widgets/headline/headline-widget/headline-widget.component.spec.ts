import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HeadlineWidget} from '@widgets/headline/headline-widget';
import sizeOptions from '@widgets/headline/size-options';
import {HeadlineWidgetComponent} from './headline-widget.component';

describe('HeadlineWidgetComponent', () => {
  let component: HeadlineWidgetComponent;
  let fixture: ComponentFixture<HeadlineWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeadlineWidgetComponent]
    }).overrideTemplate(HeadlineWidgetComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadlineWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {} as HeadlineWidget;
    fixture.detectChanges();
  });

  it('should set initial settings', () => {
    expect(component.widget.settings._headline).toBe(sizeOptions[1]);
  });
});
