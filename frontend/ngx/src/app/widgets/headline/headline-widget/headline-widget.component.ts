import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {WidgetComponent} from '@widgets/api/widget-component';
import {HeadlineWidget} from '@widgets/headline/headline-widget';
import sizeOptions from '@widgets/headline/size-options';
import * as _ from 'lodash';

/**
 * The headline widget displays a headline.
 */
@Component({
  selector: 'coyo-headline-widget',
  templateUrl: './headline-widget.component.html',
  styleUrls: ['./headline-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeadlineWidgetComponent extends WidgetComponent<HeadlineWidget> implements OnInit {

  /**
   * The state of the widget edit mode.
   */
  @Input() editMode: boolean;

  constructor(cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit(): void {
    if (!_.get(this.widget, 'settings._headline')) {
      _.set(this.widget, 'settings._headline', sizeOptions[1]);
    }
  }
}
