import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.HTML.DESCRIPTION': 'Renders custom HTML.',
    'WIDGET.HTML.NAME': 'HTML',
    'WIDGET.HTML.SETTINGS.TEXT': 'HTML',
    'WIDGET.HTML.SETTINGS.TEXT.HELP': 'Enter your custom HTML to be rendered.'
  }
};
