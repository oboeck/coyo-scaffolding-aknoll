import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';

import {HtmlWidgetSettingsComponent} from './html-widget-settings.component';

describe('HtmlWidgetSettingsComponent', () => {
  let component: HtmlWidgetSettingsComponent;
  let fixture: ComponentFixture<HtmlWidgetSettingsComponent>;

  let widget: any;
  let parentForm: FormGroup;
  const onSubmit: Subject<any> = new Subject<any>();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HtmlWidgetSettingsComponent]
    }).overrideTemplate(HtmlWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    widget = {settings: {}};
    parentForm = new FormGroup({});
    fixture = TestBed.createComponent(HtmlWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    component.parentForm = parentForm;
    component.onSubmit = onSubmit;
    fixture.detectChanges();
  });

  it('should validate settings', () => {
    // given / when
    component.parentForm.patchValue({
      html_content: 'HTML Content'
    });

    // then
    expect(component.parentForm.valid).toBeTruthy();
  });

  it('should not validate settings', () => {
    // given / when
    component.parentForm.patchValue({
      html_content: ''
    });

    // then
    expect(component.parentForm.valid).toBeFalsy();
  });
});
