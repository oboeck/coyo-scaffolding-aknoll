import {Component} from '@angular/core';
import {WidgetComponent} from '@widgets/api/widget-component';
import {HtmlWidget} from '@widgets/html/html-widget';

@Component({
  selector: 'coyo-html-widget',
  templateUrl: './html-widget.component.html'
})
export class HtmlWidgetComponent extends WidgetComponent<HtmlWidget> {
}
