import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SettingsService} from '@domain/settings/settings.service';
import {of, Subject} from 'rxjs';
import {IframeWidgetSettingsComponent} from './iframe-widget-settings.component';

describe('IframeWidgetSettingsComponent', () => {
  let component: IframeWidgetSettingsComponent;
  let fixture: ComponentFixture<IframeWidgetSettingsComponent>;

  let settingsService: jasmine.SpyObj<SettingsService>;
  let widget: any;
  let parentForm: FormGroup;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [FormBuilder, {
        provide: SettingsService,
        useValue: jasmine.createSpyObj('SettingsService', ['retrieveByKey'])
      }],
      declarations: [IframeWidgetSettingsComponent]
    }).overrideTemplate(IframeWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    settingsService = TestBed.get(SettingsService);
    settingsService.retrieveByKey.and.returnValue(of('https?:\\/\\/\\S+'));

    widget = {settings: {}};
    parentForm = new FormGroup({});
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IframeWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    component.parentForm = parentForm;
    component.onSubmit = new Subject<any>();
    fixture.detectChanges();
  });

  it('should validate settings', () => {
    component.ngOnInit();

    component.parentForm.patchValue({
      url: 'http://example.com/',
      height: 300,
      scrolling: true
    });

    expect(component.parentForm.valid).toBeTruthy();
    expect(component.parentForm.get('url').value).toEqual('http://example.com/');
    expect(component.parentForm.get('height').value).toEqual(300);
    expect(component.parentForm.get('scrolling').value).toBeTruthy();
    expect(component.parentForm.get('url').errors).toBeNull();
  });

  it('should invalidate settings without url', () => {
      component.ngOnInit();

      component.parentForm.patchValue({
        url: 'http://example',
        height: 1,
        scrolling: false
      });

      expect(component.parentForm.valid).toBeFalsy();
      expect(component.parentForm.get('url').errors).not.toBeNull();
    }
  );
});
