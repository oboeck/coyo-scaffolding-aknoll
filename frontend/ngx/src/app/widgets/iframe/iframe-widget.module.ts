import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {HelpModule} from '@shared/help/help.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {messagesDe} from '@widgets/iframe/de.iframe-widget.messages';
import {messagesEn} from '@widgets/iframe/en.iframe-widget.messages';
import {IFRAME_WIDGET} from '@widgets/iframe/iframe-widget-config';
import {IframeWidgetSettingsComponent} from '@widgets/iframe/iframe-widget-settings/iframe-widget-settings.component';
import {IframeWidgetComponent} from '@widgets/iframe/iframe-widget/iframe-widget.component';

/**
 * Module for the iframe widget
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    CoyoFormsModule,
    HelpModule
  ],
  declarations: [
    IframeWidgetComponent,
    IframeWidgetSettingsComponent
  ],
  entryComponents: [
    IframeWidgetComponent,
    IframeWidgetSettingsComponent
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: IFRAME_WIDGET, multi: true},
    {provide: 'messages', useValue: messagesDe, multi: true},
    {provide: 'messages', useValue: messagesEn, multi: true}
  ]
})
export class IframeWidgetModule {}
