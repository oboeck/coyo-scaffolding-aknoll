import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TrustUrlPipe} from '@shared/trust-url/trust-url.pipe';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {Subject} from 'rxjs';
import {IframeWidgetComponent} from './iframe-widget.component';

describe('IframeWidgetComponent', () => {
  let component: IframeWidgetComponent;
  let fixture: ComponentFixture<IframeWidgetComponent>;
  let widgetSettingsService: jasmine.SpyObj<WidgetSettingsService>;
  const widgetChangesSubject$: Subject<WidgetSettings> = new Subject();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IframeWidgetComponent, TrustUrlPipe],
      providers: [{
        provide: WidgetSettingsService,
        useValue: jasmine.createSpyObj('WidgetSettingsService', ['getChanges$'])
      }, ChangeDetectorRef]
    })
    .overrideTemplate(IframeWidgetComponent, '')
    .compileComponents();

    widgetSettingsService = TestBed.get(WidgetSettingsService);
    widgetSettingsService.getChanges$.and.returnValue(widgetChangesSubject$.asObservable());
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IframeWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {
        url: 'http://example.com',
        height: 240,
        scrolling: false
      }
    };
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should set scrolling to yes', () => {
    // given
    component.widget.settings.scrolling = true;

    // when
    fixture.detectChanges();

    // then
    expect(component.scrolling).toEqual('yes');
  });

  it('should set scrolling to no', () => {
    // given
    component.widget.settings.scrolling = false;

    // when
    fixture.detectChanges();

    // then
    expect(component.scrolling).toEqual('no');
  });
});
