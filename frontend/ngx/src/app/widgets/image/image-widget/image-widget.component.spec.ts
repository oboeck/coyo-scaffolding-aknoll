import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Document} from '@domain/file/document';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {ImageWidget} from '@widgets/image/image-widget';
import {Subject} from 'rxjs';
import {ImageWidgetComponent} from './image-widget.component';

describe('ImageWidgetComponent', () => {
  let component: ImageWidgetComponent;
  let fixture: ComponentFixture<ImageWidgetComponent>;
  let widgetSettingsService: jasmine.SpyObj<WidgetSettingsService>;
  const widgetChangesSubject$: Subject<WidgetSettings> = new Subject();
  const imageData = {_image: {id: 'document-id', senderId: 'sender-id'} as Document};

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ImageWidgetComponent],
        providers: [{
          provide: WidgetSettingsService,
          useValue: jasmine.createSpyObj('WidgetSettingsService', ['getChanges$'])
        }, ChangeDetectorRef]
      })
      .overrideTemplate(ImageWidgetComponent, '')
      .compileComponents();

    widgetSettingsService = TestBed.get(WidgetSettingsService);
  }));

  beforeEach(() => {
    widgetSettingsService.getChanges$.and.returnValue(widgetChangesSubject$.asObservable());
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id'
    } as ImageWidget;
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init with object data', () => {
    // given
    component.widget = {
      id: 'widget-id',
      settings: {
        _image: imageData._image
      }
    };

    // when
    fixture.detectChanges();

    // then
    expect(component.image).toBe(imageData._image);
  });

  it('should init with changes from widget settings service', () => {
    // given
    fixture.detectChanges();

    // when
    widgetChangesSubject$.next(imageData);

    // then
    expect(component.image).toBe(imageData._image);
  });

});
