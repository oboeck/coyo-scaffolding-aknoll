import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Document} from '@domain/file/document';
import {WidgetComponent} from '@widgets/api/widget-component';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {ImageWidget} from '@widgets/image/image-widget';
import {ImageWidgetSettings} from '@widgets/image/image-widget-settings.model';

/**
 * The image widget component.
 * Renders a preselected image.
 */
@Component({
  selector: 'coyo-image-widget',
  templateUrl: './image-widget.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageWidgetComponent extends WidgetComponent<ImageWidget> implements OnInit {

  image: Document;

  constructor(cd: ChangeDetectorRef,
              private widgetSettingsService: WidgetSettingsService) {
    super(cd);
  }

  ngOnInit(): void {
    if (this.widget && this.widget.settings && this.widget.settings._image) {
      this.image = this.widget.settings._image;
    }
    this.widgetSettingsService.getChanges$(this.widget.id).subscribe(options => {
      this.image = (options as ImageWidgetSettings)._image;
      super.detectChanges();
    });
  }
}
