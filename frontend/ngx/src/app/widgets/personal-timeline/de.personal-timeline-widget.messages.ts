import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.PERSONAL_TIMELINE.DESCRIPTION': 'Dieses Widget stellt die persönliche Timeline des aktiven Nutzers dar.',
    'WIDGET.PERSONAL_TIMELINE.NAME': 'Timeline'
  }
};
