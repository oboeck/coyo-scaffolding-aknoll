import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.PERSONAL_TIMELINE.DESCRIPTION': 'A widget to display the personal timeline of the current user.',
    'WIDGET.PERSONAL_TIMELINE.NAME': 'Timeline'
  }
};
