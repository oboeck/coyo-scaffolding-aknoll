import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export interface PersonalTimelineWidget extends Widget<WidgetSettings> {
}
