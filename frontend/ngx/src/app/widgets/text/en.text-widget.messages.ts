import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.TEXT.DESCRIPTION': 'Displays plain text with an optional title.',
    'WIDGET.TEXT.NAME': 'Text',
    'WIDGET.TEXT.PLACEHOLDER': 'Enter text here…'
  }
};
