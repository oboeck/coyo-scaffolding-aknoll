import {Directive, ElementRef, Injector, Input} from '@angular/core';
import {UpgradeComponent} from '@angular/upgrade/static';
import {BaseModel} from '@domain/base-model/base-model';

/**
 * Upgraded widget layout component
 */
@Directive({
  selector: 'coyo-widget-layout-ngx' // tslint:disable-line directive-selector
})
export class WidgetLayoutDirective extends UpgradeComponent {

  /**
   * The name of the layout
   */
  @Input() layoutName: string;

  /**
   * The parent of the layout
   */
  @Input() parent: BaseModel;

  /**
   * Flag if the current user can change the layout
   */
  @Input() canManage: boolean = false;

  /**
   * The render style. At the moment only plain is allowed as no other style is used by now.
   */
  @Input() renderStyle: 'plain' = 'plain';

  constructor(elementRef: ElementRef, injector: Injector) {
    super('coyoWidgetLayout', elementRef, injector);

  }

}
