import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {UpgradeModule} from '@upgrade/upgrade.module';
import {BlogArticleWidgetModule} from '@widgets/blog-article/blog-article-widget.module';
import {BookmarkingWidgetModule} from '@widgets/bookmarking/bookmarking-widget.module';
import {HeadlineWidgetModule} from '@widgets/headline/headline-widget.module';
import {HtmlWidgetModule} from '@widgets/html/html-widget.module';
import {IframeWidgetModule} from '@widgets/iframe/iframe-widget.module';
import {ImageWidgetModule} from '@widgets/image/image-widget.module';
import {PersonalTimelineWidgetModule} from '@widgets/personal-timeline/personal-timeline-widget.module';
import {SubscriptionWidgetModule} from '@widgets/subscription/subscription-widget.module';
import {UserOnlineWidgetModule} from '@widgets/user-online/user-online-widget.module';
import {ModalModule} from 'ngx-bootstrap/modal';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {Ng1WidgetSettingsComponent} from './api/ng1-widget-settings/ng1-widget-settings.component';
import {WidgetChooserComponent} from './api/widget-chooser/widget-chooser.component';
import {WidgetChooserService} from './api/widget-chooser/widget-chooser.service';
import './api/widget-chooser/widget-chooser.service.downgrade';
import {WidgetContainerComponent} from './api/widget-container/widget-container.component';
import './api/widget-container/widget-container.component.downgrade';
import {WidgetInlineSettingsContainerComponent} from './api/widget-inline-settings-container/widget-inline-settings-container.component';
import './api/widget-inline-settings-container/widget-inline-settings-container.component.downgrade';
import './api/widget-registry/widget-registry.downgrade';
import {WidgetRegistryService} from './api/widget-registry/widget-registry.service';
import {WidgetSettingsContainerComponent} from './api/widget-settings-container/widget-settings-container.component';
import './api/widget-settings-container/widget-settings-container.component.downgrade';
import {WidgetSettingsModalComponent} from './api/widget-settings-modal/widget-settings-modal.component';
import {WidgetSettingsModalService} from './api/widget-settings-modal/widget-settings-modal.service';
import './api/widget-settings-modal/widget-settings-modal.service.downgrade';
import './api/widget-settings/widget-settings.service.downgrade';
import {ButtonWidgetModule} from './button/button-widget.module';
import {CalloutWidgetModule} from './callout/callout-widget.module';
import {messagesDe} from './de.widgets.messages';
import {DividerWidgetModule} from './divider/divider-widget.module';
import {messagesEn} from './en.widgets.messages';
import {HashtagWidgetModule} from './hashtag/hashtag-widget.module';
import {TextWidgetModule} from './text/text-widget.module';
import {WelcomeWidgetModule} from './welcome/welcome-widget.module';
import {WidgetLayoutDirective} from './widget-layout/widget-layout.directive';

/**
 * This module provides an API for managing, registering and displaying widgets.
 */
@NgModule({
  imports: [
    BlogArticleWidgetModule,
    BookmarkingWidgetModule,
    ButtonWidgetModule,
    CalloutWidgetModule,
    CoyoCommonsModule,
    CoyoFormsModule,
    DividerWidgetModule,
    HashtagWidgetModule,
    HeadlineWidgetModule,
    HtmlWidgetModule,
    IframeWidgetModule,
    ImageWidgetModule,
    ModalModule,
    PersonalTimelineWidgetModule,
    SubscriptionWidgetModule,
    TabsModule,
    TextWidgetModule,
    UpgradeModule,
    UserOnlineWidgetModule,
    WelcomeWidgetModule
  ],
  declarations: [
    Ng1WidgetSettingsComponent,
    WidgetChooserComponent,
    WidgetContainerComponent,
    WidgetInlineSettingsContainerComponent,
    WidgetLayoutDirective,
    WidgetSettingsContainerComponent,
    WidgetSettingsModalComponent
  ],
  exports: [
    WidgetLayoutDirective
  ],
  entryComponents: [
    WidgetChooserComponent,
    WidgetContainerComponent,
    WidgetInlineSettingsContainerComponent,
    WidgetSettingsContainerComponent,
    WidgetSettingsModalComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn},
    WidgetChooserService,
    WidgetRegistryService,
    WidgetSettingsModalService
  ]
})
export class WidgetsModule {
}
