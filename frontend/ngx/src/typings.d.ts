import {HttpHeaders, HttpParams} from '@angular/common/http';
import {Document} from '@domain/file/document';
import {Sender} from '@domain/sender/sender';
import {Target} from '@domain/sender/target';
import {User} from '@domain/user/user';
import {Observable} from 'rxjs';
import {FilePreview} from './app/core/domain/preview/file-preview';

/* SystemJS module definition */
declare var module: NodeModule;

interface NodeModule {
  id: string;
}

interface Ng1CoyoConfig {
  backendUrl: string;
  backendUrlStrategy: string;
  applicationName: string;
  version: {
    major: number;
    minor: number;
    patch: number;
    qualifier: string;
  };
  entityTypes: { [key: string]: EntityType };

  versionString(): string;
}

interface Ng1CoyoNotification {
  primary(i18n: string, useI18n?: boolean): void;
  error(i18n: string, useI18n?: boolean): void;
  success(i18n: string, useI18n?: boolean): void;
  info(i18n: string, useI18n?: boolean): void;
  warning(i18n: string, useI18n?: boolean): void;
}

interface EntityType {
  label: string;
  icon: string;
  color: string;
  plural: string;
}

interface Ng1DefaultThemeColors {
  'color-primary': string;
  'color-secondary': string;
  'color-navbar-border': string;
  'coyo-navbar-text': string;
  'coyo-navbar': string;
  'coyo-navbar-active': string;
  'btn-primary-color': string;
  'btn-primary-bg': string;
  'color-background-main': string;
  'text-color': string;
  'link-color': string;
}

interface Ng1AdminThemeConfig {
  imageConfigs: Ng1AdminThemeImageConfig[];
}

interface Ng1AdminThemeImageConfig {
  key: string;
  retinaKey: string;
  variables: { [key: string]: (originalFile: any, resizedFile: any, Upload: any) => void };
}

interface Ng1FileLibraryModalService {
  open(sender: Sender | { [id: string]: string }, options: Ng1FileLibraryModalOptions, cropSettings?: CropSettings): Promise<Document | Document[]>;
}

type Ng1CoyoTranslationLoader = (input: { key: string }) => Promise<Map<any>>;

interface Ng1TranslationRegistry {
  getTranslationTable(language: string): (language: string) => string;
  getTranslationTables(): any[];
  addTranslations(language: string, translation: {[key: string]: string}): (language: string, translation: string) => string;
}

interface Ng1BackendUrlService {
  getUrl(): string | null;
  isSet(): boolean;
}

interface Ng1CsrfService {
  getToken(): Promise<any>;
  clearToken(): void;
}

interface Ng1MobileEventsService {
  propagate(eventName: string, payload?: any): void;
  evaluate(jsonString: string): void;
}

/* We have two authServices, this authService type represent the AngularJS authService */
interface Ng1AuthService {
  userUpdated$: Observable<[any, User, User]>;
  userLoggedIn$: Observable<[any]>;
  userLoggedOut$: Observable<[any]>;
  clearSession(): Promise<any>;
  isAuthenticated(): boolean;
  canUseHashtags(): boolean;
  getCurrentUserId(): string | null;

  validateUserId(userId: string, content: () => T | string): void;
  getUser(forceRefresh: boolean = false): Promise<User> | null;
}

interface Ng1TargetService {
  go(target: Target): Promise;
  getLink(target: Target): string;
}

interface Ng1ErrorService {
  showErrorPage(message: string, status: number, buttons: object[] | string[]): void;
}

interface Ng1AppService {
  getCurrentAppIdOrSlug(): string | null;
}

interface Ng1WidgetRegistry {
  getAll(): any[];
  get(key: string): object;
  getEnabled(): object;
}

interface Options {
  context?: { [key: string]: string };
  path?: string;
  headers?: HttpHeaders | { [header: string]: string | string[] };
  params?: HttpParams | { [param: string]: string | string[] };
  permissions?: string[];
  handleErrors?: boolean;
}

interface Ng1FileLibraryModalOptions {
  uploadMultiple?: boolean;
  selectMode?: string;
  filterContentType?: string | string[];
  initialFolder?: object;
}

interface CropSettings {
  cropImage: boolean;
}

interface Ng1NotificationService {
  primary(message: string): void;
  error(message: string): void;
  success(message: string): void;
  info(message: string): void;
  warning(message: string): void;
  clearAll(): void;
}

interface Ng1ModalAlerts {
  level: string;
  title: string;
  text: string;
}

interface Ng1ModalOptions {
  size?: string;
  title?: string;
  text?: string;
  translationContext?: object;
  alerts?: Ng1ModalAlerts[];
  close?: object;
  dismiss?: object;
}

interface Ng1ModalService {
  open(options: Ng1ModalOptions): Observable<any>;
  confirm(options: Ng1ModalOptions): Observable<any>;
  confirmDelete(options: Ng1ModalOptions): Observable<any>;
}

interface CoyoConfig {
  entityTypes: {[key: string]: {icon: string}};
  likeReloadIntervalMinutes: number;
}

interface Ng1SublineService {
  getSublineForUser(user: Sender): Observable<string>;
  getSublineForSender(sender: Sender): Observable<string>;
}

interface Ng1StateLockService {
  ignoreLock(callback: () => Promise<void>): void;
}

interface Ng1ScrollBehaviourService {
  enableBodyScrolling(): void;
  disableBodyScrolling(): void;
  disableBodyScrollingOnXsScreen(): void;
}

interface Ng1SocketService {
  webSocketReconnected$: Observable<any>;
  subscribe(destination: string, callback: Function, eventFilter?: string | object | Function): Function;
}

interface Ng1fileDetailsModalService {
  open(files: FilePreview[],
       currentIndex?: number,
       linkToFileLibrary?: boolean,
       showAuthors?: boolean,
       fileAuthors?: { [key: string]: any },
       appIdOrSlug?: string,
       filesTotal?: number,
       loadNext?: () => File[]): void;
}

interface Ng1ImageModalService {
  open(imageUrl: string, title: string): void;
}

export interface Ng1MobileStorageHandler {
  getStorageValue(key: string): any;
  setStorageValue(args: {key: string, value: any}): void;
}

export interface Ng1DeviceRegistrationHandler {
  registerPushDevice(args: {device: string}): void;
}

export interface Ng1AppRegistry {
  getDefaultStateName(key: any, senderType: any): string;
}

export interface Ng1MobileEventService {
  propagate(eventName: string, payload: any): void;
  evaluate(json: string): void;
}

export interface Ng1MessagingService {
  start(partnerId: string): Promise<any>;
}

export interface Ng1SocketReconnectDelays {
  TIMELINE_ITEMS_RELOAD_DELAY: number;
}

interface EventDates {
  startDate: Date;
  startTime: Date;
  endDate: Date;
  endTime: Date;
}

export interface Ng1EventDateSyncService {
  updateStartDate(eventDates: EventDates, updateEnd: boolean): void;
  updateStartTime(eventDates: EventDates, updateEnd: boolean): void;
  updateEndDate(eventDates: EventDates): void;
  updateEndTime(eventDates: EventDates): void;
}
