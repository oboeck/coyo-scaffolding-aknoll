(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.events')
      .controller('EventsAppMainController', EventsAppMainController);

  function EventsAppMainController($timeout, EventModel, Pageable, sender, eventsAppConfig, selectionFilterService) {
    var vm = this;

    vm.sender = sender;

    vm.search = search;
    vm.filter = filter;
    vm.setParticipationStatusFilter = setParticipationStatusFilter;
    vm.clearDate = clearDate;
    vm.filtersResettable = true;
    vm.resetFilters = resetFilters;
    vm.$onInit = _init;

    /* ==================== */

    function search(searchTerm) {
      vm.query.term = searchTerm;
      _loadEvents();
    }

    function filter(dateRange) {
      _setDateRange(dateRange);
      _loadEvents();
    }

    function setParticipationStatusFilter(selected) {
      _setParticipationStatus(selected);
      _loadEvents();
    }

    function clearDate() {
      _resetDateRangeFilter();
      _loadEvents();
    }

    function resetFilters() {
      _resetParticipationStatusFilter();
      _resetDateRangeFilter();
      _loadEvents();
    }

    /* ==================== */

    var PARAMS_IN_SEPARATE_ARGS = ['term', 'from', 'to']; // filter parameters that are passed as separate arguments

    function _loadEvents() {
      if (vm.loading) {
        return;
      }

      // perform search
      vm.loading = true;
      var term = vm.query.term;
      var from = vm.query.from ? (vm.query.from + 'T00:00:00') : undefined;
      var to = vm.query.to ? (vm.query.to + 'T23:59:59') : undefined;
      var sort = term ? ['_score,DESC', 'displayName.sort'] : 'displayName.sort';
      var pageable = new Pageable(0, eventsAppConfig.list.paging.pageSize, sort);
      var filters = angular.extend({sender: [sender.id]}, _.omit(vm.query, PARAMS_IN_SEPARATE_ARGS));
      EventModel.searchWithFilter(term, from, to, pageable, filters).then(function (page) {
        vm.currentPage = page;
        vm.filtersResettable = _isFiltersResettable();
      }).finally(function () {
        vm.loading = false;
      });
    }

    function _setParticipationStatus(participationStatus) {
      vm.query.participationStatus = participationStatus;
    }

    function _resetParticipationStatusFilter() {
      vm.participationStatusFilter.clearAll();
      _setParticipationStatus();
    }

    function _setDateRange(dateRange) {
      vm.query.from = dateRange && dateRange[0] ? dateRange[0].format('YYYY-MM-DD') : null;
      vm.query.to = dateRange && dateRange[1] ? dateRange[1].format('YYYY-MM-DD') : null;
    }

    function _resetDateRangeFilter() {
      vm.dateRange = [];
      _setDateRange(vm.dateRange);
    }

    function _isFiltersResettable() {
      return vm.participationStatusFilter.isSelected() || vm.dateRange.length > 0;
    }

    function _init() {
      vm.query = {};
      vm.dateRange = [];

      var participationStatus = EventModel.getAllParticipationStatuses().map(function (status) {
        return {key: status};
      });
      vm.participationStatusFilter = selectionFilterService.builder().items(participationStatus).build();

      $timeout(function () {
        _loadEvents();
      });
    }
  }

})(angular);
