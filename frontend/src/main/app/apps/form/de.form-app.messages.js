(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.form')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "ADD_ANOTHER": "Weiteres Formular ausfüllen",
          "APP.FORM.DESCRIPTION": "Erstelle und pflege dynamische Formulare mit selbstdefinierten Einträgen.",
          "APP.FORM.DETAIL.MODAL.TITLE.VIEW": "Eintrag ansehen",
          "APP.FORM.FIELDS.CONFIGURE.NO_FIELDS": "Das Formular ist noch nicht konfiguriert. Du benötigst Felder um den Inhalt deines Formulars zu definieren. Lege jetzt die ersten Felder an!",
          "APP.FORM.MAX_SUBMISSIONS": "Dein Formular wurde an den Administrator gesendet. Du kannst kein weiteres Forumlar ausfüllen, da du bereits die maximale Anzahl erreicht hast.",
          "APP.FORM.SUBMIT_ANOTHER": "Dein Formular wurde an den Administrator gesendet. Du kannst weitere Formulare ausfüllen.",
          "APP.FORM.SUCCESSFULLY_SUBMITTED": "Das Formular wurde erfolgreich übermittelt",
          "APP.FORM.MODAL.DELETE.TITLE": "Eintrag löschen",
          "APP.FORM.MODAL.DELETE.TEXT": "Möchtest du den Eintrag wirklich löschen? Der Eintrag kann nicht wiederhergestellt werden.",
          "APP.FORM.NAME": "Formular",
          "APP.FORM.SEE_FORM": "Formular anzeigen",
          "APP.FORM.SEE_RESULTS": "Ergebnisse einsehen",
          "APP.FORM.SETTINGS.DESCRIPTION": "Beschreibung",
          "APP.FORM.SETTINGS.DESCRIPTION.HELP": "Die Beschreibung des Formulars wird den Benutzern auf der Formularansicht unterhalb des Titels angezeigt.",
          "APP.FORM.SETTINGS.EMAIL_NOTIFICATIONS": "E-Mail Benachrichtigungen",
          "APP.FORM.SETTINGS.GENERAL": "Allgemein",
          "APP.FORM.SETTINGS.NOTIFICATIONS": "Benachrichtigungen",
          "APP.FORM.SETTINGS.NOTIFICATIONS.ADMIN": "Administratoren",
          "APP.FORM.SETTINGS.NOTIFICATIONS.NONE": "Keiner",
          "APP.FORM.SETTINGS.NOTIFICATIONS.HELP": "Legt fest, ob Administratoren bei Formulareinsendungen benachrichtigt werden sollen.",
          "APP.FORM.SETTINGS.SEE_RESULTS": "Ergebnisse einsehen",
          "APP.FORM.SETTINGS.SEE_RESULTS.DESCRIPTION": "Benutzer können die Ergebnisse einsehen",
          "APP.FORM.SETTINGS.SEE_RESULTS.HELP": "Wenn aktiviert, können alle Benutzer der App die übermittelten Formulare einsehen, ansonsten können dies nur Administratoren.",
          "APP.FORM.SETTINGS.SET_SUBMIT_LIMIT": "Begrenzung",
          "APP.FORM.SETTINGS.SET_SUBMIT_LIMIT.DESCRIPTION": "Maximale Anzahl an Einsendungen pro Benutzer festlegen",
          "APP.FORM.SETTINGS.SET_SUBMIT_LIMIT.HELP": "Begrenzt die Anzahl an Formularen die ein Benutzer absenden kann.",
          "APP.FORM.SETTINGS.SUBMIT_LIMIT": "Maximale Einsendungen",
          "APP.FORM.SETTINGS.TITLE": "Titel",
          "APP.FORM.SETTINGS.TITLE.HELP": "Der Titel des Formulars wird den Benutzern als Überschrift angezeigt und für die Benachrichtigungen verwendet.",
          "APP.FORM.SETTINGS.EMAIL.USERS": "Benutzer",
          "APP.FORM.SETTINGS.EMAIL.USERS.HELP": "Gib an, welche Benutzer bei neuen Einsendungen per E-Mail benachrichtigt werden sollen.",
          "APP.FORM.SETTINGS.EMAIL.ADDITIONAL": "E-Mail-Adresse",
          "APP.FORM.SETTINGS.EMAIL.ADDITIONAL.HELP": "Du kannst E-Mail-Adressen von externen Benutzern angeben, die über neue Einsendungen benachrichtigt werden sollen.",
          "APP.FORM.SETTINGS.EMAIL.CONFIRMATION": "Bestätigung",
          "APP.FORM.SETTINGS.EMAIL.CONFIRMATION.LABEL": "Benutzer erhalten nach Absenden des Formulars eine Bestätigung per Email",
          "APP.FORM.SETTINGS.EMAIL.CONFIRMATION.HELP": "Aktiviere diese Option, wenn du möchtest, dass der Absender eine Bestätigung mit allen seinen gesendeten Daten erhält.",
          "APP.FORM.TABLE.AUTHOR": "Autor",
          "APP.FORM.TABLE.CREATED": "Erstellt",
          "NOTIFICATIONS.FORM.ENTRY.NEW": "*{s1}* hat ein Formular in *{s2}* > *{s3}* eingereicht."
        });
        /* eslint-enable quotes */
      });
})(angular);
