(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.form')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "ADD_ANOTHER": "Fill out another",
          "APP.FORM.DESCRIPTION": "Create and manage forms of items you define dynamically.",
          "APP.FORM.DETAIL.MODAL.TITLE.VIEW": "Entry details",
          "APP.FORM.FIELDS.CONFIGURE.NO_FIELDS": "The form is not configured, yet. You will need to define fields to specify the content of your form. Start creating the first fields, now!",
          "APP.FORM.MAX_SUBMISSIONS": "Your form will be sent to the administrator. You are not able to fill out another form, because you have reached the submit limit for this form.",
          "APP.FORM.SUBMIT_ANOTHER": "Your form will be sent to the administrator. You are able to fill out another form.",
          "APP.FORM.SUCCESSFULLY_SUBMITTED": "Form successfully submitted!",
          "APP.FORM.MODAL.DELETE.TITLE": "Delete entry",
          "APP.FORM.MODAL.DELETE.TEXT": "Do you really want to delete this entry? The deletion can't be undone.",
          "APP.FORM.NAME": "Form",
          "APP.FORM.SEE_FORM": "View form",
          "APP.FORM.SEE_RESULTS": "View results",
          "APP.FORM.SETTINGS.DESCRIPTION": "Description",
          "APP.FORM.SETTINGS.DESCRIPTION.HELP": "The description is shown beneath the title in the form.",
          "APP.FORM.SETTINGS.EMAIL_NOTIFICATIONS": "Email notifications",
          "APP.FORM.SETTINGS.GENERAL": "General",
          "APP.FORM.SETTINGS.NOTIFICATIONS": "Notifications",
          "APP.FORM.SETTINGS.NOTIFICATIONS.ADMIN": "Admins",
          "APP.FORM.SETTINGS.NOTIFICATIONS.NONE": "None",
          "APP.FORM.SETTINGS.NOTIFICATIONS.HELP": "Specifies whether administrators should be notified when forms are submitted.",
          "APP.FORM.SETTINGS.SEE_RESULTS": "See results",
          "APP.FORM.SETTINGS.SEE_RESULTS.DESCRIPTION": "Users are allowed to see all results",
          "APP.FORM.SETTINGS.SEE_RESULTS.HELP": "If activated, all users of the app can view the submitted forms, otherwise only administrators can view them.",
          "APP.FORM.SETTINGS.SET_SUBMIT_LIMIT": "Submission limit",
          "APP.FORM.SETTINGS.SET_SUBMIT_LIMIT.DESCRIPTION": "Set maximum submissions per user",
          "APP.FORM.SETTINGS.SET_SUBMIT_LIMIT.HELP": "Limits the number of forms a user can submit.",
          "APP.FORM.SETTINGS.SUBMIT_LIMIT": "Submissions per user",
          "APP.FORM.SETTINGS.TITLE": "Form title",
          "APP.FORM.SETTINGS.TITLE.HELP": "The title is displayed as the headline of the form and used for notifications.",
          "APP.FORM.SETTINGS.EMAIL.USERS": "Users",
          "APP.FORM.SETTINGS.EMAIL.USERS.HELP": "Select users who should be notified via email about new submissions.",
          "APP.FORM.SETTINGS.EMAIL.ADDITIONAL": "Email address",
          "APP.FORM.SETTINGS.EMAIL.ADDITIONAL.HELP": "Specify email addresses to be notified of new submissions. These addresses can belong to external, non-registered users.",
          "APP.FORM.SETTINGS.EMAIL.CONFIRMATION": "Confirmation",
          "APP.FORM.SETTINGS.EMAIL.CONFIRMATION.LABEL": "Send a confirmation to users that submit the form",
          "APP.FORM.SETTINGS.EMAIL.CONFIRMATION.HELP": "Indicate if the sender of the submission should receive an email as confirmation with all submitted data.",
          "APP.FORM.TABLE.AUTHOR": "Author",
          "APP.FORM.TABLE.CREATED": "Created",
          "NOTIFICATIONS.FORM.ENTRY.NEW": "*{s1}* submitted a new form in *{s2}* > *{s3}*."
        });
        /* eslint-enable quotes */
      });
})(angular);
