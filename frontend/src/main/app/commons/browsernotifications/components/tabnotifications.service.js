(function (angular) {
  'use strict';

  angular
      .module('commons.browsernotifications')
      .factory('tabNotificationsService', tabNotificationsService);

  /**
   * @ngdoc service
   * @name commons.browsernotifications.tabNotificationsService
   *
   * @description
   * Provides methods for notifications in the current browser tab, for example sounds.
   *
   * @requires anTinycon
   * @requires $timeout
   * @requires coyo.domain.UserNotificationSettingModel
   * @requires $rootScope
   * @requires $localStorage
   * @requires commons.browsernotifications.browserNotificationsService
   */
  function tabNotificationsService(anTinycon, $timeout, $rootScope, $localStorage, browserNotificationsService) {
    var counter = {};
    var sound = new Audio('assets/sounds/new.ogg');
    $rootScope.$on('authService:logout:success', _clear);

    return {
      setCounter: setCounter,
      resetCounter: resetCounter
    };

    /**
     * @ngdoc method
     * @name commons.browsernotifications.tabNotificationsService#setCounter
     * @methodOf commons.browsernotifications.tabNotificationsService
     *
     * @description
     * Sets the counter for the given category and updates the tab title. Plays a sound notification if activated and necessary.
     *
     */
    function setCounter(category, number, silent) {
      if (angular.isDefined(counter[category]) && number !== 0 && number !== counter[category] && !silent) {
        _playSound();
      }
      counter[category] = number;
      _setCounter();
    }

    /**
     * @ngdoc method
     * @name commons.browsernotifications.tabNotificationsService#resetCounter
     * @methodOf commons.browsernotifications.tabNotificationsService
     *
     * @description
     * Resets the counter of the given category to zero and updates the tab title.
     *
     */
    function resetCounter(category) {
      counter[category] = 0;
      _setCounter();
    }

    function _clear() {
      counter = {};
      _setCounter();
    }

    function _setCounter() {
      $timeout(function () {
        var sum = _.sum(_.values(counter));
        if (sum > 0) {
          anTinycon.setBubble(sum);
        } else {
          anTinycon.reset();
        }
      }, 0);
    }

    function _playSound() {
      if ($rootScope.tabId === $localStorage.activeTabId) {
        browserNotificationsService.checkNotificationChannel({channel: 'SOUND', active: true})
            .then(function (isChannelActive) {
              if (isChannelActive) {
                sound.play();
              }
            });
      }
    }

  }

})(angular);
