(function (angular) {
  'use strict';

  angular
      .module('commons.layout')
      .directive('oyocAdminSidebar', adminSidebar)
      .controller('AdminSidebarController', AdminSidebarController);

  /**
   * @ngdoc directive
   * @name commons.layout.oyocAdminSidebar:oyocAdminSidebar
   * @restrict E
   * @element OWN
   *
   * @description
   * Displays the sidebar navigation menu of the admin area.
   *
   * @requires $rootScope
   * @requires commons.ui.sidebarService
   * @requires coyo.domain.SettingsModel
   */
  function adminSidebar() {
    return {
      restrict: 'E',
      scope: true,
      replace: true,
      templateUrl: 'app/commons/layout/components/admin-sidebar/admin-sidebar.html',
      controller: 'AdminSidebarController',
      controllerAs: '$ctrl'
    };
  }

  function AdminSidebarController($rootScope, $scope, sidebarService) {
    var vm = this;

    vm.$onInit = _init;
    vm.showSidebar = false;
    vm.open = open;
    vm.close = close;

    function open() {
      vm.showSidebar = true;
      $rootScope.showBackdrop = true;
    }

    function close() {
      vm.showSidebar = false;
      $rootScope.showBackdrop = false;
    }

    sidebarService.register({
      name: 'admin-menu',
      open: open,
      close: close,
      isOpen: function () {
        return vm.showSidebar;
      }
    });

    function _init() {
      var unsubscribe = $rootScope.$on('license:loaded', function (event, license) {
        vm.tenantEnv = license.environment;
      });
      $scope.$on('$destroy', unsubscribe);
    }
  }

})(angular);
