(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .component('coyoEmptyListNotice', emptyListNotice());

  /**
   * @ngdoc directive
   * @name commons.ui.coyoEmptyListNotice:coyoEmptyListNotice
   *
   * @description
   * Shows a notice when a list is empty and provides the optional possibility to reset filter settings.
   *
   * @param {string} textKey
   * Translation key for the notice to be displayed.
   *
   * @param {boolean} filtersResettable
   * Whether or not filter settings are resettable.
   *
   * @param {expression} resetFilters
   * Function provided to reset filter settings.
   */
  function emptyListNotice() {
    return {
      templateUrl: 'app/commons/ui/components/empty-list-notice/empty-list-notice.html',
      bindings: {
        textKey: '@',
        translateValues: '<?',
        filtersResettable: '<?',
        resetFilters: '&?'
      },
      controller: function () {
      }
    };
  }

})(angular);
