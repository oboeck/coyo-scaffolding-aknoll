(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .component('coyoCategoryFilter', categoryFilter())
      .controller('CategoryFilterController', CategoryFilterController);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoCategoryFilter:coyoCategoryFilter
   *
   * @description
   * Filter for categories.
   *
   * @param {object} categories The categories.
   *
   * @param {array} filterModel
   * The filter model containing the selected items.
   *
   * @param {object} categoryModelClass
   * A specific category model class to perform operations like creating a new category or ordering.
   *
   * @param {string} titleKey Translation key for the title.
   *
   * @param {number} totalCount Count to be shown on the 'All' option.
   *
   * @param {number} notAvailableCount Count to be shown on the 'N/A' option.
   *
   * @param {boolean} canManage Manage permission.
   *
   * @param {expression} onSelectionChange
   * Callback that will be invoked when selection changed. A named parameter 'selected' will receive an array of
   * the selected keys.
   *
   * @param {boolean} editingCategory Communicates editing status to parent controller.
   *
   * @param {string} categoryIcon Label for the icons of the category-filter items. By default it will be "layers".
   */
  function categoryFilter() {
    return {
      templateUrl: 'app/commons/ui/components/filterbox/filters/category-filter/category-filter.html',
      bindings: {
        categories: '=',
        filterModel: '=',
        categoryModelClass: '<',
        titleKey: '@',
        totalCount: '<',
        notAvailableCount: '<',
        canManage: '<',
        onSelectionChange: '&?',
        editingCategory: '=',
        categoryIcon: '@?'
      },
      controller: 'CategoryFilterController'
    };
  }

  function CategoryFilterController($rootScope, $scope, $timeout, $q, coyoNotification, modalService) {
    var vm = this,
        CategoryModel,
        editedCategory;

    vm.savingCategory = false;
    vm.treeOptions = undefined;
    vm.categoryIcon = angular.isDefined(vm.categoryIcon) ? vm.categoryIcon : 'layers';

    vm.$onInit = init;
    vm.isCategoryActive = isCategoryActive;
    vm.isCategoryEdited = isCategoryEdited;
    vm.toggleCategory = toggleCategory;
    vm.resetCategory = resetCategory;
    vm.addCategory = addCategory;
    vm.editCategory = editCategory;
    vm.saveCategory = saveCategory;
    vm.onCategoryKeypress = onCategoryKeypress;
    vm.onCategoryClickOutside = onCategoryClickOutside;
    vm.deleteCategory = deleteCategory;

    function isCategoryActive(category) {
      return category ? vm.filterModel.indexOf(category.id) !== -1 : vm.filterModel.length === 0;
    }

    function isCategoryEdited(category) {
      return vm.editingCategory && editedCategory && (editedCategory.isNew() ? category.isNew() : editedCategory.id
          === category.id);
    }

    function toggleCategory(category) {
      if (!vm.editingCategory) {
        var index = _.indexOf(vm.filterModel, category.id);
        var notAvailableSet = _.indexOf(vm.filterModel, 'N/A');
        if (category.id === 'N/A') {
          vm.filterModel = [];
        }
        if (notAvailableSet !== -1) {
          _.pullAt(vm.filterModel, notAvailableSet);
        }
        if (index !== -1) {
          _.pull(vm.filterModel, category.id);
        } else if (category && category.id) {
          vm.filterModel.push(category.id);
        }
        vm.onSelectionChange({selected: vm.filterModel});
      }
    }

    function resetCategory() {
      vm.filterModel = [];
      vm.onSelectionChange({selected: vm.filterModel});
    }

    function addCategory($event) {
      if (vm.editingCategory || vm.savingCategory) {
        return;
      }

      beginEditMode(new CategoryModel());
      vm.categories.push(editedCategory);

      $timeout(function () {
        var target = angular.element($event.currentTarget);
        target.parent().find('input').select();
      });
    }

    function editCategory(category, $event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();

      if (vm.editingCategory || vm.savingCategory) {
        return;
      }

      beginEditMode(angular.copy(category));

      $timeout(function () {
        var target = angular.element($event.currentTarget);
        target.parent().parent().find('input').select();
      });
    }

    function saveCategory(category, $event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();

      if (!category.name || vm.savingCategory) {
        return $q.reject();
      }
      if (category.name.trim().toUpperCase() === 'N/A') {
        coyoNotification.error('ERRORS.STATUSCODE.NAME_TAKEN', category);
        return $q.reject();
      }
      vm.savingCategory = true;
      return category.save().then(function (result) {
        vm.categories.splice(_.findIndex(vm.categories, {id: category.id}), 1, result);
        endEditMode();
      }).finally(function () {
        vm.savingCategory = false;
      });
    }

    function onCategoryKeypress(category, $event) {
      if ($event.keyCode === 13) {
        saveCategory(category, $event);
      }
    }

    function onCategoryClickOutside(category) {
      if (!vm.editingCategory || !category) {
        return; // not in edit mode
      }

      if (category.isNew()) {
        vm.categories.pop();
      } else {
        category.name = editedCategory.name;
      }
      endEditMode();
    }

    function deleteCategory(category, $event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();

      if (vm.editingCategory || vm.savingCategory) {
        return $q.reject();
      }

      return modalService.confirmDelete({
        title: 'PAGE.CATEGORY.DELETE.MODAL.TITLE',
        text: 'PAGE.CATEGORY.DELETE.MODAL.TEXT',
        translationContext: {category: category.name}
      }).result.then(function () {
        vm.savingCategory = true;
        return category.delete().then(function (result) {
          _.remove(vm.categories, {id: category.id});
          if (category.id === _.get(vm.filterModel, '[0]', null)) {
            vm.onSelectionChange({selected: vm.filterModel});
          }
          return result;
        });
      }).finally(function () {
        vm.savingCategory = false;
      });
    }

    function buildTreeOptions() {
      return {
        dropped: function (event) {
          // persist new sort order
          if (event.source.index !== event.dest.index) {
            CategoryModel.order(_.map(vm.categories, 'id'));
          }
        }
      };
    }

    function beginEditMode(category) {
      vm.editingCategory = true;
      editedCategory = category;
    }

    function endEditMode() {
      vm.editingCategory = false;
      editedCategory = null;
    }

    function init() {
      CategoryModel = vm.categoryModelClass;
      vm.treeOptions = buildTreeOptions();
      endEditMode();

      // register ESC callback
      var unsubscribeEscFn = $rootScope.$on('keyup:esc', function () {
        if (vm.editingCategory) {
          var category = _.find(vm.categories, isCategoryEdited);
          $timeout(function () {
            onCategoryClickOutside(category);
          });
        }
      });

      // unregister callbacks on $scope death
      $scope.$on('$destroy', function () {
        unsubscribeEscFn();
      });
    }
  }

})(angular);
