(function () {
  'use strict';

  angular
      .module('commons.ui')
      .component('coyoUserList', userList());

  function userList() {
    return {
      templateUrl: 'app/commons/ui/components/user-list/user-list.html',
      bindings: {
        page: '='
      }
    };
  }
})();

