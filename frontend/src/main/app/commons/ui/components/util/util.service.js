(function () {
  'use strict';

  angular
      .module('coyo.base')
      .factory('utilService', utilService);

  /**
   * @ngdoc service
   * @name coyo.base.utilService
   *
   * @description
   *     Provides basic utility functions.
   */
  function utilService($q) {
    return {
      hash: hash,
      uuid: uuid,
      matchTextLinks: matchTextLinks,
      requireInternetConnection: requireInternetConnection,
      setCursorPosition: setCursorPosition
    };

    /**
     * @ngdoc method
     * @name coyo.base.utilService#hash
     * @methodOf coyo.base.utilService
     *
     * @description
     *     A simple string hashing function using the {@link http://www.cse.yorku.ca/~oz/hash.html djb2}.
     * @param {string} str
     *     The input string.
     * @returns {number}
     *     The absolute numeric hash of the input string.
     */
    function hash(str) {
      if (!str) {
        return -1;
      }

      var h = 5381;
      for (var i = 0; i < str.length; i++) {
        h = ((h << 5) + h) + str.charCodeAt(i);
      }
      return Math.abs(h);
    }

    /**
     * @ngdoc method
     * @name coyo.base.utilService#uuid
     * @methodOf coyo.base.utilService
     *
     * @description
     *     Generates a new UUID.
     * @returns {string}
     *     A UUID string, e.g. *4f87322d-f337-996f-8a9b-1ad08b82853c*.
     */
    function uuid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
      }

      return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    /**
     * @ngdoc method
     * @name coyo.base.utilService#matchTextLinks
     * @methodOf coyo.base.utilService
     *
     * @description
     *     Finds all links in the provided text.
     * @param {string} text
     *     The input text.
     * @returns {Array}
     *     An array of the matched links or an empty array.
     */
    function matchTextLinks(text) {
      var regex = /https?:\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/g;
      return text.match(regex) || [];
    }

    /**
     * @ngdoc method
     * @name coyo.base.utilService#requireInternetConnection
     * @methodOf coyo.base.utilService
     *
     * @description
     *     Checks whether the client is connected to the internet
     * @returns {Object}
     *     A promise resolving the client's internet connection state
     */
    function requireInternetConnection() {
      if (navigator.onLine === false) {
        return $q.reject();
      }
      return $q.resolve();
    }

    /**
     * @ngdoc method
     * @name coyo.base.utilService#setCursorPosition
     * @methodOf coyo.base.utilService
     *
     * @description
     *    Sets the cursor position on the given input field. If end is provided and differs from start,
     *    all letters between start and end are selected.
     *
     * @param {object} inputField the text input field to set the cursor position
     * @param {number} cursorPosition the index where the cursor is set
     * @param {number} selectionEnd if set, the text from cursorPosition to this index is selected
     */
    function setCursorPosition(inputField, cursorPosition, selectionEnd) {
      if (inputField.createTextRange) {
        var selRange = inputField.createTextRange();
        selRange.collapse(true);
        selRange.moveStart('character', cursorPosition);
        selRange.moveEnd('character', Math.abs(selectionEnd - cursorPosition));
        selRange.select();
      } else if (inputField.setSelectionRange) {
        inputField.setSelectionRange(cursorPosition, selectionEnd);
      } else if (inputField.selectionStart) {
        inputField.selectionStart = cursorPosition;
        inputField.selectionEnd = selectionEnd;
      }
      inputField.focus();
    }
  }
})();
