/* eslint-disable angular/no-private-call */
(function (angular) {
  'use strict';
  angular
      .module('commons.ui')
      .directive('coyoCheckTrustUrl', checkTrustUrl);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoCheckEquals:coyoCheckTrustUrl
   * @scope
   * @restrict 'A'
   * @element ANY
   *
   * @description
   * Checks the given form value against the trusted-url filter to validate if the url is considered trusted.
   *
   * @requires $filter
   */
  function checkTrustUrl($filter) {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {
        ngModel.$validators.trustUrl = function (modelValue) {
          var filteredValue = $filter('trustUrl')(modelValue);
          if (!filteredValue || !_.isFunction(filteredValue.$$unwrapTrustedValue)) {
            return false;
          }
          return filteredValue.$$unwrapTrustedValue() === modelValue;
        };
      }
    };
  }

})(angular);
