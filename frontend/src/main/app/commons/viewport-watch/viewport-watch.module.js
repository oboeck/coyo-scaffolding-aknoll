/* eslint-disable angular/no-private-call */
(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name commons.viewportWatch
   *
   * @description
   * # Viewport-Watch #
   * This module uses the scrollMonitor library to check whether or not a DOM element is visible
   * in the clients viewport. If not, all watchers of this element and its children get backed up and removed.
   * When an element gets scrolled back to the viewport, it is getting its watchers back.
   *
   * The purpose of this is to reduce the amount of concurrent watchers for long lists of content.
   *
   * This script originates from {@link https://github.com/wix/angular-viewport-watch angular-viewport-watch}
   * and was modified to fit our needs.
   */

  angular
      .module('commons.viewportWatch', [])
      .directive('viewportWatch', viewportWatch);

  function viewportWatch($window, $timeout) {
    var scrollMonitor = $window.scrollMonitor;
    var viewportUpdateTimeout;

    function debouncedViewportUpdate() {
      $timeout.cancel(viewportUpdateTimeout);
      viewportUpdateTimeout = $timeout(function () {
        scrollMonitor.update();
      }, 10, false);
    }

    return {
      restrict: 'AE',
      link: function (scope, element, attr) {
        var elementWatcher;

        function watchDuringDisable() {
          this.$$watchersBackup = this.$$watchersBackup || [];
          this.$$watchers = this.$$watchersBackup;
          var unwatch = this.constructor.prototype.$watch.apply(this, arguments);
          this.$$watchers = null;
          return unwatch;
        }

        function toggleWatchers(scope, enable) {
          var digest, current, next = scope;
          do {
            current = next;
            if (enable) {
              if (current.hasOwnProperty('$$watchersBackup')) {
                current.$$watchers = current.$$watchersBackup;
                delete current.$$watchersBackup;
                delete current.$watch;
                digest = !scope.$root.$$phase;
              }
            } else if (!current.hasOwnProperty('$$watchersBackup')) {
              current.$$watchersBackup = current.$$watchers;
              current.$$watchers = null;
              current.$watch = watchDuringDisable;
            }
            next = current.$$childHead;
            while (!next && current !== scope) {
              if (current.$$nextSibling) {
                next = current.$$nextSibling;
              } else {
                current = current.$parent;
              }
            }
          } while (next);
          if (digest) {
            scope.$digest();
          }
        }

        function disableDigest() {
          toggleWatchers(scope, false);
        }

        function enableDigest() {
          toggleWatchers(scope, true);
        }

        function createElementWatcher() {
          elementWatcher = scrollMonitor.create(element, scope.$eval(attr.viewportWatch || '0'));
          if (!elementWatcher.isInViewport) {
            scope.$evalAsync(disableDigest);
            debouncedViewportUpdate();
          }
          elementWatcher.enterViewport(enableDigest);
          elementWatcher.exitViewport(disableDigest);
        }

        scope.$applyAsync(createElementWatcher);
        var unsubscribeToggleWatchers = scope.$on('toggleWatchers', function (event, enable) {
          toggleWatchers(scope, enable);
        });
        scope.$on('$destroy', function () {
          elementWatcher.destroy();
          debouncedViewportUpdate();
          unsubscribeToggleWatchers();
        });
      }
    };
  }
})(angular);
