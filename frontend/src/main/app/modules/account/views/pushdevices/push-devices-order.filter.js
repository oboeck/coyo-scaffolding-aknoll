(function (angular) {
  'use strict';

  angular
      .module('coyo.account')
      .filter('devicesToOrderedArray', devicesToOrderedArrayFilter);

  /**
   * @ngdoc filter
   * @name coyo.account:devicesToOrderedArray
   * @function
   *
   * @description
   * Transforms the device object to an ordered by name array structure.
   *
   * @param {string} input The device object that contains the grouped by name app installation array
   */
  function devicesToOrderedArrayFilter() {
    return function (input) {
      if (!_.isObject(input)) {
        return input;
      }
      var orderedArray = _.sortBy(_.values(input), [function (appInstallations) {
        return appInstallations[0].name;
      }]);
      return orderedArray;
    };
  }

})(angular);
