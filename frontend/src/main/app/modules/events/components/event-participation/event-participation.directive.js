(function (angular) {
  'use strict';

  angular
      .module('coyo.events')
      .directive('coyoEventParticipation', EventParticipation)
      .controller('EventParticipationController', EventParticipationController);

  /**
   * @ngdoc directive
   * @name coyo.events.coyoEventParticipation:coyoEventParticipation
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Renders component where the current participation status for an event is shown and can be selected via a
   * drop-down-box.
   *
   * @param {object} event
   * The event to show/set the participation status for.
   *
   * @param {boolean} disableChange
   * Set to 'true' if the participation status is to be shown only without the possibility to change it.
   *
   * @param {expression} onStatusChanged
   * Callback that will be invoked when the status was changed. A named parameter 'newStatus' will receive the new
   * participation status, a named parameter 'oldStatus' will receive the previous status.
   *
   * @requires $rootScope
   */
  function EventParticipation() {
    return {
      require: 'ngModel',
      restrict: 'E',
      replace: true,
      templateUrl: 'app/modules/events/components/event-participation/event-participation.html',
      scope: {},
      bindToController: {
        event: '<',
        disableChange: '<?',
        onStatusChanged: '&'
      },
      controller: 'EventParticipationController',
      controllerAs: '$ctrl'
    };
  }

  function EventParticipationController($rootScope, $timeout, $document) {
    var vm = this;
    var element;
    vm.$onInit = onInit;
    vm.statuses = ['PENDING', 'ATTENDING', 'MAYBE_ATTENDING', 'NOT_ATTENDING'];
    vm.allowedChoices = [];
    vm.onSelectionChanged = onSelectionChanged;
    vm.isChoiceDisabled = isChoiceDisabled;
    vm.isChangeDisabled = isChangeDisabled;
    vm.getDisabledStatus = getDisabledStatus;
    vm.toggleEventParticipationVisibility = toggleEventParticipationVisibility;

    function onSelectionChanged() {
      vm.event.setStatus(vm.event.status).then(function () {
        vm.onStatusChanged({newStatus: vm.event.status, oldStatus: vm.oldStatus});
        vm.oldStatus = vm.event.status;
      });
      vm.eventParticipationVisible = false;
    }

    function isChoiceDisabled(choice) {
      return vm.allowedChoices.indexOf(choice) < 0;
    }

    function isChangeDisabled() {
      return vm.disableChange || _isFullyBooked();
    }

    function getDisabledStatus() {
      return _isFullyBooked() ? 'FULLY_BOOKED' : vm.event.status;
    }

    function toggleEventParticipationVisibility() {
      vm.eventParticipationVisible = !vm.eventParticipationVisible;
      if (vm.eventParticipationVisible) {
        $timeout(function () {
          var id = '#event-participation-select-' + vm.event.id;
          element = angular.element($document[0].querySelector(id + ' .ui-select-toggle'))[0];
          element.focus();
          element.click();
          angular.element($document[0].body).one('click', function () {
            vm.eventParticipationVisible = false;
          });
        });
      }
    }

    function _isFullyBooked() {
      return _participationLimitReached() && _notAttending();
    }

    function _participationLimitReached() {
      return vm.event.limitedParticipants
          && (vm.event.limitedParticipants.numberOfAttendingParticipants
              >= vm.event.limitedParticipants.participantsLimit);
    }

    function _notAttending() {
      return vm.event.status !== 'ATTENDING';
    }

    function onInit() {
      if (vm.event.status === null) {
        vm.event.status = 'PARTICIPATE';
      }
      vm.eventParticipationVisible = false;
      vm.allowedChoices = vm.event.requestDefiniteAnswer
        ? ['ATTENDING', 'NOT_ATTENDING'] : ['ATTENDING', 'MAYBE_ATTENDING', 'NOT_ATTENDING'];
      vm.oldStatus = vm.event.status;
    }
  }

})(angular);
