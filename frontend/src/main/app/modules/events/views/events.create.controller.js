(function (angular) {
  'use strict';

  angular
      .module('coyo.events')
      .controller('EventsCreateController', EventsCreateController);

  function EventsCreateController($scope, $q, $state, EventModel, coyoNotification, moment, currentUser, host,
                                  eventDateSyncService, authService) {
    var vm = this;

    vm.$onInit = onInit;

    vm.endDateOrEndTimeTouched = false;
    vm.host = host ? host : currentUser;
    vm.back = back;
    vm.next = next;
    vm.cancel = cancel;
    vm.addGeneralFormData = addGeneralFormData;

    function back() {
      vm.wizard.active = Math.max(0, vm.wizard.active - 1);
    }

    function next(form) {
      if (form && form.$valid) {
        if (vm.wizard.active < vm.wizard.states.length - 1) {
          return $q.resolve(vm.wizard.active++);
        } else {
          var start = moment(vm.event.startDate);
          var end = moment(vm.event.endDate);
          if (vm.event.fullDay) {
            start.hour(12).minute(0).seconds(0);
            end.hour(12).minute(0).seconds(0);
          }
          vm.event.visibility = vm.host && vm.host.public ? 'PUBLIC' : 'PRIVATE';
          vm.event.senderId = vm.host ? vm.host.id : currentUser.id;
          vm.event.startDate = start.format('YYYY-MM-DDTHH:mm:ss');
          vm.event.endDate = end.format('YYYY-MM-DDTHH:mm:ss');
          return vm.event.create().then(function (response) {
            $state.go('main.event.show', {idOrSlug: response.event.slug});
            coyoNotification.success('MODULE.EVENTS.CREATE.SUCCESS');
          });
        }
      }
      return $q.reject();
    }

    function cancel() {
      var state = _.get($state, 'previous.name', 'main.event');
      var params = _.get($state, 'previous.params', {});
      $state.go(state, params);
    }

    function addGeneralFormData(data) {
      vm.event.name = data.name;
      vm.event.place = data.location;
      vm.event.description = data.description;
      vm.event.fullDay = data.fullDay;
      vm.event.startDate = data.eventStart;
      vm.event.endDate = data.eventEnd;
      vm.event.showParticipants = data.showParticipants;
      vm.event.requestDefiniteAnswer = data.requestDefiniteAnswer;
      vm.event.limitedParticipants = data.limitedParticipants;
      if (data.limitedParticipants) {
        vm.event.limitedParticipants = {
          participantsLimit: data.participantsLimit
        };
      } else {
        vm.event.limitedParticipants = null;
      }
      vm.host = data.host;
      next({$valid: true}); // fake first form
      $scope.$apply();
    }

    function onInit() {
      vm.wizard = {
        states: [
          'MODULE.EVENTS.CREATE.GENERAL',
          'MODULE.EVENTS.CREATE.PARTICIPANTS'
        ],
        active: 0
      };
      vm.event = new EventModel({
        adminIds: [currentUser.id],
        memberIds: [],
        adminGroupIds: [],
        memberGroupIds: [],
        limitedParticipants: null
      });
      authService.onGlobalPermissions('PERMIT_EVENT_GROUP_INVITES', function (canInviteGroups) {
        vm.canInviteGroups = canInviteGroups;
      }, true);

    }
  }

})(angular);
