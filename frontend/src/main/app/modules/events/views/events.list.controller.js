(function (angular) {
  'use strict';

  angular
      .module('coyo.events')
      .controller('EventsListController', EventsListController);

  function EventsListController($sessionStorage, $state, $stateParams, $timeout, EventModel, Pageable, moment,
                                eventsConfig, selectionFilterService, authService) {

    var vm = this;

    vm.loading = true;
    vm.filtersResettable = true;

    vm.$onInit = _init;
    vm.search = search;
    vm.filter = filter;
    vm.clearDate = clearDate;
    vm.setStatusFilter = setStatusFilter;
    vm.setParticipationStatusFilter = setParticipationStatusFilter;
    vm.resetFilters = resetFilters;

    function search(searchTerm) {
      vm.query.term = searchTerm;
      _loadEvents();
    }

    function filter(dateRange) {
      vm.query.from = dateRange && dateRange[0] ? dateRange[0].format('YYYY-MM-DD') : null;
      vm.query.to = dateRange && dateRange[1] ? dateRange[1].format('YYYY-MM-DD') : null;
      _loadEvents();
    }

    function clearDate() {
      vm.dateRange = [];
      filter(vm.dateRange);
    }

    function setStatusFilter(selected) {
      _setStatus(selected);
      _loadEvents();
    }

    function setParticipationStatusFilter(selected) {
      _setParticipationStatus(selected);
      _loadEvents();
    }

    function resetFilters() {
      _resetParticipationStatusFilter();
      _resetStatusFilter();
      _resetDateRangeFilter();
      _loadEvents();
    }

    /* ==================== */

    var PARAMS_IN_SEPARATE_ARGS = ['term', 'from', 'to']; // filter parameters that are passed as separate arguments
    var PARAMS = PARAMS_IN_SEPARATE_ARGS.concat('participationStatus', 'status');
    var ALL_STATUSES = ['SUBSCRIBED'];

    function _pick(obj, keys) {
      return obj ? _.chain(obj)
          .pick(keys)
          .omitBy(_.isEmpty)
          .value() : {};
    }

    function _loadEvents() {
      if (vm.loading && vm.currentPage) {
        return;
      }

      // write params to URL
      $state.transitionTo('main.event', _pick(vm.query, PARAMS), {notify: false});

      // perform search
      $sessionStorage.eventQuery = vm.query;
      vm.loading = true;
      var term = vm.query.term;
      var from = vm.query.from ? (vm.query.from + 'T00:00:00') : undefined;
      var to = vm.query.to ? (vm.query.to + 'T23:59:59') : undefined;
      var sort = term ? ['_score,DESC', 'displayName.sort'] : 'displayName.sort';
      var pageable = new Pageable(0, eventsConfig.list.paging.pageSize, sort);
      var filters = _.omit(vm.query, PARAMS_IN_SEPARATE_ARGS);
      var searchFields = ['displayName', 'description'];
      EventModel.searchWithFilter(term, from, to, pageable, filters, searchFields).then(function (page) {
        vm.currentPage = page;
        vm.filtersResettable = _isFiltersResettable();
      }).finally(function () {
        vm.loading = false;
      });
    }

    function _setStatus(status) {
      vm.query.status = status;
    }

    function _resetStatusFilter() {
      vm.statusFilterModel.clearAll();
      _setStatus();
    }

    function _setParticipationStatus(participationStatus) {
      vm.query.participationStatus = participationStatus;
    }

    function _resetParticipationStatusFilter() {
      vm.participationStatusFilter.clearAll();
      _setParticipationStatus();
    }

    function _resetDateRangeFilter() {
      vm.dateRange = [];
      vm.query.from = vm.query.to = null;
    }

    function _isFiltersResettable() {
      return vm.statusFilterModel.isSelected() || vm.participationStatusFilter.isSelected() || vm.dateRange.length > 0;
    }

    function _init() {
      authService.onGlobalPermissions('CREATE_EVENT', function (canCreateEvent) {
        vm.canCreateEvent = canCreateEvent;
      });

      vm.filtersResettable = true;
      // extract search from URL / storage
      var hadStatus = $sessionStorage.eventQuery && angular.isArray($sessionStorage.eventQuery.status);
      $sessionStorage.eventQuery = angular.extend(
          _pick($sessionStorage.eventQuery, PARAMS),
          _pick($stateParams, PARAMS));
      vm.query = $sessionStorage.eventQuery;
      if (vm.query.participationStatus && !angular.isArray(vm.query.participationStatus)) {
        vm.query.participationStatus = [vm.query.participationStatus];
      }
      if (!hadStatus) {
        vm.query.status = ['SUBSCRIBED']; // default
      }
      vm.query.status = _.intersection(vm.query.status, ALL_STATUSES);

      // // rewrite to URL
      $state.transitionTo('main.event', _pick(vm.query, PARAMS), {notify: false});

      // init date range filter
      vm.dateRange = [];
      if (vm.query.from) {
        vm.dateRange.push(moment(vm.query.from, 'YYYY-MM-DD'));
        if (vm.query.to) {
          vm.dateRange.push(moment(vm.query.to, 'YYYY-MM-DD'));
        }
      }

      var participationStatus = EventModel.getAllParticipationStatuses().map(function (status) {
        return {key: status, active: _.includes(vm.query.participationStatus, status)};
      });
      vm.participationStatusFilter = selectionFilterService.builder().items(participationStatus).build();

      var status = ALL_STATUSES.map(function (status) {
        return {key: status, icon: 'zmdi-notifications', active: _.includes(vm.query.status, status)};
      });
      vm.statusFilterModel = selectionFilterService.builder().items(status).build();

      $timeout(function () {
        _loadEvents();
      });
    }
  }

})(angular);
