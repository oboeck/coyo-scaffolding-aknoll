(function (angular) {
  'use strict';

  angular
      .module('coyo.events')
      .controller('EventsSettingsController', EventsSettingsController);

  function EventsSettingsController($scope, $state, event, currentUser, admins, moment, EventModel, coyoNotification,
                                    modalService) {
    var vm = this;
    vm.currentUser = currentUser;
    vm.saveEvent = saveEvent;
    vm.deleteEvent = deleteEvent;
    vm.$onInit = _init;
    vm.addGeneralFormData = addGeneralFormData;

    function saveEvent() {
      if (!vm.event.limitedParticipantsFlag) {
        vm.event.limitedParticipants = null;
      }
      vm.event.adminIds = vm.admins.ids;
      var start = moment(vm.event.startDate);
      var end = moment(vm.event.endDate);
      if (vm.event.fullDay) {
        start.hour(12).minute(0).seconds(0);
        end.hour(12).minute(0).seconds(0);
      }
      vm.event.startDate = start.format('YYYY-MM-DDTHH:mm:ss');
      vm.event.endDate = end.format('YYYY-MM-DDTHH:mm:ss');
      return vm.event.updateEvent().then(function () {
        coyoNotification.success('MODULE.EVENTS.EDIT.SUCCESS');
        $state.go('main.event.show.timeline', {idOrSlug: vm.event.slug}, {reload: true});
      }).catch(function () {
        _updateParticipationLimit().then(function () {
          vm.eventsForm.$validate();
        });
      });
    }

    function deleteEvent() {
      modalService.confirmDelete({
        title: 'EVENT.DELETE',
        text: 'EVENT.DELETE.CONFIRM'
      }).result.then(function () {
        event.deleteEvent().then(function () {
          $state.go('main.event');
          coyoNotification.success('EVENT.DELETE.SUCCESS');
        });
      });
    }

    function addGeneralFormData(eventData) {
      vm.formIsValid = eventData.valid;
      vm.eventsForm.$valid = eventData.valid;
      vm.eventsForm.$invalid = !eventData.valid;
      $scope.$evalAsync(function () {
        vm.event = new EventModel(Object.assign({}, vm.event, eventData));
      });
    }

    function _updateParticipationLimit() {
      return vm.event.getNumberOfParticipants('ATTENDING').then(function (count) {
        vm.event.limitedParticipants.numberOfAttendingParticipants = count;
        return count;
      });
    }

    function _init() {
      vm.baseUrl = $state.href('main.event', {}) + '/';
      vm.oldSlug = event.slug;
      vm.event = new EventModel(event);
      vm.admins = {
        ids: admins.adminIds
      };
      vm.event.adminIds = vm.admins.ids;
      _updateParticipationLimit();

      // update form valid status if admin user chooser changed
      $scope.$watch(
          function () {
            return vm.admins.ids;
          },
          function (newValue, oldValue) {
            if (!angular.equals(oldValue, newValue)) {
              vm.eventsForm.$valid = vm.formIsValid;
              vm.eventsForm.$invalid = !vm.formIsValid;
            }
          },
          true
      );
    }
  }

})(angular);
