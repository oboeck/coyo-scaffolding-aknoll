(function (angular) {
  'use strict';

  angular
      .module('coyo.events')
      .controller('EventsShowController', EventsShowController);

  function EventsShowController($q, $scope, $rootScope, moment, event, senderService, currentUser, Pageable, EventModel,
                                userChooserModalService, selectionFilterService, authService) {
    var vm = this;

    vm.currentUser = currentUser;
    vm.event = event;
    vm.currentPage = 0;
    vm.hosts = [];
    vm.iCalExportUrl = null;

    vm.search = search;
    vm.changeAvatar = changeAvatar;
    vm.isOngoing = isOngoing;
    vm.hasStarted = hasStarted;
    vm.currentUser = currentUser;
    vm.changeCover = senderService.changeCover({title: 'MODULE.EVENTS.MODALS.CHANGE_BG_IMAGE.TITLE'});
    vm.setParticipationStatusFilter = setParticipationStatusFilter;
    vm.onParticipationStatusChanged = onParticipationStatusChanged;
    vm.inviteMembers = inviteMembers;
    vm.removeMember = removeMember;

    vm.params = angular.extend({
      searchTerm: undefined,
      status: []
    });
    vm.$onInit = _init;

    var canInviteGroupsPermission = false;

    function changeAvatar(sender) {
      senderService.changeAvatar({title: 'MODULE.EVENTS.MODALS.CHANGE_AVATAR.TITLE'})(sender).then(function () {
        $scope.$broadcast('eventAvatar:changed');
      });
    }

    function setParticipationStatusFilter(selected) {
      vm.params.status = selected;
      return _loadMemberships();
    }

    function onParticipationStatusChanged(newStatus, oldStatus) {
      var newIsAttending = newStatus === 'ATTENDING';
      var oldIsAttending = oldStatus === 'ATTENDING';
      if (newIsAttending !== oldIsAttending) {
        if (newIsAttending) {
          vm.event.attendingCount++;
        } else {
          vm.event.attendingCount--;
        }
      }

      _loadMemberships();
    }

    function search(term) {
      vm.params.searchTerm = term;
      return _loadMemberships();
    }

    function inviteMembers() {
      userChooserModalService.open({}, {usersOnly: !canInviteGroupsPermission}).then(function (selected) {
        event.inviteMembers(selected).then(function () {
          _loadMemberships();
        });
      });
    }

    function removeMember(userId, removedMemberParticipationStatus) {
      if (vm.loading) {
        $q.reject();
      }
      vm.loading = true;
      return event.removeMember(userId).then(function () {
        if (removedMemberParticipationStatus === 'ATTENDING') {
          vm.event.attendingCount--;
        }
      }).finally(function () {
        vm.loading = false;
        _init();
      });
    }

    function _loadMemberships() {
      if (vm.loading || !_shouldShowParticipants()) {
        return $q.reject();
      }
      vm.loading = true;

      var sort = vm.params.searchTerm ? ['_score,DESC', 'displayName'] : 'displayName';
      var pageable = new Pageable(0, 20, sort);
      var filter = _.omit(vm.params, 'searchTerm');
      var searchFields = ['displayName'];
      var aggregations = {status: ''};
      return EventModel.getMembershipsWithFilter(vm.params.searchTerm, pageable, filter, searchFields, aggregations,
          vm.event.slug).then(function (page) {
        vm.currentPage = page;
        // sort departments by:
        //   1. status count descending
        //   2. status name ascending
        var participationStatus;
        if (page.aggregations) {
          participationStatus = _.orderBy(page.aggregations.status,
              ['count', 'key'],
              ['desc', 'asc']);
        } else {
          participationStatus = [];
        }
        // set active state for every status
        participationStatus.forEach(function (status) {
          status.active = vm.params.status.indexOf(status.key) >= 0;
        });
        vm.participationStatusFilter = selectionFilterService.builder().items(participationStatus).build();
      }).finally(function () {
        vm.loading = false;
      });
    }

    function _loadHosts() {
      event.getHosts().then(function (hosts) {
        vm.hosts = hosts;
      });
    }

    function _updateIcalExportUrl() {
      vm.iCalExportUrl = vm.event.getIcalExportUrl();
    }

    function _init() {
      _loadMemberships();
      _loadHosts();
      _updateIcalExportUrl();
      vm.shouldShowParticipants = _shouldShowParticipants();
      vm.canManage = _canManage();
      authService.onGlobalPermissions('PERMIT_EVENT_GROUP_INVITES', function (canInviteGroups) {
        canInviteGroupsPermission = canInviteGroups;
      }, true);
    }

    function isOngoing() {
      var now = moment();
      return now.isSameOrAfter(vm.event.startDate) && now.isBefore(vm.event.endDate);
    }

    function hasStarted() {
      return moment().isSameOrAfter(vm.event.startDate);
    }

    function _shouldShowParticipants() {
      return vm.event.showParticipants || _canManage();
    }

    function _canManage() {
      return vm.event.canManage();
    }
  }

})(angular);
