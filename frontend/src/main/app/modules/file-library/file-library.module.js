(function (angular) {
  'use strict';

  angular.module('coyo.filelibrary', [
    'coyo.base',
    'commons.ui'
  ]).config(moduleConfig);

  function moduleConfig($stateProvider) {
    $stateProvider.state('main.fileLibrary', {
      url: '/files/{senderId}/{fileId}/*fileName',
      resolve: {
        file: function (FileModel, $stateParams) {
          return FileModel.getWithPermissions({senderId: $stateParams.senderId, id: $stateParams.fileId}, {},
              ['*']).catch(angular.noop);
        },
        previousState: function ($state) {
          return $state.current.name ? $state.target($state.current.name, angular.copy($state.params)) : undefined;
        },
        fromLogin: function ($state) {
          return $state.current.name && _.startsWith($state.current.name, 'front.');
        }
      },
      onEnter: function (fileLibraryModalService, fileDetailsModalService, file, $state, previousState, $q,
                         SenderModel, $stateParams, fromLogin) {
        var deferred = $q.defer();

        if (!file) {
          closeCurrentState('file-not-found', previousState, deferred, $state);
          return deferred.promise;
        }

        var promise = file.folder ?
          openFileLibrary(file, $stateParams.senderId, fileLibraryModalService, SenderModel) :
          openDetailView(file, $stateParams.senderId, fileDetailsModalService);

        promise.then(function (result) {
          closeCurrentState(result, previousState, deferred, $state, fromLogin);
        }, function (error) {
          closeCurrentState(error, previousState, deferred, $state, fromLogin);
        });

        // returns true if opened in a blank window or a promise if the link where opened in context of another state
        return previousState && !fromLogin ? deferred.promise : $q.resolve(true);
      }
    });

    function openFileLibrary(file, senderId, fileLibraryModalService, SenderModel) {
      return SenderModel.getWithPermissions({id: senderId}, {}, ['*']).then(function (sender) {
        return fileLibraryModalService
            .open(sender, {initialFolder: file, initialFolderAsRoot: true, showAuthors: false}, {});
      });
    }

    function openDetailView(file, senderId, fileDetailsModalService) {
      return fileDetailsModalService.open({senderId: senderId, id: file.id}).result;
    }

    function closeCurrentState(result, previousState, deferred, $state, fromLogin) {
      // just cancel the current state if modal started a transition (e.g. clicked on the file author) or there is a
      // previous state
      if ((previousState || result === 'state-transition-started') && !fromLogin) {
        deferred.resolve(false);
      } else {
        // if opened in a blank window open the default starting state when file modal is closed
        deferred.resolve(true);
        $state.go('main.default');
      }
    }
  }
})(angular);
