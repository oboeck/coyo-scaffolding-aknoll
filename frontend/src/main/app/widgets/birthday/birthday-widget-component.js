(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.birthday')
      /**
       * @ngdoc directive
       * @name coyo.widgets.birthday.coyoBirthdayWidget
       * @restrict 'E'
       *
       * @description
       * A widget which contains upcoming birthdays
       *
       * @param {object} widget - birthday widget
       * @param {object} editMode - true when widget editing is activated
       * @param {object} settingsMode - true when widget is shown in settings dialog
       */
      .component('coyoBirthdayWidget', {
        templateUrl: 'app/widgets/birthday/birthday-widget.html',
        bindings: {
          widget: '=',
          editMode: '<',
          settingsMode: '<',
          config: '<'
        },
        controller: 'BirthdayWidgetController'
      })
      .controller('BirthdayWidgetController', BirthdayWidgetController);

  function BirthdayWidgetController(BirthdayWidgetModel, widgetStatusService, $scope, Pageable, birthdayService) {
    var vm = this;
    vm.users = [];
    vm.birthdayToday = false;
    vm.loadBirthdays = loadBirthdays;
    vm.isSameDayAsPreviousUser = isSameDayAsPreviousUser;
    vm.$onInit = init;

    vm.params = angular.extend({
      subscribedTo: [true]
    });

    widgetStatusService.refreshOnSettingsChange($scope);

    function loadBirthdays() {
      var filter = null;
      if (vm.widget.settings._fetchBirthdays === 'FOLLOWERS') {
        filter = _.omit(vm.params, 'searchTerm');
      }
      var pageable = new Pageable((vm.currentPage ? vm.currentPage.number + 1 : 0),
          vm.widget.settings._birthdayNumber);
      var id = vm.widget.id;
      if (!vm.widget.id || vm.editMode) {
        vm.users = [];
        vm.loadData = undefined;
        vm.currentPage = 0;
        pageable = new Pageable((vm.currentPage ? vm.currentPage.number + 1 : 0),
            vm.widget.settings._birthdayNumber);
        vm.loadData = BirthdayWidgetModel.getBirthdaysPreview(vm.widget.settings._daysBeforeBirthday, pageable, filter)
            .then(function (page) {
              return handelBirthdayResult(page);
            });
      } else {
        vm.loadData = BirthdayWidgetModel.getBirthdays(vm.widget.settings._daysBeforeBirthday, pageable, filter, id)
            .then(function (page) {
              return handelBirthdayResult(page);
            });
      }
    }

    function init() {
      var unregisterWatch = $scope.$watch(function () {
        return vm.widget.settings;
      }, function (before, after) {
        if (after._daysBeforeBirthday !== before._daysBeforeBirthday
            || before._displayAge !== after._displayAge
            || before._fetchBirthdays !== after._fetchBirthdays
            || before._birthdayNumber !== after._birthdayNumber) {
          loadBirthdays();
        }
      });
      $scope.$on('$destroy', unregisterWatch);
      loadBirthdays();
    }

    function handelBirthdayResult(page) {
      vm.currentPage = page;
      vm.users = vm.users.concat(page.content);
      vm.users.forEach(function (user) {
        user.display = user.birthday !== 'null';
        var birthday;
        var today = new Date();
        if (birthdayService.hasYear(user.birthday)) {
          birthday = new Date(user.birthday);
          user.years =
              birthday.getMonth() < today.getMonth() ? today.getYear() - birthday.getYear() + 1 : today.getYear()
                  - birthday.getYear();
        } else {
          birthday = new Date('1900-' + user.birthday);
        }
        user.birthdayToday = birthday.getDate() === today.getDate() && birthday.getMonth() === today.getMonth();

      });
      return vm.users;
    }

    function isSameDayAsPreviousUser(index) {
      if (index > 0 && vm.users[index - 1] && vm.users[index]) {
        var previous = vm.users[index - 1].birthday;
        var current = vm.users[index].birthday;
        return previous.length > 5 && current.length > 5
          ? _isSameDayByDate(previous, current) : _isSameDayByString(previous, current);
      }
      return false;
    }

    function _isSameDayByDate(previousBirthday, currentBirthday) {
      return new Date(previousBirthday).getMonth() === new Date(currentBirthday).getMonth()
          && new Date(previousBirthday).getDate() === new Date(currentBirthday).getDate();
    }

    function _isSameDayByString(previousBirthday, currentBirthday) {
      return previousBirthday.substring(previousBirthday.length - 5, previousBirthday.length) ===
          currentBirthday.substring(currentBirthday.length - 5, currentBirthday.length);
    }
  }

})(angular);
