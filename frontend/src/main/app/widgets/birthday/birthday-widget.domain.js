(function (angular) {
  'use strict';

  angular.module('coyo.widgets.birthday')
      .factory('BirthdayWidgetModel', BirthdayWidgetModel);

  /**
   * @ngdoc service
   * @name coyo.widgets.birthday.BirthdayWidgetModel
   *
   * @description
   * Domain model representation for birthdays shown in the birthday widget
   *
   * @requires restResourceFactory
   */
  function BirthdayWidgetModel(restResourceFactory, Page, $httpParamSerializer) {
    var Widget = restResourceFactory({
      url: '/web/widgets/birthday',
      httpConfig: {
        autoHandleErrors: false
      }
    });

    angular.extend(Widget, {

      /**
       * @ngdoc function
       * @name coyo.widgets.birthday.BirthdayWidgetModel#getBirthdays
       * @methodOf coyo.widgets.birthday.BirthdayWidgetModel
       *
       * @description
       * Returns a cached list of upcoming birthdays
       *
       * @param {int} daysFuture The number of days to fetch birthdays in advance
       * @param {object} pageable pageable pagenation information
       * @param {object} filters search filters
       * @param {string} id the widget id
       *
       * @returns {promise} An $http promise resolving the birthdays.
       */
      getBirthdays: function (daysFuture, pageable, filters, id) {
        var url = Widget.$url() + '/birthdays';
        var params = angular.extend({
          filters: filters ? $httpParamSerializer(filters) : undefined,
          daysFuture: daysFuture,
          id: id
        }, pageable.getParams());
        return Widget.$get(url, params).then(function (response) {
          return new Page(response, params, {
            url: url,
            resultMapper: function (item) {
              return new Widget(item);
            }
          });
        });
      },

      /**
       * @ngdoc function
       * @name coyo.widgets.birthday.BirthdayWidgetModel#getBirthdaysPreview
       * @methodOf coyo.widgets.birthday.BirthdayWidgetModel
       *
       * @description
       * Returns a preview list of upcoming birthdays
       *
       * @param {int} daysFuture The number of days to fetch birthdays in advance
       * @param {object} pageable pageable pagenation information
       * @param {object} filters search filters
       *
       * @returns {promise} An $http promise resolving the birthdays.
       */
      getBirthdaysPreview: function (daysFuture, pageable, filters) {
        var url = Widget.$url() + '/birthdays-preview';
        var params = angular.extend({
          filters: filters ? $httpParamSerializer(filters) : undefined,
          daysFuture: daysFuture
        }, pageable.getParams());
        return Widget.$get(url, params).then(function (response) {
          return new Page(response, params, {
            url: url,
            resultMapper: function (item) {
              return new Widget(item);
            }
          });
        });
      }
    });

    return Widget;
  }

})(angular);
