(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.blog')
      .directive('coyoBlogWidget', blogWidget)
      .controller('BlogWidgetController', BlogWidgetController);

  /**
   * @ngdoc directive
   * @name coyo.widgets.blog.coyoBlogWidget:coyoBlogWidget
   * @element ANY
   * @restrict E
   * @scope
   *
   * @description
   * Renders the widget to show the latest blog articles
   *
   * @param {object} widget
   * The widget configuration
   */
  function blogWidget() {
    return {
      restrict: 'E',
      templateUrl: 'app/widgets/blog/blog-widget.html',
      scope: {},
      bindToController: {
        widget: '=',
        config: '<'
      },
      controller: 'BlogWidgetController',
      controllerAs: '$ctrl'
    };
  }

  function BlogWidgetController($scope, BlogWidgetModel, targetService, widgetStatusService) {
    var vm = this;

    vm.loadArticles = loadArticles;
    vm.links = {
      articles: {},
      senders: {},
      apps: {}
    };

    widgetStatusService.refreshOnSettingsChange($scope);

    function loadArticles() {
      var sourceSelection = vm.widget.settings._sourceSelection;
      var size = vm.widget.settings._articleCount;
      var showTeaserImage = vm.widget.settings._showTeaserImage;
      var appIds = _.map(vm.widget.settings._app, 'id');

      delete vm.articles;
      return BlogWidgetModel.getLatest(sourceSelection, size, showTeaserImage, appIds).then(function (articles) {
        vm.articles = articles;
        articles.forEach(function (article) {
          vm.links.articles[article.articleTarget.params.id] = targetService.getLink(article.articleTarget);
          vm.links.senders[article.senderTarget.params.id] = targetService.getLink(article.senderTarget);
          vm.links.apps[article.appTarget.params.id] = targetService.getLink(article.appTarget);
        });
        return articles;
      }).finally(function () {
        vm.lastUpdate = new Date().getTime();
      });
    }
  }

})(angular);
