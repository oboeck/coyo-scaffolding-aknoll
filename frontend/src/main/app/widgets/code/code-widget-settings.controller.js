(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.code')
      .controller('CodeWidgetSettingsController', CodeWidgetSettingsController);

  function CodeWidgetSettingsController($scope) {
    var vm = this;

    vm.$onInit = onInit;

    var nl = '\n';
    var t = '\t';
    var nlt = nl + t;

    function onInit() {
      vm.model = $scope.model;
      vm.placeholderHtml = _getPlaceholderHtml();
      vm.placeholderJs = _getPlaceholderJs();
      vm.placeholderCss = _getPlaceholderCss();
    }

    function _getPlaceholderHtml() {
      var wrapperStart = '<div class="code-widget-wrapper">';
      var wrapperEnd = '</div>';
      var helloWorldSpan = '<span class="code-widget-hello-world">Hello World!</span>';
      var greetingSpan = '<span id="code-widget-greeting"></span>';

      return wrapperStart + nlt + helloWorldSpan + nlt + greetingSpan + nl + wrapperEnd;
    }

    function _getPlaceholderJs() {
      var wrapperStart = '$(function () {';
      var wrapperEnd = '});';
      var element = 'var element = angular.element(document.querySelector(\'#code-widget-greeting\'));';
      var text = 'element.text(\'Have a nice day!\');';

      return wrapperStart + nlt + element + nlt + text + nl + wrapperEnd;
    }

    function _getPlaceholderCss() {
      var wrapperStart = '#code-widget-greeting {';
      var wrapperEnd = '}';
      var content = 'background-color: #f5b6b6;';

      return wrapperStart + nlt + content + nl + wrapperEnd;
    }
  }

})(angular);
