(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.newcolleagues')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "WIDGETS.INTERESTINGCOLLEAGUES.DESCRIPTION": "Zeigt eine Liste von Kollegen, die du vielleicht kennen könntest.",
          "WIDGETS.INTERESTINGCOLLEAGUES.NAME": "Kennst du?"
        });
        /* eslint-enable quotes */
      });
})(angular);
