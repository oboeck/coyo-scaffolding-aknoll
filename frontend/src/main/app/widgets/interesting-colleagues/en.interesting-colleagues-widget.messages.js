(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.newcolleagues')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "WIDGETS.INTERESTINGCOLLEAGUES.DESCRIPTION": "Displays a list of colleagues you might know.",
          "WIDGETS.INTERESTINGCOLLEAGUES.NAME": "Do you know?"
        });
        /* eslint-enable quotes */
      });
})(angular);
