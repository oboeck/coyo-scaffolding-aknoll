(function (angular) {
  'use strict';

  angular.module('coyo.widgets.interestingcolleagues')
      .factory('interestingColleaguesService', interestingColleaguesService);

  /**
   * @ngdoc service
   * @name coyo.widgets.interestingcolleagues.interestingColleaguesService
   *
   * @description
   * Service for receiving users that the current user might know because of friend of friends relation.
   *
   */
  function interestingColleaguesService($http) {

    return {
      getColleagues: getColleagues
    };

    /**
     * @ngdoc method
     * @name coyo.widgets.interestingcolleagues.interestingColleaguesService#getColleagues
     * @methodOf coyo.widgets.interestingcolleagues.interestingColleaguesService
     *
     * @description
     * Sends an request to the backend to return up to 3 colleagues that are subscribed by subscriptions of
     * the current user but not directly. If there are more than 3 the colleagues backend will choose randomly.
     *
     * @returns {promise} the http promise
     */
    function getColleagues() {
      return $http.get('/web/widgets/interestingcolleagues/');
    }
  }

})(angular);
