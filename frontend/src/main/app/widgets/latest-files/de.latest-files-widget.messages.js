(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.latestfiles')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "WIDGETS.LATESTFILES.DESCRIPTION": "Zeigt die neuesten Dateiänderungen innerhalb einer Dokumente-App an.",
          "WIDGETS.LATESTFILES.NAME": "Neueste Dateien",
          "WIDGETS.LATESTFILES.NO_FILES": "Keine Dateien vorhanden",
          "WIDGETS.LATESTFILES.UPDATED": "Aktualisiert",
          "WIDGETS.LATESTFILES.SETTINGS.DOCUMENTAPP.HELP": "Bitte wähle die Dokumente-App aus, für die Dateiänderungen angezeigt werden sollen.",
          "WIDGETS.LATESTFILES.SETTINGS.DOCUMENTAPP.LABEL": "Dokumente-App",
          "WIDGETS.LATESTFILES.SETTINGS.FILE_COUNT.LABEL": "Anzahl von Dateien",
          "WIDGETS.LATESTFILES.SETTINGS.FILE_COUNT.HELP": "Die maximale Anzahl an Dateiänderungen die angezeigt werden. Der Wert muss zwischen 1 und 20 liegen.",
          "WIDGETS.LATESTFILES.SETTINGS.NO_DOCUMENT_APPS_FOUND": "Keine Dokumente-App gefunden"
        });
        /* eslint-enable quotes */
      });
})(angular);
