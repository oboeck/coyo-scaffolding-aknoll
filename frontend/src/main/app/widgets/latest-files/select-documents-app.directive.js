(function (angular) {
  'use strict';

  angular.module('coyo.widgets.latestfiles')
      .directive('coyoSelectDocumentsApp', selectDocumentsApp);

  /**
   * @ngdoc directive
   * @name coyo.widgets.latestfiles.coyoSelectDocumentsApp:coyoSelectDocumentsApp
   * @element ANY
   * @restrict E
   * @scope
   *
   * @description
   * Renders a ui-select to select a specific documents app.
   */
  function selectDocumentsApp(selectFactoryModel, GlobalAppModel, Pageable) {
    return selectFactoryModel({
      refresh: refresh,
      multiple: false,
      pageSize: 50,
      sublines: ['senderName'],
      emptyText: 'WIDGETS.LATESTFILES.SETTINGS.NO_DOCUMENT_APPS_FOUND'
    });

    function refresh(pageable, search) {
      return GlobalAppModel.pagedQuery(new Pageable(pageable.page, pageable.size), {name: search, key: 'file-library'})
          .then(function (response) {
            return angular.extend(_.map(response.content, function (app) {
              return {
                id: app.id,
                displayName: app.name,
                senderName: app.senderName,
                senderId: app.senderId
              };
            }), {
              meta: {
                last: response.last,
                number: response.number
              }
            });
          });
    }
  }

})(angular);
