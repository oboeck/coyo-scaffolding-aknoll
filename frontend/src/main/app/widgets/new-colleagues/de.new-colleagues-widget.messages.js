(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.newcolleagues')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "WIDGETS.NEWCOLLEAGUES.DESCRIPTION": "Zeigt eine Liste der neuesten Kollegen.",
          "WIDGETS.NEWCOLLEAGUES.NAME": "Neue Kollegen"
        });
        /* eslint-enable quotes */
      });
})(angular);
