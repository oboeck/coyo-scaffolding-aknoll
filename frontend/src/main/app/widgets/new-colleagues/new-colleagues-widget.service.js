(function (angular) {
  'use strict';

  angular.module('coyo.widgets.newcolleagues')
      .factory('newColleaguesWidgetService', newColleaguesWidgetService);

  /**
   * @ngdoc service
   * @name coyo.widgets.newcolleagues.newColleaguesWidgetService
   *
   * @description
   * Service for retrieving new colleagues
   *
   * @requires Page
   */
  function newColleaguesWidgetService($http, Page) {
    return {
      getNewColleagues: getNewColleagues
    };

    /**
     * @ngdoc method
     * @name coyo.widgets.newcolleagues.newColleaguesWidgetService#getNewColleagues
     * @methodOf coyo.widgets.newcolleagues.newColleaguesWidgetService
     *
     * @description
     * Sends an request to the backend to return a paged list of colleagues that are new in the network. The list is
     * sorted by creation date and only contains user that are not followed by the current user.
     *
     * @returns {promise} the http promise
     */
    function getNewColleagues() {
      return $http.get('/web/widgets/newcolleagues').then(function (response) {
        return new Page(response.data, {}, {
          url: '/web/widgets/newcolleagues',
          resultMapper: function (item) {
            return item;
          }
        });
      });
    }
  }

})(angular);
