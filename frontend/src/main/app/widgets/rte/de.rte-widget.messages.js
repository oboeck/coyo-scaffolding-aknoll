(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.rte')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "WIDGET.RTE.NAME": "Rich-Text-Editor",
          "WIDGET.RTE.DESCRIPTION": "Zeigt Rich-Text mit einem optionalen Titel."
        });
        /* eslint-enable quotes */
      });
})(angular);
