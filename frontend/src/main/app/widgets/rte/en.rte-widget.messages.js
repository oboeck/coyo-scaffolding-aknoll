(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.rte')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "WIDGET.RTE.NAME": "Rich text editor",
          "WIDGET.RTE.DESCRIPTION": "Displays rich text with an optional title."
        });
        /* eslint-enable quotes */
      });
})(angular);
