(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.rte')
      .filter('youtubenocookie', youtubeNocookie);

  /**
   * @ngdoc filter
   * @name widgets.rte.youtubenocookie:youtubeNocookie
   * @function
   *
   * @description
   * Replaces all occurrences of a youtube embed link with the nocookie variant.
   * E.g. https://www.youtube.com/embed/Axqx19M4S2M will become https://www.youtube-nocookie.com/embed/Axqx19M4S2M
   *
   *
   * @param {string} string The string to be replaced
   */
  function youtubeNocookie(youtubeEmbedUrlRegEx, youtubeEmbedUrlReplacement) {
    return function (string) {
      if (!angular.isString(string)) {
        return '';
      }
      return string.replace(youtubeEmbedUrlRegEx, youtubeEmbedUrlReplacement);
    };
  }

})(angular);
