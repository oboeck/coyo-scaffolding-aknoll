(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.upcomingEvents')
      .controller('UpcomingEventsWidgetSettingsController', UpcomingEventsWidgetSettingsController);

  function UpcomingEventsWidgetSettingsController($scope) {
    var vm = this;
    vm.$onInit = onInit;

    function onInit() {
      vm.model = $scope.model;
      vm.senderChooserParams = {
        'allowedTypeNames': ['page', 'workspace'],
        'findOnlyManagedSenders': false
      };

      if (!vm.model.settings._numberOfDisplayedEvents && !vm.model.settings._displayOngoingEvents
          && !vm.model.settings._fetchUpcomingEvents) {
        $scope.model.settings._numberOfDisplayedEvents = 5;
        $scope.model.settings._displayOngoingEvents = false;
        $scope.model.settings._fetchUpcomingEvents = 'ALL';
      }
    }
  }
})(angular);
