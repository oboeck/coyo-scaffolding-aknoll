(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.upcomingEvents')
      /**
       * @ngdoc directive
       * @name coyo.widgets.birthday.coyoUpcomingEventsWidget
       * @restrict 'E'
       *
       * @description
       * A widget which contains upcoming events
       *
       * @param {object} widget - upcoming events widget
       * @param {object} editMode - true when widget editing is activated
       * @param {object} settingsMode - true when widget is shown in settings dialog
       */
      .component('coyoUpcomingEventsWidget', {
        templateUrl: 'app/widgets/upcoming-events/upcoming-events-widget.html',
        bindings: {
          widget: '=',
          editMode: '<',
          settingsMode: '<'
        },
        controller: 'upcomingEventsWidgetController'
      })
      .controller('upcomingEventsWidgetController', upcomingEventsWidgetController);

  function upcomingEventsWidgetController(widgetStatusService, $scope, UpcomingEventsWidgetModel, moment) {
    var vm = this;
    vm.events = [];
    vm.loadEvents = loadEvents;
    vm.$onInit = init;
    vm.isOngoing = isOngoing;

    vm.params = angular.extend({
      subscribedTo: [true]
    });

    widgetStatusService.refreshOnSettingsChange($scope);

    function loadEvents() {
      var filters = {};

      if (vm.widget.settings._fetchUpcomingEvents === 'SUBSCRIBED') {
        filters = _.omit(vm.params, 'searchTerm');
      }

      if (vm.widget.settings._fetchUpcomingEvents === 'SELECTED') {
        filters = {sender: [vm.widget.settings._selectedSender.id]};
      }
      if (!vm.widget.id || vm.editMode) {
        vm.events = [];
        return UpcomingEventsWidgetModel.getUpcomingEventsWithFiltersPreview(filters,
            vm.widget.settings._numberOfDisplayedEvents)
            .then(function (response) {
              return handleResponse(response);
            });
      } else {
        return UpcomingEventsWidgetModel.getUpcomingEventsWithFilters(filters,
            vm.widget.settings._numberOfDisplayedEvents,
            vm.widget.id)
            .then(function (response) {
              return handleResponse(response);
            });
      }
    }

    function handleResponse(response) {
      vm.events = response;
      if (!vm.widget.settings._displayOngoingEvents) {
        vm.events.forEach(function (event, index) {
          if (isOngoing(event.startDate, event.endDate)) {
            vm.events.splice(index, 1);
          }
        });
      }
      return vm.events;
    }

    function isOngoing(start, end) {
      var now = moment();
      return now.isSameOrAfter(start) && now.isSameOrBefore(end);
    }

    function init() {
      vm.query = {};
      var unregisterWatch = $scope.$watch(function () {
        return vm.widget.settings;
      }, function (before, after) {
        if (after._numberOfDisplayedEvents !== before._numberOfDisplayedEvents
            || before._displayOngoingEvents !== after._displayOngoingEvents
            || before._fetchUpcomingEvents !== after._fetchUpcomingEvents
            || before.configuredSender !== after.configuredSender) {
          loadEvents();
        }
      });
      $scope.$on('$destroy', unregisterWatch);
      loadEvents();
    }
  }

})(angular);
