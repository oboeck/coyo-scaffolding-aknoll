(function (angular) {
  'use strict';

  angular.module('coyo.widgets.upcomingEvents')
      .factory('UpcomingEventsWidgetModel', UpcomingEventsWidgetModel);

  /**
   * @ngdoc service
   * @name coyo.widgets.upcoming-events.UpcomingEventsWidgetModel
   *
   * @description
   * Domain model representation for events shown in the upcoming events widget
   *
   * @requires restResourceFactory
   */
  function UpcomingEventsWidgetModel(restResourceFactory, $httpParamSerializer) {
    var Widget = restResourceFactory({
      url: '/web/widgets/upcoming-events',
      httpConfig: {
        autoHandleErrors: false
      }
    });

    angular.extend(Widget, {

      /**
       * @ngdoc function
       * @name coyo.domain.EventModel#getUpcomingEventsWithFilters
       * @methodOf coyo.domain.EventModel
       *
       * @description
       * Event elastic search with filter.
       *
       * @param {number} limit the number of events that should be displayed
       * @param {object} [filters] search filters
       * @param {id} id of the widget
       *
       * @returns {promise} An $http promise
       */
      getUpcomingEventsWithFilters: function (filters, limit, id) {
        var url = Widget.$url();
        var params = angular.extend({
          id: id,
          limit: limit,
          filters: filters ? $httpParamSerializer(filters) : undefined
        });
        return Widget.$get(url, params);
      },

      /**
       * @ngdoc function
       * @name coyo.domain.EventModel#getUpcomingEventsWithFiltersPreview
       * @methodOf coyo.domain.EventModel
       *
       * @description
       * Preview for event elastic search with filter.
       *
       * @param {number} limit the number of events that should be displayed
       * @param {object} [filters] search filters
       *
       * @returns {promise} An $http promise
       */
      getUpcomingEventsWithFiltersPreview: function (filters, limit) {
        var url = Widget.$url('preview');
        var params = angular.extend({
          limit: limit,
          filters: filters ? $httpParamSerializer(filters) : undefined
        });
        return Widget.$get(url, params);
      }
    });

    return Widget;
  }

})(angular);
