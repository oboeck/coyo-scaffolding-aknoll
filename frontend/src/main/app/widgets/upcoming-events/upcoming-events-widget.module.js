(function (angular) {
  'use strict';

  angular
  /**
   * @ngdoc overview
   * @name coyo.widgets.upcomingEvents
   *
   * @description
   * # Upcoming events Widget #
   * Shows upcoming events within a widget.
   */
      .module('coyo.widgets.upcomingEvents', [
        'coyo.widgets.api',
        'commons.i18n'
      ])
      .config(registerUpcomingEventsWidget);

  function registerUpcomingEventsWidget(widgetRegistryProvider) {
    widgetRegistryProvider.register({
      key: 'upcoming-events',
      name: 'WIDGETS.UPCOMING.EVENTS.NAME',
      description: 'WIDGETS.UPCOMING.EVENTS.DESCRIPTION',
      titles: ['WIDGETS.UPCOMING.EVENTS.NAME'],
      icon: 'zmdi-calendar',
      categories: 'dynamic',
      directive: 'coyo-upcoming-events-widget',
      settings: {
        controller: 'UpcomingEventsWidgetSettingsController',
        controllerAs: '$ctrl',
        templateUrl: 'app/widgets/upcoming-events/upcoming-events-widget-settings.html'
      },
      whitelistExternal: true
    });
  }

})(angular);
