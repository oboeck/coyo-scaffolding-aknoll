(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.widgets
   *
   * @description
   * This module contains all widgets and the API used by widgets.
   */
  angular
      .module('coyo.widgets', [
        'coyo.widgets.birthday',
        'coyo.widgets.blog',
        'coyo.widgets.completeprofile',
        'coyo.widgets.downloads',
        'coyo.widgets.media',
        'coyo.widgets.poll',
        'coyo.widgets.rte',
        'coyo.widgets.singlefile',
        'coyo.widgets.suggestpages',
        'coyo.widgets.teaser',
        'coyo.widgets.userprofile',
        'coyo.widgets.rss',
        'coyo.widgets.video',
        'coyo.widgets.upcomingEvents',
        'coyo.widgets.wiki',
        'coyo.widgets.wikiarticle',
        'coyo.widgets.code',
        'coyo.widgets.newcolleagues',
        'coyo.widgets.interestingcolleagues',
        'coyo.widgets.latestfiles'
      ]);

})(angular);
