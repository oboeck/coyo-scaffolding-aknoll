(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    var appResourceFactory;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_appResourceFactory_) {
      appResourceFactory = _appResourceFactory_;
    }));

    it('should fromApp include app details and additional context', function () {
      // given
      var Resource = appResourceFactory({});
      var app = {
        id: 'AppId',
        senderId: 'SenderId',
        otherAppProperty: 'foo'
      };
      var context = {
        contextProperty: 'bar'
      };

      // when
      var resource = Resource.fromApp(app, context);

      // then
      expect(resource).toBeDefined();
      expect(resource.appId).toBe('AppId');
      expect(resource.senderId).toBe('SenderId');
      expect(resource.contextProperty).toBe('bar');
      expect(resource.otherAppProperty).not.toBeDefined();
    });

    it('should prefix appKey to url', function () {
      // given
      var config = {
        appKey: 'my-key',
        url: '/my-url'
      };
      var app = {
        id: 'AppId',
        senderId: 'SenderId'
      };

      // when
      var resource = appResourceFactory(config).fromApp(app);

      // then
      expect(resource.$url()).toBe('/web/senders/SenderId/apps/AppId/my-key/my-url');
    });
  });

})();
