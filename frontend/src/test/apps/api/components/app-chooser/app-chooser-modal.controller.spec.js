(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    beforeEach(
        module(moduleName, function () {
        })
    );

    describe('controller: AppChooserModalController', function () {

      var $controller, sender, apps, $scope, $q, $uibModalInstance, appRegistry, SenderModel, AppModel;

      beforeEach(function () {

        inject(function (_$controller_, _$q_, $rootScope, _SenderModel_, _AppModel_) {
          $controller = _$controller_;
          $q = _$q_;
          $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close']);
          $scope = $rootScope.$new();
          appRegistry = jasmine.createSpyObj('appRegistry', ['get']);
          SenderModel = _SenderModel_;
          AppModel = _AppModel_;
        });

        sender = new SenderModel({
          id: 'SENDER-ID',
          typeName: 'testSender'
        });

        var app1 = new AppModel({
          key: 'key1',
          allowedInstances: 1,
          allowedInstancesErrorMessage: 'ERROR.MESSAGE'
        });
        var app2 = new AppModel({
          key: 'key2',
          allowedInstances: 2,
          allowedInstancesErrorMessage: 'ERROR.MESSAGE'
        });
        var app3 = new AppModel({
          key: 'key3',
          allowedInstances: -1,
          allowedInstancesErrorMessage: 'ERROR.MESSAGE'
        });
        var app4 = new AppModel({
          key: 'key4',
          allowedInstances: 0,
          allowedInstancesErrorMessage: 'ERROR.MESSAGE'
        });

        spyOn(sender, 'getDefaultLanguage').and.callThrough();
        spyOn(sender, 'initTranslations').and.callThrough();
        spyOn(app1, 'prepareTranslationsForSave').and.callThrough();
        spyOn(app2, 'prepareTranslationsForSave').and.callThrough();

        apps = [app1, app2, app3, app4];

      });

      function buildController() {
        return $controller('AppChooserModalController', {
          sender: sender,
          apps: apps,
          $uibModalInstance: $uibModalInstance,
          appRegistry: appRegistry
        });
      }

      it('should init controller with correct values', function () {
        // given
        appRegistry.get.and.returnValue(apps[0]);

        // when
        var ctrl = buildController();
        ctrl.$onInit();

        // then
        expect(ctrl.senderType).toBe('testSender');
        expect(ctrl.isSenderTranslated).toBe(false);
        expect(sender.initTranslations).toHaveBeenCalledWith(ctrl);
      });

      it('should successfully check that can create app (allowedInstances = -1)', function () {
        // given
        appRegistry.get.and.returnValue(apps[2]);

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var result = ctrl.canCreateApp({key: 'key3'});

        // then
        expect(result).toBe(true);
      });

      it('should successfully check that can create app (allowedInstances = 0)', function () {
        // given
        appRegistry.get.and.returnValue(apps[3]);

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var result = ctrl.canCreateApp({key: 'key4'});

        // then
        expect(result).toBe(true);
      });

      it('should successfully check that can create app (allowedInstances = 2)', function () {
        // given
        appRegistry.get.and.returnValue(apps[1]);

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var result = ctrl.canCreateApp({key: 'key2'});

        // then
        expect(result).toBe(true);
      });

      it('should successfully check that cannot create app (allowedInstances = 1)', function () {
        // given
        appRegistry.get.and.returnValue(apps[0]);

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var result = ctrl.canCreateApp({key: 'key1'});

        // then
        expect(result).toBe(false);
      });

      it('should be possible to get app type information', function () {
        // given
        appRegistry.get.and.returnValue(apps[0]);

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var result = ctrl.getAppTypeInformation({key: 'key1'});

        // then
        expect(result).toEqual({key: 'key1', instances: 1, allowedInstances: 1, allowedInstancesErrorMessage: 'ERROR.MESSAGE'});
      });

      it('should clear the selected app on goBack', function () {
        // given
        var ctrl = buildController();
        ctrl.selectedApp = {};

        // when
        ctrl.goBack();

        // then
        expect(ctrl.selectedApp).toBeNull();
      });

      it('should save the selected app', function () {
        // given
        appRegistry.get.and.returnValue(apps[1]);

        var ctrl = buildController();
        ctrl.$onInit();
        var app = {id: 'TEST-ID'};

        ctrl.selectedApp = jasmine.createSpyObj('App', ['createWithPermissions', 'prepareTranslationsForSave']);
        ctrl.selectedApp.createWithPermissions.and.returnValue($q.resolve(app));

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(ctrl.selectedApp.senderId).toBe('SENDER-ID');
        expect(ctrl.selectedApp.prepareTranslationsForSave).toHaveBeenCalledWith(ctrl);
        expect(ctrl.selectedApp.createWithPermissions).toHaveBeenCalled();
        expect($uibModalInstance.close).toHaveBeenCalledWith(app);
      });

      it('should not save the selected app if too many instances', function () {
        // given
        appRegistry.get.and.returnValue(apps[0]);

        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.selectedApp = apps[0];

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect($uibModalInstance.close).not.toHaveBeenCalled();
      });

      it('should call the save callback before saving', function () {
        // given
        appRegistry.get.and.returnValue(apps[0]);
        var ctrl = buildController();
        var app = {id: 'TEST-ID'};

        ctrl.selectedApp = jasmine.createSpyObj('App', ['createWithPermissions', 'prepareTranslationsForSave']);
        ctrl.selectedApp.createWithPermissions.and.returnValue($q.resolve(app));

        ctrl.saveCallbacks = jasmine.createSpyObj('ctrl.saveCallbacks', ['onBeforeSave']);
        ctrl.saveCallbacks.onBeforeSave.and.returnValue($q.resolve());

        // when
        ctrl.$onInit();
        ctrl.save();
        $scope.$apply();

        // then
        expect(ctrl.saveCallbacks.onBeforeSave).toHaveBeenCalledTimes(1);
        expect(ctrl.selectedApp.createWithPermissions).toHaveBeenCalledTimes(1);
      });

      it('should update form validity state for specified language', function () {
        // given
        var ctrl = buildController();
        ctrl.languages = {
          DE: {valid: false}
        };

        // when
        ctrl.updateValidity('DE', true);

        // then
        expect(ctrl.languages.DE.valid).toBe(true);
      });

      it('should provide the multi language functionality required state', function () {
        // given
        spyOn(sender, 'isTranslationRequired').and.returnValue('anyBooleanValue');

        // when
        var ctrl = buildController();
        ctrl.languages = {
          EN: {name: 'the-name'}
        };
        ctrl.currentLanguage = 'EN';
        var actualResult = ctrl.isTranslationRequired('DE');

        // then
        expect(actualResult).toEqual('anyBooleanValue');
        expect(sender.isTranslationRequired).toHaveBeenCalled();
        var args = sender.isTranslationRequired.calls.mostRecent().args;
        expect(args[0]).toBe(ctrl.languages);
        expect(args[1]).toEqual('EN');
        expect(args[2]).toEqual('DE');
      });
    });
  });

})();
