(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    var sender, modalService;

    beforeEach(function () {

      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.returnValue({
        result: {
          then: function (callback) {
            return callback(sender);
          }
        }
      });

      module(moduleName, function ($provide) {
        $provide.value('modalService', modalService);
      });

    });

    describe('controller: AppTranslationsModalController', function () {

      var $scope, sender, $q, $controller, $uibModalInstance, senderModel, senderNavigationUpdateService, appRegistry;

      var apps = [{
        'id': 'app-id-1',
        'name': 'app1',
        'key': 'blog'
      }, {
        'id': 'app-id-2',
        'name': 'app2',
        'key': 'blog'
      }, {
        'id': 'app-id-3',
        'name': 'app3',
        'key': 'blog'
      }, {
        'id': 'app-id-4',
        'name': 'app4',
        'key': 'blog'
      }, {
        'id': 'app-id-5',
        'name': 'app5',
        'key': 'blog'
      }];

      beforeEach(function () {

        inject(function ($rootScope, _$q_, _$controller_, _SenderModel_, _appRegistry_) {
          $scope = $rootScope.$new();
          $q = _$q_;
          $controller = _$controller_;
          appRegistry = _appRegistry_;
          sender = new _SenderModel_({
            id: 'SENDER-ID'
          });
        });

        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close']);
        $uibModalInstance.close.and.callThrough();
        spyOn(appRegistry, 'getIcon').and.returnValue('blog');

        spyOn(sender, 'initTranslations').and.callThrough();

        senderModel = jasmine.createSpyObj('senderModel', ['get', 'getApps', 'updateNavigation']);
        var senderDeferred = $q.defer();
        senderDeferred.resolve(sender);
        senderModel.get.and.returnValue(senderDeferred.promise);
        var senderAppDeferred = $q.defer();
        senderAppDeferred.resolve(apps);
        senderModel.getApps.and.returnValue(senderAppDeferred.promise);
        var saveDeferred = $q.defer();
        saveDeferred.resolve();
        senderModel.updateNavigation.and.returnValue(saveDeferred.promise);
      });

      function buildController() {
        return $controller('AppTranslationsModalController', {
          $q: $q,
          $uibModalInstance: $uibModalInstance,
          sender: sender,
          SenderModel: function () {
            return senderModel;
          },
          appRegistry: appRegistry,
          senderNavigationUpdateService: senderNavigationUpdateService
        });
      }

      it('should init controller without translations', function () {
        // given
        spyOn(sender, 'isSenderTranslated').and.returnValue(false);
        spyOn(sender, 'getDefaultLanguage').and.returnValue('NONE');

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.isSenderTranslated).toBe(false);
        expect(sender.initTranslations).toHaveBeenCalledWith(ctrl);
        expect(ctrl.languages.NONE.translations.appNavigation).toBeDefined();
        expect(ctrl.languages.NONE.translations.appNavigation.length).toBe(0);
        expect(ctrl.currentLanguage).toBe('NONE');
      });

      it('should init controller with translations', function () {
        // given
        spyOn(sender, 'isSenderTranslated').and.returnValue(true);
        spyOn(sender, 'getDefaultLanguage').and.returnValue('EN');

        sender.translations = {
          DE: {
            displayName: 'the-sender-name',
            appNavigation: ['Titel 1', 'Titel 2', 'Titel 3']
          }
        };
        sender.appNavigation = [{
          name: 'Title 1',
          apps: ['app-id-1', 'app-id-2']
        }, {
          name: 'Title 2',
          apps: ['app-id-3', 'app-id-4']
        }, {
          name: 'Title 3',
          apps: ['app-id-1']
        }];
        apps[0].name = 'App no. 1';
        apps[0].translations = {DE: {name: 'App Nr. 1'}};
        apps[1].name = 'App no. 2';
        apps[1].translations = {DE: {name: 'App Nr. 2'}};
        apps[2].name = 'App no. 3';
        apps[2].translations = {DE: {name: 'App Nr. 3'}};
        apps[3].name = 'App no. 4';
        apps[3].translations = {DE: {name: 'App Nr. 4'}};
        apps[4].name = 'App no. 5';
        apps[4].translations = {DE: {name: 'App Nr. 5'}};

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.isSenderTranslated).toBe(true);
        expect(sender.initTranslations).toHaveBeenCalledWith(ctrl);
        expect(ctrl.languages.EN.translations.appNavigation).toBeDefined();
        expect(ctrl.languages.EN.translations.appNavigation.length).toBe(3);
        expect(ctrl.languages.DE.translations.appNavigation).toBeDefined();
        expect(ctrl.languages.DE.translations.appNavigation.length).toBe(3);
        expect(ctrl.currentLanguage).toBe('EN');
      });

      it('should transfer translation data into the request object structure, call onBeforeSave and then updateNavigation',
          function () {
            // given
            spyOn(sender, 'isSenderTranslated').and.returnValue(true);
            spyOn(sender, 'getDefaultLanguage').and.returnValue('EN');
            sender.defaultLanguage = 'EN';

            sender.translations = {
              DE: {
                displayName: 'the-sender-name',
                appNavigation: ['Titel 1', 'Titel 2', 'Titel 3']
              }
            };
            sender.appNavigation = [{
              name: 'Title 1',
              apps: ['app-id-1', 'app-id-2']
            }, {
              name: 'Title 2',
              apps: ['app-id-3', 'app-id-4']
            }, {
              name: 'Title 3',
              apps: ['app-id-5']
            }];
            apps[0].name = 'App no. 1';
            apps[0].translations = {DE: {name: 'App Nr. 1'}};
            apps[1].name = 'App no. 2';
            apps[1].translations = {DE: {name: 'App Nr. 2'}};
            apps[2].name = 'App no. 3';
            apps[2].translations = {DE: {name: 'App Nr. 3'}};
            apps[3].name = 'App no. 4';
            apps[3].translations = {DE: {name: 'App Nr. 4'}};
            apps[4].name = 'App no. 5';
            apps[4].translations = {DE: {name: 'App Nr. 5'}};

            // when
            var ctrl = buildController();
            ctrl.defaultLanguage = 'EN';
            ctrl.saveCallbacks = jasmine.createSpyObj('ctrl.saveCallbacks', ['onBeforeSave']);
            ctrl.saveCallbacks.onBeforeSave.and.returnValue($q.resolve());

            // when
            ctrl.$onInit();
            $scope.$apply();
            ctrl.save();
            $scope.$apply();

            expect(ctrl.saveCallbacks.onBeforeSave).toHaveBeenCalledTimes(1);
            expect(ctrl.senderModel.updateNavigation).toHaveBeenCalledWith(ctrl.appNavigation, true);
            expect(ctrl.senderModel.getApps).toHaveBeenCalledTimes(1);
            expect(ctrl.appNavigation[0].name).toBe('Title 1');
            expect(ctrl.appNavigation[1].name).toBe('Title 2');
            expect(ctrl.appNavigation[2].name).toBe('Title 3');
            expect(ctrl.appNavigation[0].apps.length).toBe(2);
            expect(ctrl.appNavigation[1].apps.length).toBe(2);
            expect(ctrl.appNavigation[2].apps.length).toBe(1);
            expect(ctrl.appNavigation[0].translations.DE.apps[0].id).toBe('app-id-1');
            expect(ctrl.appNavigation[0].translations.DE.apps[0].name).toBe('App Nr. 1');
            expect(ctrl.appNavigation[0].translations.DE.apps.length).toBe(2);
            expect(ctrl.appNavigation[2].translations.DE.apps[0].id).toBe('app-id-5');
            expect(ctrl.appNavigation[2].translations.DE.apps[0].name).toBe('App Nr. 5');
            expect(ctrl.appNavigation[2].translations.DE.apps.length).toBe(1);
          });
    });
  });
})();
