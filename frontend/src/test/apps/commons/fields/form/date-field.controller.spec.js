(function () {
  'use strict';

  var moduleName = 'coyo.apps.commons.fields';

  describe('module: ' + moduleName, function () {

    beforeEach(
        module(moduleName, function () {
        })
    );

    describe('controller: DateFieldController', function () {

      var $controller, $scope, rootScope;

      beforeEach(function () {

        inject(function (_$controller_, _$timeout_, $rootScope) {
          $controller = _$controller_;
          $scope = $rootScope.$new();
          rootScope = $rootScope;
        });
      });

      function buildController() {
        return $controller('DateFieldController', {
          $scope: $scope
        });
      }

      it('should change the date format if format is incorrect', function () {
        //given
        rootScope.dateFormat = {short: 'M/D/YYYY'};
        $scope.$parent.model = {value: new Date()};

        //when
        var ctrl = buildController();
        ctrl.$onInit();

        //then
        expect(ctrl.dateFormat).toBe('M/d/yyyy');
      });

      it('should not change the date format if format is correct', function () {
        //given
        rootScope.dateFormat = {short: 'M/d/yyyy'};
        $scope.$parent.model = {value: new Date()};

        //when
        var ctrl = buildController();
        ctrl.$onInit();

        //then
        expect(ctrl.dateFormat).toBe('M/d/yyyy');
      });

      it('should use default date format if dateFormat.short is not given', function () {
        //given
        rootScope.dateFormat = {};
        $scope.$parent.model = {value: new Date()};

        //when
        var ctrl = buildController();
        ctrl.$onInit();

        //then
        expect(ctrl.dateFormat).toBe('yyyy-MM-dd');
      });

      it('should parse to date if input is a string', function () {
        //given
        var date = new Date();
        rootScope.dateFormat = {short: 'M/d/yyyy'};
        $scope.$parent.model = {value: date.toString()};

        //when
        var ctrl = buildController();
        ctrl.$onInit();

        //then
        expect($scope.$parent.model.value.toString()).toEqual(date.toString());
      });

      it('should parse to date if input is a number', function () {
        //given
        var date = new Date();
        rootScope.dateFormat = {short: 'M/d/yyyy'};
        $scope.$parent.model = {value: date.getTime()};

        //when
        var ctrl = buildController();
        ctrl.$onInit();

        //then
        expect($scope.$parent.model.value.getTime()).toEqual(date.getTime());
      });

      it('should not return a date if input is empty', function () {
        //given
        rootScope.dateFormat = {};
        $scope.$parent.model = {};

        //when
        var ctrl = buildController();
        ctrl.$onInit();

        //then
        expect($scope.$parent.model.value).toBe(undefined);
      });
    });
  });

})();
