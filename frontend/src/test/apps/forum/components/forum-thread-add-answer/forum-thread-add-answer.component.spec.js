(function () {
  'use strict';

  var moduleName = 'coyo.apps.forum';

  describe('module: ' + moduleName, function () {

    var $rootScope, $scope, $q, $controller, authService;
    var tempUploadService, app, thread, answer, ForumThreadAnswerModel;

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$q_, _$controller_, _ForumThreadAnswerModel_, _authService_) {
      $rootScope = _$rootScope_;
      $scope = _$rootScope_.$new();
      $q = _$q_;
      $controller = _$controller_;
      authService = _authService_;

      tempUploadService = jasmine.createSpyObj('tempUploadService', ['upload']);
      tempUploadService.upload.and.returnValue($q.resolve());

      app = jasmine.createSpyObj('app', ['']);
      thread = jasmine.createSpyObj('thread', ['isNew', 'create']);
      answer = jasmine.createSpyObj('answer', ['isNew', 'createWithPermissions']);

      ForumThreadAnswerModel = _ForumThreadAnswerModel_;

      thread.id = 'ThreadId';
      answer.id = 'AnswerId';
      answer.typeName = 'forum-thread-answer';
      answer.text = 'Some text';
      app.settings = {};
    }));

    var controllerName = 'ForumThreadAddAnswerController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        var controller = $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          $q: $q,
          tempUploadService: tempUploadService,
          ForumThreadAnswerModel: ForumThreadAnswerModel,
          authService: authService
        });
        controller.app = app;
        controller.thread = thread;
        return controller;
      }

      it('should init correctly', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.newItemAttachments).toEqual([]);
        expect(ctrl.answer.threadId).toEqual(thread.id);
      });

      it('should save a new forum thread answer', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        var defer = $q.defer();
        ctrl.answer = answer;
        answer.isNew.and.returnValue(true);
        answer.createWithPermissions.and.returnValue(defer.promise);
        ctrl.status.saving = false;
        spyOn($rootScope, '$broadcast').and.callThrough();

        // when
        ctrl.save();
        defer.resolve(answer);
        $rootScope.$apply();

        // then
        expect(answer.createWithPermissions).toHaveBeenCalledWith(['delete']);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('forum:answer:published', answer);
      });

      it('should not save a new forum thread answer if text is empty', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.answer = answer;
        ctrl.answer.text = '';

        // when
        ctrl.save();
        $rootScope.$apply();

        // then
        expect(answer.createWithPermissions).not.toHaveBeenCalled();
      });

      it('should add attachments', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        var files = [{id: 'file-id-1'}, {id: 'file-id-2'}];

        // when
        ctrl.addAttachments(files);

        // then
        expect(tempUploadService.upload).toHaveBeenCalledTimes(2);
        expect(ctrl.newItemAttachments).toEqual(files);
      });

      it('should remove attachment', function () {
        // given
        var ctrl = buildController();
        var file1 = {id: 'file-id-1'};
        var file2 = {id: 'file-id-2'};
        ctrl.newItemAttachments = [file1, file2];

        // when
        ctrl.removeAttachment(file1);

        // then
        expect(ctrl.newItemAttachments).toEqual([file2]);
      });

    });
  });

})();
