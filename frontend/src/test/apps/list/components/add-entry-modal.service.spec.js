(function () {
  'use strict';

  var moduleName = 'coyo.apps.list';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $uibModalInstance, coyoNotification, $scope;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
      $controller = _$controller_;
      $q = _$q_;
      $scope = _$rootScope_;
      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close']);
      coyoNotification = jasmine.createSpyObj('coyoNotification', ['success']);
    }));

    var controllerName = 'AddEntryModalController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $uibModalInstance: $uibModalInstance,
          app: {
            senderId: 'sender-id',
            id: 'app-id',
            settings: {}
          },
          fields: [],
          listService: {},
          coyoNotification: coyoNotification
        });
      }

      it('should return promise for save function', function () {
        // given
        var ctrl = buildController();

        $uibModalInstance.close.and.returnValue({result: 'testDone'});

        var deferred = $q.defer();

        ctrl.entry = jasmine.createSpyObj('entry', ['save']);
        ctrl.entry.save.and.returnValue(deferred.promise);

        // when
        var returnValue = ctrl.save();
        var result = undefined;

        returnValue.then(function (value) {
          result = value;
        });

        deferred.resolve('');
        $scope.$apply();

        // then
        expect(result).toBe('testDone');
        expect(coyoNotification.success).toHaveBeenCalled();
      });
    });
  });

})();
