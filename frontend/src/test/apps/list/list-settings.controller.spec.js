(function () {
  'use strict';

  var moduleName = 'coyo.apps.list';
  var controllerName = 'ListSettingsController';

  describe('module: ' + moduleName, function () {

    var $controller, $scope;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, $rootScope) {
      $controller = _$controller_;
      $scope = $rootScope.$new();
    }));

    describe('controller: ' + controllerName, function () {

      function buildController() {
        var controller = $controller(controllerName, {
          $scope: $scope
        });

        // init empty app model stub
        $scope.model = {settings: {}};
        controller.$onInit();
        return controller;
      }

      it('should initialize with default values for permissions', function () {
        // when
        var ctrl = buildController();

        // then
        expect(ctrl.app.settings.permissionCreate).toEqual('YES');
        expect(ctrl.app.settings.permissionRead).toEqual('ALL');
        expect(ctrl.app.settings.permissionEdit).toEqual('OWN');
        expect(ctrl.app.settings.permissionDelete).toEqual('OWN');
      });

      it('should set edit/delete permission to NONE when setting read permission to NONE', function () {
        // given
        var ctrl = buildController();
        ctrl.app.settings.permissionRead = 'NONE';

        // when
        ctrl.handleReadPermissionChange();

        // then
        expect(ctrl.app.settings.permissionEdit).toBe('NONE');
        expect(ctrl.app.settings.permissionDelete).toBe('NONE');
      });

      it('should not modify edit/delete permissions when setting read permission to OWN', function () {
        // given
        var ctrl = buildController();
        ctrl.app.settings.permissionRead = 'OWN';

        // when
        ctrl.handleReadPermissionChange();

        // then
        expect(ctrl.app.settings.permissionEdit).toBe('OWN');
        expect(ctrl.app.settings.permissionDelete).toBe('OWN');
      });

      it('should disable edit/delete permissions when setting read permission to NONE', function () {
        // given
        var ctrl = buildController();
        ctrl.app.settings.permissionRead = 'NONE';

        // when
        ctrl.handleReadPermissionChange();

        // then
        expect(ctrl.isEnabled('ALL')).toBe(false);
        expect(ctrl.isEnabled('OWN')).toBe(false);
        expect(ctrl.isEnabled('NONE')).toBe(true);
      });

      it('should set folder settings to viewer as default', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.app.settings.folderPermissions.modifyRole).toBe('VIEWER');
      });

      it('should set folder settings to admin if read permission changes to none', function () {
        // given
        var ctrl = buildController();
        ctrl.app.settings.permissionRead = 'NONE';

        // when
        ctrl.handleReadPermissionChange();

        // then
        expect(ctrl.app.settings.folderPermissions.modifyRole).toBe('ADMIN');
      });

      it('should set folder settings to admin if read permission changes to own', function () {
        // given
        var ctrl = buildController();
        ctrl.app.settings.permissionRead = 'OWN';

        // when
        ctrl.handleReadPermissionChange();

        // then
        expect(ctrl.app.settings.folderPermissions.modifyRole).toBe('ADMIN');
      });

      it('should set folder settings to viewer if read permission changes to all', function () {
        // given
        var ctrl = buildController();
        ctrl.app.settings.permissionRead = 'ALL';
        ctrl.app.settings.folderPermissions.modifyRole = 'ADMIN';

        // when
        ctrl.handleReadPermissionChange();

        // then
        expect(ctrl.app.settings.folderPermissions.modifyRole).toBe('VIEWER');
      });
    });
  });
})();
