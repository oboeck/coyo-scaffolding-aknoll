(function () {
  'use strict';

  var moduleName = 'coyo.apps.task';
  var ctrlName = 'TaskSettingsController';

  describe('module: ' + moduleName, function () {

    beforeEach(module(moduleName));

    describe('controller: ' + ctrlName, function () {
      var $controller, $scope;

      beforeEach(inject(function ($rootScope, _$controller_) {
        $scope = $rootScope.$new();
        $controller = _$controller_;

        $scope.model = {
          settings: {}
        };
      }));

      function buildController() {
        return $controller(ctrlName, {
          $scope: $scope
        });
      }

      describe('controller init', function () {

        it('should initialize with default values for manageList and manageTasks', function () {
          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect($scope.model.settings.manageTasks).toEqual('VIEWER');
          expect($scope.model.settings.manageList).toEqual('ADMIN');
        });
      });
    });
  });

})();
