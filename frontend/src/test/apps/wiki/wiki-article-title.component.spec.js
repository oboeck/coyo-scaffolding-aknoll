(function () {
  'use strict';

  var moduleName = 'coyo.apps.wiki';
  var directiveName = 'coyoWikiArticleTitle';

  describe('module: ' + moduleName, function () {

    var $controller, $rootScope, $scope, wikiArticleTranslationService, SettingsModel, SenderModel,
        authService, app;

    beforeEach(module(moduleName, function ($provide) {
      $provide.value('SenderModel', SenderModel);
    }));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$rootScope_, _$controller_, _authService_, _SettingsModel_) {
        $rootScope = _$rootScope_;
        authService = _authService_;
        $controller = _$controller_;
        SettingsModel = _SettingsModel_;
        $scope = $rootScope.$new();
        app = jasmine.createSpyObj('app', ['getTranslatedContent']);

        wikiArticleTranslationService =
            jasmine.createSpyObj('wikiArticleTranslationService', ['init', 'cleanup', 'prepareTranslations']);

        app.id = 'AP-ID';
        app.senderId = 'SENDER-ID';
        app.settings = {
          publisherIds: []
        };
      }));

      var controllerName = 'WikiArticleTitleController';

      describe('controller: ' + controllerName, function () {

        function buildController(article, app) {
          return $controller(controllerName, {
            $scope: $scope,
            authService: authService,
            SettingsModel: SettingsModel,
            wikiArticleTranslationService: wikiArticleTranslationService
          }, {article: article, wikiApp: app, currentLanguage: 'en'});
        }

        it('should check article state new (article is new)', function () {
          // given
          var newDateMS = new Date().getTime();
          var article = {created: newDateMS, modified: newDateMS};
          var ctrl = buildController(article);

          // when
          ctrl.$onInit();

          // then
          expect(ctrl.article.isNewArticle).toBeTrue();
        });

        it('should check article state new (article is not new)', function () {
          // given
          var newDateMS = new Date().getTime() - 10;
          var modifiedDateMS = new Date().getTime() - 5;
          var article = {created: newDateMS, modified: modifiedDateMS};
          var ctrl = buildController(article);

          // when
          ctrl.$onInit();

          // then
          expect(ctrl.article.isNewArticle).toBeFalse();
        });

        it('should check article state update (article has been updated)', function () {
          // given
          var newDateMS = new Date().getTime() - 10;
          var modifiedDateMS = new Date().getTime() - 5;
          var article = {created: newDateMS, modified: modifiedDateMS};
          var ctrl = buildController(article);

          // when
          ctrl.$onInit();

          // then
          expect(ctrl.article.isUpdated).toBeTrue();
        });

        it('should check article state update (article has been updated a long time ago)', function () {
          // given
          var newDateMS = new Date().getTime() - 10;
          var modifiedDateMS = new Date().getTime() - 8 * 24 * 60 * 60 * 1000;
          var article = {created: newDateMS, modified: modifiedDateMS};
          var ctrl = buildController(article);

          // when
          ctrl.$onInit();

          // then
          expect(ctrl.article.isUpdated).toBeFalse();
        });
      });

    });

  });
})();
