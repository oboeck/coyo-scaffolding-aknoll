(function () {
  'use strict';

  var moduleName = 'coyo.apps.wiki';

  describe('module: ' + moduleName, function () {

    var $q, $timeout, $httpBackend, $scope, widgetLayoutService, wikiArticleTranslationService, vm, sender, article,
        currentUser, SettingsModel;

    beforeEach(function () {

      module(moduleName, function ($provide) {
        widgetLayoutService =
            jasmine.createSpyObj('widgetLayoutService', ['cancel', 'save', 'edit', 'onload', 'collect', 'fill']);
        $provide.value('widgetLayoutService', widgetLayoutService);
      });

      inject(function (_$q_, _$timeout_, _$httpBackend_, $rootScope, _wikiArticleTranslationService_, _SettingsModel_) {
        $q = _$q_;
        $scope = $rootScope.$new();
        $timeout = _$timeout_;
        $httpBackend = _$httpBackend_;
        wikiArticleTranslationService = _wikiArticleTranslationService_;
        SettingsModel = _SettingsModel_;
        currentUser = jasmine.createSpyObj('currentUser', ['getBestSuitableLanguage']);
        article = jasmine.createSpyObj('article', ['isNew', 'buildLayoutName']);
        article.id = 'ARTICLE-ID';
        article.title = 'A title';
        article.wikiArticles = 0;

        vm = {
          article: article,
          app: jasmine.createSpyObj('app', [''])
        };

        sender = {
          defaultLanguage: 'EN',
          translations: {
            DE: {
              title: 'Ein Titel'
            }
          },
          getDefaultLanguage: function () {
          },
          isSenderTranslated: function () {
          }
        };

        spyOn(sender, 'getDefaultLanguage');
        spyOn(sender, 'isSenderTranslated');

        widgetLayoutService.save.and.returnValue($q.resolve());
        widgetLayoutService.edit.and.returnValue($q.resolve());
        widgetLayoutService.cancel.and.returnValue($q.resolve());
        widgetLayoutService.onload.and.returnValue($q.resolve());
        widgetLayoutService.collect.and.returnValue($q.resolve({layout: {}, slots: []}));
        widgetLayoutService.fill.and.returnValue($q.resolve());
      });
    });

    describe('Service: wikiArticleTranslationService', function () {

      it('should init the service with translations', function () {
        // given
        sender.getDefaultLanguage.and.returnValue('EN');
        sender.isSenderTranslated.and.returnValue(true);
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve('EN'));
        spyOn(SettingsModel, 'retrieveByKey').and.callFake(function () {
          return $q.resolve('true');
        });
        vm.sender = sender;
        // when
        wikiArticleTranslationService.initLanguages(vm, currentUser);
        $scope.$apply();

        // then
        expect(vm.defaultLanguage).toEqual(sender.defaultLanguage);
        expect(vm.currentLanguage).toEqual(sender.defaultLanguage);
        expect(vm.isSenderTranslated).toBe(true);
        expect(vm.languages).toEqual({
          DE: Object({active: true, translations: Object({}), 'empty': true}),
          EN: Object({active: true, translations: Object({title: 'A title'}), 'empty': false})
        });
        expect(vm.languageInitialised).toEqual({EN: true});
        expect(article.defaultLanguage).toBe('EN');
      });

      it('should init the service with translations and try preferred language but use the default', function () {
        // given
        var defaultLang = 'EN';
        var preferredLang = 'DE';
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve(defaultLang));
        sender.getDefaultLanguage.and.returnValue(defaultLang);
        sender.isSenderTranslated.and.returnValue(true);
        spyOn(SettingsModel, 'retrieveByKey').and.callFake(function () {
          return $q.resolve('true');
        });
        vm.sender = sender;
        // when
        wikiArticleTranslationService.initLanguages(vm, currentUser, preferredLang);
        $scope.$apply();

        // then
        expect(vm.languages).toEqual({
          DE: Object({active: true, translations: Object({}), 'empty': true}),
          EN: Object({active: true, translations: Object({title: 'A title'}), 'empty': false})
        });
        expect(vm.currentLanguage).toEqual(defaultLang);
        expect(vm.preferredLanguage).toEqual(preferredLang);
        expect(vm.languageInitialised).toEqual({EN: true});
      });

      it('should init the service with translations and use users best suitable language', function () {
        // given
        var defaultLang = 'EN';
        var preferredLang = 'DE';
        var bestSuitable = 'FR';
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve(bestSuitable));
        sender.getDefaultLanguage.and.returnValue(defaultLang);
        sender.isSenderTranslated.and.returnValue(true);
        spyOn(SettingsModel, 'retrieveByKey').and.callFake(function () {
          return $q.resolve('true');
        });
        sender.translations = {
          FR: {
            title: 'Merci'
          }
        };
        vm.sender = sender;
        vm.article.translations = {'FR': {'title': 'Merci'}};

        // when
        wikiArticleTranslationService.initLanguages(vm, currentUser, preferredLang);
        $scope.$apply();

        // then
        expect(vm.languages).toEqual({
          FR: Object({active: true, translations: Object({title: 'Merci'}), 'empty': false}),
          EN: Object({active: true, translations: Object({title: 'A title'}), 'empty': false})
        });
        expect(vm.currentLanguage).toEqual(bestSuitable);
        expect(vm.preferredLanguage).toEqual(preferredLang);
        expect(vm.languageInitialised).toEqual({EN: true, FR: true});
      });

      it('should init the service with translations and use preferred language', function () {
        // given
        var defaultLang = 'EN';
        var preferredLang = 'DE';
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve(defaultLang));
        sender.getDefaultLanguage.and.returnValue(defaultLang);
        sender.isSenderTranslated.and.returnValue(true);
        spyOn(SettingsModel, 'retrieveByKey').and.callFake(function () {
          return $q.resolve('true');
        });
        vm.sender = sender;
        vm.article.translations = {'DE': {'title': 'Ein Titel'}};

        // when
        wikiArticleTranslationService.initLanguages(vm, currentUser, preferredLang);
        $scope.$apply();

        // then
        expect(vm.languages).toEqual({
          DE: Object({active: true, translations: Object({title: 'Ein Titel'}), 'empty': false}),
          EN: Object({active: true, translations: Object({title: 'A title'}), 'empty': false})
        });
        expect(vm.currentLanguage).toEqual(preferredLang);
        expect(vm.preferredLanguage).toEqual(preferredLang);
        expect(vm.languageInitialised).toEqual({EN: true, DE: true});
      });

      it('should set default language if multi language is active and current language is NONE', function () {
        // given
        var defaultLanguage = 'FI';
        var preferredLanguage = 'NONE';
        sender.getDefaultLanguage.and.returnValue(defaultLanguage);
        sender.isSenderTranslated.and.returnValue(true);
        spyOn(SettingsModel, 'retrieveByKey').and.callFake(function () {
          return $q.resolve('true');
        });
        vm.sender = sender;

        // when
        wikiArticleTranslationService.initLanguages(vm, currentUser, preferredLanguage);
        $scope.$apply();

        // then
        expect(vm.defaultLanguage).toEqual(defaultLanguage);
        expect(vm.currentLanguage).toEqual(defaultLanguage);
        expect(vm.languageInitialised).toEqual({FI: true});
      });

      it('should init the service without translations', function () {
        // given
        var sender = {
          getDefaultLanguage: function () {
          },
          isSenderTranslated: function () {
          }
        };
        vm.sender = sender;
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve('EN'));
        spyOn(vm.sender, 'getDefaultLanguage').and.returnValue('NONE');
        spyOn(vm.sender, 'isSenderTranslated').and.returnValue(false);
        spyOn(SettingsModel, 'retrieveByKey').and.callFake(function () {
          return $q.resolve('false');
        });
        // when
        wikiArticleTranslationService.initLanguages(vm, currentUser);
        $scope.$apply();

        // then
        expect(vm.defaultLanguage).toBe('NONE');
        expect(vm.currentLanguage).toBe('NONE');
        expect(vm.isSenderTranslated).toBe(false);
        expect(vm.languages)
            .toEqual({NONE: Object({active: true, translations: Object({title: 'A title'}), 'empty': false})});
        expect(vm.languageInitialised).toEqual({NONE: true});
        expect(article.defaultLanguage).toBe('NONE');
      });

      it('should collect layout + widgets and discard them, on language delete', function () {
        // given
        vm.sender = sender;
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve('EN'));
        wikiArticleTranslationService.initLanguages(vm, currentUser);
        var current = 'EN';

        // when
        wikiArticleTranslationService.onLanguageDeleted($scope, vm, current);
        $scope.$apply();

        // then
        expect(widgetLayoutService.collect).toHaveBeenCalled();
      });

      it('should delete collected layouts, on save', function () {
        // given
        var current = 'DE';
        vm.sender = sender;
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve(current));
        $httpBackend.expectDELETE('/web/widget-layouts/layoutName').respond(200);
        widgetLayoutService.collect.and.returnValue($q.resolve({
          layout: {name: 'layoutName', rows: [{slots: [{name: 'slotName'}]}]}, slots: []
        }));
        wikiArticleTranslationService.initLanguages(vm, currentUser);

        // when
        wikiArticleTranslationService.onLanguageDeleted($scope, vm, current);
        $scope.$apply();
        wikiArticleTranslationService.cleanup();
        $timeout.flush();

        // then
        expect(widgetLayoutService.collect).toHaveBeenCalled();
      });

      it('should not collect layout + widgets if not copyFromDefault, on language change', function () {
        // given
        vm.sender = sender;
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve('EN'));

        wikiArticleTranslationService.initLanguages(vm, currentUser);

        // when
        wikiArticleTranslationService.onLanguageChange($scope, vm, false);
        $timeout.flush();

        // then
        expect(widgetLayoutService.edit).toHaveBeenCalled();
        expect(widgetLayoutService.collect).not.toHaveBeenCalled();
      });

      it('should collect/fill layout + widgets and activate edit if copyFromDefault, on language change', function () {
        // given
        vm.sender = sender;
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve('EN'));

        wikiArticleTranslationService.initLanguages(vm, currentUser);

        // when
        wikiArticleTranslationService.onLanguageChange($scope, vm, true);
        $scope.$apply();

        // then
        expect(widgetLayoutService.collect).toHaveBeenCalled();
        expect(widgetLayoutService.fill).toHaveBeenCalledTimes(2);
        expect(widgetLayoutService.edit).toHaveBeenCalled();
      });
    });
  });

})();
