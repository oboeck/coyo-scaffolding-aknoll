(function () {
  'use strict';

  var moduleName = 'coyo.apps.wiki';

  describe('module: ' + moduleName, function () {

    var $rootScope, $scope, $log, $interval, $q, $controller, $state, app, article, modalService, socketService,
        widgetLayoutService, WikiArticleModel, $timeout, moment, currentUser, wikiArticleService,
        wikiArticleRevisionsModal, sender, wikiArticleTranslationService, SettingsModel;

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$log_, _$interval_, _$q_, _$controller_, _$timeout_, _moment_) {
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $log = _$log_;
      $interval = _$interval_;
      $q = _$q_;
      $controller = _$controller_;
      $timeout = _$timeout_;

      $state = jasmine.createSpyObj('$state', ['go', 'transitionTo']);
      app = jasmine.createSpyObj('app', ['save']);

      article = jasmine.createSpyObj('article', ['isNew', 'create', 'update', 'getRevisions']);

      wikiArticleService = jasmine.createSpyObj('wikiArticleService',
          ['deleteArticle', 'exportPreview', 'exportPreviewWithSubArticles', 'confirmWikiExport', 'lock', 'releaseLock', 'unlock', 'isLocked', 'hasLock']);
      wikiArticleService.deleteArticle.and.returnValue($q.resolve());
      wikiArticleService.lock.and.returnValue($q.resolve(article));
      wikiArticleService.releaseLock.and.returnValue($q.resolve());

      modalService = jasmine.createSpyObj('modalService', ['note', 'confirm']);
      wikiArticleRevisionsModal = jasmine.createSpyObj('wikiArticleRevisionsModal', ['open']);

      widgetLayoutService = jasmine.createSpyObj('widgetLayoutService', ['onload', 'save', 'edit', 'cancel']);
      widgetLayoutService.onload.and.returnValue($q.resolve());
      widgetLayoutService.save.and.returnValue($q.resolve());

      wikiArticleTranslationService =
          jasmine.createSpyObj('wikiArticleTranslationService', ['initLanguages', 'cleanup', 'prepareTranslations']);
      wikiArticleTranslationService.cleanup.and.returnValue($q.resolve());

      socketService = jasmine.createSpyObj('socketService', ['subscribe']);
      WikiArticleModel = jasmine.createSpyObj('WikiArticleModel', ['getWithPermissions']);
      moment = _moment_;

      currentUser = {
        id: 1,
        name: 'A man has no name',
        language: 'EN',
        getBestSuitableLanguage: function () {
          return $q.resolve('EN');
        }
      };

      SettingsModel = {
        retrieve: function () {
          return {
            then: function (callback) {
              callback({defaultLanguage: 'EN'});
            }
          };
        }
      };

      article.id = 'ArticleId';
      article.title = 'Title1';
      sender = {};
      app.id = 'APP-ID';
      app.senderId = 'SENDER-ID';
      app.settings = {
        publisherIds: []
      };
    }));

    var controllerName = 'WikiArticleViewController';

    describe('controller: ' + controllerName, function () {

      function buildController($stateParams, editMode) {
        return $controller(controllerName, {
          $scope: $scope,
          $state: $state,
          $stateParams: $stateParams || {},
          $log: $log,
          $interval: $interval,
          $timeout: $timeout,
          modalService: modalService,
          wikiArticleRevisionsModal: wikiArticleRevisionsModal,
          widgetLayoutService: widgetLayoutService,
          wikiArticleService: wikiArticleService,
          socketService: socketService,
          WikiArticleModel: WikiArticleModel,
          app: app,
          article: article,
          editMode: editMode || false,
          moment: moment,
          currentUser: currentUser,
          sender: sender,
          SettingsModel: SettingsModel,
          wikiArticleTranslationService: wikiArticleTranslationService
        });
      }

      it('should be able to edit', function () {
        // given
        article.locked = true;
        article.lockHolder = {
          id: currentUser.id
        };

        var ctrl = buildController();
        ctrl.loading = false;

        // when
        ctrl.edit();
        $scope.$apply();

        // then
        expect(ctrl.editMode).toBe(true);
        expect(widgetLayoutService.edit).toHaveBeenCalled();
        expect(wikiArticleService.lock).toHaveBeenCalledWith(article, currentUser);
      });

      it('should be able to set as home article', function () {
        // given
        modalService.confirm.and.returnValue({result: $q.resolve()});
        app.save.and.returnValue($q.resolve(app));

        spyOn($rootScope, '$emit').and.callThrough();

        var ctrl = buildController();
        ctrl.app.settings.home = undefined;
        ctrl.article.id = 'ARTICLE-ID';
        // when
        ctrl.setAsHomeArticle();
        $scope.$apply();

        // then
        expect(ctrl.app.settings.home).toEqual(ctrl.article.id);
        expect($rootScope.$emit).toHaveBeenCalledWith('app:updated', app);
      });

      it('should be able to open in edit mode', function () {
        // when
        var ctrl = buildController({}, true);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.editMode).toBe(true);
        expect(wikiArticleService.lock).toHaveBeenCalled();
        expect(widgetLayoutService.edit).toHaveBeenCalled();
      });

      it('should be able to open export preview of single article', function () {
        // given
        wikiArticleTranslationService.initLanguages.and.callFake(function () {
          ctrl.languages = {
            'EN': {
              'active': true,
              'translations': {
                'title': article.title
              },
              'empty': false
            }
          };
        });
        var ctrl = buildController();
        ctrl.currentLanguage = 'EN';
        ctrl.$onInit();
        $scope.$apply();

        // when
        ctrl.exportPreview();

        // then
        expect(wikiArticleService.exportPreview).toHaveBeenCalledWith({appId: app.id, articles: [article], modalTitle: article.title, articleTitle: article.title, selectedLanguage: 'EN'});
      });

      it('should be able to open export preview of article with sub-articles', function () {
        // given
        wikiArticleTranslationService.initLanguages.and.callFake(function () {
          ctrl.languages = {
            'EN': {
              'active': true,
              'translations': {
                'title': article.title
              },
              'empty': false
            }
          };
        });
        article.deepCountChildren = function () {
          return $q.resolve(5);
        };
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();
        wikiArticleService.confirmWikiExport.and.returnValue($q.resolve());

        // when
        ctrl.exportSubArticlePreview();
        $scope.$apply();

        // then
        expect(wikiArticleService.confirmWikiExport).toHaveBeenCalledWith(6);
        expect(wikiArticleService.exportPreviewWithSubArticles).toHaveBeenCalledWith(app, article);
      });

      it('should not open export preview of article with many sub-articles when user cancels export', function () {
        // given
        wikiArticleTranslationService.initLanguages.and.callFake(function () {
          ctrl.languages = {
            'EN': {
              'active': true,
              'translations': {
                'title': article.title
              }
            }
          };
        });
        article.deepCountChildren = function () {
          return $q.resolve(81);
        };
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();
        wikiArticleService.confirmWikiExport.and.returnValue($q.reject());

        // when
        ctrl.exportSubArticlePreview();
        $scope.$apply();

        // then
        expect(wikiArticleService.confirmWikiExport).toHaveBeenCalledWith(82);
        expect(wikiArticleService.exportPreviewWithSubArticles).not.toHaveBeenCalled();
      });

      it('should save an updated wiki article', function () {
        // given
        var ctrl = buildController({revision: 4});
        var defer = $q.defer();
        article.isNew.and.returnValue(false);
        article.update.and.returnValue(defer.promise);
        ctrl.editMode = true;
        ctrl.loading = false;
        $state.current = 'current.state';

        // when
        ctrl.save();
        defer.resolve(article);
        $scope.$apply();
        $timeout.flush();

        // then
        expect(widgetLayoutService.save).toHaveBeenCalled();
        expect(article.update).toHaveBeenCalled();
        expect($state.transitionTo).toHaveBeenCalled();
        var args = $state.transitionTo.calls.mostRecent().args;
        expect(args[1]).toEqual({editMode: false});
        expect(args[2]).toEqual({reload: $state.current, inherit: false, notify: true});
      });

      it('should be able to cancel', function () {
        // given
        var ctrl = buildController({editMode: true});
        ctrl.editMode = true;
        ctrl.loading = false;
        article.locked = true;
        article.lockHolder = {
          id: currentUser.id
        };
        wikiArticleService.releaseLock.and.returnValue($q.resolve(article));
        $state.current = 'current.state';

        // when
        ctrl.cancel();
        $scope.$apply();

        // then
        expect(widgetLayoutService.cancel).toHaveBeenCalled();
        expect($state.transitionTo).toHaveBeenCalled();
        var args = $state.transitionTo.calls.mostRecent().args;
        expect(args[1]).toEqual({editMode: false});
        expect(args[2]).toEqual({reload: $state.current, inherit: false, notify: true});
      });

      it('should delete an article', function () {
        // given
        var ctrl = buildController();
        ctrl.editMode = false;
        ctrl.loading = false;

        // when
        ctrl.deleteArticle(article);
        $scope.$apply();

        // then
        expect(wikiArticleService.deleteArticle).toHaveBeenCalledWith(app, article);
        expect($state.go).toHaveBeenCalledWith('^');
      });

      it('should forcefully remove a lock', function () {
        // given
        article.locked = true;
        article.lockHolder = currentUser;

        var ctrl = buildController();
        modalService.confirm.and.returnValue({result: $q.resolve()});
        wikiArticleService.unlock.and.returnValue($q.resolve());

        var newLock = {
          locked: false,
          lockDate: new Date(),
          lockHolder: undefined
        };
        WikiArticleModel.getWithPermissions.and.returnValue($q.resolve(newLock));

        // when
        ctrl.removeLock();
        $scope.$apply();

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect(wikiArticleService.unlock).toHaveBeenCalledWith(article);
        expect(WikiArticleModel.getWithPermissions).toHaveBeenCalled();

        var context = WikiArticleModel.getWithPermissions.calls.mostRecent().args[0];
        expect(context.senderId = app.senderId);
        expect(context.appId = app.id);
        expect(context.id = article.id);

        expect(ctrl.article.locked).toBe(false);
        expect(ctrl.article.lockHolder).toBeUndefined();
      });

      it('should handle article lock set events', function () {
        // given
        var setLock = angular.noop;
        socketService.subscribe = function (destination, callback, filter) {
          if (destination === '/topic/item.' + article.id + '.lock' && filter === 'set') {
            setLock = callback;
          }
        };

        var newLock = {
          locked: true,
          lockDate: new Date(),
          lockHolder: {id: 'USER-ID'}
        };
        WikiArticleModel.getWithPermissions.and.returnValue($q.resolve(newLock));

        var ctrl = buildController();
        ctrl.editMode = false;
        ctrl.$onInit();

        // when
        setLock();
        $scope.$apply();

        // then
        expect(WikiArticleModel.getWithPermissions).toHaveBeenCalled();
        expect(ctrl.article.locked).toBe(true);
        expect(ctrl.article.lockHolder.id).toBe('USER-ID');
      });

      it('should handle article lock released events', function () {
        // given
        var lockReleased = angular.noop;
        socketService.subscribe = function (destination, callback, filter) {
          if (destination === '/topic/item.' + article.id + '.lock' && filter === 'released') {
            lockReleased = callback;
          }
        };

        var newLock = {
          locked: false,
          lockDate: new Date(),
          lockHolder: undefined
        };
        WikiArticleModel.getWithPermissions.and.returnValue($q.resolve(newLock));

        var ctrl = buildController();
        ctrl.editMode = false;
        ctrl.$onInit();

        // when
        lockReleased({content: {changed: true}});
        $scope.$apply();

        // then
        expect(WikiArticleModel.getWithPermissions).toHaveBeenCalled();
        expect(ctrl.article.locked).toBe(false);
        expect(ctrl.article.latestRevision).toBe(false);
        expect(ctrl.article.lockHolder).toBeUndefined();
      });

      it('should handle article lock removed events', function () {
        // given
        var lockRemoved = angular.noop;
        socketService.subscribe = function (destination, callback, filter) {
          if (destination === '/topic/item.' + article.id + '.lock' && filter === 'removed') {
            lockRemoved = callback;
          }
        };

        var newLock = {
          locked: false,
          lockDate: new Date(),
          lockHolder: undefined
        };
        WikiArticleModel.getWithPermissions.and.returnValue($q.resolve(newLock));

        var ctrl = buildController();
        ctrl.editMode = true;
        ctrl.$onInit();

        // when
        lockRemoved({content: {previousHolder: currentUser.id}});
        $scope.$apply();

        // then
        expect(modalService.note).toHaveBeenCalled();
        expect(WikiArticleModel.getWithPermissions).toHaveBeenCalled();
        expect(ctrl.article.locked).toBe(false);
        expect(ctrl.article.lockHolder).toBeUndefined();
      });

      it('should show revisions and navigate to revision upon selection', function () {
        // given
        var revision = {id: 'revision-one'};
        var ctrl = buildController();
        $state.current = 'test.current.state';
        wikiArticleRevisionsModal.open.and.returnValue($q.resolve(revision));

        // when
        ctrl.showRevisions();
        $scope.$apply();

        // then
        expect(wikiArticleRevisionsModal.open).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith(
            $state.current, {revision: revision, editMode: false}, {reload: $state.current}
        );
      });
    });
  });

})();
