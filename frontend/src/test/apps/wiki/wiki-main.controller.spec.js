(function () {
  'use strict';

  var moduleName = 'coyo.apps.wiki';

  describe('module: ' + moduleName, function () {

    var $controller, $state, $timeout;

    beforeEach(module(moduleName));

    beforeEach(function () {
      $state = jasmine.createSpyObj('$state', ['go']);

      inject(function (_$controller_, _$timeout_) {
        $timeout = _$timeout_;
        $controller = _$controller_;
      });
    });

    var controllerName = 'WikiMainController';

    describe('controller: ' + controllerName, function () {

      function buildController(app) {
        return $controller(controllerName, {
          app: app,
          $state: $state,
          $timeout: $timeout
        });
      }

      it('should redirect to list if no home article is set', function () {
        // given
        var app = {
          settings: {
            home: undefined
          }
        };

        // when
        buildController(app);
        $timeout.flush();

        // then
        expect($state.go).toHaveBeenCalledWith('^.list', {}, {location: 'replace'});
      });

      it('should redirect to home article if set', function () {
        // given
        var app = {
          settings: {
            home: 'ARTICLE-ID'
          }
        };

        // when
        buildController(app);
        $timeout.flush();

        // then
        expect($state.go).toHaveBeenCalledWith('^.list.view', {id: 'ARTICLE-ID'}, {location: 'replace'});
      });

    });
  });

})();
