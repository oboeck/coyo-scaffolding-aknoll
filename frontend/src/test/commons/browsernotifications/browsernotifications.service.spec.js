(function () {
  'use strict';

  var moduleName = 'commons.browsernotifications';

  describe('module: ' + moduleName, function () {
    var browserNotificationsService;
    var $rootScope, $localStorage, $window, $timeout, $q;
    var authService, UserNotificationSettingModel, TimelineItemModel;

    var currentUser = {
      id: 'userId',
      hasGlobalPermissions: function () {
        return true;
      }
    };

    function _getObj(active, discussion, activity, post, message) {
      return {
        channel: 'BROWSER',
        active: active,
        properties: {
          notifications: {
            discussion: discussion,
            activity: activity,
            post: post,
            message: message
          }
        }
      };
    }

    function _getNotificationEvent(category) {
      return {
        content: {
          notification: {
            category: category,
            messageKey: 'messageKey',
            messageArguments: 'messageArguments',
            target: {id: 'someId'},
            author: {
              imageUrls: {
                avatar: '/api/some/url'
              }
            }
          }
        }
      };
    }

    function _getPostEvent(authorId) {
      return {
        content: {
          id: 'POST-ID',
          data: {
            message: 'A new post'
          },
          target: {id: 'someId'},
          author: {
            id: authorId,
            displayName: 'userName',
            imageUrls: {
              avatar: '/api/some/url'
            }
          }
        }
      };
    }

    function _getMessageChannel() {
      return {
        name: 'channelName'
      };
    }

    function _getMessageEvent(authorId) {
      return {
        content: {
          data: {
            message: 'A new post'
          },
          channelId: 'channelId',
          author: {
            id: authorId,
            displayName: 'userName',
            imageUrls: {
              avatar: '/api/some/url'
            }
          }
        }
      };
    }

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $window = jasmine.createSpyObj('$window', ['Notification']);
        $provide.value('$window', $window);
        authService =
            jasmine.createSpyObj('authService', ['isAuthenticated', 'getUser', 'logout', 'subscribeToUserUpdate']);
        authService.getUser = function () {
          return {
            then: function (callback) {
              return callback(currentUser);
            }
          };
        };
        $provide.value('authService', authService);
        $provide.value('targetService', jasmine.createSpyObj('targetService', ['go']));
        var backendUrlServiceMock = jasmine.createSpyObj('backendUrlService', ['getUrl']);
        backendUrlServiceMock.getUrl = function () {
          return 'https://coyo4.com';
        };
        $provide.value('backendUrlService', backendUrlServiceMock);
        $timeout = jasmine.createSpy();
        $provide.value('$timeout', $timeout);
        $timeout.cancel = function () {
        };
        $provide.value('ngxMarkdownService', jasmine.createSpyObj('ngxMarkdownService', ['strip']));
        var translateFn = function (key) {
          return {
            then: function (cb) {
              cb(key + '_TRANSLATED');
            }
          };
        };
        translateFn.storageKey = function () {
        };
        translateFn.storage = function () {
        };
        translateFn.preferredLanguage = function () {
        };
        translateFn.onReady = function () {
        };
        $provide.value('$translate', translateFn);
      });

      inject(
          function (_browserNotificationsService_, _$rootScope_, _$localStorage_, _$q_, _UserNotificationSettingModel_,
                    _TimelineItemModel_) {
            browserNotificationsService = _browserNotificationsService_;
            $rootScope = _$rootScope_;
            $localStorage = _$localStorage_;
            $q = _$q_;
            UserNotificationSettingModel = _UserNotificationSettingModel_;
            TimelineItemModel = _TimelineItemModel_;

            $rootScope.tabId = 'activeTabId';
            $localStorage.activeTabId = 'activeTabId';
          });
    });

    describe('service: browserNotificationsService', function () {

      it('should cache settings', function () {
        // given
        spyOn(UserNotificationSettingModel, 'query').and.returnValue($q.resolve());
        browserNotificationsService.getNotificationSettings();
        UserNotificationSettingModel.query.calls.reset();

        // when
        browserNotificationsService.getNotificationSettings();

        // then
        expect(UserNotificationSettingModel.query).not.toHaveBeenCalled();
      });

      function shouldClearCacheOn(e) {
        return function () {
          // given
          spyOn(UserNotificationSettingModel, 'query').and.returnValue($q.resolve());
          browserNotificationsService.getNotificationSettings();
          UserNotificationSettingModel.query.calls.reset();

          // when
          $rootScope.$emit(e);

          // then
          browserNotificationsService.getNotificationSettings();
          expect(UserNotificationSettingModel.query).toHaveBeenCalled();
        };
      }

      it('should clear cache on "backendUrlService:url:updated"', shouldClearCacheOn('backendUrlService:url:updated'));
      it('should clear cache on "backendUrlService:url:cleared"', shouldClearCacheOn('backendUrlService:url:cleared'));
      it('should clear cache on "authService:logout:success"', shouldClearCacheOn('authService:logout:success'));

      it('should check availability', function () {
        // given

        // when
        var available = browserNotificationsService.available();

        // then
        expect(available).toBeTrue();
      });

      it('should check whether is active (only active)', function () {
        // given
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(true, false, false, false, false)]);
          }
        });

        // when
        var active = browserNotificationsService.active(true, false, false, false, false);

        // then
        active.then(function (isActive) {
          expect(isActive).toBeTrue();
        });
      });

      it('should check whether is active (only discussion)', function () {
        // given
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(false, true, false, false, false)]);
          }
        });

        // when
        var active = browserNotificationsService.active(false, true, false, false, false);

        // then
        active.then(function (isActive) {
          expect(isActive).toBeTrue();
        });
      });

      it('should check whether is active (only activity)', function () {
        // given
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(false, false, true, false, false)]);
          }
        });

        // when
        var active = browserNotificationsService.active(false, false, true, false, false);

        // then
        active.then(function (isActive) {
          expect(isActive).toBeTrue();
        });
      });

      it('should check whether is active (only post)', function () {
        // given
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(false, false, false, true, false)]);
          }
        });

        // when
        var active = browserNotificationsService.active(false, false, false, true, false);

        // then
        active.then(function (isActive) {
          expect(isActive).toBeTrue();
        });
      });

      it('should check whether is active (only message)', function () {
        // given
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(false, false, false, false, true)]);
          }
        });

        // when
        var active = browserNotificationsService.active(false, false, false, false, true);

        // then
        active.then(function (isActive) {
          expect(isActive).toBeTrue();
        });
      });

      it('should check whether permissions have been granted (not granted)', function () {
        // given
        $window.Notification.permission = 'not granted';

        // when
        var permissionsGranted = browserNotificationsService.permissionGranted();

        // then
        expect(permissionsGranted).toBeFalse();
      });

      it('should check whether permissions have been granted (granted)', function () {
        // given
        $window.Notification.permission = 'granted';

        // when
        var permissionsGranted = browserNotificationsService.permissionGranted();

        // then
        expect(permissionsGranted).toBeTrue();
      });

      it('should check whether a permission request is needed (needed 1)', function () {
        // given
        $window.Notification.permission = 'not granted';
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(false, true, false, false, false)]);
          }
        });

        // when
        var permissionsGranted = browserNotificationsService.permissionRequestNeeded();

        // then
        permissionsGranted.then(function (granted) {
          expect(granted).toBeFalse();
        });
      });

      it('should check whether a permission request is needed (needed 2)', function () {
        // given
        $window.Notification.permission = 'granted';
        $localStorage.notificationSettings = [_getObj(false, true, false, false, false)];

        // when
        var permissionsGranted = browserNotificationsService.permissionRequestNeeded();
        $rootScope.$apply();

        // then
        permissionsGranted.then(function (granted) {
          expect(granted).toBeFalse();
        });
      });

      it('should check whether a permission request is needed (not needed 1)', function () {
        // given
        $window.Notification.permission = 'not granted';
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(true, false, false, false, false)]);
          }
        });

        // when
        var permissionsGranted = browserNotificationsService.permissionRequestNeeded();
        $rootScope.$apply();

        // then
        permissionsGranted.then(function (granted) {
          expect(granted).toBeTrue();
        });
      });

      it('should check whether a permission request is needed (not needed 2)', function () {
        $window.Notification.permission = 'granted';
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(false, true, false, false, false)]);
          }
        });
        // when
        var permissionsGranted = browserNotificationsService.permissionRequestNeeded();
        $rootScope.$apply();

        // then
        permissionsGranted.then(function (granted) {
          expect(granted).toBeFalse();
        });
      });

      it('should not request for browser notifications permissions if permissions already granted', function () {
        // given
        $window.Notification.permission = 'granted';

        // when
        var requestPermission = browserNotificationsService.requestPermission();
        $rootScope.$apply();

        // then
        requestPermission.then(function (result) {
          expect(result.result).toEqual({granted: 'granted'});
          expect(result.requested).toBeFalse();
        });
      });

      it('should request for browser notifications permissions if permissions not granted', function () {
        // given
        $window.Notification.permission = 'not granted';
        $window.Notification.requestPermission = function () {
          return {
            then: function (callback) {
              return callback('granted');
            }
          };
        };

        // when
        var requestPermission = browserNotificationsService.requestPermission();

        // then
        expect(requestPermission.result).toEqual('granted');
        expect(requestPermission.requested).toBeTrue();
      });

      it('should not notify in case of an event (discussion) if permissions not granted', function () {
        // given
        $window.Notification.permission = 'not granted';
        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getNotificationEvent('DISCUSSION');

        // when
        browserNotificationsService.notifyEvent(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should not notify in case of an event (discussion) if tab is not currently active tab', function () {
        // given
        $localStorage.activeTabId = 'activeTabId_other';
        $window.Notification.permission = 'not granted';
        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getNotificationEvent('DISCUSSION');

        // when
        browserNotificationsService.notifyEvent(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should not notify in case of an event (activity) if permissions not granted', function () {
        // given
        $window.Notification.permission = 'not granted';
        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getNotificationEvent('ACTIVITY');

        // when
        browserNotificationsService.notifyEvent(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should not notify in case of an event (activity) if tab is not currently active tab', function () {
        // given
        $localStorage.activeTabId = 'activeTabId_other';
        $window.Notification.permission = 'not granted';
        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getNotificationEvent('ACTIVITY');

        // when
        browserNotificationsService.notifyEvent(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should notify in case of an event (discussion)', function () {
        // given
        $window.Notification.permission = 'granted';
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(true, true, true, true, true)]);
          }
        });

        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getNotificationEvent('DISCUSSION');

        // when
        browserNotificationsService.notifyEvent(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).toHaveBeenCalled();
        expect($timeout).toHaveBeenCalled();
      });

      it('should notify in case of an event (activity)', function () {
        // given
        $window.Notification.permission = 'granted';
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(true, true, true, true, true)]);
          }
        });

        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getNotificationEvent('ACTIVITY');

        // when
        browserNotificationsService.notifyEvent(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).toHaveBeenCalled();
        expect($timeout).toHaveBeenCalled();
      });

      it('should not notify in case of a post if permissions not granted', function () {
        // given
        $window.Notification.permission = 'not granted';
        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getPostEvent('AUTHOR-ID');

        // when
        browserNotificationsService.notifyPost(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should not notify in case of a post if tab is not currently active tab', function () {
        // given
        $localStorage.activeTabId = 'activeTabId_other';
        $window.Notification.permission = 'not granted';
        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getPostEvent('AUTHOR-ID');

        // when
        browserNotificationsService.notifyPost(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should not notify in case of a post if current user is author', function () {
        // given
        $window.Notification.permission = 'granted';
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(true, true, true, true, true)]);
          }
        });

        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getPostEvent(currentUser.id);

        // when
        browserNotificationsService.notifyPost(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should not notify the current user in case he shares as functional user', function () {
        // given
        $window.Notification.permission = 'granted';
        $localStorage.notificationSettings = [_getObj(true, true, true, true, true)];

        spyOn(TimelineItemModel, 'get').and.returnValue($q.resolve({
          relevantShare: {
            originalAuthorId: currentUser.id,
            author: {id: 'workspace-id', displayName: 'workspace-name'}
          },
          target: 'someId'
        }));
        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });

        var event = {
          content: {
            id: 'POST-ID'
          }
        };

        // when
        browserNotificationsService.notifyPost(event);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should notify the user in case he is not the initiator of the share', function () {
        // given
        var translatedShareTitle = 'BROWSER.NOTIFICATION.SHARE.ITEM.LABEL_TRANSLATED';
        $window.Notification.permission = 'granted';
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(true, true, true, true, true)]);
          }
        });

        spyOn(TimelineItemModel, 'get').and.returnValue($q.resolve({
          relevantShare: {
            originalAuthorId: 'some-other-user-id',
            author: {id: 'workspace-id', displayName: 'workspace-name'}
          },
          target: 'someId'
        }));
        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });

        var event = {
          content: {
            id: 'POST-ID',
            data: {
              message: 'A new post '
            },
            author: {
              id: 'workspace-id',
              displayName: 'workspace-name',
              imageUrls: {
                avatar: '/api/some/url'
              }
            }
          }
        };

        // when
        browserNotificationsService.notifyPost(event);
        $rootScope.$apply();

        // then
        expect($window.Notification)
            .toHaveBeenCalledWith(translatedShareTitle, {body: undefined});
      });

      it('should notify in case of a post', function () {
        // given
        $window.Notification.permission = 'granted';
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(true, true, true, true, true)]);
          }
        });

        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        spyOn(TimelineItemModel, 'get').and.returnValue($q.resolve({author: {id: 'author-id'}}));
        var event = _getPostEvent('AUTHOR-ID');

        // when
        browserNotificationsService.notifyPost(event);
        $rootScope.$apply();

        // then
        expect(TimelineItemModel.get).toHaveBeenCalledWith('POST-ID', {
          timelineType: 'PERSONAL',
          senderId: '',
          _permissions: 'edit,delete,accessoriginalauthor,like,comment,share,sticky,actAsSender'
        });
        expect($window.Notification).toHaveBeenCalled();
        expect($timeout).toHaveBeenCalled();
      });

      it('should not notify in case of a message if permissions not granted', function () {
        // given
        $window.Notification.permission = 'not granted';
        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getMessageEvent('someId');
        var channel = _getMessageChannel();

        // when
        browserNotificationsService.notifyMessage(event, channel);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should not notify in case of a message if author is current user', function () {
        // given
        $window.Notification.permission = 'granted';
        var event = _getMessageEvent(currentUser.id);
        var channel = _getMessageChannel();

        // when
        browserNotificationsService.notifyMessage(event, channel);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should not notify in case of the author is not defined', function () {
        // given
        $window.Notification.permission = 'granted';
        var event = _getMessageEvent('someId');
        var channel = _getMessageChannel();

        event.content.author = undefined;

        // when
        browserNotificationsService.notifyMessage(event, channel);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should not notify in case of a message if tab is not currently active tab', function () {
        // given
        $localStorage.activeTabId = 'activeTabId_other';
        $window.Notification.permission = 'granted';
        var event = _getMessageEvent(currentUser.id);
        var channel = _getMessageChannel();

        // when
        browserNotificationsService.notifyMessage(event, channel);
        $rootScope.$apply();

        // then
        expect($window.Notification).not.toHaveBeenCalled();
      });

      it('should notify in case of a message', function () {
        // given
        $window.Notification.permission = 'granted';
        spyOn(UserNotificationSettingModel, 'query').and.returnValue({
          then: function () {
            return $q.resolve([_getObj(true, true, true, true, true)]);
          }
        });

        $window.Notification.and.returnValue({
          onclick: function () {
          },
          close: {
            bind: function () {
            }
          }
        });
        var event = _getMessageEvent('someId');
        var channel = _getMessageChannel();

        // when
        browserNotificationsService.notifyMessage(event, channel);
        $rootScope.$apply();

        // then
        expect($window.Notification).toHaveBeenCalled();
        expect($timeout).toHaveBeenCalled();
      });
    });
  });

})();
