(function () {
  'use strict';

  var moduleName = 'commons.error';

  describe('module: ' + moduleName, function () {

    var stateLockService, $window, $timeout;

    beforeEach(function () {

      module(moduleName, function ($provide) {
        $window = {};
        $provide.value('$window', $window);
        var translateFn = function (key) {
          return {
            then: function (cb) {
              cb(key + '_TRANSLATED');
            }
          };
        };
        translateFn.storageKey = function () {};
        translateFn.storage = function () {};
        translateFn.preferredLanguage = function () {};
        $provide.value('$translate', translateFn);
      });

      inject(function (_stateLockService_, _$timeout_) {
        stateLockService = _stateLockService_;
        $timeout = _$timeout_;
      });

    });

    describe('Service: stateLockService', function () {

      it('should lock', function () {
        var event = {
          returnValue: true
        };

        // when
        stateLockService.lock();

        // then
        expect(stateLockService.isLocked()).toBe(true);
        expect($window.onbeforeunload).toBeDefined();
        expect($window.onbeforeunload(event)).toBe('CONFIRMATION.LEAVE_STATE.TEXT_TRANSLATED');
        expect(event.returnValue).toBeDefined();
        expect(event.returnValue).toBeFalsy();
      });

      it('should unlock', function () {
        // given
        stateLockService.lock();

        // when
        stateLockService.unlock();

        // then
        expect(stateLockService.isLocked()).toBe(false);
        expect($window.onbeforeunload).not.toBeDefined();
      });

      it('should keep additional lock', function () {
        // given
        stateLockService.lock();
        stateLockService.lock();

        // when
        stateLockService.unlock();

        // then
        expect(stateLockService.isLocked()).toBe(true);
        expect($window.onbeforeunload).toBeDefined();
      });

      it('should handle over unlock', function () {
        // given
        stateLockService.lock();

        // when
        stateLockService.unlock();
        stateLockService.unlock();

        // then
        expect(stateLockService.isLocked()).toBe(false);
        expect($window.onbeforeunload).not.toBeDefined();
      });

      it('should unlock all', function () {
        // given
        stateLockService.lock();
        stateLockService.lock();

        // when
        stateLockService.unlockAll();

        // then
        expect(stateLockService.isLocked()).toBe(false);
        expect($window.onbeforeunload).not.toBeDefined();
      });

      it('should run code ignoring the lock', function () {
        // given
        stateLockService.lock();
        stateLockService.lock();

        // when
        var callbackCalled = false;
        var isLockedDuringCallback = null;
        stateLockService.ignoreLock(function () {
          isLockedDuringCallback = stateLockService.isLocked();
          callbackCalled = true;
        });

        // then
        expect(callbackCalled).toBe(true);
        expect(isLockedDuringCallback).toBe(false);
        expect(stateLockService.isLocked()).toBe(false);
        $timeout.flush();

        // verify 2x lock is restored
        expect(stateLockService.isLocked()).toBe(true);
        stateLockService.unlock();
        expect(stateLockService.isLocked()).toBe(true);
        stateLockService.unlock();
        expect(stateLockService.isLocked()).toBe(false);
      });

    });

  });
})();
