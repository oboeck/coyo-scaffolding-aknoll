(function () {
  'use strict';

  var moduleName = 'commons.i18n';

  describe('module: ' + moduleName, function () {
    var i18nService, $translate, $numeral, amMoment, $q, $rootScope, angularSpy;
    var bodyElementSpy = jasmine.createSpyObj('bodyElement', ['append']);

    beforeEach(function () {
      module(moduleName);

      inject(function (_i18nService_, _$translate_, _$numeral_, _amMoment_, _$q_, _$rootScope_) {
        i18nService = _i18nService_;
        $translate = _$translate_;
        $numeral = _$numeral_;
        amMoment = _amMoment_;
        $q = _$q_;
        $rootScope = _$rootScope_;
      });

      angularSpy = spyOn(angular, 'element').and.callFake(function (arg) {
        if (arg === 'body') {
          return bodyElementSpy;
        } else {
          return angularSpy.and.callThrough()(arg);
        }
      });
    });

    describe('service: i18nService', function () {
      it('should initialize', function () {
        // then
        expect(i18nService).toBeDefined();
      });

      it('should set interface language', function () {
        // given
        spyOn($translate, 'use').and.returnValue($q.when('de'));
        spyOn($numeral, 'locale');
        spyOn(amMoment, 'changeLocale').and.returnValue('de');

        // when
        i18nService.setInterfaceLanguage('de');
        $rootScope.$apply();

        // then
        expect($translate.use).toHaveBeenCalledWith('de');
        expect($numeral.locale).toHaveBeenCalledWith('de');
        expect(amMoment.changeLocale).toHaveBeenCalledTimes(1);
        expect(amMoment.changeLocale).toHaveBeenCalledWith('de');
        expect(bodyElementSpy.append).not.toHaveBeenCalled();
      });

      it('should use moment mapping for specific languages', function () {
        // given
        spyOn($translate, 'use').and.returnValue($q.when('hy'));
        spyOn($numeral, 'locale');
        spyOn(amMoment, 'changeLocale').and.returnValue('hy-am');

        // when
        i18nService.setInterfaceLanguage('hy');
        $rootScope.$apply();

        // then
        expect($translate.use).toHaveBeenCalledWith('hy');
        expect($numeral.locale).toHaveBeenCalledWith('hy');
        expect(amMoment.changeLocale).toHaveBeenCalledTimes(1);
        expect(amMoment.changeLocale).toHaveBeenCalledWith('hy-am');
      });

      it('should load new moment language file when it was not loaded yet', function () {
        // given
        spyOn($translate, 'use').and.returnValue($q.when('sv'));
        spyOn($numeral, 'locale');
        spyOn(amMoment, 'changeLocale').and.returnValue('de');

        // when
        i18nService.setInterfaceLanguage('sv');
        $rootScope.$apply();

        // then
        expect(amMoment.changeLocale).toHaveBeenCalledTimes(2);
        expect(amMoment.changeLocale.calls.all()[0].args[0]).toMatch(/sv/);
        expect(amMoment.changeLocale.calls.all()[1].args[0]).toMatch(/sv/);
        expect(bodyElementSpy.append).toHaveBeenCalledWith('<script type="application/javascript" src="/moment-locale/sv.js"></script>');
      });
    });
  });
})();
