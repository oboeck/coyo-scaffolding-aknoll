(function () {
  'use strict';

  var moduleName = 'commons.layout';

  describe('module: ' + moduleName, function () {

    var $rootScope, $controller, sidebarService, $scope;

    beforeEach(
        module(moduleName, function ($provide) {
          $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        })
    );

    beforeEach(inject(function (_$rootScope_, _$controller_) {
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $controller = _$controller_;
      sidebarService = jasmine.createSpyObj('sidebarService', ['register']);
    }));

    describe('directive: oyoc-admin-sidebar', function () {

      var controllerName = 'AdminSidebarController';

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          sidebarService: sidebarService
        });
      }

      describe('controller: ' + controllerName, function () {

        it('should register at service and be hidden on init', function () {
          // given
          var ctrl = buildController();

          // then
          expect(ctrl.showSidebar).toBeFalsy();
          expect(sidebarService.register).toHaveBeenCalled();

          var api = sidebarService.register.calls.mostRecent().args[0];
          expect(api.name).toBe('admin-menu');
        });

        it('should open the sidebar and handle backdrop', function () {
          // given
          var ctrl = buildController();

          // when
          var api = sidebarService.register.calls.mostRecent().args[0];
          api.open();

          // then
          expect(ctrl.showSidebar).toBeTruthy();
          expect($rootScope.showBackdrop).toBeTruthy();
        });

        it('should close the sidebar and handle backdrop', function () {
          // given
          var ctrl = buildController();

          // when
          var api = sidebarService.register.calls.mostRecent().args[0];
          api.close();

          // then
          expect(ctrl.showSidebar).toBeFalsy();
          expect($rootScope.showBackdrop).toBeFalsy();
        });

      });
    });
  });
})();
