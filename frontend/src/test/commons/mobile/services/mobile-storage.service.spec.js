(function () {
  'use strict';

  var moduleName = 'commons.mobile';
  var targetName = 'mobileStorageService';

  describe('module: ' + moduleName, function () {

    var mobileStorageService, $localStorage, mobileServiceConfig;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        mobileServiceConfig = {storageKeyPrefix: '__mobile_'};
        $provide.value('mobileServiceConfig', mobileServiceConfig);
        $localStorage = function () {
          return {};
        };
        $provide.value('$localStorage', $localStorage);
      });

      inject(function (_mobileStorageService_) {
        mobileStorageService = _mobileStorageService_;
      });
    });

    describe('service: ' + targetName, function () {

      it('should save and read data with prefixed keys', function () {
        // given
        var data = {key: 'data-key', value: 'data-value'};

        // when
        mobileStorageService.set(data.key, data.value);

        // then
        var prefixedKey = mobileServiceConfig.storageKeyPrefix + data.key;
        expect($localStorage[prefixedKey]).toBe(data.value);
      });

    });
  });
})();
