(function () {
  'use strict';

  var moduleName = 'commons.oembed';

  describe('module: ' + moduleName, function () {
    var oembedVideoService, container, VideoInfoModel, $q, $rootScope;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.value('VideoInfoModel', VideoInfoModel);
      });

      VideoInfoModel = jasmine.createSpyObj('VideoInfoModel', ['generateVideoInfo']);

      container = {
        'clientWidth': 450
      };

      inject(function (_oembedVideoService_, _$q_, _$rootScope_) {
        oembedVideoService = _oembedVideoService_;
        $q = _$q_;
        $rootScope = _$rootScope_;
      });
    });

    describe('service: oembedVideoService', function () {
      it('Match youtube video by url pattern', function () {
        // given
        var url = 'https://www.youtube.com/watch?v=Bh4x2jCWsB4';
        var result = '<iframe webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" src="//www.youtube-nocookie.com/embed/Bh4x2jCWsB4" width="450" height="252.00000000000003"></iframe>';

        // when
        var html = oembedVideoService.createByUrl(url, container);

        // then
        expect(html[0].outerHTML).toBe(result);
      });

      it('Match vimeo video by url pattern', function () {
        // given
        var url = 'https://vimeo.com/213100067';
        var result = '<iframe webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" src="//player.vimeo.com/video/213100067" width="450" height="252.00000000000003"></iframe>';

        // when
        var html = oembedVideoService.createByUrl(url, container);

        // then
        expect(html[0].outerHTML).toBe(result);
      });

      it('Match mp4 video by url pattern', function () {
        // given
        var url = 'https://www.seitmitvideo.com/video.mp4';
        var result = '<video controls="" src="https://www.seitmitvideo.com/video.mp4" width="450" height="252.00000000000003"></video>';

        // when
        var html = oembedVideoService.createByUrl(url, container);

        // then
        expect(html[0].outerHTML).toBe(result);
      });

      it('should match video url by backend', function () {
        // given
        var url = 'https://vimeo.com/213100067';
        VideoInfoModel.generateVideoInfo.and.returnValue($q.resolve({
          videoUrl: url,
          width: 1920,
          height: 1080
        }));

        // when
        var result = oembedVideoService.isVideoUrl(url);

        // then
        expect(result).toBe(true);
      });

      it('should match video url by regex', function () {
        // given
        var url = 'https://vimeo.com/213100067';
        VideoInfoModel.generateVideoInfo.and.returnValue($q.reject());

        // when
        var result = oembedVideoService.isVideoUrl(url);

        // then
        expect(result).toBe(true);
      });

      describe('get embed code', function () {
        var videoInfo = {
          videoUrl: 'http://test',
          width: 1920,
          height: 1080
        };
        var videoInfoYoutube = {
          videoUrl: 'http://www.youtube.com/embedsomething',
          width: 1920,
          height: 1080
        };

        it('should get embed code with video info from backend', function () {
          // given
          var embedCodeResult = undefined,
              url = 'http://test';
          VideoInfoModel.generateVideoInfo.and.returnValue($q.resolve(videoInfo));

          // when
          oembedVideoService.getEmbedCode(url, container).then(function (response) {
            embedCodeResult = response;
          });
          $rootScope.$apply();

          // then
          expect(embedCodeResult.prop('tagName')).toBe('IFRAME');
        });

        it('should replace youtube with nocookie version in embed code', function () {
          // given
          var embedCodeResult = undefined,
              url = 'http://test';
          VideoInfoModel.generateVideoInfo.and.returnValue($q.resolve(videoInfoYoutube));

          // when
          oembedVideoService.getEmbedCode(url, container).then(function (response) {
            embedCodeResult = response;
          });
          $rootScope.$apply();

          // then
          expect(embedCodeResult.prop('src')).toBe('http://www.youtube-nocookie.com/embedsomething');
        });

        it('should get embed code from fallback on error', function () {
          // given
          var embedCodeResult = undefined,
              url = 'https://www.youtube.com/watch?v=Bh4x2jCWsB4';
          VideoInfoModel.generateVideoInfo.and.returnValue($q.reject());

          // when
          oembedVideoService.getEmbedCode(url, container).then(function (response) {
            embedCodeResult = response;
          });
          $rootScope.$apply();

          // then
          expect(embedCodeResult.prop('tagName')).toBe('IFRAME');
        });
      });

      it('should calculate height percentage', function () {
        // given
        var videoEmbedCode = angular.element('<iframe width="1920" height="1080">');

        // when
        var heightPercentage = oembedVideoService.getHeightPercentage(videoEmbedCode);

        // then
        expect(heightPercentage).toBe(57);
      });
    });
  });
})();
