(function () {
  'use strict';

  var moduleName = 'commons.resource';
  var targetName = 'backendUrlService';

  describe('module: ' + moduleName, function () {
    var backendUrlService;

    var staticBackendUrl = 'http://static:8000';

    beforeEach(function () {
      module(moduleName, function (_coyoConfig_) {
        _coyoConfig_.backendUrl = staticBackendUrl;
      });

      inject(function (_backendUrlService_) {
        backendUrlService = _backendUrlService_;
      });
    });

    it('should be registered', function () {
      expect(backendUrlService).not.toBeUndefined();
    });

    describe('service: ' + targetName, function () {

      it('should apply static strategy', function () {
        // given

        // when
        backendUrlService.setStrategy('static');

        // then
        expect(backendUrlService.getUrl()).toEqual(staticBackendUrl);
      });

      it('should apply relative strategy', function () {
        // given

        // when
        backendUrlService.setStrategy('relative');

        // then
        expect(backendUrlService.getUrl()).toEqual('');
      });
    });
  });
})();
