(function () {
  'use strict';

  var moduleName = 'commons.auth';

  describe('module: ' + moduleName, function () {
    var $httpBackend, $http, backendUrlService, $localStorage, utilService;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $localStorage = {};
        backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl', 'isSet']);
        backendUrlService.getUrl.and.returnValue('');
        utilService = jasmine.createSpyObj('utilService', ['uuid']);
        $provide.value('backendUrlService', backendUrlService);
        $provide.value('$localStorage', $localStorage);
        $provide.value('utilService', utilService);
      });

      inject(function (_$httpBackend_, _$http_) {
        $http = _$http_;
        $httpBackend = _$httpBackend_;
      });
    });

    describe('http request interceptor', function () {

      it('should add new client ID to request', function () {
        // given
        backendUrlService.isSet.and.returnValue(true);
        utilService.uuid.and.returnValue('my-uuid');
        var url = '/web/foo';
        $httpBackend.expectGET(url, function (data) {
          var header = data['X-Coyo-Client-ID'];
          expect(header).toBe('my-uuid');
          return true;
        }).respond(200, 'response');

        // when
        $http.get(url);
        $httpBackend.flush();
      });

      it('should add existing client ID to request', function () {
        // given
        utilService.uuid.calls.reset();
        backendUrlService.isSet.and.returnValue(true);
        $localStorage.clientId = 'existing-uuid';
        var url = '/web/foo';
        $httpBackend.expectGET(url, function (data) {
          var header = data['X-Coyo-Client-ID'];
          expect(header).toBe('existing-uuid');
          return true;
        }).respond(200, 'response');

        // when
        $http.get(url);
        $httpBackend.flush();

        // then
        expect(utilService.uuid).not.toHaveBeenCalled();
      });

    });
  });
}());
