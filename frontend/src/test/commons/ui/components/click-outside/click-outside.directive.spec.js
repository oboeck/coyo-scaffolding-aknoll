(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'clickOutside';

  describe('module: ' + moduleName, function () {

    var scope, element, onSpy, offSpy, $timeout;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function ($rootScope, $compile, $document, _$timeout_) {
        offSpy = jasmine.createSpy();
        offSpy.and.callThrough();
        onSpy = jasmine.createSpy();
        onSpy.and.callThrough();
        $document.on = onSpy;
        $document.off = offSpy;
        $timeout = _$timeout_;

        scope = $rootScope.$new();
        element = angular.element('<input click-outside="">');
        $compile(element)(scope);
        scope.$digest();
      }));

      it('should not add handler after scope is destroyed', function () {
        // given
        scope.$destroy();

        // when
        $timeout.flush();

        // then
        expect(onSpy).not.toHaveBeenCalled();
      });

      it('should add handler and deactivate it after scope destroy', function () {
        // given

        // when
        $timeout.flush();
        scope.$destroy();

        // then
        expect(onSpy).toHaveBeenCalledWith('click', jasmine.any(Function));
        expect(onSpy).toHaveBeenCalledBefore(offSpy);
        expect(offSpy).toHaveBeenCalledWith('click', jasmine.any(Function));
      });
    });

  });
})();
