(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $timeout, rootScope;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, $rootScope, _$timeout_) {
      $controller = _$controller_;
      $scope = $rootScope.$new();
      $timeout = _$timeout_;
      rootScope = $rootScope;
    }));

    var controllerName = 'ContentSliderController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $timeout: $timeout
        });
      }

      it('should destroy swiper on destroy of scope', function () {
        // given
        var ctrl = buildController();
        var swiper = jasmine.createSpyObj('Swiper', ['destroy']);
        ctrl.swiper = swiper;

        // when
        $scope.$destroy();

        // then
        expect(swiper.destroy).toHaveBeenCalled();
        expect(ctrl.swiper).toBeFalsy();
      });

      it('should destroy swiper when another swiper is initialized', function () {
        // given
        var ctrl = buildController();
        var swiper = jasmine.createSpyObj('Swiper', ['destroy']);
        var swiperNew = jasmine.createSpyObj('SwiperNew', ['destroy']);
        ctrl.swiper = swiper;

        // when
        ctrl.onReadySwiper(swiperNew);

        // then
        expect(swiper.destroy).toHaveBeenCalled();
        expect(swiperNew.destroy).not.toHaveBeenCalled();
        expect(ctrl.swiper).toBe(swiperNew);
      });

      it('should update swiper if messaging bar has been toggled', function () {
        // given
        var ctrl = buildController();
        var swiper = jasmine.createSpyObj('Swiper', ['toggleMessagingSidebar', 'update']);
        ctrl.swiper = swiper;

        // when
        rootScope.$broadcast('toggleMessagingSidebar', swiper);

        // then
        $timeout(function () {
          expect(swiper.update()).toHaveBeenCalledWith(true);
        });
      });
    });
  });

})();
