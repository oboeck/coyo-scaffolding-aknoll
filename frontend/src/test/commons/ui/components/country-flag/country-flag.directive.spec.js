(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'CountryFlagController';

  describe('module: ' + moduleName, function () {
    var $controller, $filter, $q, $scope;
    var SettingsModel;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$filter_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $filter = _$filter_;
        $q = _$q_;
      });

      SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieveByKey']);
    });

    function buildController() {
      return $controller(controllerName, {
        $filter: $filter,
        SettingsModel: SettingsModel
      });
    }

    describe('controller: ' + controllerName, function () {
      it('should init the controller with deactivated multi language support', function () {
        // given
        var ctrl = buildController();
        ctrl.dependentToTranslations = true;

        // when
        SettingsModel.retrieveByKey.and.returnValue($q.resolve('false'));
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.enabled).toBe(false);
      });

      it('should init the controller with activated multi language support', function () {
        // given
        var ctrl = buildController();
        ctrl.dependentToTranslations = true;

        // when
        SettingsModel.retrieveByKey.and.returnValue($q.resolve('false'));
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.enabled).toBe(false);
      });

      it('should init the controller with activated multi language support due dependent translations', function () {
        // given
        var ctrl = buildController();
        ctrl.dependentToTranslations = false;

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.enabled).toBe(true);
        expect(SettingsModel.retrieveByKey).not.toHaveBeenCalled();
      });

      it('getCountry() should resolve en language key to gb', function () {
        // given
        var ctrl = buildController();
        ctrl.country = 'en';

        // when
        var language = ctrl.getCountry();

        // then
        expect(language).toBe('gb');
      });

      it('getCountry() should resolve DE language key to lowercase de', function () {
        // given
        var ctrl = buildController();
        ctrl.country = 'DE';

        // when
        var language = ctrl.getCountry();

        // then
        expect(language).toBe('de');
      });

    });
  });
})();
