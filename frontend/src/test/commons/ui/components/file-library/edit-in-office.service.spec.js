(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'editInOfficeService';

  describe('module: ' + moduleName, function () {

    var editInOfficeService, $rootScope, backendUrlService, $http, $q, $window, modalService;

    beforeEach(function () {

      module(moduleName);

      inject(function (_editInOfficeService_, _$rootScope_, _$http_, _$q_, _$window_, _backendUrlService_, _modalService_) {
        editInOfficeService = _editInOfficeService_;
        $rootScope = _$rootScope_;
        $http = _$http_;
        $q = _$q_;
        $window = _$window_;
        backendUrlService = _backendUrlService_;
        modalService = _modalService_;
      });
    });

    describe('Service: ' + targetName, function () {

      it('should open correct url', function () {
        // given
        var backendUrl = 'https://backend.coyoapp.com';
        var file = {
          id: 'file-id',
          senderId: 'sender-id',
          name: 'test',
          extension: 'docx'
        };
        var accessTokenResponse = {
          data: {
            accessCode: '1234567'
          }
        };

        spyOn($http, 'post').and.returnValue($q.when(accessTokenResponse));
        spyOn(backendUrlService, 'getUrl').and.returnValue(backendUrl);
        spyOn($window, 'open').and.callFake(angular.noop);
        spyOn(modalService, 'note').and.callThrough();

        // when
        editInOfficeService.editInOffice(file);
        $rootScope.$apply();

        // then
        var expectedUri = encodeURI('ms-word:ofe|u|' + backendUrl + '/webdav/1234567/sender-id/file-id/test');
        expect($window.open).toHaveBeenCalledWith(expectedUri, '_self');
        expect(modalService.note).toHaveBeenCalled();
      });

      it('should display fail modal', function () {
        // given
        var backendUrl = 'https://backend.coyoapp.com';
        var file = {
          id: 'file-id',
          senderId: 'sender-id',
          name: 'test',
          extension: 'docx'
        };
        var accessTokenResponse = {
          data: {
            accessCode: '1234567'
          }
        };

        spyOn($http, 'post').and.returnValue($q.when(accessTokenResponse));
        spyOn(backendUrlService, 'getUrl').and.returnValue(backendUrl);
        spyOn($window, 'open').and.callFake(angular.noop);
        spyOn(modalService, 'note').and.callThrough();
        var realDefer = $q.defer;
        spyOn($q, 'defer').and.callFake(function () {
          var defer = realDefer();
          defer.reject();
          return defer;
        });

        // when
        editInOfficeService.editInOffice(file);
        $rootScope.$apply();

        // then
        expect(modalService.note).toHaveBeenCalledWith({
          title: 'FILE_LIBRARY.EDIT_IN_OFFICE_FAILED.TITLE',
          text: 'FILE_LIBRARY.EDIT_IN_OFFICE_FAILED.TEXT'
        });

      });
    });
  });
})();
