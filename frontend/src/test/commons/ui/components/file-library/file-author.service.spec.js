(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'fileAuthorService';

  describe('module: ' + moduleName, function () {

    var fileAuthorService, FileAuthorModel, $rootScope;

    beforeEach(function () {

      module(moduleName);

      inject(function (_fileAuthorService_, _FileAuthorModel_, _$rootScope_) {
        fileAuthorService = _fileAuthorService_;
        FileAuthorModel = _FileAuthorModel_;
        $rootScope = _$rootScope_;
      });
    });

    describe('Service: ' + targetName, function () {

      it('should have method', function () {
        expect(fileAuthorService.loadFileAuthors).toBeDefined();
        expect(fileAuthorService.loadVersionAuthors).toBeDefined();
      });

      it('should return file authors', function () {
        // given
        var senderId = 'anySenderId';
        var appOrSlugId = 'anyAppOrSlugId';
        var fileIds = ['anyId', 'anotherId'];
        var showAuthors = true;

        var fileAuthors = {
          'any': {anyKey: 'anyContent'}, 'anotherId': {anotherKey: 'anotherContent'}
        };

        spyOn(FileAuthorModel, 'getFileAuthors').and.returnValue(fileAuthors);

        // when
        var promise = fileAuthorService.loadFileAuthors(senderId, appOrSlugId, fileIds, showAuthors);
        $rootScope.$apply();

        // then
        expect(FileAuthorModel.getFileAuthors).toHaveBeenCalledWith(senderId, appOrSlugId, fileIds);
        promise.then(function (authors) {
          expect(authors).toEqual(fileAuthors);
        });
      });

      it('should not call the backend for authors when showAuthors is false', function () {
        // given
        var senderId = 'anySenderId';
        var appOrSlugId = 'anyAppOrSlugId';
        var fileIds = ['anyId', 'anotherId'];
        var showAuthors = false;

        spyOn(FileAuthorModel, 'getFileAuthors');

        // when
        fileAuthorService.loadFileAuthors(senderId, appOrSlugId, fileIds, showAuthors);
        $rootScope.$apply();

        // then
        expect(FileAuthorModel.getFileAuthors).not.toHaveBeenCalled();
      });

      it('should return version authors', function () {
        // given
        var senderId = 'senderId';
        var fileId = 'fileId';
        var appIdOrSlug = 'documents';
        var showAuthors = true;
        var canAccessSender = true;

        var versionAuthors = {
          'anyVersionId': {displayName: 'anyName'}
        };

        spyOn(FileAuthorModel, 'getVersionAuthors').and.returnValue(versionAuthors);

        // when
        var promise = fileAuthorService.loadVersionAuthors(senderId, fileId, appIdOrSlug, showAuthors, canAccessSender);
        $rootScope.$apply();

        // then
        expect(FileAuthorModel.getVersionAuthors).toHaveBeenCalledWith(senderId, appIdOrSlug, fileId);
        promise.then(function (authors) {
          expect(authors).toBe(versionAuthors);
        });
      });

      it('should not call the backend when canManageSender is false and showAuthors is false', function () {
        // given
        var senderId = 'senderId';
        var fileId = 'fileId';
        var appIdOrSlug = 'documents';
        var showAuthors = false;
        var canAccessSender = false;

        spyOn(FileAuthorModel, 'getVersionAuthors');

        // when
        fileAuthorService.loadVersionAuthors(senderId, fileId, appIdOrSlug, showAuthors, canAccessSender);
        $rootScope.$apply();

        // then
        expect(FileAuthorModel.getVersionAuthors).not.toHaveBeenCalled();
      });

      it('should call the backend when canManageSender is false but showAuthors is true', function () {
        // given
        var senderId = 'senderId';
        var fileId = 'fileId';
        var appIdOrSlug = 'documents';
        var showAuthors = true;
        var canAccessSender = false;

        spyOn(FileAuthorModel, 'getVersionAuthors');

        // when
        fileAuthorService.loadVersionAuthors(senderId, fileId, appIdOrSlug, showAuthors, canAccessSender);
        $rootScope.$apply();

        // then
        expect(FileAuthorModel.getVersionAuthors).toHaveBeenCalled();
      });

      it('should call the backend when canManageSender is true but showAuthors is false', function () {
        // given
        var senderId = 'senderId';
        var fileId = 'fileId';
        var appIdOrSlug = 'documents';
        var showAuthors = false;
        var canAccessSender = true;

        spyOn(FileAuthorModel, 'getVersionAuthors');

        // when
        fileAuthorService.loadVersionAuthors(senderId, fileId, appIdOrSlug, showAuthors, canAccessSender);
        $rootScope.$apply();

        // then
        expect(FileAuthorModel.getVersionAuthors).toHaveBeenCalled();
      });
    });
  });
})();
