(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'fileLibraryService';

  describe('module: ' + moduleName, function () {

    var $q, fileLibraryModalService, modalService;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$q_, _fileLibraryModalService_, _modalService_) {
      $q = _$q_;
      fileLibraryModalService = _fileLibraryModalService_;
      modalService = _modalService_;
    }));

    describe('service: ' + targetName, function () {

      it('should have loaded the service', function () {
        expect(fileLibraryModalService).not.toBeUndefined();
      });

      it('should open modal', function () {
        // given
        spyOn(modalService, 'open');
        modalService.open.and.returnValue($q.resolve({result: {}}));

        // when
        fileLibraryModalService.open();

        // then
        expect(modalService.open).toHaveBeenCalled();
      });
    });
  });
})();
