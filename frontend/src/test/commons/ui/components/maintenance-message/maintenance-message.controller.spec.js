(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $rootScope, SettingsModel, $q, authService, $timeout;
    var message = {
      id: 'messageId',
      duration: 10000,
      text: 'my maintenance message'
    };

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _$timeout_) {
      $controller = _$controller_;
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieveByKey']);
      $q = _$q_;
      $timeout = _$timeout_;
      authService = jasmine.createSpyObj('authService', ['getUser']);

      SettingsModel.retrieveByKey.and.callFake(function (key) {
        var settingsValue = null;
        switch (key) {
          case 'maintenanceMessage':
            settingsValue = message.text;
            break;
          case 'maintenanceMessageOnlyAdmins':
            settingsValue = true;
            break;
          case 'maintenanceMessageEndDate':
            settingsValue = Date.now() + message.duration;
            break;
          case 'maintenanceMessageId':
            settingsValue = message.id;
            break;
        }
        return $q.resolve(settingsValue);
      });

      var user = jasmine.createSpyObj('UserModel', ['hasGlobalPermissions']);
      user.hasGlobalPermissions.and.callFake(function (permission) {
        return permission === 'P1';
      });
      authService.getUser.and.returnValue($q.resolve(user));
    }));

    var controllerName = 'MaintenanceMessageController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $rootScope: $rootScope,
          adminStates: [{globalPermission: 'P1'}],
          SettingsModel: SettingsModel,
          authService: authService
        });
      }

      it('should init with message', function () {
        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.display).toBe(true);
        expect(ctrl.message.message).toBe(message.text);
        expect(ctrl.message.id).toBe(message.id);
      });

      it('should remove expired message', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();
        $timeout.flush(message.duration - 10);
        expect(ctrl.display).toBe(true);

        // when
        $timeout.flush(10);
        $scope.$apply();

        // then
        expect(ctrl.display).toBe(false);
      });

    });
  });

})();
