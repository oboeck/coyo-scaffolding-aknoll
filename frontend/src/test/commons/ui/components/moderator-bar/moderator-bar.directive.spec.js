(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'oyoc-moderator-bar';

  describe('module: ' + moduleName, function () {

    var $scope, $localStorage, $compile, $controller, $q, $state;
    var template, authService;

    var user = {
      moderatorMode: true,
      hasGlobalPermissions: function () {
        return true;
      },
      setModeratorMode: function () {}
    };

    beforeEach(module('commons.templates'));
    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {

        // Mocks
        authService = jasmine.createSpyObj('authService', ['getUser', 'isAuthenticated', 'subscribeToUserUpdate',
          'unsubscribeFromUserUpdate', 'getCurrentUserId']);
        authService.getCurrentUserId.and.returnValue('user-id');
        authService.getUser = function () {
          return {
            then: function (callback) {
              callback(user);
            }
          };
        };

        $localStorage = {
          messagingSidebar: {
            compact: false
          }
        };

        module(function ($provide) {
          $provide.value('authService', authService);
          $provide.value('$localStorage', $localStorage);
        });

        inject(function (_$rootScope_, _$compile_, _$controller_, _$q_) {
          $scope = _$rootScope_.$new();
          $compile = _$compile_;
          $controller = _$controller_;
          $q = _$q_;
          template = '<oyoc-moderator-bar></oyoc-moderator-bar>';

          $state = jasmine.createSpyObj('$state', ['reload']);
        });
      });

      describe('element: ' + directiveName, function () {
        it('should be displayed if moderator mode active', function () {
          // given
          user.moderatorMode = true;

          // when
          var element = $compile(template)($scope);
          $scope.$digest();

          // then
          var moderatorBar = element.find('.moderator-bar');
          expect(moderatorBar.length).toBe(1);
          expect(moderatorBar.hasClass('ng-hide')).toBe(false);
        });

        it('should be hidden if moderator mode inactive', function () {
          // given
          user.moderatorMode = false;

          // when
          var element = $compile(template)($scope);
          $scope.$digest();

          // then
          var moderatorBar = element.find('.moderator-bar');
          expect(moderatorBar.length).toBe(1);
          expect(moderatorBar.hasClass('ng-hide')).toBe(true);
        });
      });

      describe('controller: ' + directiveName, function () {

        function buildController() {
          var ctrl = $controller('moderatorBarController', {
            authService: authService,
            $scope: $scope,
            $state: $state,
            $localStorage: $localStorage
          }, $scope);
          return ctrl;
        }

        it('should reload when moderator mode deactivated', function () {
        // given
          user.moderatorMode = true;
          var ctrl = buildController();
          $scope.$apply();
          spyOn(user, 'setModeratorMode').and.returnValue($q.resolve());

          // when
          ctrl.deactivate();
          $scope.$apply();

          // then
          expect(authService.unsubscribeFromUserUpdate).toHaveBeenCalled();
          expect(user.setModeratorMode).toHaveBeenCalledWith(false);
          expect($state.reload).toHaveBeenCalled();
        });

        it('should manage subscription to user update when moderator mode deactivate fails', function () {
          // given
          user.moderatorMode = true;
          var ctrl = buildController();
          $scope.$apply();
          spyOn(user, 'setModeratorMode').and.returnValue($q.reject());

          // when
          ctrl.deactivate();
          $scope.$apply();

          // then
          expect(authService.unsubscribeFromUserUpdate).toHaveBeenCalled();
          expect(user.setModeratorMode).toHaveBeenCalledWith(false);
          expect(authService.subscribeToUserUpdate).toHaveBeenCalled();
          expect($state.reload).not.toHaveBeenCalled();
        });
      });

    });
  });
})();
