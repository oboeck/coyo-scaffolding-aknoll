(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'oyoc-moderator-navbar-item';

  describe('module: ' + moduleName, function () {

    var $scope, $compile, $injector, $controller, $state, $q, template, authService, user,
        etagCacheService, ngxEtagCacheService;

    beforeEach(module('commons.templates'));
    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function ($rootScope, _$compile_, _$controller_, _$q_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $controller = _$controller_;
        $q = _$q_;
        template = '<oyoc-moderator-navbar-item user="user"></oyoc-moderator-navbar-item>';

        // Mocks
        user = {
          moderatorMode: true,
          setModeratorMode: function () {}
        };
        authService = jasmine.createSpyObj('authService', ['getUser', 'subscribeToUserUpdate', 'unsubscribeFromUserUpdate']);
        $state = jasmine.createSpyObj('$state', ['reload']);
        etagCacheService = jasmine.createSpyObj('etagCacheService', ['clearAll']);
        ngxEtagCacheService = jasmine.createSpyObj('ngxEtagCacheService', ['clearAll']);
        $injector = jasmine.createSpyObj('$injector', ['get']);
        $injector.get.and.returnValue(ngxEtagCacheService);

      }));

      function buildController(moderatorMode) {
        user.moderatorMode = moderatorMode;
        var ctrl = $controller('ModeratorNavbarItemController', {
          authService: authService,
          $scope: $scope,
          $state: $state,
          $injector: $injector,
          etagCacheService: etagCacheService
        }, $scope);
        ctrl.user = user;
        return ctrl;
      }

      it('should be able to enable moderator mode when user has moderator permission', function () {
        // given
        $scope.user = {
          moderatorMode: false
        };

        // when
        var element = $compile(template)($scope);
        $scope.$digest();

        // then
        expect(element.hasClass('moderator-navbar-item')).toBe(true);
        expect(element.hasClass('ng-hide')).toBe(false);
        expect(element.find('i.zmdi-eye').length).toBe(1);
        expect(element.find('i.zmdi-eye-off').length).toBe(0);
      });

      it('should be able to disable moderator mode when user is in moderator mode', function () {
        // given
        $scope.user = {
          moderatorMode: true
        };

        // when
        var element = $compile(template)($scope);
        $scope.$digest();

        // then
        expect(element.hasClass('moderator-navbar-item')).toBe(true);
        expect(element.hasClass('ng-hide')).toBe(false);
        expect(element.find('i.zmdi-eye').length).toBe(0);
        expect(element.find('i.zmdi-eye-off').length).toBe(1);
      });

      it('should reload when moderator mode activated', function () {
        // given
        var ctrl = buildController(false);
        spyOn(user, 'setModeratorMode').and.returnValue($q.resolve());

        // when
        ctrl.toggleModeratorMode();
        $scope.$apply();

        // then
        expect(authService.unsubscribeFromUserUpdate).toHaveBeenCalled();
        expect(authService.getUser).toHaveBeenCalled();
        expect(user.setModeratorMode).toHaveBeenCalledWith(true);
        expect(etagCacheService.clearAll).toHaveBeenCalled();
        expect(ngxEtagCacheService.clearAll).toHaveBeenCalled();
        expect($state.reload).toHaveBeenCalled();
      });

      it('should reload when moderator mode deactivated', function () {
        // given
        var ctrl = buildController(true);
        authService.getUser.and.returnValue($q.resolve());
        spyOn(user, 'setModeratorMode').and.returnValue($q.resolve());

        // when
        ctrl.toggleModeratorMode();
        $scope.$apply();

        // then
        expect(authService.unsubscribeFromUserUpdate).toHaveBeenCalled();
        expect(authService.getUser).toHaveBeenCalled();
        expect(user.setModeratorMode).toHaveBeenCalledWith(false);
        expect(etagCacheService.clearAll).toHaveBeenCalled();
        expect(ngxEtagCacheService.clearAll).toHaveBeenCalled();
        expect($state.reload).toHaveBeenCalled();
      });

      it('should manage subscription to user update when moderator mode activation fails', function () {
        // given
        var ctrl = buildController(false);
        authService.getUser.and.returnValue($q.resolve());
        spyOn(user, 'setModeratorMode').and.returnValue($q.reject());

        // when
        ctrl.toggleModeratorMode();
        $scope.$apply();

        // then
        expect(authService.unsubscribeFromUserUpdate).toHaveBeenCalled();
        expect(user.setModeratorMode).toHaveBeenCalledWith(true);
        expect(authService.subscribeToUserUpdate).toHaveBeenCalled();
        expect($state.reload).not.toHaveBeenCalled();
      });

    });

  });
})();
