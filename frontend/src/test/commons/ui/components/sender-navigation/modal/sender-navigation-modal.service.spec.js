(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $scope, senderNavigationModalService, modalService, sender, apps, currentApp;

    beforeEach(function () {

      sender = {
        id: 'SENDER-TEST-ID'
      };
      apps = [{
        id: 'APP-TEST-ID-1'
      }, {
        id: 'APP-TEST-ID-2'
      }, {
        id: 'APP-TEST-ID-3'
      }, {
        id: 'APP-TEST-ID-4'
      }];
      currentApp = {
        id: 'APP-TEST-ID-1'
      };

      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.returnValue({
        result: {
          then: function (callback) {
            return callback('success-or-error-message');
          }
        }
      });

      module(moduleName, function ($provide) {
        $provide.value('modalService', modalService);
      });

      inject(function (_senderNavigationModalService_, $rootScope) {
        $scope = $rootScope.$new();
        senderNavigationModalService = _senderNavigationModalService_;
      });

    });

    describe('service: senderNavigationModalService', function () {

      it('should open sender navigation and options modal and pass the necessary objects', function () {
        // given - sender, apps, currentApp

        // when
        senderNavigationModalService.open(sender, apps, currentApp);
        $scope.$apply();

        // then
        expect(modalService.open).toHaveBeenCalled();

        var args = modalService.open.calls.mostRecent().args;
        // check that a copy is created but not the actual object is returned
        expect(args[0].resolve.sender()).toEqual(sender);
        expect(args[0].resolve.sender()).toBe(sender);
        expect(args[0].resolve.apps()).toEqual(apps);
        expect(args[0].resolve.apps()).toBe(apps);
        expect(args[0].resolve.currentApp()).toEqual(currentApp);
        expect(args[0].resolve.currentApp()).toBe(currentApp);
      });

    });
  });

})();
