(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'senderNavigationUpdateService';

  describe('module: ' + moduleName, function () {

    var senderNavigationUpdateService, senderModel;

    beforeEach(function () {
      senderModel = jasmine.createSpyObj('senderModel', ['updateNavigation', 'navigationPreUpdatePreparation']);
      module(moduleName, function ($provide) {
        $provide.value('senderModel', senderModel);
        $provide.value('SenderModel', function () {
          return senderModel;
        });
      });
      inject(function (_senderNavigationUpdateService_) {
        senderNavigationUpdateService = _senderNavigationUpdateService_;
      });
    });

    describe('service: ' + targetName, function () {

      it('should convert app navigation request with translations and call senderModel update Navigation', function () {
        // given: translated appNavigation
        // when
        senderNavigationUpdateService.updateNavigation(appsTranslatedReq);
        //then
        expect(senderModel.updateNavigation).toHaveBeenCalled();
      });

      it('should convert app navigation response with translations to the needed structure', function () {
        // given: translated appNavigation
        // when
        var response = senderNavigationUpdateService.prepareNavigationUpdateResponse(appNavigationTranslatedResponse);
        //then
        expect(response[0].apps[1]).toBe(sender.appNavigation[0].apps[1]);
        expect(response.length).toBe(3);
        expect(response[0].name).toBe(sender.appNavigation[0].name);
      });

      it('should convert app navigation response without translations to the needed structure', function () {
        // given: translated appNavigation
        // when
        var response = senderNavigationUpdateService.prepareNavigationUpdateResponse(appNavigationResponse);
        //then
        expect(response[0].apps[1]).toBe(sender.appNavigation[0].apps[1]);
        expect(response.length).toBe(3);
        expect(response[0].name).toBe(sender.appNavigation[0].name);
      });

    });
  });
  // request Objects with translations
  var appsTranslatedReq = [{
    'id': 'app-id-1',
    'name': 'app1',
    'key': 'blog',
    'defaultLanguage': 'DE',
    'translations': {DE: {name: 'App Nr. 1'}}
  }, {
    'id': 'app-id-2',
    'name': 'app2',
    'key': 'blog',
    'defaultLanguage': 'DE',
    'translations': {DE: {name: 'App Nr. 2'}}
  }, {
    'id': 'app-id-3',
    'name': 'app3',
    'key': 'blog',
    'defaultLanguage': 'DE',
    'translations': {DE: {name: 'App Nr. 3'}}
  }, {
    'id': 'app-id-4',
    'name': 'app4',
    'key': 'blog',
    'defaultLanguage': 'DE',
    'translations': {DE: {name: 'App Nr. 4'}}
  }, {
    'id': 'app-id-5',
    'name': 'app5',
    'key': 'blog',
    'defaultLanguage': 'DE',
    'translations': {DE: {name: 'App Nr. 5'}}
  }];
  var sender = {id: 'SENDER-ID'};
  sender.defaultLanguage = 'EN';

  sender.translations = {
    DE: {
      displayName: 'the-sender-name',
      appNavigation: ['Titel 1', 'Titel 2', 'Titel 3']
    }
  };
  sender.appNavigation = [{
    name: 'Title 1',
    apps: ['app-id-1', 'app-id-2']
  }, {
    name: 'Title 2',
    apps: ['app-id-3', 'app-id-4']
  }, {
    name: 'Title 3',
    apps: ['app-id-5']
  }];

  // response Objects with translations

  var appNavigationTranslatedResponse = [{
    'name': sender.appNavigation[0].name,
    'apps': [appsTranslatedReq[0], appsTranslatedReq[1]],
    'translations': {
      'appNavigation': {
        'DE': {
          'name': sender.translations.DE.appNavigation[0],
          'apps': [{
            'id': appsTranslatedReq[0].id,
            'name': appsTranslatedReq[0].name
          }, {
            'id': appsTranslatedReq[1].id,
            'name': appsTranslatedReq[1].name
          }]
        }
      }
    }
  }, {
    'name': sender.appNavigation[1].name,
    'apps': [appsTranslatedReq[2], appsTranslatedReq[3]],
    'translations': {
      'appNavigation': {
        'DE': {
          'name': sender.translations.DE.appNavigation[1],
          'apps': [{
            'id': appsTranslatedReq[2].id,
            'name': appsTranslatedReq[2].name
          }, {
            'id': appsTranslatedReq[3].id,
            'name': appsTranslatedReq[3].name
          }]
        }
      }
    }
  }, {
    'name': sender.appNavigation[2].name,
    'apps': [appsTranslatedReq[4]],
    'translations': {
      'appNavigation': {
        'DE': {
          'name': sender.translations.DE.appNavigation[2],
          'apps': [{
            'id': appsTranslatedReq[4].id,
            'name': appsTranslatedReq[4].name
          }]
        }
      }
    }
  }];

  // request Objects without translations
  var appsRequest = [{
    'id': 'app-id-1',
    'name': 'app1',
    'key': 'blog'
  }, {
    'id': 'app-id-2',
    'name': 'app2',
    'key': 'blog'
  }, {
    'id': 'app-id-3',
    'name': 'app3',
    'key': 'blog'
  }, {
    'id': 'app-id-4',
    'name': 'app4',
    'key': 'blog'
  }, {
    'id': 'app-id-5',
    'name': 'app5',
    'key': 'blog'
  }];

  // response Objects without translations
  var appNavigationResponse = [{
    'name': sender.appNavigation[0].name,
    'apps': [appsRequest[0], appsRequest[1]],
    'translations': {}
  }, {
    'name': sender.appNavigation[1].name,
    'apps': [appsRequest[2], appsRequest[3]],
    'translations': {}
  }, {
    'name': sender.appNavigation[2].name,
    'apps': [appsRequest[4]],
    'translations': {}
  }];
})();
