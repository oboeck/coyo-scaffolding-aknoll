(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'sidebarService';

  describe('module: ' + moduleName, function () {

    var sidebarService, $log, $rootScope, mobileEventsService;

    beforeEach(module(moduleName, function ($provide) {
      mobileEventsService = jasmine.createSpyObj('mobileEventsService', ['propagate']);
      $provide.value('mobileEventsService', mobileEventsService);
    }));

    beforeEach(inject(function (_sidebarService_, _$log_, _$rootScope_) {
      sidebarService = _sidebarService_;
      $log = _$log_;
      $rootScope = _$rootScope_;
    }));

    function createApi(name) {
      return {
        name: name,
        state: '',
        close: function () {
          this.state = 'closed';
        },
        open: function () {
          this.state = 'open';
        },
        isOpen: function () {
          return this.state === 'open';
        }
      };
    }

    describe('service: ' + targetName, function () {

      it('should not accept empty api', function () {
        var emptyApi = {};
        spyOn($log, 'error');
        sidebarService.register(emptyApi);
        expect($log.error).toHaveBeenCalledWith('[sidebarService] Error while registering new sidebar. API not sufficient.', emptyApi);
      });

      it('should not accept api missing a close or open function', function () {
        var invalidApi = {
          name: 'testApi',
          close: null,
          open: null
        };
        spyOn($log, 'error');
        sidebarService.register(invalidApi);
        expect($log.error).toHaveBeenCalledWith('[sidebarService] Error while registering new sidebar. API not sufficient.', invalidApi);
      });

      it('should open and close a sidebar', function () {
        // given
        var api = createApi('testApi');

        // init
        sidebarService.register(api);
        expect(api.state).toBe('');
        // open
        sidebarService.open('testApi');
        expect(api.state).toBe('open');
        expect(mobileEventsService.propagate).toHaveBeenCalledWith('pull-to-refresh:sidebar:disabled');

        // close
        sidebarService.close('testApi');
        expect(api.state).toBe('closed');
        expect(mobileEventsService.propagate).toHaveBeenCalledWith('pull-to-refresh:sidebar:enabled');
      });

      it('should log an error if sidebar can not be opened', function () {
        // given
        spyOn($log, 'error');

        // when
        sidebarService.open('unknown');

        // then
        expect($log.error).toHaveBeenCalled();
      });

      it('should log an error if sidebar can not be closed', function () {
        // given
        spyOn($log, 'error');

        // when
        sidebarService.close('unknown');

        // then
        expect($log.error).toHaveBeenCalled();
      });

      it('should close all sidebars', function () {
        // given
        var apiOne = createApi('one');
        var apiTwo = createApi('two');

        // init
        sidebarService.register(apiOne);
        sidebarService.register(apiTwo);
        expect(apiOne.state).toBe('');
        expect(apiTwo.state).toBe('');

        // when
        sidebarService.closeAll();

        // then
        expect(apiOne.state).toBe('closed');
        expect(apiTwo.state).toBe('closed');
        expect(mobileEventsService.propagate).toHaveBeenCalledWith('pull-to-refresh:sidebar:enabled');
      });

      it('should close all sidebars when backdrop is closed', function () {
        // given
        var apiOne = createApi('one');
        var apiTwo = createApi('two');

        // init
        sidebarService.register(apiOne);
        sidebarService.register(apiTwo);

        // when
        $rootScope.$emit('backdrop:hidden');

        // then
        expect(apiOne.state).toBe('closed');
        expect(apiTwo.state).toBe('closed');
        expect(mobileEventsService.propagate).toHaveBeenCalledWith('pull-to-refresh:sidebar:enabled');
      });

    });
  });
})();
