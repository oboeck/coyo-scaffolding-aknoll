(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'oyoc-settings-view';

  describe('module: ' + moduleName, function () {
    var $scope, $compile, template, $templateCache;

    /**
     * Create fake module with a controller to be injected. We need to test whether controller
     * functions are working as normal.
     */
    beforeEach(function () {
      angular.module('coyo.apps.api.test', [moduleName]);
      angular.module('coyo.apps.api.test').controller('TestController', function () {
        var vm = this;

        vm.value = 'Before Click';
        vm.testClick = function () {
          vm.value = 'After Click';
        };
      });
    });

    beforeEach(module('coyo.apps.api.test'));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function ($rootScope, _$compile_, _$templateCache_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $templateCache = _$templateCache_;
        template = '<oyoc-settings-view model="app" config="config"></oyoc-settings-view>';
      }));

      it('should inject controller and include given template', function () {
        // given
        $scope.app = {
          key: 'test'
        };

        $scope.config = {
          settings: {
            templateUrl: 'template.html',
            controller: 'TestController',
            controllerAs: 'vm'
          }
        };

        $templateCache.put('template.html', '<div id="result">{{vm.value}}</div><a class="btn" ng-click="vm.testClick()">Click Me</a>');
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // when
        element.find('a.btn').triggerHandler('click');
        $scope.$digest();

        // then
        var value = element.find('div#result').text();
        expect(value).toBe('After Click');
      });

      it('should include given template without controller and access provided app', function () {
        // given
        $scope.app = {
          key: 'test'
        };

        $scope.config = {
          settings: {
            templateUrl: 'template.html'
          }
        };

        $templateCache.put('template.html', '<div id="result">Content</div>');

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        var value = element.find('div#result').text();
        expect(value).toBe('Content');
      });

    });

  });
})();
