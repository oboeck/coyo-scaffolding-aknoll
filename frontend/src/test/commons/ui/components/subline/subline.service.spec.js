(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    // the service's name
    var sublineService, profileFieldsService, birthdayService, $scope;

    beforeEach(function () {

      profileFieldsService = jasmine.createSpyObj('profileFieldService', ['getGroups']);
      birthdayService = jasmine.createSpyObj('birthdayService', ['birthdayStringToDateString']);

      module(moduleName, function ($provide) {
        // provide dependencies for service
        $provide.value('profileFieldsService', profileFieldsService);
        $provide.value('birthdayService', birthdayService);
      });

      inject(function (_sublineService_, _$q_, _$rootScope_) {
        // inject dependencies for test
        sublineService = _sublineService_;
        $scope = _$rootScope_;

        profileFieldsService.getGroups.and.returnValue(_$q_.resolve([{
          fields: [{
            name: 'jobTitle',
            userChooser: true,
            type: 'TEXT',
            order: 1
          }, {
            name: 'birthday',
            userChooser: true,
            type: 'BIRTHDAY',
            order: 2
          }, {
            name: 'date',
            userChooser: true,
            type: 'DATE',
            order: 4
          }, {
            name: 'any',
            userChooser: false,
            order: 3,
            type: 'TEXT'
          }, {
            name: 'empty',
            userChooser: true,
            order: 5,
            type: 'TEXT'
          }
          ]
        }]));

        birthdayService.birthdayStringToDateString.and.callFake(function (param) {
          return param;
        });
      });

    });

    describe('Service: sublineService', function () {

      it('should get a users subline', function (done) {
        // given
        var user = {
          properties: {
            jobTitle: 'Developer',
            birthday: '08-08',
            date: '2001-02-03',
            any: 'Hello World'
          }
        };

        // when
        var result = sublineService.getSublineForUser(user);

        // then
        result.then(function (subline) {
          expect(subline).toBe('Developer · 08-08 · 02/03/2001');
        }).finally(done);
        $scope.$apply();
      });

      it('should get a user typed senders subline', function (done) {
        // given
        var user = {
          typeName: 'user',
          properties: {
            jobTitle: 'Developer',
            birthday: '08-08',
            date: '2001-02-03',
            any: 'Hello World'
          }
        };

        // when
        var result = sublineService.getSublineForSender(user);

        // then
        result.then(function (subline) {
          expect(subline).toBe('Developer · 08-08 · 02/03/2001');
        }).finally(done);
        $scope.$apply();
      });

      it('should get a non user typed senders subline', function (done) {
        // given
        var user = {
          typeName: 'workspace',
        };

        // when
        var result = sublineService.getSublineForSender(user);

        // then
        result.then(function (subline) {
          expect(subline).toBe('workspace');
        }).finally(done);
        $scope.$apply();
      });

      it('should return empty string if no properties are given', function (done) {
        // given
        var user = {
          typeName: 'user',
        };

        // when
        var result = sublineService.getSublineForSender(user);

        // then
        result.then(function (subline) {
          expect(subline).toBe('');
        }).finally(done);
        $scope.$apply();
      });

    });

  });
})();
