/* eslint-disable angular/no-private-call */
(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'trustUrl';

  describe('module: ' + moduleName, function () {
    var $filter;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$filter_) {
        $filter = _$filter_;
      });
    });

    describe('filter: ' + targetName, function () {

      it('should be registered', function () {
        expect($filter(targetName)).toBeDefined();
      });

      it('should handle empty values', function () {
        // When
        var filter = $filter(targetName);

        // Then
        expect(filter(undefined)).toBeUndefined();
        expect(filter(null)).toEqual(null);
        expect(filter('')).toEqual('');
      });

      it('should convert to trusted url', function () {
        // When
        var filter = $filter(targetName);

        // Then
        var result = filter('http://some.url');
        expect(result.$$unwrapTrustedValue()).toEqual('http://some.url');
      });

      it('should strip executable javascript', function () {
        // given
        var filter = $filter(targetName);

        // when
        var result = filter('javascript:alert(1)');

        // then
        expect(result.$$unwrapTrustedValue()).toEqual('alert(1)');
      });

      it('should strip data urls', function () {
        // given
        var filter = $filter(targetName);

        // when
        var result = filter('data:text/html;base64,PHNjcmlwdD5hbGVydCg5KTwvc2NyaXB0Pg');

        // then
        expect(result.$$unwrapTrustedValue()).toEqual('text/html;base64,PHNjcmlwdD5hbGVydCg5KTwvc2NyaXB0Pg');
      });
    });
  });
})();
