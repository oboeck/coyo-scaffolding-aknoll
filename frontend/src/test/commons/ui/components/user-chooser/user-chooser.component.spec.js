(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $q, userChooserModalService, ngModel;

    beforeEach(module(moduleName));

    describe('directive: userChooser', function () {

      describe('controller: UserChooserController', function () {

        beforeEach(inject(function (_$controller_, $rootScope, _$q_) {
          $scope = $rootScope.$new();
          $q = _$q_;
          $controller = _$controller_;
          userChooserModalService = jasmine.createSpyObj('userChooserModalService', ['open']);
          ngModel = jasmine.createSpyObj('ngModel', ['$render', '$setTouched', '$setDirty', '$setViewValue']);
          ngModel.$validators = {};
        }));

        function buildController(config) {
          var controller = $controller('UserChooserController', {
            userChooserModalService: userChooserModalService
          }, angular.extend({
            ngModel: ngModel,
            usersOnly: false,
            groupsOnly: false
          }, config || {}));

          controller.$onInit();
          controller.ngModel.$render();

          return controller;
        }

        it('should init with undefined model', function () {
          // given
          ngModel.$viewValue = {};

          // when
          var ctrl = buildController();

          // then
          expect(ctrl.ngModel.$modelValue).toEqual({
            userIds: [],
            groupIds: []
          });
        });

        it('should fail if ngModel is not an object', function () {
          // given
          ngModel.$modelValue = 'JUST-A-STRING';

          // when
          function initController() {
            buildController();
          }

          // then
          expect(initController).toThrowError(/ngModel must be an object/);
        });

        it('should fail if ngModel.userIds is not an array', function () {
          // given
          ngModel.$viewValue = {
            userIds: 'JUST-A-STRING',
            groupIds: ['ID-4', 'ID-5', 'ID-6']
          };

          // when
          function initController() {
            buildController();
          }

          // then
          expect(initController).toThrowError(/ngModel\.userIds must be an array/);
        });

        it('should fail if ngModel.userIds contains objects with "useObjects" not being set', function () {
          // given
          ngModel.$viewValue = {
            userIds: [{}, 'ID-1', {}],
            groupIds: ['ID-4', 'ID-5', 'ID-6']
          };

          // when
          function initController() {
            buildController();
          }

          // then
          expect(initController).toThrowError(/ngModel\.userIds must be an array of strings/);
        });

        it('should open the modal', function () {
          // given
          ngModel.$viewValue = {};
          ngModel.$modelValue = {};
          var model = {
            userIds: ['ID-1', 'ID-2', 'ID-3'],
            groupIds: ['ID-4', 'ID-5', 'ID-6']
          };
          userChooserModalService.open.and.returnValue($q.resolve(model));

          // when
          var ctrl = buildController();
          ctrl.openChooser();
          $scope.$apply();

          // then
          expect(userChooserModalService.open).toHaveBeenCalledWith(ctrl.ngModel.$modelValue, {
            usersOnly: false,
            groupsOnly: false,
            internalOnly: undefined,
            usersField: 'userIds',
            groupsField: 'groupIds',
            single: false
          });
          expect(ctrl.ngModel.$setViewValue).toHaveBeenCalledWith(model);
        });

        it('should init with btn titles', function () {
          // given
          ngModel.$viewValue = {};
          var btnTitle = 'USER_CHOOSER.BUTTON_TITLE.SELECT',
              btnTitleSingular = 'USER_CHOOSER.BUTTON_TITLE.SELECT.SINGULAR';

          // when
          var ctrl = buildController({
            btnTitle: btnTitle,
            btnTitleSingle: btnTitleSingular
          });

          // then
          expect(ctrl.btnTitle).toEqual(btnTitle);
          expect(ctrl.btnTitleSingle).toEqual(btnTitleSingular);
        });

        it('should init singular with title if not provided', function () {
          // given
          ngModel.$viewValue = {};
          var btnTitle = 'USER_CHOOSER.BUTTON_TITLE.SELECT',
              btnTitleSingular = 'USER_CHOOSER.BUTTON_TITLE.SELECT.SINGULAR';

          // when
          var ctrl = buildController({
            btnTitle: btnTitle,
            btnTitleSingle: undefined
          });

          // then
          expect(ctrl.btnTitle).toEqual(btnTitle);
          expect(ctrl.btnTitleSingle).toEqual(btnTitleSingular);
        });

        it('should init singular with title if not defined and title not default', function () {
          // given
          ngModel.$viewValue = {};
          var btnTitle = 'USER_CHOOSER.BUTTON_TITLE.SELECT.NOT.DEFAULT';

          // when
          var ctrl = buildController({
            btnTitle: btnTitle,
            btnTitleSingle: undefined
          });

          // then
          expect(ctrl.btnTitle).toEqual(btnTitle);
          expect(ctrl.btnTitleSingle).toEqual(btnTitle);
        });

        it('should init singular with provided value if defined and title not default', function () {
          // given
          ngModel.$viewValue = {};
          var btnTitle = 'USER_CHOOSER.BUTTON_TITLE.SELECT.NOT.DEFAULT',
              btnTitleSingular = 'USER_CHOOSER.BUTTON_TITLE.SELECT.SINGULAR';

          // when
          var ctrl = buildController({
            btnTitle: btnTitle,
            btnTitleSingle: btnTitleSingular
          });

          // then
          expect(ctrl.btnTitle).toEqual(btnTitle);
          expect(ctrl.btnTitleSingle).toEqual(btnTitleSingular);
        });
      });

      describe('controller: ngModelController', function () {

        var $compile,
            $scope,

            buildNgModelCtrl = function (scope) {
              var template = '<coyo-user-chooser name="userChooserSettings.name" '
                  + 'ng-model="userChooserSettings.ngModel" '
                  + 'min="userChooserSettings.min" '
                  + 'single="userChooserSettings.single" '
                  + 'ng-required="userChooserSettings.required" '
                  + 'ng-model-options="{allowInvalid: true}" '
                  + '></coyo-user-chooser>',
                  userChooserElement = $compile(template)(scope);

              $scope.$digest();

              return userChooserElement.controller('ngModel');
            };

        beforeEach(module('commons.templates'));

        beforeEach(inject(function (_$compile_, $rootScope) {
          $compile = _$compile_;
          $scope = $rootScope.$new();
        }));

        it('should be valid with min option and sufficient user ids passed', function () {
          // given
          $scope.userChooserSettings = {
            ngModel: {
              userIds: ['ID-1', 'ID-2', 'ID-3']
            },
            min: 3
          };

          // when
          var ngModelCtrl = buildNgModelCtrl($scope);

          // then
          expect(ngModelCtrl.$valid).toBe(true);
        });

        it('should be invalid with min option and insufficient user ids passed', function () {
          // given
          $scope.userChooserSettings = {
            ngModel: {
              userIds: ['ID-1', 'ID-2']
            },
            min: 3
          };

          // when
          var ngModelCtrl = buildNgModelCtrl($scope);

          // then
          expect(ngModelCtrl.$valid).toBe(false);
          expect(ngModelCtrl.$error.min).toBe(true);
        });

        it('should be valid with single option and one user id passed', function () {
          // given
          $scope.userChooserSettings = {
            ngModel: {
              userIds: ['ID-1']
            },
            single: true
          };

          // when
          var ngModelCtrl = buildNgModelCtrl($scope);

          // then
          expect(ngModelCtrl.$valid).toBe(true);
        });

        it('should be invalid with single option and more than one user id passed', function () {
          // given
          $scope.userChooserSettings = {
            ngModel: {
              userIds: ['ID-1', 'ID-2']
            },
            single: true
          };

          // when
          var ngModelCtrl = buildNgModelCtrl($scope);

          // then
          expect(ngModelCtrl.$valid).toBe(false);
          expect(ngModelCtrl.$error.single).toBe(true);
        });

        it('should be valid with required option and one user id passed', function () {
          // given
          $scope.userChooserSettings = {
            ngModel: {
              userIds: ['ID-1']
            },
            required: true
          };

          // when
          var ngModelCtrl = buildNgModelCtrl($scope);

          // then
          expect(ngModelCtrl.$valid).toBe(true);
        });

        it('should be invalid with required option and no user id passed', function () {
          // given
          $scope.userChooserSettings = {
            ngModel: {
              userIds: []
            },
            required: true
          };

          // when
          var ngModelCtrl = buildNgModelCtrl($scope);

          // then
          expect(ngModelCtrl.$valid).toBe(false);
          expect(ngModelCtrl.$error.required).toBe(true);
        });
      });
    });
  });
})();
