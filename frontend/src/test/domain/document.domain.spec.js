(function () {
  'use strict';

  describe('domain: DocumentModel', function () {
    beforeEach(module('coyo.domain'));

    var $httpBackend, DocumentModel, backendUrlService, documentModel;

    beforeEach(inject(function (_$httpBackend_, _DocumentModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      DocumentModel = _DocumentModel_;
      documentModel = new DocumentModel({id: 'docId', senderId: 'senderId'});
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('get paged versions', function () {
        // given
        var version1 = {
          'id': '1',
          'author': {
            'id': '111',
            'displayName': 'Robert Lang'
          },
          'versionNumber': 0,
          'created': 1493277658976,
          'length': 827377
        };
        var version2 = {
          'id': '2',
          'author': {
            'id': '111',
            'displayName': 'Robert Lang'
          },
          'versionNumber': 1,
          'created': 1493277659546,
          'length': 827377
        };

        var versions = [version1, version2];
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/documents/docId/versions?_page=0&_pageSize=5&_sort=created,desc')
            .respond(200, {content: versions});

        // when
        var result = null;
        documentModel.getVersions({
          _page: 0,
          _pageSize: 5,
          _sort: 'created,desc'
        }).then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result.content[0].id).toEqual(version1.id);
        expect(result.content[1].id).toEqual(version2.id);
        expect(result.content[0].versionNumber).toEqual(version1.versionNumber);
        expect(result.content[1].versionNumber).toEqual(version2.versionNumber);
      });

      it('should restore version', function () {
        // given
        var version1 = {
          'id': '1',
          'author': {
            'id': '111',
            'displayName': 'Robert Lang'
          },
          'versionNumber': 0,
          'created': 1493277658976,
          'length': 827377
        };

        $httpBackend
            .expectPUT(backendUrlService.getUrl() + '/web/senders/senderId/documents/docId/versions/1/restore')
            .respond(200, {id: documentModel.id, senderId: documentModel.senderId});

        // when
        var result = null;
        documentModel.restoreVersion(version1.id).then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual(documentModel);
      });

    });

  });
}());
