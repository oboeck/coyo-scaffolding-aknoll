(function () {
  'use strict';

  describe('domain: PageModel', function () {

    var $rootScope, $httpBackend, PageModel, backendUrlService, page;

    beforeEach(module('coyo.domain'));

    beforeEach(inject(function (_$rootScope_, _$httpBackend_, _PageModel_, _backendUrlService_) {
      $rootScope = _$rootScope_;
      $httpBackend = _$httpBackend_;
      PageModel = _PageModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});

      spyOn($rootScope, '$emit').and.callThrough();
      page = new PageModel({id: 'page-id'});
    }));

    describe('instance members', function () {

      it('should trigger event on create', function () {
        // given
        delete page.id;
        $httpBackend.expectPOST(backendUrlService.getUrl() + '/web/pages').respond(200);

        // when
        page.create();
        $httpBackend.flush();

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('page:subscribed', page);
      });

      it('should trigger event on delete', function () {
        // given
        $httpBackend.expectDELETE(backendUrlService.getUrl() + '/web/pages/page-id').respond(200);

        // when
        page.delete();
        $httpBackend.flush();

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('page:unSubscribed', page.id);
      });
    });
  });
}());
