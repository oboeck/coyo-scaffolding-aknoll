(function () {
  'use strict';

  describe('domain: UserModel', function () {
    var $rootScope, $httpBackend, UserModel, Pageable, backendUrlService, SystemModelRetrieve;

    beforeEach(module('coyo.domain', function ($provide) {
      var PageModel = jasmine.createSpyObj('PageModel', ['constructor']);
      PageModel.constructor.and.callFake(function (data, queryParams, config) {
        this.data = data;
        this.queryParams = queryParams;
        this.config = config;
      });
      $provide.value('Page', PageModel.constructor);
    }));

    beforeEach(inject(function (_$rootScope_, _$httpBackend_, _UserModel_, _Pageable_, _backendUrlService_) {
      $rootScope = _$rootScope_;
      $httpBackend = _$httpBackend_;
      UserModel = _UserModel_;
      Pageable = _Pageable_;
      backendUrlService = _backendUrlService_;

      SystemModelRetrieve = function () {
        return {
          then: function (callback) {
            callback({defaultLanguage: 'EN'});
          }
        };
      };

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('class members', function () {
      it('should search', function () {
        // given
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/users?displayName=searchString&status=ACTIVE&with=adminFields')
            .respond(200, {
              content: [{
                displayName: 'Bob Baumeister'
              }]
            });

        // when
        var result = null;
        UserModel.searchWithAdminFields({
          status: 'ACTIVE',
          displayName: 'searchString'
        }).then(function (data) {
          result = data;
        });

        $httpBackend.flush();

        // then
        expect(result).toBeDefined();
        expect(result.data.content.length).toBe(1);
        expect(result.data.content[0].displayName).toBe('Bob Baumeister');
      });

      it('should find all Users with given ids', function () {
        // given
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/users?userIds=1&userIds=2')
            .respond(200, [{
              id: 1,
              displayName: 'Robert Lang'
            }, {
              id: 2,
              displayName: 'Nancy Pork'
            }]);

        // when
        var result = null;
        UserModel.getUsers([1, 2]).then(function (data) {
          result = data;
        });

        $httpBackend.flush();

        // then
        expect(result).toBeDefined();
        expect(result.length).toBe(2);
        expect(result[0].displayName).toBe('Robert Lang');
        expect(result[1].displayName).toBe('Nancy Pork');
      });

      it('should not request backend when array of user ids is empty', function () {
        // when
        var result = null;
        UserModel.getUsers([]).then(function (data) {
          result = data;
        });

        $rootScope.$apply();

        // then
        expect(result).toBeDefined();
        expect(result.length).toBe(0);
        $httpBackend.verifyNoOutstandingRequest();
      });
    });

    describe('instance members', function () {

      it('should create user', function () {
        // given
        var user = new UserModel({
          firstname: 'First',
          lastname: 'Last',
          email: 'first.last@mindsmash.com',
          active: true
        });
        $httpBackend.expectPOST(backendUrlService.getUrl() + '/web/users', function (jsonData) {
          var data = angular.fromJson(jsonData);
          expect(data.firstname).toBe('First');
          expect(data.lastname).toBe('Last');
          expect(data.email).toBe('first.last@mindsmash.com');
          expect(data.active).toBe(true);
          return true;
        }).respond(200);

        // when
        var resolved = false;
        user.create().then(function () {
          resolved = true;
        });
        $httpBackend.flush();

        // then
        expect(resolved).toBe(true);
      });

      it('should update user', function () {
        // given
        var user = new UserModel({
          id: 'USER-ID',
          firstname: 'First',
          lastname: 'Last',
          email: 'first.last@mindsmash.com',
          active: true
        });
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/users/USER-ID', function (jsonData) {
          var data = angular.fromJson(jsonData);
          expect(data.firstname).toBe('First');
          expect(data.lastname).toBe('Last');
          expect(data.email).toBe('first.last@mindsmash.com');
          expect(data.active).toBe(true);
          return true;
        }).respond(200);

        // when
        var resolved = false;
        user.update().then(function () {
          resolved = true;
        });
        $httpBackend.flush();

        // then
        expect(resolved).toBe(true);
      });

      it('should delete user', function () {
        // given
        var user = new UserModel({id: '1'});
        $httpBackend.expectDELETE(backendUrlService.getUrl() + '/web/users/1').respond(200);

        // when
        var resolved = false;
        user.delete().then(function () {
          resolved = true;
        });
        $httpBackend.flush();

        // then
        expect(resolved).toBe(true);
      });

      it('should recover user', function () {
        // given
        var user = new UserModel({id: '1'});
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/users/1/recover').respond(200);

        // when
        var resolved = false;
        user.recover().then(function () {
          resolved = true;
        });
        $httpBackend.flush();

        // then
        expect(resolved).toBe(true);
      });

      it('should deactivate user', function () {
        // given
        var user = new UserModel({id: '1'});
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/users/1/deactivate').respond(200);

        // when
        var resolved = false;
        user.deactivate().then(function () {
          resolved = true;
        });
        $httpBackend.flush();

        // then
        expect(resolved).toBe(true);
      });

      it('should activate user', function () {
        // given
        var user = new UserModel({id: '1'});
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/users/1/activate').respond(200);

        // when
        var resolved = false;
        user.activate().then(function () {
          resolved = true;
        });
        $httpBackend.flush();

        // then
        expect(resolved).toBe(true);
      });

      describe('hasGlobalPermissions', function () {
        it('should match permission', function () {
          // given
          var user = new UserModel({globalPermissions: ['P1', 'P2']});

          // when
          var result = user.hasGlobalPermissions('P1');

          // then
          expect(result).toBe(true);
        });

        it('should not match missing permission', function () {
          // given
          var user = new UserModel({globalPermissions: ['P1', 'P2']});

          // when
          var result = user.hasGlobalPermissions('P3');

          // then
          expect(result).toBe(false);
        });

        it('should match one permission', function () {
          // given
          var user = new UserModel({globalPermissions: ['P1', 'P2']});

          // when
          var result = user.hasGlobalPermissions('P1,P3');

          // then
          expect(result).toBe(true);
        });

        it('should not match missing permissions when all are required', function () {
          // given
          var user = new UserModel({globalPermissions: ['P1']});

          // when
          var result = user.hasGlobalPermissions('P1,P2', true);

          // then
          expect(result).toBe(false);
        });

        it('should match permissions when all are required', function () {
          // given
          var user = new UserModel({globalPermissions: ['P1', 'P2']});

          // when
          var result = user.hasGlobalPermissions('P1,P2', true);

          // then
          expect(result).toBe(true);
        });

        it('should handle array parameter', function () {
          // given
          var user = new UserModel({globalPermissions: ['P1', 'P2']});

          // when
          var result = user.hasGlobalPermissions(['P1', 'P2'], true);

          // then
          expect(result).toBe(true);
        });

        it('should handle missing permisions', function () {
          // given
          var user = new UserModel();

          // when
          var result = user.hasGlobalPermissions('P1');

          // then
          expect(result).toBe(false);
        });

        it('should get user language as best suited language', function () {
          // given
          var user = new UserModel({language: 'DE'});

          // when
          var result;
          user.getBestSuitableLanguage(['DE', 'EN'], SystemModelRetrieve).then(function (language) {
            result = language;
          });
          $rootScope.$apply();

          // then
          expect(result).toBe('DE');
        });

        it('should get system language as best suited language', function () {
          // given
          var user = new UserModel({language: 'FR'});

          // when
          var result;
          user.getBestSuitableLanguage(['DE', 'EN'], SystemModelRetrieve).then(function (language) {
            result = language;
          });
          $rootScope.$apply();

          // then
          expect(result).toBe('EN');
        });

        it('should get no best suited language', function () {
          // given
          var user = new UserModel({language: 'FR'});

          // when
          var result;
          user.getBestSuitableLanguage(['DE', 'IT'], SystemModelRetrieve).then(function (language) {
            result = language;
          });
          $rootScope.$apply();

          // then
          expect(result).toBe('NONE');
        });
      });

      describe('searchWithFilter', function () {
        it('should search with defaults', function () {
          // given
          var pageable = new Pageable();
          $httpBackend.expectGET(backendUrlService.getUrl() + '/web/users' +
              '?_page=0' +
              '&_pageSize=20')
              .respond(200, 'response');

          // when
          var result = null;
          UserModel.searchWithFilter(null, pageable).then(function (response) {
            result = response;
          });
          $httpBackend.flush();

          // then
          expect(result.data).toBe('response');
        });

        it('should search with parameters', function () {
          // given
          var pageable = new Pageable(1, 2, 'sortProperty');
          $httpBackend.expectGET(backendUrlService.getUrl() + '/web/users' +
              '?_orderBy=sortProperty' +
              '&_page=1&_pageSize=2' +
              '&aggregations=field1%3D1%26field2%3D2' +
              '&filters=filter1%3Dvalue1%26filter2%3Dvalue2' +
              '&searchFields=searchField1,searchField2' +
              '&term=term')
              .respond(200, 'response');

          // when
          var result = null;
          var filters = {'filter1': 'value1', 'filter2': 'value2'};
          var searchFields = ['searchField1', 'searchField2'];
          var aggregations = {field1: 1, field2: 2};
          UserModel.searchWithFilter('term', pageable, filters, searchFields, aggregations).then(function (response) {
            result = response;
          });
          $httpBackend.flush();

          // then
          expect(result.data).toBe('response');
        });
      });
    });
  });
}());
