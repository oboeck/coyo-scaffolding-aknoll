(function () {
  'use strict';

  var moduleName = 'coyo.account';
  var targetName = 'devicesToOrderedArray';

  describe('module: ' + moduleName, function () {
    var devicesToOrderedArray;

    beforeEach(function () {
      module(moduleName);

      inject(function (_devicesToOrderedArrayFilter_) {
        devicesToOrderedArray = _devicesToOrderedArrayFilter_;
      });
    });

    describe('filter: ' + targetName, function () {

      it('should transform devices object to ordered array', function () {

        var appInstallation1 = {name: 'Google Pixel'};
        var appInstallation2 = {name: 'Google Pixel'};
        var appInstallation3 = {name: 'Google Pixel 2'};

        var pushDevices = {
          'Google Pixel': [appInstallation1, appInstallation2],
          'Google Pixel 2': [appInstallation3]
        };

        var actualOrderedArray = devicesToOrderedArray(pushDevices);

        expect(actualOrderedArray).toEqual([[appInstallation1, appInstallation2], [appInstallation3]]);
      });

    });
  });
})();
