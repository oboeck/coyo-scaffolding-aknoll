(function () {
  'use strict';

  var moduleName = 'coyo.admin.apiClients';
  var controllerName = 'AdminApiClientsListController';

  describe('module: ' + moduleName, function () {

    beforeEach(
        module(moduleName, function ($provide) {
          $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        })
    );

    describe('controller: ' + controllerName, function () {
      var $controller, $rootScope, $scope, ctrl, apiClient, ApiClientModel, emptyResolve, modalService;

      beforeEach(inject(function (_$controller_, _$rootScope_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;

        // mock screensize information
        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };

        ApiClientModel = jasmine.createSpyObj('ApiClientModel', ['pagedQuery']);
        apiClient = jasmine.createSpyObj('apiClient', ['delete']);
        modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);

        emptyResolve = {
          then: function (thenCallback) {
            thenCallback();
            return {
              finally: function (finallyCallback) {
                finallyCallback();
              }
            };
          }
        };

        mockApiClientSearch({});
      }));

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          ApiClientModel: ApiClientModel,
          modalService: modalService
        });
      }

      function mockApiClientSearch(result) {
        ApiClientModel.pagedQuery.and.returnValue({
          then: function (callback) {
            callback(result);
            return {
              finally: function (callback) {
                callback();
              }
            };
          }
        });
      }

      describe('controller init', function () {
        beforeEach(function () {
          mockApiClientSearch({content: 'testdata', last: true, totalPages: 1, totalElements: 1, size: 20,
            sort: [{direction: 'ASC', property: 'created', ignoreCase: false}],
            number: 0, first: true, numberOfElements: 5, _queryParams: {
              _page: 0,
              _pageSize: 20,
              _sort: 'created,desc'
            }});
        });

        it('should perform initial search', function () {
          // given
          ctrl = buildController();

          // when
          ctrl.$onInit();

          // then
          expect(ctrl.currentPage.content).toBe('testdata');
          expect(ctrl.currentPage._queryParams._page).toBe(0);
          expect(ctrl.currentPage._queryParams._sort).toBe('created,desc');
          expect(ctrl.loading).toBe(false);
          expect(ApiClientModel.pagedQuery).toHaveBeenCalled();
        });

        it('should initialize default paged query parameters', function () {
          // given
          ctrl = buildController();

          // when
          ctrl.$onInit();

          // then
          expect(ApiClientModel.pagedQuery).toHaveBeenCalled();
          expect(ApiClientModel.pagedQuery.calls.mostRecent().args[1]).toEqual(jasmine.objectContaining({
            _page: 0,
            _pageSize: 20,
            _sort: 'created,desc'
          }));
        });

      });

      describe('actions', function () {
        beforeEach(function () {
          mockApiClientSearch({content: 'testdata', last: true, totalPages: 1, totalElements: 1, size: 20,
            sort: [{direction: 'ASC', property: 'created', ignoreCase: false}],
            number: 0, first: true, numberOfElements: 5});
        });

        it('should delete api client', function () {
          //given
          apiClient.delete.and.returnValue(emptyResolve);
          apiClient.clientId = 'Test Client Id';
          modalService.confirmDelete.and.returnValue({result: emptyResolve});
          ctrl = buildController();

          // when
          ctrl.$onInit();
          ctrl.actions.deleteApiClient(apiClient);

          // then
          expect(apiClient.delete).toHaveBeenCalled();
          expect(modalService.confirmDelete).toHaveBeenCalled();
          var args = modalService.confirmDelete.calls.mostRecent().args;
          expect(args[0].title).toBe('ADMIN.API_CLIENTS.OPTIONS.DELETE.MODAL.TITLE');
          expect(args[0].text).toBe('ADMIN.API_CLIENTS.OPTIONS.DELETE.MODAL.TEXT');
          expect(args[0].translationContext.clientId).toBe(apiClient.clientId);
          expect(ApiClientModel.pagedQuery).toHaveBeenCalled();
        });

      });
    });
  });
})();
