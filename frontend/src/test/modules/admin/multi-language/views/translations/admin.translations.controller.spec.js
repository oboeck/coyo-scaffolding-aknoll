(function () {
  'use strict';

  var moduleName = 'coyo.admin.multiLanguage';
  var controllerName = 'AdminTranslationsController';

  describe('module: ' + moduleName, function () {

    beforeEach(function () {
      module('coyo.admin');

      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });
    });

    describe('controller: ' + controllerName, function () {
      var $q, $controller, $rootScope, $scope, $translate;
      var LanguagesModel, SettingsModel, translationRegistry, settings;
      var ctrl, availableLanguages, currentLanguage, allKeysDE, allKeysEN, allTranslatedKeys, allTranslatedBackendKeys, languages;

      beforeEach(inject(function (_$controller_, _$rootScope_, _$q_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $q = _$q_;

        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };

        availableLanguages = ['en', 'de'];
        currentLanguage = 'en';
        languages = [{language: 'en'}, {language: 'de'}, {language: 'aa'}];
        allKeysEN = {ACTIVE: 'active', INACTIVE: 'inactive'};
        allKeysDE = {ACTIVE: 'aktiv', INACTIVE: 'inaktiv'};
        allTranslatedKeys = [{id: '35fbd72a-0b99-4097-9d7b-a4c118a21954', key: 'ACTIVE', language: 'EN', translation: 'activated'}];
        allTranslatedBackendKeys = {'translate me': 'translation'};

        $translate = jasmine.createSpyObj('$translate', ['getAvailableLanguageKeys', 'instant']);
        $translate.getAvailableLanguageKeys.and.returnValue(availableLanguages);
        $translate.instant.and.returnValue('language name');

        settings = jasmine.createSpyObj('settings', ['defaultLanguage']);
        settings.defaultLanguage.and.returnValue($q.resolve());
        settings.defaultLanguage = currentLanguage;

        SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieve']);
        SettingsModel.retrieve.and.returnValue($q.resolve(settings));

        translationRegistry = jasmine.createSpyObj('translationRegistry', ['getTranslationTable']);

        mockTranslationTable('en');

        LanguagesModel = jasmine.createSpyObj('LanguagesModel', ['getTranslations', 'getBackendTranslations']);
        LanguagesModel.getTranslations.and.returnValue({
          then: function (callback) {
            callback(allTranslatedKeys);
          }
        });

        LanguagesModel.getBackendTranslations.and.returnValue($q.resolve(allTranslatedBackendKeys));
      }));

      function buildController(done) {
        var controller = $controller(controllerName, {
          $q: $q,
          $rootScope: $rootScope,
          $scope: $scope,
          $translate: $translate,
          settings: settings,
          LanguagesModel: LanguagesModel,
          translationRegistry: translationRegistry,
          languages: languages
        });

        $rootScope.$apply();
        done();
        return controller;
      }

      function mockTranslationTable(language) {
        if (language === 'en') {
          translationRegistry.getTranslationTable.and.returnValue(allKeysEN);
        } else if (language === 'de') {
          translationRegistry.getTranslationTable.and.returnValue(allKeysDE);
        }
      }

      describe('controller init', function () {
        it('should perform initial load', function (done) {
          // given
          $translate.instant.and.returnValues('English', 'German', 'Afar');
          $rootScope.screenSize.isXs = false;
          $rootScope.screenSize.isSm = false;
          mockTranslationTable('en');

          // when
          ctrl = buildController(done);

          // then
          expect(translationRegistry.getTranslationTable).toHaveBeenCalledWith('en');
          expect(LanguagesModel.getTranslations).toHaveBeenCalledWith('en');
          expect(LanguagesModel.getBackendTranslations).toHaveBeenCalledWith('en');
          expect(settings.defaultLanguage).toBe('en');

          expect(ctrl.filter.languages[0]).toEqual({language: 'aa', name: 'Afar'});
          expect(ctrl.filter.languages[1]).toEqual({language: 'en', name: 'English'});
          expect(ctrl.filter.languages[2]).toEqual({language: 'de', name: 'German'});
          expect(ctrl.filter.activeLanguage).toBe(currentLanguage);
          expect(ctrl.translatedKeys).toBe(allTranslatedKeys);
          expect(ctrl.translationKeys.length).toBe(2);

          expect(ctrl.mobile).toBe(false);
        });

        it('should init deviating page size on mobile', function (done) {
          // given
          $rootScope.screenSize.isXs = true;
          $rootScope.screenSize.isMd = false;
          mockTranslationTable('en');

          // when
          ctrl = buildController(done);

          // then
          expect(ctrl.mobile).toBe(true);
        });
      });

      describe('controller active', function () {
        it('should filter result list when use search', function (done) {
          // given
          mockTranslationTable('en');

          // when
          ctrl = buildController(done);
          ctrl.filter.search('inactive');

          // then
          expect(ctrl.translationKeys.length).toBe(1);
          expect(ctrl.filter.searchTerm).toBe('inactive');
        });

        it('should not find german translations when search in english results', function (done) {
          // given
          mockTranslationTable('en');

          // when
          ctrl = buildController(done);
          ctrl.filter.search('inaktiv');

          // then
          expect(ctrl.translationKeys.length).toBe(0);
          expect(ctrl.filter.searchTerm).toBe('inaktiv');
        });

        it('should change language when use filter', function (done) {
          // given
          mockTranslationTable('de');

          // when
          ctrl = buildController(done);
          ctrl.filter.status.language('de');

          // then
          expect(ctrl.filter.activeLanguage).toBe('de');
        });

        it('should only update current translation when not already focused', function () {
          // given
          mockTranslationTable('en');
          var event = {
            currentTarget: {}
          };
          var element = jasmine.createSpyObj('translationElement',
              ['find', 'removeClass', 'addClass', 'focus', 'select']);
          element.length = 0;
          element.find.and.returnValue(element);
          element.removeClass.and.returnValue(element);
          element.addClass.and.returnValue(element);
          element.focus.and.returnValue(element);
          element.select.and.returnValue(element);

          spyOn(angular, 'element').and.returnValue(element);
          ctrl = buildController(angular.noop);

          // when
          ctrl.editKey(event, 'ACTIVE');

          // then
          expect(ctrl.currentEditValues.ACTIVE).toBe('activated');
        });

        it('should not update translation when element is focused', function () {
          // given
          var event = {
            currentTarget: {}
          };
          var element = jasmine.createSpyObj('translationElement',
              ['find']);
          element.length = 1;
          element.find.and.returnValue(element);

          spyOn(angular, 'element').and.returnValue(element);
          ctrl = buildController(angular.noop);

          // when
          ctrl.editKey(event, 'ACTIVE');

          // then
          expect(ctrl.currentEditValues.ACTIVE).toBe(undefined);
          expect(element.find).toHaveBeenCalledWith(':focus');
        });
      });
    });
  });
})();
