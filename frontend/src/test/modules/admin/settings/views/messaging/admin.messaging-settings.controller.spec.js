(function () {
  'use strict';

  var moduleName = 'coyo.admin.settings';
  var controllerName = 'AdminMessagingSettingsController';

  describe('module: ' + moduleName, function () {

    var $scope, $q, $controller;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$rootScope_, _$q_, _$controller_) {
        $scope = _$rootScope_.$new();
        $q = _$q_;
        $controller = _$controller_;
      });
    });

    describe('controller: ' + controllerName, function () {
      var SettingsModel, settings;

      beforeEach(function () {
        settings = {};
        Object.setPrototypeOf(settings, jasmine.createSpyObj('settings', ['update', 'get']));
        settings.update.and.returnValue($q.resolve());
        settings.get.and.returnValue($q.resolve(settings));

        SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieve']);
        SettingsModel.retrieve.and.returnValue($q.resolve(settings));
      });

      function buildController() {
        return $controller(controllerName, {
          SettingsModel: SettingsModel,
          settings: settings
        });
      }

      describe('controller', function () {

        it('should init', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.$onInit();
          $scope.$apply();

          // then
          expect(ctrl.transferObject.deleteChannelsPeriod).not.toBeUndefined();
          expect(ctrl.transferObject.deleteMessagesPeriod).not.toBeUndefined();
        });

        it('should save settings', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.$onInit();
          $scope.$apply();

          ctrl.save();
          $scope.$apply();

          // then
          expect(settings.update).toHaveBeenCalled();
          expect(SettingsModel.retrieve).toHaveBeenCalledWith(true);
        });

        it('should not save null values for deleteChannelsPeriod', function () {
          // given
          var ctrl = buildController();
          ctrl.transferObject = {
            deleteChannelsPeriod: undefined
          };

          // when
          ctrl.onDeleteChannelsPeriodChange();

          // then
          expect(_.has(ctrl.settings, 'deleteChannelsPeriod')).toBeFalsy();
        });

        it('should not save null values for deleteMessagesPeriod', function () {
          // given
          var ctrl = buildController();
          ctrl.transferObject = {
            deleteMessagesPeriod: undefined
          };

          // when
          ctrl.onDeleteMessagesPeriodChange();

          // then
          expect(_.has(ctrl.transferObject, 'deleteMessagesPeriod')).toBeFalsy();
        });

        it('should correct generate transfer object (settings)', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.$onInit();
          $scope.$apply();

          var result = {
            chatCron: '',
            deleteMessagesPeriod: '',
            deleteMessagesActive: '',
            deleteChannelsPeriod: '',
            deleteChannelsActive: ''
          };

          // then
          expect(Object.keys(ctrl.transferObject)).toEqual(Object.keys(result));
        });

      });
    });
  });
})();
