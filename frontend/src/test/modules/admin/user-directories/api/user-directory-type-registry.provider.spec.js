(function () {
  'use strict';

  var moduleName = 'coyo.admin.userDirectories.api';

  describe('module: ' + moduleName, function () {

    var userDirectoryTypeRegistryProvider, userDirectoryTypeRegistry;

    beforeEach(function () {
      // initialize the service provider by injecting it to a fake module's config block
      angular.module('coyo.admin.userDirectories.test', ['coyo.admin.userDirectories.api']);
      angular.module('coyo.admin.userDirectories.test').config(function (_userDirectoryTypeRegistryProvider_) {
        userDirectoryTypeRegistryProvider = _userDirectoryTypeRegistryProvider_;

        userDirectoryTypeRegistryProvider.register({
          key: 'ldap',
          name: 'ADMIN.USER_DIRECTORIES.LDAP.NAME',
          description: 'ADMIN.USER_DIRECTORIES.LDAP.DESCRIPTION',
          directive: 'oyoc-ldap-settings'
        });

        userDirectoryTypeRegistryProvider.register({
          key: 'ldap2',
          name: 'ADMIN.USER_DIRECTORIES.LDAP_TWO.NAME',
          description: 'ADMIN.USER_DIRECTORIES.LDAP_TWO.DESCRIPTION',
          directive: 'oyoc-ldap-two-settings'
        });
      });

      // initialize test.app injector
      module('coyo.admin.userDirectories.test');
    });

    beforeEach(inject(function (_userDirectoryTypeRegistry_) {
      userDirectoryTypeRegistry = _userDirectoryTypeRegistry_;
    }));

    describe('service: userDirectoryTypeRegistry', function () {

      it('should get all registered user directories', function () {
        // when
        var directories = userDirectoryTypeRegistry.getAll();

        // then
        expect(directories.length).toBe(2);
      });

      it('should get a registered user directory', function () {
        // when
        var directory = userDirectoryTypeRegistry.get('ldap');

        // then
        expect(directory.name).toBe('ADMIN.USER_DIRECTORIES.LDAP.NAME');
      });

      it('should return null if requested user directory is not available', function () {
        // when
        var directory = userDirectoryTypeRegistry.get('ldap3');

        // then
        expect(directory).toBeNull();
      });
    });
  });

  describe('module: ' + moduleName, function () {

    describe('provider: userDirectoryTypeRegistryProvider', function () {

      // Create a fake module with the provider. You can use this method in any test
      function createModule(userDirectoryConfig) {
        angular.module('coyo.admin.userDirectories.test', ['coyo.admin.userDirectories.api']);
        angular.module('coyo.admin.userDirectories.test').config(function (_userDirectoryTypeRegistryProvider_) {
          // Call methods on the provider in config phase here
          _userDirectoryTypeRegistryProvider_.register(userDirectoryConfig);
        });

        module('coyo.admin.userDirectories.test');
        inject(function () {
        });
      }

      it('should register a valid user directory type', function () {
        // given
        var config = {
          key: 'test-user-directory-type',
          name: 'Test User Directory Type',
          description: 'A User Directory Type',
          directive: 'example-user-directory-type-settings'
        };

        // when
        createModule(config);

        // then -> perform without error
      });

      it('should fail if key is missing', function () {
        // given
        var config = {
          name: 'Test User Directory Type',
          description: 'A User Directory Type',
          directive: 'example-user-directory-type-settings'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "key" is required/);
      });

      it('should fail if name is missing', function () {
        // given
        var config = {
          key: 'test-user-directory-type',
          description: 'A User Directory Type',
          directive: 'example-user-directory-type-settings'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "name" is required/);
      });

      it('should fail if description is missing', function () {
        // given
        var config = {
          key: 'test-user-directory-type',
          name: 'Test User Directory Type',
          directive: 'example-user-directory-type-settings'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "description" is required/);
      });

      it('should fail if directive is missing', function () {
        // given
        var config = {
          key: 'test-user-directory-type',
          name: 'Test User Directory Type',
          description: 'A User Directory Type'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "directive" is required/);
      });
    });
  });

})();
