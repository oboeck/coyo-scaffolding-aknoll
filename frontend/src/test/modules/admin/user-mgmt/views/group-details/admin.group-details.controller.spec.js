(function () {
  'use strict';

  var moduleName = 'coyo.admin.userManagement';
  var controllerName = 'AdminGroupDetailsController';

  describe('module: ' + moduleName, function () {

    beforeEach(
        module(moduleName, function ($provide) {
          $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        })
    );

    describe('controller: ' + controllerName, function () {
      var $controller, $scope, group, $state, ctrl;

      beforeEach(inject(function (_$controller_, $rootScope) {
        $scope = $rootScope.$new();
        $controller = _$controller_;

        $state = jasmine.createSpyObj('$state', ['go']);
        group = jasmine.createSpyObj('GroupModel', ['save']);
      }));

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          group: group,
          $state: $state
        });
      }

      describe('controller active', function () {
        beforeEach(function () {
          ctrl = buildController();
        });

        it('should save group and go to the parent state', function () {
          // given
          group.save.and.returnValue({
            then: function (callback) {
              callback();
            }
          });

          // when
          ctrl.save();

          // then
          expect(group.save).toHaveBeenCalled();
          expect($state.go).toHaveBeenCalledWith('^');
        });
      });
    });
  });
})();
