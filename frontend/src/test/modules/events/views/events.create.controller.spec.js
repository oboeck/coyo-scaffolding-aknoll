(function () {
  'use strict';

  var moduleName = 'coyo.events';
  var targetName = 'EventsCreateController';

  describe('module: ' + moduleName, function () {
    var $controller, $rootScope, $scope, $q, $timeout, $state, $compile;
    var EventModelMock, coyoNotificationMock, eventDateSyncService;

    var user = {id: 'userId', public: true};
    var event;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$rootScope_, _$controller_, _$q_, _$compile_, _$timeout_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $timeout = _$timeout_;
        $q = _$q_;
        $compile = _$compile_;
        $state = jasmine.createSpyObj('$state', ['go', 'transitionTo']);
        EventModelMock = function (data) {
          event = angular.extend(jasmine.createSpyObj('EventModel', ['create']), data);
          return event;
        };
        coyoNotificationMock = jasmine.createSpyObj('coyoNotification', ['success']);
        eventDateSyncService = jasmine.createSpyObj('eventDateSyncService', [
          'updateStartDate',
          'updateStartTime',
          'updateEndDate',
          'updateEndTime'
        ]);
      });
    });

    describe('controller: ' + targetName, function () {

      function buildController() {
        var ctrl = $controller(targetName, {
          $scope: $scope,
          $q: $q,
          $timeout: $timeout,
          $state: $state,
          EventModel: EventModelMock,
          coyoNotification: coyoNotificationMock,
          currentUser: user,
          host: null,
          eventDateSyncService: eventDateSyncService
        });
        $compile('<form name="this.eventsForm2"><input id="eventEnd" type="text" name="eventEnd"></form>')(ctrl);
        ctrl.$onInit();
        return ctrl;
      }

      it('should init', function () {
        // when
        var ctrl = buildController();

        // then
        expect(ctrl).toBeDefined();
        expect(ctrl.wizard.states).toEqual([
          'MODULE.EVENTS.CREATE.GENERAL',
          'MODULE.EVENTS.CREATE.PARTICIPANTS']);
        expect(ctrl.wizard.active).toBe(0);
        expect(ctrl.event).toBeDefined();
        expect(ctrl.event.adminIds).toEqual([user.id]);
        expect(ctrl.event.memberIds).toEqual([]);
        expect(ctrl.event.adminGroupIds).toEqual([]);
        expect(ctrl.event.memberGroupIds).toEqual([]);
      });

      it('should go back', function () {
        // given
        var ctrl = buildController();
        ctrl.wizard.active = 2;

        // when
        ctrl.back();

        // then
        expect(ctrl.wizard.active).toBe(1);
      });

      it('should not go back before start', function () {
        // given
        var ctrl = buildController();
        ctrl.wizard.active = 0;

        // when
        ctrl.back();

        // then
        expect(ctrl.wizard.active).toBe(0);
      });

      it('should go next', function () {
        // given
        var ctrl = buildController();
        ctrl.wizard.active = 0;

        // when
        ctrl.next({$valid: true});

        // then
        expect(ctrl.wizard.active).toBe(1);
      });

      it('should not go next if form is invalid', function () {
        // given
        var ctrl = buildController();
        ctrl.wizard.active = 0;

        // when
        ctrl.next({$valid: false});

        // then
        expect(ctrl.wizard.active).toBe(0);
      });

      it('should submit after last step', function () {
        // given
        var ctrl = buildController();
        ctrl.wizard.active = 3;
        event.fullDay = false;
        event.startDate = new Date(2017, 5, 1, 10, 0);
        event.endDate = new Date(2017, 5, 1, 20, 0);
        event.create.and.returnValue($q.resolve({
          event: angular.extend(event, {
            slug: 'slug'
          })
        }));

        // when
        ctrl.next({$valid: true});

        // then
        expect(ctrl.event.visibility).toBe('PUBLIC');
        expect(ctrl.event.senderId).toBe(user.id);
        expect(ctrl.event.startDate).toEqual('2017-06-01T10:00:00');
        expect(ctrl.event.endDate).toEqual('2017-06-01T20:00:00');
        expect(ctrl.event.limitedParticipants).toBeNull();
        $rootScope.$apply();
        expect($state.go).toHaveBeenCalledWith('main.event.show', {idOrSlug: event.slug});
        expect(coyoNotificationMock.success).toHaveBeenCalledWith('MODULE.EVENTS.CREATE.SUCCESS');
      });

      it('should submit after last step with limited participants', function () {
        // given
        var ctrl = buildController();
        ctrl.wizard.active = 3;
        event.fullDay = false;
        event.startDate = new Date(2017, 5, 1, 10, 0);
        event.endDate = new Date(2017, 5, 1, 20, 0);
        event.limitedParticipants = {
          participantsLimit: 4
        };
        event.create.and.returnValue($q.resolve({
          event: angular.extend(event, {
            slug: 'slug'
          })
        }));
        ctrl.host = {id: 'host-id', public: false};

        // when
        ctrl.next({$valid: true});

        // then
        expect(ctrl.event.visibility).toBe('PRIVATE');
        expect(ctrl.event.senderId).toBe('host-id');
        expect(ctrl.event.startDate).toEqual('2017-06-01T10:00:00');
        expect(ctrl.event.endDate).toEqual('2017-06-01T20:00:00');
        expect(ctrl.event.limitedParticipants).toEqual({participantsLimit: 4});
        $rootScope.$apply();
        expect($state.go).toHaveBeenCalledWith('main.event.show', {idOrSlug: event.slug});
        expect(coyoNotificationMock.success).toHaveBeenCalledWith('MODULE.EVENTS.CREATE.SUCCESS');
      });
    });

  });

})();
