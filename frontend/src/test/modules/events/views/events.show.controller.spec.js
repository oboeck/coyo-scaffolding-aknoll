(function () {
  'use strict';

  var moduleName = 'coyo.events';
  var targetName = 'EventsShowController';

  describe('module: ' + moduleName, function () {
    var $controller, $q, $scope;

    var event;
    var EventModel, senderService, userChooserModalService, shareService, authService;
    var currentUser = {};

    function buildController() {
      var ctrl = $controller(targetName, {
        $scope: $scope,
        EventModel: EventModel,
        senderService: senderService,
        event: event,
        currentUser: currentUser,
        userChooserModalService: userChooserModalService,
        shareService: shareService,
        authService: authService
      });
      ctrl.$onInit();
      $scope.$apply();
      return ctrl;
    }

    beforeEach(function () {
      module(moduleName);

      module('commons.ui');

      inject(function (_$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $q = _$q_;

        event = {
          id: '1',
          slug: 'new-event-slug',
          showParticipants: true,
          attendingCount: 0,
          canManage: function () {
          },
          getHosts: function () {
          },
          deleteEvent: function () {
          },
          removeMember: function () {
          },
          getIcalExportUrl: function () {
          },
          inviteMembers: function () {
          }
        };
        spyOn(event, 'getHosts').and.returnValue($q.resolve([
          {userId: '1234-5678-90', status: 'ATTENDING', role: 'ADMIN'}
        ]));
        spyOn(event, 'inviteMembers').and.returnValue($q.resolve());

        senderService = jasmine.createSpyObj('senderService', ['changeAvatar', 'changeCover']);
        shareService = jasmine.createSpyObj('shareService', ['share']);
        userChooserModalService = jasmine.createSpyObj('userChooserModalService', ['open']);
        userChooserModalService.open.and.returnValue($q.resolve());
        authService = jasmine.createSpyObj('authService', ['onGlobalPermissions']);

        EventModel = jasmine.createSpyObj('EventModel', ['getMembershipsWithFilter', 'canManage', 'getIcalExportUrl']);
        EventModel.getMembershipsWithFilter.and.returnValue($q.resolve({
          number: 0,
          last: false,
          content: [
            {userId: '1234-5678-90', status: 'ATTENDING', role: 'ADMIN'},
            {userId: '1234-5678-91', status: 'ATTENDING', role: 'USER'},
            {userId: '1234-5678-92', status: 'PENDING', role: 'USER'}
          ],
          aggregations: {
            status: [
              {active: false, count: 2, key: 'ATTENDING'},
              {active: false, count: 1, key: 'PENDING'}
            ]
          }
        }));
      });
    });

    describe('controller: ' + targetName, function () {
      it('should initialize the hosts', function () {
        // given
        // ... see beforeEach()

        // when
        var ctrl = buildController();

        // then
        expect(ctrl.hosts[0]).toBeDefined();
        expect(ctrl.hosts[0]).toEqual(jasmine.objectContaining({
          userId: '1234-5678-90', status: 'ATTENDING', role: 'ADMIN'
        }));
      });

      it('should initialize the participants overview', function () {
        // given
        // ... see beforeEach()

        // when
        var ctrl = buildController();

        // then
        expect(ctrl.currentPage.content).toBeDefined();
        expect(ctrl.currentPage.content.length).toEqual(3);
        expect(ctrl.currentPage.aggregations).toBeDefined();
        expect(ctrl.currentPage.aggregations.status).toBeDefined();
      });

      it('should load more member', function () {
        // given
        var page = {
          number: 0,
          last: false,
          content: [
            {userId: '1', status: 'ATENDING', role: 'ADMIN'},
            {userId: '2', status: 'ATENDING', role: 'USER'},
            {userId: '3', status: 'PENDING', role: 'USER'}
          ],
          aggregations: {status: []}
        };
        EventModel.getMembershipsWithFilter.and.returnValue($q.resolve(page));

        // when
        var ctrl = buildController();

        // then
        expect(EventModel.getMembershipsWithFilter).toHaveBeenCalled();
        expect(ctrl.currentPage).toEqual(page);
        expect(ctrl.participationStatusFilter.items).toEqual([]);

        // given
        EventModel.getMembershipsWithFilter.calls.reset();

        // when
        $scope.$apply();

        // then
        expect(ctrl.currentPage).toEqual(page);
        expect(EventModel.getMembershipsWithFilter).not.toHaveBeenCalled();
      });

      it('should search members with defaults', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.search();
        $scope.$apply();

        // then
        expect(EventModel.getMembershipsWithFilter).toHaveBeenCalled();
        var args = EventModel.getMembershipsWithFilter.calls.mostRecent().args;
        expect(args[0]).toBeUndefined();
        expect(args[1].getSort()).toBe('displayName');
        expect(args[3]).toEqual(['displayName']);
      });

      it('should search members with term', function () {
        // give
        var ctrl = buildController();

        // when
        ctrl.search('searchTerm');
        $scope.$apply();

        // then
        expect(EventModel.getMembershipsWithFilter).toHaveBeenCalled();
        var args = EventModel.getMembershipsWithFilter.calls.mostRecent().args;
        expect(args[0]).toBe('searchTerm');
        expect(args[1].getSort()).toEqual(['_score,DESC', 'displayName']);
        expect(args[3]).toEqual(['displayName']);
      });

      it('should not reset filters on members search', function () {
        // given
        var ctrl = buildController();
        ctrl.params.status = ['ATTEND', 'PENDING'];

        // when
        ctrl.search('searchTerm');
        $scope.$apply();

        // then
        expect(ctrl.params.searchTerm).toEqual('searchTerm');
        expect(ctrl.params.status).toEqual(['ATTEND', 'PENDING']);
      });

      it('should search members by participation status', function () {
        // given
        var ctrl = buildController();
        ctrl.params.status = ['Status 1', 'Status 2'];

        // when
        ctrl.setParticipationStatusFilter(['PENDING']);
        $scope.$apply();

        // then
        expect(ctrl.params.status).toEqual(['PENDING']);
        expect(EventModel.getMembershipsWithFilter.calls.mostRecent().args[2]).toEqual({status: ['PENDING']});
        expect(EventModel.getMembershipsWithFilter.calls.mostRecent().args[3]).toEqual(['displayName']);
        expect(EventModel.getMembershipsWithFilter.calls.mostRecent().args[4]).toEqual({status: ''});
      });

      it('should allow participants listing if it is enabled for the event', function () {
        // given
        event.showParticipants = true;
        spyOn(event, 'canManage').and.returnValue(false);

        // when
        var ctrl = buildController();

        // then
        expect(ctrl.shouldShowParticipants).toEqual(true);
      });

      it('should allow participants listing if manage permissions assigned to the user despite it is disabled for the the event',
          function () {
            // given
            event.showParticipants = false;
            spyOn(event, 'canManage').and.returnValue(true);

            // when
            var ctrl = buildController();

            // then
            expect(ctrl.shouldShowParticipants).toEqual(true);
            expect(event.canManage).toHaveBeenCalled();
          });

      it('should not allow participants listing if disabled and manage permissions are not assigned to the user',
          function () {
            // given
            event.showParticipants = false;
            spyOn(event, 'canManage').and.returnValue(false);

            // when
            var ctrl = buildController();

            // then
            expect(ctrl.shouldShowParticipants).toEqual(false);
            expect(event.canManage).toHaveBeenCalled();
          });

      it('should delete a member of the current event, decrease the amount of attendingCount by one and call method '
          + 'loadMemberships', function () {
        // given
        spyOn(event, 'removeMember').and.returnValue({
          then: function (callback) {
            callback();
            return {
              finally: function (callback) {
                callback();
              }
            };
          }
        });
        var userId = 'USER_ID';

        // when
        var ctrl = buildController();
        ctrl.removeMember(userId, 'ATTENDING');
        $scope.$apply();

        // then
        expect(event.removeMember).toHaveBeenCalledWith(userId);
        expect(event.attendingCount).toBe(-1);
      });

      it('should not decrease attendingCount if deleted member was not attending', function () {
        // given
        spyOn(event, 'removeMember').and.returnValue({
          then: function (callback) {
            callback();
            return {
              finally: function (callback) {
                callback();
              }
            };
          }
        });
        var userId = 'USER_ID';

        // when
        var ctrl = buildController();
        ctrl.removeMember(userId, 'NOT_ATTENDING');
        $scope.$apply();

        // then
        expect(event.removeMember).toHaveBeenCalledWith(userId);
        expect(event.attendingCount).toBe(0);
      });

      it('should manage events only with manage permission', function () {
        // given
        event.manage = true;
        spyOn(event, 'canManage').and.returnValue(true);

        // when
        var ctrl = buildController();

        // then
        expect(ctrl.canManage).toEqual(true);
        expect(event.canManage).toHaveBeenCalled();
      });

      it('should provide iCal download URL', function () {
        // given
        spyOn(event, 'getIcalExportUrl').and.returnValue('http://localhost/events/1/ical');

        // when
        var ctrl = buildController();

        // then
        expect(event.getIcalExportUrl).toHaveBeenCalled();
        expect(ctrl.iCalExportUrl).toEqual('http://localhost/events/1/ical');
      });

      it('should show participant groups if permission granted', function () {
        // given
        authService.onGlobalPermissions.and.callFake(function (a1, callback) {
          callback(true);
        });
        var ctrl = buildController();

        // when
        ctrl.inviteMembers();

        // then
        expect(userChooserModalService.open).toHaveBeenCalledWith({}, {usersOnly: false});
      });

      it('should not show participant groups if no permission granted', function () {
        // given
        authService.onGlobalPermissions.and.callFake(function (a1, callback) {
          callback(false);
        });

        var ctrl = buildController();

        // when
        ctrl.inviteMembers();

        // then
        expect(userChooserModalService.open).toHaveBeenCalledWith({}, {usersOnly: true});
      });

    });
  });

})();
