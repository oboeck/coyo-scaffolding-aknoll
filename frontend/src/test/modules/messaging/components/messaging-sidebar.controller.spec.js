(function () {
  'use strict';

  var moduleName = 'coyo.messaging';
  var targetName = 'MessagingSidebarController';

  describe('module: ' + moduleName, function () {
    var $controller;
    var $localStorage;
    var $rootScope;
    var authService, sidebarService, userService;

    var userMock = {
      id: 123
    };

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$localStorage_, _$rootScope_, _authService_, _sidebarService_, _userService_) {
        $controller = _$controller_;
        $localStorage = _$localStorage_;
        $rootScope = _$rootScope_;
        authService = _authService_;
        sidebarService = _sidebarService_;
        userService = _userService_;
      });

      $rootScope.screenSize = {
        isXs: false,
        isSm: false
      };
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl() {
        return $controller(targetName, {
          $localStorage: $localStorage,
          $rootScope: $rootScope,
          $scope: $rootScope.$new(),
          authService: authService,
          sidebarService: sidebarService,
          userService: userService
        });
      }

      it('should init controller', function () {
        // given
        spyOn(sidebarService, 'register');
        spyOn(authService, 'getUser').and.returnValue({
          then: function (callback) {
            callback(userMock);
            return {
              finally: function (callback) {
                callback();
              }
            };
          }
        });
        spyOn(userService, 'getPresenceStatus');

        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // then
        expect($localStorage.messagingSidebar.compact).not.toBeUndefined();
        expect($localStorage.messagingSidebar.compact).toBeTruthy();
        expect(ctrl.compact).toBeTruthy();
        expect(sidebarService.register).toHaveBeenCalledTimes(1);
        expect(userService.getPresenceStatus).toHaveBeenCalledWith(userMock, jasmine.any(Function), jasmine.any(Object));
      });

      it('should open and close', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        $rootScope.screenSize = {};
        $rootScope.screenSize.isLg = true;

        // when open
        sidebarService.open('messaging');

        // then
        expect(ctrl.show).toBeTruthy();

        // when open
        sidebarService.close('messaging');

        // then
        expect(ctrl.show).toBeFalsy();
      });

      it('should toggle compact mode', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        spyOn($rootScope, '$broadcast');

        // when toggle on
        ctrl.toggleCompact(true);

        // then
        expect(ctrl.compact).toBeTruthy();
        expect($rootScope.$broadcast.calls.argsFor(0)[0]).toBe('toggleMessagingSidebar');
        expect($localStorage.messagingSidebar.compact).toBeTruthy();
        expect($rootScope.messagingCompact).toBe(true);

        // when toggle off
        ctrl.toggleCompact(false);

        // then
        expect(ctrl.compact).toBeFalsy();
        expect($localStorage.messagingSidebar.compact).toBeFalsy();
        expect($rootScope.messagingCompact).toBe(false);
      });

      it('should reset view to home', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.view = 'foo';

        // when
        ctrl.home();

        // then
        expect(ctrl.view).toBeNull();
      });

      it('should switch view in expanded sidebar', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.compact = false;

        // when
        ctrl.switchView('someView');

        // then
        expect(ctrl.compact).toBeFalsy();
        expect(ctrl.view).toBe('someView');
      });

      it('should switch view in compact sidebar', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.compact = true;

        // when
        ctrl.switchView('someView');

        // then
        expect(ctrl.compact).toBeFalsy();
        expect(ctrl.view).toBe('someView');
      });
    });
  });
})();
