(function () {
  'use strict';

  var moduleName = 'coyo.notifications';
  var targetName = 'NotificationsDialogController';

  describe('module: ' + moduleName, function () {
    var ctrl;
    var $scope, $q, $rootScope, $timeout, notificationsMainServiceMock, targetServiceMock, socketServiceMock,
        authServiceMock, scrollBehaviourServiceMock;

    var category = 'CATEGORY';
    var url = '/some-url';
    var item1 = {id: '1', target: 'target'};
    var item2 = {id: '2', target: 'target'};
    var items = [item1, item2];
    var page = {content: [item1, item2], last: true};

    beforeEach(function () {
      module(moduleName, function () {});

      inject(function (_$controller_, _$rootScope_, _$q_, _$timeout_) {
        $scope = _$rootScope_.$new();
        $rootScope = _$rootScope_;
        $q = _$q_;
        $timeout = _$timeout_;
        spyOn($scope, '$on');

        notificationsMainServiceMock = jasmine.createSpyObj('notificationsMainService',
            ['getNotifications', 'getStatus', 'markAllSeen', 'markClicked', 'markAllClicked']);
        notificationsMainServiceMock.getNotifications.and.returnValue($q.resolve(page));
        notificationsMainServiceMock.getStatus.and.returnValue(
            $q.resolve({data: {unseen: {CATEGORY: 100}, unclicked: {CATEGORY: 50}}}));
        notificationsMainServiceMock.markAllSeen.and.returnValue($q.resolve());
        notificationsMainServiceMock.markClicked.and.returnValue($q.resolve());
        notificationsMainServiceMock.markAllClicked.and.returnValue($q.resolve());

        socketServiceMock = jasmine.createSpyObj('socketService', ['subscribe']);
        socketServiceMock.subscribe.and.returnValue(angular.noop);

        targetServiceMock = jasmine.createSpyObj('targetService', ['getLink', 'go']);
        targetServiceMock.getLink.and.returnValue(url);

        authServiceMock = jasmine.createSpyObj('authService', ['onGlobalPermissions', 'getUser']);
        authServiceMock.onGlobalPermissions.and.callFake(function (permissionNames, callback) {
          callback(permissionNames === 'ACCESS_NOTIFICATIONS');
        });
        authServiceMock.getUser.and.returnValue($q.resolve({id: 'ID'}));

        scrollBehaviourServiceMock = jasmine.createSpyObj('scrollBehaviourService',
            ['enableBodyScrolling', 'disableBodyScrolling', 'disableBodyScrollingOnXsScreen']);

        ctrl = _$controller_(targetName, {
          $scope: $scope,
          notificationsMainService: notificationsMainServiceMock,
          targetService: targetServiceMock,
          socketService: socketServiceMock,
          authService: authServiceMock,
          scrollBehaviourService: scrollBehaviourServiceMock
        });
      });
    });

    describe('controller: ' + targetName, function () {

      it('should toggle on', function () {
        spyOn(ctrl, 'switchCategory').and.stub();
        expect(ctrl.show).toBe(false);
        ctrl.toggle();
        expect(ctrl.switchCategory).toHaveBeenCalled();
        expect(ctrl.show).toBe(true);
      });

      it('should toggle off', function () {
        ctrl.show = true;
        spyOn(ctrl, 'switchCategory').and.stub();
        expect(ctrl.show).toBe(true);
        ctrl.toggle();
        expect(ctrl.switchCategory).not.toHaveBeenCalled();
        expect(ctrl.show).toBe(false);
      });

      it('should close', function () {
        ctrl.show = true;
        ctrl.close();

        expect(scrollBehaviourServiceMock.enableBodyScrolling).toHaveBeenCalled();
        expect(ctrl.show).toBe(false);
      });

      it('should switch category', function () {
        ctrl.status = {unseen: {CATEGORY: 100}};
        ctrl.switchCategory(category, event);
        expect(ctrl.category).toEqual(category);
        expect(ctrl.status.unseen[category]).toBe(0);
        expect(notificationsMainServiceMock.markAllSeen).toHaveBeenCalled();
      });

      it('should mark target as clicked and close dialog', function () {
        var notification = {target: 'target'};
        ctrl.show = true;
        ctrl.markClicked(notification);
        expect(ctrl.show).toBeFalse();
        expect(notificationsMainServiceMock.markClicked).toHaveBeenCalledWith(notification);
      });

      it('should return icon', function () {
        var icon = ctrl.getIcon('user-follow');
        expect(icon).toEqual('account-add');
      });

      it('should mark clicked', function () {
        ctrl.markAllClicked(category);
        expect(notificationsMainServiceMock.markAllClicked).toHaveBeenCalled();
      });

      it('should load more', function () {
        ctrl.loadMore(category);
        expect(ctrl.data[category]).not.toBeUndefined();
        expect(ctrl.data[category].loading).toBeTrue();
        $scope.$apply();
        expect(ctrl.data[category].currentPage).toEqual(page);
        expect(ctrl.data[category].items).toEqual(items);
        expect(ctrl.data[category].loading).toBeFalse();

        ctrl.loadMore(category);
        expect(ctrl.data[category]).not.toBeUndefined();
        expect(ctrl.data[category].loading).toBeFalse();
        $scope.$apply();
        expect(ctrl.data[category].currentPage).toEqual(page);
        expect(ctrl.data[category].items).toEqual(items);
        expect(ctrl.data[category].loading).toBeFalse();
      });

      it('should register event handlers', function () {
        // when
        ctrl.$onInit();

        // then
        expect(socketServiceMock.subscribe)
            .toHaveBeenCalledWith('/user/topic/notification', jasmine.any(Function), 'raised');
        expect(socketServiceMock.subscribe)
            .toHaveBeenCalledWith('/user/topic/notification', jasmine.any(Function), 'statusChanged');
        expect($scope.$on).toHaveBeenCalledWith('$destroy', jasmine.any(Function));
      });

      it('should update status on reconnect', function () {
        // given
        ctrl.status = {};
        notificationsMainServiceMock.getStatus.and.returnValues(
            $q.resolve({data: {unseen: {CATEGORY: 150}, unclicked: {CATEGORY: 75}}}), // response on init
            $q.resolve({data: {unseen: {CATEGORY: 151}, unclicked: {CATEGORY: 76}}}) // response on reconnect
        );

        // when
        ctrl.$onInit();
        $rootScope.$emit('socketService:reconnected');
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(ctrl.status.data.unseen.CATEGORY).toBe(151);
        expect(ctrl.status.data.unclicked.CATEGORY).toBe(76);
      });

      it('should load links for all notifications for current page', function () {
        // when
        ctrl.loadMore(category);
        $scope.$apply();

        // then
        expect(ctrl.links).not.toBeEmptyObject();
        expect(_.size(ctrl.links)).toEqual(2);
        expect(ctrl.links[item1.id]).toEqual(url);
        expect(ctrl.links[item2.id]).toEqual(url);
      });

      it('should not load link for file notification', function () {
        // given
        var item = {id: '2', target: {name: 'file'}};
        var pageWithFileNotification = {content: [item], last: true};
        notificationsMainServiceMock.getNotifications.and.returnValue($q.resolve(pageWithFileNotification));

        // when
        ctrl.loadMore(category);
        $scope.$apply();

        // then
        expect(ctrl.links).toBeEmptyObject();
      });

      it('should open file when click handler is called with file notification item', function () {
        // given
        var target = {name: 'file'};

        // when
        ctrl.clickHandler(target);

        // then
        expect(targetServiceMock.go).toHaveBeenCalled();
      });

      it('should not open file when click handler is called with a not file notification item', function () {
        // given
        var target = {name: 'user'};

        // when
        ctrl.clickHandler(target);

        // then
        expect(targetServiceMock.go).not.toHaveBeenCalled();
      });

      it('should enable scrolling when button is clicked', function () {
        // given
        var target = {name: 'user'};
        ctrl.show = true;

        // when
        ctrl.clickHandler(target);

        // then
        expect(scrollBehaviourServiceMock.enableBodyScrolling).toHaveBeenCalled();
      });

      it('should enable scrolling when button is clicked', function () {
        // given
        ctrl.show = true;

        // when
        ctrl.toggle();

        // then
        expect(scrollBehaviourServiceMock.enableBodyScrolling).toHaveBeenCalled();
      });

      it('should disable scrolling when toggle is called and show is false', function () {
        // given
        ctrl.show = false;
        spyOn(ctrl, 'switchCategory').and.stub();

        // when
        ctrl.toggle();

        // then
        expect(scrollBehaviourServiceMock.disableBodyScrollingOnXsScreen).toHaveBeenCalled();
      });

      it('should enable scrolling when toggle is called and show is false', function () {
        // given
        ctrl.show = true;
        spyOn(ctrl, 'switchCategory').and.stub();

        // when
        ctrl.toggle();

        // then
        expect(scrollBehaviourServiceMock.enableBodyScrolling).toHaveBeenCalled();
      });
    });
  });

})();
