(function () {
  'use strict';

  var moduleName = 'coyo.pages';

  describe('module: ' + moduleName, function () {

    var $controller, currentUser, PageModel, $scope, settings;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, $rootScope) {
      $controller = _$controller_;
      $scope = $rootScope.$new();
      settings = {defaultVisibilityPages: 'PUBLIC'};
      currentUser = jasmine.createSpyObj('UserModel', ['hasGlobalPermissions']);
      PageModel = function (data) {
        return angular.extend(jasmine.createSpyObj('PageModel', ['create']), data);
      };
    }));

    var controllerName = 'PagesCreateController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          currentUser: currentUser,
          PageModel: PageModel,
          $scope: $scope,
          settings: settings
        });
      }

      it('should init set visibility private without create public page permission', function () {
        // given
        var ctrl = buildController();
        currentUser.hasGlobalPermissions.and.returnValue(false);

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.page.visibility).toBe('PRIVATE');
        expect(currentUser.hasGlobalPermissions).toHaveBeenCalledWith('CREATE_PUBLIC_PAGE');
      });

      it('should init set visibility public with create public page permission', function () {
        // given
        var ctrl = buildController();
        currentUser.hasGlobalPermissions.and.returnValue(true);

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.page.visibility).toBe('PUBLIC');
        expect(currentUser.hasGlobalPermissions).toHaveBeenCalledWith('CREATE_PUBLIC_PAGE');
      });

      it('should init add current user as admin', function () {
        // given
        var ctrl = buildController();
        currentUser.id = 'user-id';

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.page.adminIds).toEqual(['user-id']);
        expect(ctrl.page.adminGroupIds).toEqual([]);
        expect(ctrl.page.memberIds).toEqual([]);
        expect(ctrl.page.memberGroupIds).toEqual([]);
      });

      it('should init two wizard steps without auto-subscribe permission', function () {
        // given
        var ctrl = buildController();
        currentUser.hasGlobalPermissions.and.returnValue(false);

        // when
        ctrl.$onInit();

        //
        expect(currentUser.hasGlobalPermissions).toHaveBeenCalledWith('AUTO_SUBSCRIBE_PAGE');
        expect(ctrl.wizard.states.length).toBe(2);
        expect(ctrl.wizard.active).toBe(0);
      });

      it('should init three wizard steps with auto-subscribe permission', function () {
        // given
        var ctrl = buildController();
        currentUser.hasGlobalPermissions.and.returnValue(true);

        // when
        ctrl.$onInit();

        //
        expect(currentUser.hasGlobalPermissions).toHaveBeenCalledWith('AUTO_SUBSCRIBE_PAGE');
        expect(ctrl.wizard.states.length).toBe(3);
        expect(ctrl.wizard.active).toBe(0);
      });
    });
  });

})();
