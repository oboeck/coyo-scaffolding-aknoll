(function () {
  'use strict';

  var moduleName = 'coyo.profile';
  var ctrlName = 'ProfileMainController';

  describe('module: ' + moduleName, function () {

    beforeEach(module(moduleName, function ($provide) {
      $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
    }));

    describe('controller: ' + ctrlName, function () {
      var $controller, $q, $scope, $rootScope;
      var ctrl, coyoNotification, senderService, userService, UserModel,
          currentUser, user, profileFieldGroups, linkPattern, emailPattern, phonePattern;

      beforeEach(inject(function (_$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();

        coyoNotification = jasmine.createSpyObj('coyoNotification', ['error']);
        senderService = jasmine.createSpyObj('senderService', ['changeAvatar', 'changeCover']);
        userService = {
          setProfileFields: function () {}
        };
        UserModel = {
          getWithPermissions: function () {}
        };

        currentUser = {id: 'CurrentUserId'};
        user = {properties: {}};
        linkPattern = '';
        emailPattern = '';
        phonePattern = '';
        profileFieldGroups = [
          {
            id: 1,
            name: 'group1',
            fields: [
              {id: 3, name: 'field3'},
              {id: 4, name: 'field4'}
            ],
            sortOrder: 0,
            editMode: false,
            saving: false,
            modifiable: false
          },
          {
            id: 2,
            name: 'group2',
            fields: [
              {id: 5, name: 'field5'},
              {id: 6, name: 'field6'}
            ],
            sortOrder: 1,
            editMode: false,
            saving: false,
            modifiable: true
          }
        ];
      }));

      function buildController(_profileFieldGroups) {
        return $controller(ctrlName, {
          $scope: $scope,
          senderService: senderService,
          coyoNotification: coyoNotification,
          userService: userService,
          currentUser: currentUser,
          UserModel: UserModel,
          user: user,
          profileFieldGroups: _profileFieldGroups ? _profileFieldGroups : angular.copy(profileFieldGroups),
          linkPattern: linkPattern,
          emailPattern: emailPattern,
          phonePattern: phonePattern
        });
      }

      describe('controller init', function () {

        it('should perform initial group and user data preparation', function () {
          // given
          // nothing to see here...

          // when
          ctrl = buildController();

          // then
          expect(ctrl.user.properties).toEqual({
            field3: '',
            field4: '',
            field5: '',
            field6: ''
          });
        });

        it('should perform initial group sorting', function () {
          // given
          var _profileFieldGroups = angular.copy(profileFieldGroups);
          _profileFieldGroups[0].sortOrder = 1;
          _profileFieldGroups[1].sortOrder = 0;

          // when
          ctrl = buildController(_profileFieldGroups);

          // then
          expect(ctrl.profileGroups[0].sortOrder).toBe(0);
          expect(ctrl.profileGroups[1].sortOrder).toBe(1);
        });
      });

      describe('controller functions', function () {

        it('should trigger an event if submitting the form and succeeding', function () {
          // given
          spyOn(userService, 'setProfileFields').and.callFake(function () {
            var deferred = $q.defer();
            deferred.resolve('Some success result.');
            return deferred.promise;
          });

          // when
          ctrl = buildController();
          ctrl.onSubmit({
            saving: false,
            fields: {
              field1: {id: 1, name: 'field1'},
              field2: {id: 2, name: 'field2'}
            },
            editMode: true
          });

          // then
          expect(userService.setProfileFields).toHaveBeenCalled();
        });

        it('should refresh the user if cancelling the form', function () {
          // given
          spyOn(UserModel, 'getWithPermissions').and.callFake(function () {
            var deferred = $q.defer();
            deferred.resolve('Some success result.');
            return deferred.promise;
          });

          // when
          ctrl = buildController();
          ctrl.onCancel();

          // then
          expect(UserModel.getWithPermissions).toHaveBeenCalled();
        });

        it('should update the current users profile if current user changes', function () {
          // given
          user.id = currentUser.id;
          var userUpdate = {
            id: currentUser.id,
            name: 'Updated Name'
          };

          // when
          ctrl = buildController();
          $rootScope.$emit('currentUser:updated', userUpdate);
          $scope.$apply();

          // then
          expect(ctrl.user).toEqual(userUpdate);
          expect(ctrl.user.name).toBe('Updated Name');
        });

        it('should not update a colleagues profile if current user changes', function () {
          // given
          user.id = 'SomeOtherUser';
          var userUpdate = {
            id: currentUser.id,
            name: 'Updated Name'
          };

          // when
          ctrl = buildController();
          $rootScope.$emit('currentUser:updated', userUpdate);
          $scope.$apply();

          // then
          expect(ctrl.user.id).toBe('SomeOtherUser');
          expect(ctrl.user).not.toEqual(userUpdate);
          expect(ctrl.user.name).not.toBe('Updated Name');
        });

      });
    });
  });

})();
