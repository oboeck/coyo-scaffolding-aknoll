(function () {
  'use strict';

  var moduleName = 'commons.tour';
  var directiveName = 'coyo-tour-step-mobile';

  describe('module: ' + moduleName, function () {

    var $scope, $rootScope, $compile, $q, $transitions, tourService, step;

    beforeEach(module(moduleName));
    beforeEach(module('commons.templates'));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {
        step = {
          topic: 'test',
          order: 5,
          title: 'My Mobile Title',
          content: 'Mobile Tour Step Content',
          imageUrl: 'http://coyotest.com/image.png'
        };

        tourService = jasmine.createSpyObj('tourService', ['init', 'isEnabled']);
        $transitions = jasmine.createSpyObj('$transitions', ['onStart', 'onSuccess', 'onError']);

        module(function ($provide) {
          $provide.value('tourService', tourService);
          $provide.value('$transitions', $transitions);
        });
      });

      beforeEach(inject(function (_$rootScope_, _$compile_, _$q_, _$transitions_) {
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();
        $compile = _$compile_;
        $q = _$q_;
        $transitions = _$transitions_;

        $transitions.onSuccess.and.callFake(function (criteria, callback) {
          $rootScope.$on('transition-success-mock', callback);
        });
      }));

      function _createTemplate(tourStepTemplate) {
        return '<div ui-tour="mobileTour">' + tourStepTemplate + '</div>';
      }

      it('should render enabled step', function () {
        // given
        var template = '<coyo-tour-step-mobile topic="{{step.topic}}" order="{{step.order}}" title="{{step.title}}" ' +
            'content="{{step.content}}" image-url="{{step.imageUrl}}"></coyo-tour-step-mobile>';
        $scope.step = step;
        tourService.isEnabled.and.returnValue($q.resolve(true));

        // when
        var element = $compile(angular.element(_createTemplate(template)))($scope);
        $scope.$digest();

        // then
        var span = element.find('span');
        expect(span.attr('tour-step-belongs-to')).toBe('mobileTour');
        expect(span.attr('tour-step')).toBe(step.topic);
        expect(span.attr('tour-step-title')).toBe(step.title);
        expect(span.attr('tour-step-content')).toStartWith(step.content);
        expect(span.attr('tour-step-content').indexOf('src="' + step.imageUrl + '"')).toBeGreaterThan(-1);
        expect(span.attr('tour-step-order')).toBe('' + step.order);
      });

      it('should update the enabled status on state change', function () {
        // given
        var template = '<coyo-tour-step-mobile topic="{{step.topic}}" order="{{step.order}}" title="{{step.title}}" ' +
            'content="{{step.content}}"></coyo-tour-step-mobile>';
        $scope.step = step;
        tourService.isEnabled.and.returnValue($q.resolve(true));

        // when
        var element = $compile(angular.element(_createTemplate(template)))($scope);
        $scope.$digest();

        // then
        expect(element.find('span').attr('tour-step')).toBe('test');

        // when
        tourService.isEnabled.and.returnValue($q.resolve(false));
        $rootScope.$emit('transition-success-mock');
        $scope.$apply();

        // then
        expect(element.find('span').length).toBe(0);
      });

    });

  });
})();
