(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';
  var directiveName = 'coyo-workspace-member-avatars';

  describe('module: ' + moduleName, function () {

    var $scope, $rootScope, $compile, $controller, $q, template, workspace;

    beforeEach(module('commons.templates'));
    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$rootScope_, _$compile_, _$controller_, _$q_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $controller = _$controller_;
        $q = _$q_;
        template = '<coyo-workspace-member-avatars workspace="workspace" loading="loading"></coyo-workspace-member-avatars>';

        workspace = jasmine.createSpyObj('WorkspaceModel', ['getMembers']);
        var response = {
          content: [{
            user: {
              id: 'USER-ID-ONE',
              avatar: {
                empty: true
              }
            }
          }, {
            user: {
              id: 'USER-ID-TWO',
              avatar: {
                empty: true
              }
            }
          }],
          totalElements: 2
        };
        workspace.getMembers.and.returnValue($q.resolve(response));
      }));

      it('should show member avatars', function () {
        // given
        $scope.workspace = workspace;

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.find('coyo-user-avatar').length).toBe(2);
        expect(element.find('.workspace-member-count').html()).toEqual('2');
      });

      describe('controller: WorkspaceMemberAvatarsController', function () {

        function buildController() {
          return $controller('WorkspaceMemberAvatarsController', {
            $scope: $scope
          }, {
            workspace: workspace
          });
        }

        it('should reload member list on workspace:member:added event', function () {
          // given
          var ctrl = buildController();
          $scope.$apply();

          // when
          $rootScope.$emit('workspace.member:added');
          $scope.$apply();

          // then
          expect(workspace.getMembers).toHaveBeenCalledTimes(2);
          expect(ctrl.memberCount).toBe(2);
        });

        it('should correct the member count on workspace:member:added event', function () {
          // given
          var ctrl = buildController();
          $scope.$apply();
          ctrl.memberCount = 12;
          ctrl.members.length = 12; // simulate an array of 12 entries

          // when
          $rootScope.$emit('workspace.member:added');
          $scope.$apply();

          // then
          expect(workspace.getMembers).toHaveBeenCalledTimes(1);
          expect(ctrl.memberCount).toBe(13);
        });

        it('should reload member list on workspace:member:removed event', function () {
          // given
          var ctrl = buildController();
          $scope.$apply();

          // when
          $rootScope.$emit('workspace.member:removed', 'USER-ID-ONE');
          $scope.$apply();

          // then
          expect(workspace.getMembers).toHaveBeenCalledTimes(2);
          expect(ctrl.memberCount).toBe(2);
        });

        it('should correct member count on workspace:member:removed event', function () {
          // given
          var ctrl = buildController();
          $scope.$apply();

          // when
          $rootScope.$emit('workspace.member:removed', 'USER-ID-UNKNOWN');
          $scope.$apply();

          // then
          expect(workspace.getMembers).toHaveBeenCalledTimes(1);
          expect(ctrl.memberCount).toBe(1);
        });

      });

    });

  });
})();
