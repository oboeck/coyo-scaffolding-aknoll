(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';
  var targetName = 'WorkspaceSettingsController';

  describe('module: ' + moduleName, function () {
    var $controller, $state, ctrl, WorkspaceModelMock, $q, modalService, $rootScope, workspace,
        coyoNotification, $scope;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });

      inject(function (_$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        $scope = $rootScope.$new();

        $state = jasmine.createSpyObj('$state', ['go', 'href', 'is']);
        $state.is.and.returnValue(true);

        WorkspaceModelMock = jasmine.createSpyObj('WorkspaceModel', ['update', 'delete']);
        WorkspaceModelMock.update.and.returnValue({
          then: function (callback) {
            callback(workspace);
            return {
              finally: function (callback) {
                callback();
              }
            };
          }
        });
        workspace = angular.extend(WorkspaceModelMock, {
          id: 1,
          visibility: 'PUBLIC',
          name: 'Some workspace',
          categories: [{
            id: 'cat123',
            name: 'Cat 123'
          }]
        });
        modalService = jasmine.createSpyObj('modalService', ['confirm', 'confirmDelete']);
        coyoNotification = jasmine.createSpyObj('coyoNotification', ['success']);

        ctrl = $controller(targetName, {
          $state: $state,
          workspace: angular.copy(workspace),
          WorkspaceModel: function () {
            return WorkspaceModelMock;
          },
          $scope: $scope,
          modalService: modalService,
          coyoNotification: coyoNotification
        });

        $scope.$ctrl = ctrl;
      });
    });

    describe('controller: ' + targetName, function () {

      it('should init', function () {
        // when
        ctrl.$onInit();

        // then
        expect(ctrl.oldName).toEqual(workspace.displayName);
        expect(ctrl.oldSlug).toEqual(workspace.slug);
        expect(ctrl.workspace.categories[0].displayName).toEqual(workspace.categories[0].name);
      });

      it('should save', function () {
        // when
        ctrl.$onInit();
        ctrl.save();

        // then
        expect($state.is).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalled();
      });

      it('should confirm visiblity change', function () {
        // given
        modalService.confirm.and.returnValue({result: $q.resolve()});

        // when
        ctrl.$onInit();
        $rootScope.$apply();
        $scope.$ctrl.workspace.visibility = 'PRIVATE';

        $rootScope.$apply();

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect($scope.$ctrl.workspace.visibility).toBe('PRIVATE');
      });

      it('should revert visibility change', function () {
        // given
        modalService.confirm.and.returnValue({result: $q.reject()});

        // when
        ctrl.$onInit();
        $rootScope.$apply();
        $scope.$ctrl.workspace.visibility = 'PRIVATE';

        $rootScope.$apply();

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect($scope.$ctrl.workspace.visibility).toBe('PUBLIC');
      });

      it('should delete', function () {
        // given
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        workspace.delete.and.returnValue($q.resolve());

        // when
        ctrl.$onInit();

        ctrl.delete();
        $rootScope.$apply();

        // then
        expect(workspace.delete).toHaveBeenCalled();
        expect(coyoNotification.success).toHaveBeenCalledWith('WORKSPACE.DELETE.SUCCESS');
        expect($state.go).toHaveBeenCalledWith('main.workspace');
      });

      it('should not inherit state params', function () {
        // when
        ctrl.$onInit();

        // then
        expect($state.href).toHaveBeenCalledWith('main.workspace', {}, {inherit: false});
      });

    });
  });
})();
