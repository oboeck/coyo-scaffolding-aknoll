(function () {
  'use strict';

  var moduleName = 'coyo.widgets.api';
  var directiveName = 'oyocWidgetInlineOptions';

  describe('module: ' + moduleName, function () {

    var $scope, $compile, template, $templateCache;

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {
        angular.module('coyo.widgets.api.test', [moduleName]);
        angular.module('coyo.widgets.api.test').controller('TestController', function (model, config) {
          var vm = this;
          vm.model = model;
          vm.config = config;
        });
      });

      beforeEach(module('coyo.widgets.api.test'));

      beforeEach(inject(function ($rootScope, _$compile_, _$templateCache_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $templateCache = _$templateCache_;
        template = '<div><oyoc-widget-inline-options config="config" model="model"></oyoc-widget-inline-options></div>';
      }));

      it('should render configured inline options', function () {
        // given
        $scope.model = {id: 'model-id'};
        $scope.config = {inlineOptions: {
          controller: 'TestController',
          controllerAs: '$ctrl',
          templateUrl: 'test-template-url'
        }};
        $templateCache.put('test-template-url', '<div>id:{{$ctrl.model.id}}, controller:{{$ctrl.config.inlineOptions.controller}}</div>');

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.find('div').text()).toBe('id:model-id, controller:TestController');
        expect(element.find('oyoc-widget-inline-options').length).toBe(0);
      });

    });

  });
})();
