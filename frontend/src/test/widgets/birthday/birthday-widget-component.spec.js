(function () {
  'use strict';

  var moduleName = 'coyo.widgets.birthday';
  var directiveName = 'coyoBirthdayWidget';

  describe('module: ' + moduleName, function () {

    var $controller, BirthdayWidgetModel, birthdayService, widget, $rootScope, $scope, widgetStatusService;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        widgetStatusService = jasmine.createSpyObj('widgetStatusService', ['refreshOnSettingsChange']);
        widget = {settings: {}};
      }));

      var controllerName = 'BirthdayWidgetController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            BirthdayWidgetModel: BirthdayWidgetModel,
            birthdayService: birthdayService,
            $scope: $scope,
            widgetStatusService: widgetStatusService
          }, {
            widget: widget
          });
        }

        it('should compare only one birthday', function () {
          // given
          var ctrl = buildController();
          ctrl.users = [{birthday: '1970-01-01'}];

          // when
          $scope.$apply();

          // then
          expect(ctrl.isSameDayAsPreviousUser(0)).toBeFalse();
        });

        it('should compare different day birthdays', function () {
          // given
          var ctrl = buildController();
          ctrl.users = [{birthday: '1970-01-01'}, {birthday: '1970-01-02'}];

          // when
          $scope.$apply();

          // then
          expect(ctrl.isSameDayAsPreviousUser(1)).toBeFalse();
        });

        it('should compare same day birthdays', function () {
          // given
          var ctrl = buildController();
          ctrl.users = [{birthday: '1970-01-01'}, {birthday: '1970-01-01'}];

          // when
          $scope.$apply();

          // then
          expect(ctrl.isSameDayAsPreviousUser(1)).toBeTrue();
        });
      });

    });

  });
})();
