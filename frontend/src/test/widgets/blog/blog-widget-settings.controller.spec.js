(function () {
  'use strict';

  var moduleName = 'coyo.widgets.blog';

  describe('module: ' + moduleName, function () {

    var $controller, $scope;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, $rootScope) {
      $controller = _$controller_;
      $scope = $rootScope.$new();
    }));

    var controllerName = 'BlogWidgetSettingsController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope
        });
      }

      it('should initialize default article count', function () {
        // given
        $scope.model = {settings: {}};

        // when
        buildController();

        // then
        expect($scope.model.settings._articleCount).toBe(5);
      });

    });
  });

})();
