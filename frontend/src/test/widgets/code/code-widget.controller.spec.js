(function () {
  'use strict';

  var moduleName = 'coyo.widgets.code';
  var directiveName = 'coyoCodeWidget';

  describe('module: ' + moduleName, function () {

    var $scope, $controller, $sce;
    var widget;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function ($rootScope, _$controller_, _$sce_) {
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $sce = _$sce_;

        widget = {
          id: 'WIDGET-ID',
          modified: 'MODIFIED',
          settings: {}
        };
      }));

      var controllerName = 'CodeWidgetController';

      describe('controller: ' + controllerName, function () {

        function buildController(widget) {
          return $controller(controllerName, {
            $scope: $scope,
            $sce: $sce
          }, {
            widget: widget
          });
        }

        it('should init and mark as invalid if no widget ID is present', function () {
          // given
          var widget = {};

          // when
          var ctrl = buildController(widget);
          ctrl.$onInit();

          // then
          expect(ctrl.valid).toBe(false);
        });

        it('should init and mark as valid if widget ID is present', function () {
          // given

          // when
          var ctrl = buildController(widget);
          ctrl.$onInit();

          // then
          expect(ctrl.valid).toBe(true);
        });

      });

    });

  });
})();
