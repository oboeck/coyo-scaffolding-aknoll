(function () {
  'use strict';

  var moduleName = 'coyo.widgets.latestfiles';

  describe('module: ' + moduleName, function () {

    var $controller, latestFilesWidgetService, fileAuthorService, $q, $rootScope, $scope, fileDetailsModalService, widget;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
      $controller = _$controller_;
      $q = _$q_;
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      latestFilesWidgetService = jasmine.createSpyObj('latestFilesWidgetService', ['getLatestFiles']);
      fileAuthorService = jasmine.createSpyObj('fileAuthorService', ['loadFileAuthors']);
      widget = {
        settings: {
          _app: {
            id: 'app-id',
            senderId: 'sender-id'
          },
          _fileCount: 5,
          _showAuthors: true
        }
      };
    }));

    var controllerName = 'LatestFilesWidgetController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          latestFilesWidgetService: latestFilesWidgetService,
          fileAuthorService: fileAuthorService,
          $scope: $scope,
          fileDetailsModalService: fileDetailsModalService
        }, {widget: widget});
      }

      it('should load latest files', function () {
        // given
        latestFilesWidgetService.getLatestFiles.and.returnValue($q.resolve({
          app: {settings: {showAuthors: true}},
          documents: [{id: 'file-1'}, {id: 'file-2'}]
        }));
        fileAuthorService.loadFileAuthors.and.returnValue($q.resolve({
          'file-1': {id: 'author-1'},
          'file-2': {id: 'author-2'}
        }));
        var ctrl = buildController();

        // when
        ctrl.loadFiles();
        $rootScope.$apply();
        $rootScope.$apply();

        // then
        expect(ctrl.files.length).toBe(2);
        expect(ctrl.files[0].id).toBe('file-1');
        expect(ctrl.files[1].id).toBe('file-2');
        expect(ctrl.files[0].author.id).toBe('author-1');
        expect(ctrl.files[1].author.id).toBe('author-2');
      });

    });
  });

})();
