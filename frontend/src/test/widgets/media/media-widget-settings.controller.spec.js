(function () {
  'use strict';

  var moduleName = 'coyo.widgets.media';

  describe('module: ' + moduleName, function () {

    var $rootScope, $scope, $q, $controller, widgetScope, widget, mediaWidgetSelectionService, coyoEndpoints,
        modalService, fileLibraryModalService, SenderModel, appService;

    beforeEach(function () {
      fileLibraryModalService = jasmine.createSpyObj('fileLibraryModalService', ['open']);
      SenderModel = jasmine.createSpyObj('SenderModel', ['getCurrentIdOrSlug', 'getWithPermissions']);
      appService = jasmine.createSpyObj('appService', ['getCurrentAppIdOrSlug']);

      module(moduleName, function ($provide) {
        $provide.value('fileLibraryModalService', fileLibraryModalService);
        $provide.value('SenderModel', SenderModel);
        $provide.value('appService', appService);
      });

      inject(
          function (_$rootScope_, _$q_, _$controller_, _mediaWidgetSelectionService_, _coyoEndpoints_, _modalService_) {
            $rootScope = _$rootScope_;
            $scope = $rootScope.$new();
            $q = _$q_;
            $controller = _$controller_;
            widgetScope = jasmine.createSpyObj('widgetScope', ['$broadcast']);
            mediaWidgetSelectionService = _mediaWidgetSelectionService_;
            coyoEndpoints = _coyoEndpoints_;
            modalService = _modalService_;
            widget = {
              settings: {
                album: {},
                _media: []
              }
            };
          });
    });

    var controllerName = 'MediaWidgetSettingsController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $q: $q,
          mediaWidgetSelectionService: mediaWidgetSelectionService,
          widgetScope: widgetScope,
          coyoEndpoints: coyoEndpoints,
          modalService: modalService
        }, {
          widget: widget
        });
      }

      it('should apply correct sortOrderIds', function () {
        // given
        $scope.model = {};
        var media = [
          {id: 'id1', senderId: 'senderId1', contentType: 'image/jpg'},
          {id: 'id2', senderId: 'senderId2', contentType: 'video/mp4'},
          {id: 'id3', senderId: 'senderId3', contentType: 'image/jpg'}
        ];
        var ctrl = buildController();
        fileLibraryModalService.open.and.returnValue($q.resolve(media));

        // when
        ctrl.openFileLibrary();
        $rootScope.$apply();

        // then
        expect(ctrl.model.media).toBeDefined();
        expect(ctrl.model.media[0].id).toBe('id1');
        expect(ctrl.model.media[0].senderId).toBe('senderId1');
        expect(ctrl.model.media[0].contentType).toBe('image/jpg');
        expect(ctrl.model.media[0].sortOrderId).toBe(0);
        expect(ctrl.model.media[1].id).toBe('id2');
        expect(ctrl.model.media[1].senderId).toBe('senderId2');
        expect(ctrl.model.media[1].contentType).toBe('video/mp4');
        expect(ctrl.model.media[1].sortOrderId).toBe(1);
        expect(ctrl.model.media[2].id).toBe('id3');
        expect(ctrl.model.media[2].senderId).toBe('senderId3');
        expect(ctrl.model.media[2].contentType).toBe('image/jpg');
        expect(ctrl.model.media[2].sortOrderId).toBe(2);
      });

      it('should add new media after existing media with correct sortOrderIds', function () {
        // given
        $scope.model = {};
        var ctrl = buildController();
        ctrl.model.media = [
          {id: 'id1', senderId: 'senderId1', contentType: 'image/jpg', sortOrderId: 0}
        ];
        fileLibraryModalService.open.and.returnValue(
            $q.resolve([{id: 'id2', senderId: 'senderId2', contentType: 'video/mp4', sortOrderId: 1},
              {id: 'id3', senderId: 'senderId3', contentType: 'image/jpg', sortOrderId: 2}
            ])
        );

        // when
        ctrl.openFileLibrary();
        $rootScope.$apply();

        // then
        expect(ctrl.model.media).toBeDefined();
        expect(ctrl.model.media[0].id).toBe('id1');
        expect(ctrl.model.media[0].senderId).toBe('senderId1');
        expect(ctrl.model.media[0].contentType).toBe('image/jpg');
        expect(ctrl.model.media[0].sortOrderId).toBe(0);
        expect(ctrl.model.media[1].id).toBe('id2');
        expect(ctrl.model.media[1].senderId).toBe('senderId2');
        expect(ctrl.model.media[1].contentType).toBe('video/mp4');
        expect(ctrl.model.media[1].sortOrderId).toBe(1);
        expect(ctrl.model.media[2].id).toBe('id3');
        expect(ctrl.model.media[2].senderId).toBe('senderId3');
        expect(ctrl.model.media[2].contentType).toBe('image/jpg');
        expect(ctrl.model.media[2].sortOrderId).toBe(2);
      });

      it('should reorder sortOrderIds after media deletion', function () {
        // given
        $scope.model = {};
        var ctrl = buildController();
        ctrl.model.media = [
          {id: 'id1', senderId: 'senderId1', contentType: 'image/jpg', sortOrderId: 0},
          {id: 'id2', senderId: 'senderId2', contentType: 'video/mp4', sortOrderId: 1},
          {id: 'id3', senderId: 'senderId3', contentType: 'image/jpg', sortOrderId: 2}
        ];

        // when
        ctrl.deleteMedia(ctrl.model.media[0]);
        $rootScope.$apply();

        // then
        expect(ctrl.model.media).toBeDefined();
        expect(ctrl.model.media[0].id).toBe('id2');
        expect(ctrl.model.media[0].senderId).toBe('senderId2');
        expect(ctrl.model.media[0].contentType).toBe('video/mp4');
        expect(ctrl.model.media[0].sortOrderId).toBe(0);
        expect(ctrl.model.media[1].id).toBe('id3');
        expect(ctrl.model.media[1].senderId).toBe('senderId3');
        expect(ctrl.model.media[1].contentType).toBe('image/jpg');
        expect(ctrl.model.media[1].sortOrderId).toBe(1);
        expect(ctrl.model.media[2]).toBeUndefined();
      });
    });
  });

})();
