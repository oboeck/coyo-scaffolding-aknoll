(function () {
  'use strict';

  var moduleName = 'coyo.widgets.upcomingEvents';
  var directiveName = 'coyoUpcomingEventsWidget';

  describe('module: ' + moduleName, function () {

    var $controller, UpcomingEventsWidgetModel, widget, $q, $rootScope, $scope, widgetStatusService, moment;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$rootScope_, _moment_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        moment = _moment_;
        $q = _$q_;
        $scope = $rootScope.$new();
        UpcomingEventsWidgetModel = jasmine.createSpyObj('UpcomingEventsWidgetModel',
            ['getUpcomingEventsWithFilters', 'getUpcomingEventsWithFiltersPreview']);
        UpcomingEventsWidgetModel.getUpcomingEventsWithFiltersPreview.and.returnValue($q.resolve());
        UpcomingEventsWidgetModel.getUpcomingEventsWithFilters.and.returnValue($q.resolve());
        widgetStatusService = jasmine.createSpyObj('widgetStatusService', ['refreshOnSettingsChange']);
        widget = {
          settings: {
            _numberOfDisplayedEvents: 5,
            _displayOngoingEvents: true,
            _fetchUpcomingEvents: 'ALL'
          },
          id: 1
        };
      }));

      var controllerName = 'upcomingEventsWidgetController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            moment: moment,
            UpcomingEventsWidgetModel: UpcomingEventsWidgetModel,
            $scope: $scope,
            widgetStatusService: widgetStatusService
          }, {
            widget: widget
          });
        }

        function date2Str(date) {
          return date.format('YYYY-MM-DDTHH:mm:ss');
        }

        it('should load upcoming events preview', function () {
          // given
          widget.id = undefined;
          var events = ['events'];
          var filters = {};
          UpcomingEventsWidgetModel.getUpcomingEventsWithFiltersPreview.and.returnValue($q.resolve(events));
          var ctrl = buildController();

          // when
          ctrl.loadEvents();
          $rootScope.$apply();

          // then
          expect(ctrl.events).toEqual(events);
          expect(UpcomingEventsWidgetModel.getUpcomingEventsWithFiltersPreview).toHaveBeenCalledWith(filters, 5);
          var args = UpcomingEventsWidgetModel.getUpcomingEventsWithFiltersPreview.calls.mostRecent().args;
          expect(args[0]).toEqual(filters);
          expect(args[1]).toBe(5);
        });

        it('should load upcoming events with ongoing events', function () {
          // given
          var start = date2Str(moment().subtract(1, 'days'));
          var end = date2Str(moment().add(1, 'days'));
          var startFuture = date2Str(moment().add(1, 'days'));
          var endFuture = date2Str(moment().add(2, 'days'));
          var events = [{startDate: start, endDate: end}, {startDate: startFuture, endDate: endFuture}];
          var filters = {};
          widget.settings = {
            _numberOfDisplayedEvents: 5,
            _displayOngoingEvents: true,
            _fetchUpcomingEvents: 'ALL'
          };
          UpcomingEventsWidgetModel.getUpcomingEventsWithFilters.and.returnValue($q.resolve(events));
          var ctrl = buildController();

          // when
          ctrl.loadEvents();
          $rootScope.$apply();

          // then
          expect(ctrl.events).toEqual(events);
          expect(ctrl.events).toBeArrayOfSize(2);
          expect(UpcomingEventsWidgetModel.getUpcomingEventsWithFilters).toHaveBeenCalledWith(filters, 5, widget.id);
          var args = UpcomingEventsWidgetModel.getUpcomingEventsWithFilters.calls.mostRecent().args;
          expect(args[0]).toEqual(filters);
          expect(args[1]).toBe(5);
          expect(args[2]).toBe(widget.id);
        });

        it('should load upcoming events without ongoing events', function () {
          // given
          var start = date2Str(moment().subtract(1, 'days'));
          var end = date2Str(moment().add(1, 'days'));
          var startFuture = date2Str(moment().add(1, 'days'));
          var endFuture = date2Str(moment().add(2, 'days'));
          var events = [{startDate: start, endDate: end}, {startDate: startFuture, endDate: endFuture}];
          var filters = {};
          widget.settings = {
            _numberOfDisplayedEvents: 5,
            _displayOngoingEvents: false,
            _fetchUpcomingEvents: 'ALL'
          };
          UpcomingEventsWidgetModel.getUpcomingEventsWithFilters.and.returnValue($q.resolve(events));
          var ctrl = buildController();

          // when
          ctrl.loadEvents();
          $rootScope.$apply();

          // then
          expect(ctrl.events).toEqual(events);
          expect(ctrl.events).toBeArrayOfSize(1);
          expect(UpcomingEventsWidgetModel.getUpcomingEventsWithFilters).toHaveBeenCalledWith(filters, 5, widget.id);
          var args = UpcomingEventsWidgetModel.getUpcomingEventsWithFilters.calls.mostRecent().args;
          expect(args[0]).toEqual(filters);
          expect(args[1]).toBe(5);
          expect(args[2]).toBe(widget.id);
        });

        it('should load subscribed upcoming events', function () {
          // given
          var events = ['events'];
          var filters = {subscribedTo: [true]};
          widget.settings = {
            _numberOfDisplayedEvents: 5,
            _displayOngoingEvents: true,
            _fetchUpcomingEvents: 'SUBSCRIBED'
          };
          UpcomingEventsWidgetModel.getUpcomingEventsWithFilters.and.returnValue($q.resolve(events));
          var ctrl = buildController();

          // when
          ctrl.loadEvents();
          $rootScope.$apply();

          // then
          expect(ctrl.events).toEqual(events);
          expect(UpcomingEventsWidgetModel.getUpcomingEventsWithFilters).toHaveBeenCalledWith(filters, 5, widget.id);
          var args = UpcomingEventsWidgetModel.getUpcomingEventsWithFilters.calls.mostRecent().args;
          expect(args[0]).toEqual(filters);
          expect(args[1]).toBe(5);
          expect(args[2]).toBe(widget.id);
        });

        it('should load upcoming events for sender', function () {
          // given
          var sender = {id: 'senderId'};
          var events = ['events'];
          var filters = {sender: [sender.id]};
          widget.settings = {
            _numberOfDisplayedEvents: 5,
            _displayOngoingEvents: true,
            _fetchUpcomingEvents: 'SELECTED',
            _selectedSender: sender
          };
          UpcomingEventsWidgetModel.getUpcomingEventsWithFilters.and.returnValue($q.resolve(events));
          var ctrl = buildController();

          // when
          ctrl.loadEvents();
          $rootScope.$apply();

          // then
          expect(ctrl.events).toEqual(events);
          expect(UpcomingEventsWidgetModel.getUpcomingEventsWithFilters).toHaveBeenCalledWith(filters, 5, widget.id);
          var args = UpcomingEventsWidgetModel.getUpcomingEventsWithFilters.calls.mostRecent().args;
          expect(args[0]).toEqual(filters);
          expect(args[1]).toBe(5);
          expect(args[2]).toBe(widget.id);
        });

        it('should register reload on settings change', function () {
          // when
          buildController();

          // then
          expect(widgetStatusService.refreshOnSettingsChange).toHaveBeenCalledWith($scope);
        });

        it('should check if given date is ongoing', function () {
          // given
          var start = date2Str(moment().subtract(1, 'days'));
          var end = date2Str(moment().add(1, 'days'));
          var startGone = date2Str(moment().subtract(2, 'days'));
          var endGone = date2Str(moment().subtract(1, 'days'));
          var ctrl = buildController();

          // when
          ctrl.isOngoing();
          $rootScope.$apply();

          // then
          expect(ctrl.isOngoing(start, end)).toBe(true);
          expect(ctrl.isOngoing(startGone, endGone)).toBe(false);
        });

      });

    });

  });
})();
