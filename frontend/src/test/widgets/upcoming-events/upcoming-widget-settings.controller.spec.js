(function () {
  'use strict';

  var moduleName = 'coyo.widgets.upcomingEvents';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, widget;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_) {
      $controller = _$controller_;
      $scope = _$rootScope_.$new();
      widget = {settings: {}};
      $scope.model = widget;
    }));

    var controllerName = 'UpcomingEventsWidgetSettingsController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope
        });
      }

      it('should initialize default setting', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        // when
        $scope.$apply();

        // then
        expect(ctrl.model.settings._numberOfDisplayedEvents).toBe(5);
        expect(ctrl.model.settings._displayOngoingEvents).toBe(false);
        expect(ctrl.model.settings._fetchUpcomingEvents).toBe('ALL');
      });

    });
  });

})();
